@extends('admin.layouts.templates')


@section('content')
<div class="grid-margin">
	<button class="btn btn-success mb-2 create-aspek-btn">
		<i class="mdi mdi-plus-thick"></i> Tambah Aspek Kinerja
	</button>
</div>

<div class="grid-margin">
	<p> Total Bobot : <strong id="total-bobot"> 0 </strong> </p>
</div>

<div class="row" id="view-data">
	
</div>
@endsection


@section('modal')
<div class="modal fade" id="createAspekModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="createAspekForm">
				
				<div class="modal-header">
					<h5 class="modal-title"> Buat Aspek Kinerja </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">

					{!! Template::requiredBanner() !!}

					<input type="hidden" name="id_skema_penilaian" value="{{ $skemaPenilaian->id }}">

					<div class="form-group">
						<label> Nama Aspek Kinerja {!! Template::required() !!} </label>
						<input type="text" name="nama_aspek_kinerja" class="form-control" placeholder="Nama Aspek Kinerja" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editAspekModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="editAspekForm">
				
				<div class="modal-header">
					<h5 class="modal-title"> Edit Aspek Kinerja </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">

					{!! Template::requiredBanner() !!}

					<div class="form-group">
						<label> Nama Aspek Kinerja {!! Template::required() !!} </label>
						<input type="text" name="nama_aspek_kinerja" class="form-control" placeholder="Nama Aspek Kinerja" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="createIndikatorModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="createIndikatorForm">
				
				<div class="modal-header">
					<h5 class="modal-title"> Buat Indikator Penilaian </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					{!! Template::requiredBanner() !!}

					<input type="hidden" name="id_aspek_kinerja" value="">

					<div class="form-group">
						<label> Nama Indikator Penilaian {!! Template::required() !!} </label>
						<input type="text" name="nama_indikator_penilaian" class="form-control" placeholder="Nama Indikator Penilaian" required>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label> Bobot (0 - 100) {!! Template::required() !!} </label>
						<input type="number" name="bobot" class="form-control" placeholder="Bobot (0 - 100)" min="0" max="100" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editIndikatorModal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="editIndikatorForm">
				
				<div class="modal-header">
					<h5 class="modal-title"> Edit Indikator Penilaian </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					{!! Template::requiredBanner() !!}

					<div class="form-group">
						<label> Nama Indikator Penilaian {!! Template::required() !!} </label>
						<input type="text" name="nama_indikator_penilaian" class="form-control" placeholder="Nama Indikator Penilaian" required>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label> Bobot (0 - 100) {!! Template::required() !!} </label>
						<input type="number" name="bobot" class="form-control" placeholder="Bobot (0 - 100)" min="0" max="100" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		const renderEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data()
				let $btn = $(this).ladda();
				confirmation('Yakin ingin dihapus?', () => {
					$btn.ladda('start')
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						updateViewData();
					})
					.fail(error => {
						ajaxErrorHandling(error);
						$btn.ladda('stop')
					})
				})
			});

			$('.edit-aspek-kinerja').off('click')
			$('.edit-aspek-kinerja').on('click', function(){
				let { href, namaAspekKinerja } = $(this).data();

				$('#editAspekModal').modal('show')
				$('#editAspekForm').find(`[name="nama_aspek_kinerja"]`).val(namaAspekKinerja)
				$('#editAspekForm').attr('action', href)
			})

			$('.create-indikator-btn').off('click')
			$('.create-indikator-btn').on('click', function(){
				let { idAspekKinerja } = $(this).data()
				$('#createIndikatorModal').modal('show')
				$('#createIndikatorForm').find(`[name="id_aspek_kinerja"]`).val(idAspekKinerja)
			})

			$('.edit-indikator-penilaian').off('click')
			$('.edit-indikator-penilaian').on('click', function(){
				let { href, namaIndikatorPenilaian, bobot } = $(this).data();

				$('#editIndikatorForm').find(`[name="nama_indikator_penilaian"]`).val(namaIndikatorPenilaian)
				$('#editIndikatorForm').find(`[name="bobot"]`).val(bobot)
				$('#editIndikatorForm').attr('action', href)
				$('#editIndikatorModal').modal('show')
			})
		}


		const updateViewData = () => {
			let totalBobot = 0;

			$.get({
				url: `{{ route('skema_penilaian.get', $skemaPenilaian->id) }}`,
				dataType: 'json'
			})
			.done(response => {
				let { skema_penilaian } = response
				let { aspek_kinerja } = skema_penilaian

				if(aspek_kinerja.length  == 0) {
					let html = $('#emptyAspekKinerjaTemplate').text()
					$('#view-data').html(html)
				} else {
					let html = '';
					aspek_kinerja.forEach(aspekKinerja => {
						let resHtml = $('#aspekKinerjaTemplate').text()
										.replaceAll('{id}', aspekKinerja.id)
										.replaceAll('{nama_aspek_kinerja}', aspekKinerja.nama_aspek_kinerja);

						let { indikator_penilaian } = aspekKinerja
						if(indikator_penilaian.length == 0) {
							resHtml = resHtml.replaceAll('{indikator_penilaian}', '<tr><td colspan="3" align="center"> Kosong </td></tr>');
						} else {
							indikatorHtml = '';
							indikator_penilaian.forEach(indikatorPenilaian => {
								indikatorHtml += $('#indikatorPenilaianTemplate').text()
													.replaceAll('{id}', indikatorPenilaian.id)
													.replaceAll('{nama_indikator_penilaian}', indikatorPenilaian.nama_indikator_penilaian)
													.replaceAll('{bobot}', indikatorPenilaian.bobot)
								totalBobot += parseInt(indikatorPenilaian.bobot)
							})
							resHtml = resHtml.replaceAll('{indikator_penilaian}', indikatorHtml);
						}

						html += resHtml;
					})
					$('#view-data').html(html)
					$('#total-bobot').text(totalBobot)
				}

				renderEvent();
			})
		}

		updateViewData()

		$createAspekModal = $('#createAspekModal');
		$createAspekForm = $('#createAspekForm');
		$createAspekSubmitBtn = $createAspekForm.find(`[type="submit"]`).ladda();

		$('.create-aspek-btn').on('click', function(){
			$createAspekModal.modal('show')
		})

		$createAspekModal.on('shown.bs.modal', function(){
			$(this).find(`[name="nama_aspek_kinerja"]`).focus();
		})

		$createAspekForm.on('submit', function(e){
			e.preventDefault();

			let formData = $(this).serialize();
			$createAspekSubmitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('aspek_kinerja.store') }}`,
				method: 'post',
				data: formData,
				dataType: 'json'
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$createAspekForm.find(`[name="nama_aspek_kinerja"]`).val('');
				$createAspekModal.modal('hide')
				$createAspekSubmitBtn.ladda('stop')
				updateViewData();
			})
			.fail(error => {
				$createAspekSubmitBtn.ladda('stop')
				ajaxErrorHandling(error)
			})
		})


		$editAspekModal = $('#editAspekModal');
		$editAspekForm = $('#editAspekForm');
		$editAspekSubmitBtn = $editAspekForm.find(`[type="submit"]`).ladda();

		$editAspekModal.on('shown.bs.modal', function(){
			$(this).find(`[name="nama_aspek_kinerja"]`).focus();
		})

		$editAspekForm.on('submit', function(e){
			e.preventDefault();

			let formData = $(this).serialize();
			let href = $(this).attr('action')
			$editAspekSubmitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: href,
				method: 'put',
				data: formData,
				dataType: 'json'
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$editAspekModal.modal('hide')
				updateViewData();
				$editAspekSubmitBtn.ladda('stop')
			})
			.fail(error => {
				$editAspekSubmitBtn.ladda('stop')
				ajaxErrorHandling(error)
			})
		})



		$createIndikatorModal = $('#createIndikatorModal');
		$createIndikatorForm = $('#createIndikatorForm');
		$createIndikatorSubmitBtn = $createIndikatorForm.find(`[type="submit"]`).ladda();

		$createIndikatorModal.on('shown.bs.modal', function(){
			$(this).find(`[name="nama_indikator_penilaian"]`).focus();
		})

		$createIndikatorForm.on('submit', function(e){
			e.preventDefault();

			let formData = $(this).serialize();
			$createIndikatorSubmitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('indikator_penilaian.store') }}`,
				method: 'post',
				data: formData,
				dataType: 'json'
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$createIndikatorForm.find(`[name="nama_indikator_penilaian"]`).val('');
				$createIndikatorForm.find(`[name="bobot"]`).val('');
				$createIndikatorModal.modal('hide')
				$createIndikatorSubmitBtn.ladda('stop')
				updateViewData();
			})
			.fail(error => {
				$createIndikatorSubmitBtn.ladda('stop')
				ajaxErrorHandling(error)
			})
		})


		$editIndikatorModal = $('#editIndikatorModal');
		$editIndikatorForm = $('#editIndikatorForm');
		$editIndikatorSubmitBtn = $editIndikatorForm.find(`[type="submit"]`).ladda();

		$editIndikatorModal.on('shown.bs.modal', function(){
			$(this).find(`[name="nama_indikator_penilaian"]`).focus();
		})

		$editIndikatorForm.on('submit', function(e){
			e.preventDefault();

			let formData = $(this).serialize();
			let href = $(this).attr('action')
			$editIndikatorSubmitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: href,
				method: 'put',
				data: formData,
				dataType: 'json'
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$editIndikatorModal.modal('hide')
				updateViewData();
				$editIndikatorSubmitBtn.ladda('stop')
			})
			.fail(error => {
				$editIndikatorSubmitBtn.ladda('stop')
				ajaxErrorHandling(error)
			})
		})

	})

</script>

<script type="text/html" id="emptyAspekKinerjaTemplate">
	<div class="col-lg-12">
		<p align="center">
			<i> Belum Buat Aspek Kinerja </i>
		</p>
	</div>
</script>

<script type="text/html" id="aspekKinerjaTemplate">
	<div class="col-lg-12">
		<div class="card grid-margin">
			<div class="card-body">
				<h6 class="card-title mb-2">
					Aspek Kinerja : <b> {nama_aspek_kinerja} </b> | 
					<a href="javascript:void(0);" class="edit-aspek-kinerja" data-href="{{ url('admin/aspek-kinerja') }}/{id}/update" data-nama-aspek-kinerja="{nama_aspek_kinerja}">
						<i class="mdi mdi-pencil"></i> Edit
					</a>
					<a href="javascript:void(0);" class="delete" data-href="{{ url('admin/aspek-kinerja') }}/{id}/destroy">
						<i class="mdi mdi-trash-can"></i> Hapus
					</a>
				</h6>
				<p>
					<button class="btn btn-success create-indikator-btn" data-id-aspek-kinerja="{id}">
						<i class="mdi mdi-plus-thick"></i> Tambah Indikator Penilaian
					</button>
				</p>
				
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th> Nama Indikator </th>
							<th> Bobot </th>
							<th width="100"> Aksi </th>
						</tr>
					</thead>
					<tbody>
						{indikator_penilaian}
					</tbody>
				</table>

			</div>
		</div>
	</div>
</script>

<script type="text/html" id="indikatorPenilaianTemplate">
	<tr>
		<td> {nama_indikator_penilaian} </td>
		<td> {bobot} </td>
		<td>
			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
				</button>
				<div class="dropdown-menu">
					<a class="dropdown-item edit-indikator-penilaian" href="javascript:void(0);" data-href="{{ url('admin/indikator-penilaian') }}/{id}/update" data-nama-indikator-penilaian="{nama_indikator_penilaian}" data-bobot="{bobot}" title="Edit Indikator Penilaian">
						<i class="mdi mdi-pencil"></i> Edit 
					</a>
					<a class="dropdown-item delete" href="javascript:void(0);" data-href="{{ url('admin/indikator-penilaian') }}/{id}/destroy" title="Hapus Indikator Penilaian">
						<i class="mdi mdi-trash-can"></i> Hapus
					</a>
				</div>
			</div>
		</td>
	</tr>
</script>
@endsection