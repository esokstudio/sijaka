@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('asosiasi.export_perusahaan', $asosiasi->id) }}">
	<i class="mdi mdi-file-excel"></i> Download Excel
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {!! $title !!} - {{ $asosiasi->nama_asosiasi }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama Perusahaan </th>
								<th> Direktur </th>
								<th> Alamat </th>
							</tr>
						</thead>
						<tbody>
							@foreach($asosiasi->perusahaan as $perusahaan)
							<tr>
								<td> {{ $perusahaan->nama_perusahaan }} </td>
								<td> {{ $perusahaan->direktur }} </td>
								<td> {{ $perusahaan->alamat }} </td>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			autoWidth : false,
		});

	})

</script>
@endsection