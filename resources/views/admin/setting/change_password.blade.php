@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Password Lama {!! Template::required() !!} </label>
						<input type="password" name="old_password" class="form-control" placeholder="Password Lama" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Password Baru {!! Template::required() !!} </label>
						<input type="password" name="new_password" class="form-control" placeholder="Password Baru" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Konfirmasi Password Baru {!! Template::required() !!} </label>
						<input type="password" name="confirm_password" class="form-control" placeholder="Konfirmasi Password Baru" required>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="old_password"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('setting.change_password_save') }}`,
				method: 'post',
				dataType: 'json',
				data: formData
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection