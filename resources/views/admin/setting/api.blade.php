@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">

				<div class="form-group">
					<label> API Key  </label>
					<input type="text" name="api_key" class="form-control" placeholder="API Key" value="{{ \Setting::getValue('api_key', 'API_KEY') }}" readonly>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="button" class="btn btn-primary" id="copy-btn">
						<i class="mdi mdi-content-copy"></i> Salin API Key 
					</button>

					<button type="button" class="btn btn-success" id="generate-btn">
						<i class="mdi mdi-sync"></i> Generate Ulang API Key
					</button>

				<!-- </form> -->
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#copy-btn').on('click', function(){
			copyText(`{{ \Setting::getValue('api_key', 'API_KEY') }}`);
			toastrAlert()
			toastr.success('Berhasil disalin', 'Berhasil')
		})

		$('#generate-btn').on('click', function(){
			confirmation('Yakin ingin digenerate ulang?', () => {
				window.location.href = `{{ route('setting.generate_api_key') }}`
			})
		})

	})

</script>
@endsection