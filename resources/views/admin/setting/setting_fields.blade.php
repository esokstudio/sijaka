@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-8 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}


					@forelse($settings as $key => $data)
					<div class="row mb-3">
						<div class="col-lg-5 mb-1">
							<b class="d-block">
								{{ $data['label'] }} {!! Template::required() !!}
							</b>
							<span class="small"> {{ $data['description'] }} </span>
						</div>

						<div class="col-lg-7">
							@if($data['type'] == 'text')
							<?php 
								$maxLength = $data['max_length'];
							?>

							<input type="text" name="{{ $key }}" class="form-control" placeholder="{{ $data['label'] }}" value="{{ $data['value'] }}" @if($maxLength) maxlength="{{ $maxLength }}" @endif required>

							@elseif($data['type'] == 'longtext')
							<?php 
								$maxLength = $data['max_length'];
							?>

							<textarea class="form-control" rows="3" name="{{ $key }}" placeholder="{{ $data['label'] }}" @if($maxLength) maxlength="{{ $maxLength }}" @endif required>{{ $data['value'] }}</textarea>

							@elseif($data['type'] == 'option')

							<select class="select2" name="{{ $key }}" data-value="{{ $data['value'] }}" style="width: 100%;" required>
								@foreach($data['options'] as $optionValue => $optionLabel)
								<option value="{{ $optionValue }}"> {{ $optionLabel }} </option>
								@endforeach
							</select>

							@elseif($data['type'] == 'number')
							<?php 
								$min = $data['min'];
								$max = $data['max'];
							?>

							<input type="number" name="{{ $key }}" class="form-control" placeholder="{{ $data['label'] }}" value="{{ $data['value'] }}" @if($min) min="{{ $min }}" @endif @if($max) max="{{ $max }}" @endif required>

							@endif
							<span class="invalid-feedback"></span>
						</div>
					</div>
					
					@empty

					<p align="center">
						<i> Pengaturan tidak tersedia </i>
					</p>

					@endforelse

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$.each($form.find('select'), (index, elem) => {
			let { value } = $(elem).data()
			$(elem).select2({
				'placeholder': '- Pilih -',
			})
			$(elem).val(value).trigger('change');
		})

		const resetForm = () => {
			clearInvalid();
			// $('.thumbnail-preview').hide();
			// $('.thumbnail-preview').attr('src', '#')
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ $saveUrl }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		previewImageAfterChange({
			fieldSelector: `[name="thumbnail"]`,
			previewSelector: `.thumbnail-preview`,
		})

		resetForm();

	})

</script>
@endsection