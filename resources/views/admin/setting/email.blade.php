@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">

		<div class="card">
			
			<div class="card-body">
				<form id="form">

					{!! Template::requiredBanner() !!}

					<div class="form-group">
						<label> SMTP Host {!! Template::required() !!}  </label>
						<input type="text" name="smtp_host" class="form-control" placeholder="Host" value="{{ \Setting::getValue('smtp_host', 'smtp.gmail.com') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> SMTP Port {!! Template::required() !!}  </label>
						<input type="text" name="smtp_port" class="form-control" placeholder="Port" value="{{ \Setting::getValue('smtp_port', '587') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Username {!! Template::required() !!}  </label>
						<input type="text" name="smtp_username" class="form-control" placeholder="Username" value="{{ \Setting::getValue('smtp_username', 'binkonkabcirebon@gmail.com') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Password {!! Template::required() !!}  </label>
						<input type="password" name="smtp_password" class="form-control" placeholder="Password" value="{{ \Setting::getValue('smtp_password', 'usqw irpx ruin ikox') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Alamat Email Asal {!! Template::required() !!}  </label>
						<input type="text" name="mail_from_address" class="form-control" placeholder="Alamat Email Asal" value="{{ \Setting::getValue('mail_from_address', 'binkonkabcirebon@gmail.com') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nama Email Asal {!! Template::required() !!}  </label>
						<input type="text" name="mail_from_name" class="form-control" placeholder="Nama Email Asal" value="{{ \Setting::getValue('mail_from_name', 'Binkon PUTR Kabupaten Cirebon') }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan 
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('setting.save_email') }}`,
				method: 'post',
				dataType: 'json',
				data: formData
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

	})

</script>
@endsection