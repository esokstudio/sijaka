@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">
					@method('PUT')

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Kategori {!! Template::required() !!} </label>
						<select name="id_kategori_peraturan" style="width: 100%;" required>
							@foreach(\App\Models\KategoriPeraturan::all() as $kategoriPeraturan)
							<option value="{{ $kategoriPeraturan->id }}"> {{ $kategoriPeraturan->nama_kategori }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nomor {!! Template::required() !!} </label>
						<input type="text" name="nomor" class="form-control" placeholder="Nomor" value="{{ $peraturan->nomor }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Tahun {!! Template::required() !!} </label>
						<input type="number" name="tahun" class="form-control" placeholder="Tahun" value="{{ $peraturan->tahun }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Judul {!! Template::required() !!} </label>
						<input type="text" name="judul" class="form-control" placeholder="Judul" value="{{ $peraturan->judul }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File </label>
						<input type="file" name="file" class="form-control">
						<div class="text-muted small"> * Isi jika ingin ganti file </div>
					</div>

					<div class="form-group">
						<label> Publikasi {!! Template::required() !!} </label>
						<select name="is_published" class="form-control" required>
							<option value="yes"> Ya </option>
							<option value="no"> Tidak </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="id_kategori_peraturan"]`).select2({
			'placeholder': '- Pilih Kategori -'
		})

		const resetForm = () => {
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('peraturan.update', $peraturan->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

		$form.find(`[name="id_kategori_peraturan"]`).val(`{{ $peraturan->id_kategori_peraturan }}`).trigger('change')
		$form.find(`[name="is_published"]`).val(`{{ $peraturan->is_published }}`).trigger('change')

	})

</script>
@endsection