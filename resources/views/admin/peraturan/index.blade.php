@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('peraturan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Kategori </th>
								<th> Nomor </th>
								<th> Tahun </th>
								<th> Judul </th>
								<th> Publikasi </th>
								<th> Dibuat pada </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('peraturan') }}"
			},
			columns : [
				{
					data : 'kategori_peraturan.nama_kategori',
					name : 'kategori_peraturan.nama_kategori'
				},
				{
					data : 'nomor',
					name : 'nomor'
				},
				{
					data : 'tahun',
					name : 'tahun'
				},
				{
					data : 'judul',
					name : 'judul'
				},
				{
					data : 'is_published',
					name : 'is_published',
				},
				{
					data : 'created_at',
					name : 'created_at',
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			},
			order: [[ '5', 'desc' ]]
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection