@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama Tingkat Keahlian {!! Template::required() !!} </label>
						<input type="text" name="nama_tingkat_keahlian" class="form-control" placeholder="Nama Tingkat Keahlian" value="{{ $tingkatKeahlian->nama_tingkat_keahlian }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Jenis Keahlian {!! Template::required() !!} </label>
						<select name="id_jenis_keahlian" style="width: 100%;" required>
							@foreach(\App\Models\JenisKeahlian::all() as $jenisKeahlian)
							<option value="{{ $jenisKeahlian->id }}"> {{ $jenisKeahlian->nama_jenis_keahlian }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="id_jenis_keahlian"]`).select2({
			'placeholder': '- Pilih Jenis Keahlian -'
		})
		$form.find(`[name="id_jenis_keahlian"]`).val(`{{ $tingkatKeahlian->id_jenis_keahlian }}`).trigger('change')

		const resetForm = () => {
			clearInvalid();
			$form.find(`[name="nama_tingkat_keahlian"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('tingkat_keahlian.update', $tingkatKeahlian->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection