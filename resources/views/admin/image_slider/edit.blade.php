@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">
					@method('PUT')

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Judul {!! Template::required() !!} </label>
						<input type="text" name="title" class="form-control" placeholder="Judul" value="{{ $imageSlider->title }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label class="d-block"> File Foto {!! Template::required() !!} </label>
						<img src="#" style="max-width: 200px; height: auto; display: none;" class="image-preview mb-2">
						<input type="file" name="image" class="form-control" required>
					</div>

					<div class="form-group">
						<label> Publikasi {!! Template::required() !!} </label>
						<select class="form-control" name="is_published" required>
							<option value="yes"> Ya </option>
							<option value="no"> Tidak </option>
						</select>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="title"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('image_slider.update', $imageSlider->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		previewImageAfterChange({
			fieldSelector: `[name="image"]`,
			previewSelector: `.image-preview`,
			defaultSource: `{{ $imageSlider->imageLink() }}`,
		})

		$form.find(`[name="is_published"]`).val(`{{ $imageSlider->is_published }}`)

		resetForm();

	})

</script>
@endsection