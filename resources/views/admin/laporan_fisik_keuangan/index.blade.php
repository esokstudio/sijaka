@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					<div class="alert alert-info border-0">
						@if(auth()->user()->isPengguna())
						<i class="mdi mdi-information mr-1"></i> Data diambil dari paket pekerjaan yang diinput oleh instansi <b>{{ auth()->user()->namaInstansi() }}</b>
						@else
						<i class="mdi mdi-information mr-1"></i> Data diambil dari paket pekerjaan yang diinput oleh semua instansi</b>
						@endif
					</div>
					
					<div class="form-group">
						<label> Tahun {!! Template::required() !!} </label>
						<select name="tahun" style="width: 100%;" required>
							@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahun)
							<option value="{{ $tahun->tahun }}">
								{{ $tahun->tahun }}
							</option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					@if(auth()->user()->isAdmin())
					<div class="form-group">
						<label> Instansi/Bidang/SKPD </label>
						<select name="id_instansi" style="width: 100%;" required>
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\Instansi::orderBy('nama_instansi', 'asc')->get() as $instansi)
							<option value="{{ $instansi->id }}">
								{{ $instansi->nama_instansi }}
							</option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>
					@endif

					<div class="form-group">
						<label> Dengan Kolom Link Foto </label>
						<select name="is_with_photo_link" class="form-control" required>
							<option value="no"> Tidak </option>
							<option value="yes"> Ya </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Buat Laporan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('laporan_fisik_keuangan.generate') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		resetForm();

		$form.find(`[name="tahun"]`).select2({
			'placeholder': '- Pilih Tahun -'
		})
		$form.find(`[name="tahun"]`).val('').trigger('change')

		$form.find(`[name="id_instansi"]`).select2({
			'placeholder': '- Pilih Instansi/Bidang/SKPD -'
		})

	})

</script>
@endsection