@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				<div class="table-responsive mb-3">
					<table class="table table-hover">
						<tr>
							<th> Nama Pemohon </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->nama_pemohon }} </td>
							<th> Tempat Lahir </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->tempat_lahir ?? '-' }} </td>
						</tr>
						<tr>
							<th> Tanggal Lahir </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->tgl_lahir ?? '-' }} </td>
							<th> Kelurahan </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->kelurahan ?? '-' }} </td>
						</tr>
						<tr>
							<th> Kecamatan </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->kecamatan ?? '-' }} </td>
							<th> Kota </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->kota ?? '-' }} </td>
						</tr>
						<tr>
							<th> No KTP </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->profilPeserta->no_ktp ?? '-' }} </td>
							<th> Perusahaan </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->nama_perusahaan ?? '-' }} </td>
						</tr>
						<tr>
							<th> Klasifikasi </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->klasifikasi }} </td>
							<th> Sub Klasifikasi </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->sub_klasifikasi }} </td>
						</tr>
						<tr>
							<th> Kualifikasi </th>
							<td> : </td>
							<td> {{ $permohonanSertifikasi->kualifikasi }} </td>
							<th> Nomor Whatsapp </th>
							<td> : </td>
							<td>
								@if(!empty($permohonanSertifikasi->nomor_telepon))
									<?php
										$message = '';
										if($permohonanSertifikasi->status == 'Disetujui') {
											$message = "Permohonan sertifikasi anda telah disetujui. Silahkan login ke akun anda.";
										} else {
											$message = "Permohonan sertifikasi anda telah ditolak. Silahkan login ke akun anda.";
										}
									?>
									@if(Str::startsWith($permohonanSertifikasi->nomor_telepon, '08'))
									<a href="https://wa.me/{{ Str::replaceFirst('08', '628', $permohonanSertifikasi->nomor_telepon) }}?text={{ $message }}" target="_blank"> {{ $permohonanSertifikasi->nomor_telepon }} </a>
									@else
									<a href="https://wa.me/{{ $permohonanSertifikasi->nomor_telepon }}?text={{ $message }}" target="_blank"> {{ $permohonanSertifikasi->nomor_telepon }} </a>
									@endif
									<br>
									<small> (Klik Untuk Alihkan Ke Whatsapp) </small>
								@else
								-
								@endif
							</td>
						</tr>
						<tr>
							<th> Email </th>
							<td> : </td>
							<td>
								@if(!empty($permohonanSertifikasi->email))
									<a href="mailto:{{ $permohonanSertifikasi->email }}"> {{ $permohonanSertifikasi->email }} </a>
									<br>
									<small> (Klik Untuk Alihkan Ke Email) </small>
								@else
								-
								@endif
							</td>
							<th> Status </th>
							<td> : </td>
							<td> {!! $permohonanSertifikasi->statusHtml() !!} </td>
						</tr>
					</table>
				</div>

				<hr>

				<div class="card-title h6 font-weight-bold"> Pendidikan </div>

				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th> Jenjang Pendidikan </th>
								<th> Nama Sekolah </th>
								<th> Jurusan </th>
								<th> Kota </th>
								<th> Tahun Lulus </th>
								<th> No Ijazah </th>
							</tr>
						</thead>
						<tbody>
							@forelse($permohonanSertifikasi->pendidikanPeserta as $pendidikan)
							<tr>
								<td> {{ $pendidikan->jenjang_pendidikan }} </td>
								<td> {{ $pendidikan->nama_sekolah }} </td>
								<td> {{ $pendidikan->jurusan }} </td>
								<td> {{ $pendidikan->kota }} </td>
								<td> {{ $pendidikan->tahun_lulus }} </td>
								<td> {{ $pendidikan->no_ijazah }} </td>
							</tr>
							@empty
							<tr>
								<td colspan="6" align="center"> Kosong </td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>

				<hr>

				<div class="card-title h6 font-weight-bold"> Pengalaman Kerja </div>

				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th> Nama Pekerjaan </th>
								<th> Lokasi Pekerjaan </th>
								<th> Nilai Kontrak </th>
								<th> Mulai </th>
								<th> Selesai </th>
								<th> Jabatan </th>
							</tr>
						</thead>
						<tbody>
							@forelse($permohonanSertifikasi->pengalamanPeserta as $pengalaman)
							<tr>
								<td> {{ $pengalaman->nama_pekerjaan }} </td>
								<td> {{ $pengalaman->lokasi_pekerjaan }} </td>
								<td> {{ $pengalaman->nilai_kontrak }} </td>
								<td> {{ $pengalaman->mulai }} </td>
								<td> {{ $pengalaman->selesai }} </td>
								<td> {{ $pengalaman->jabatan }} </td>
							</tr>
							@empty
							<tr>
								<td colspan="6" align="center"> Kosong </td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>

				<hr>

				<div class="card-title h6 font-weight-bold"> Berkas </div>

				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th> Dokumen </th>
								<th> Link </th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td> File Scan Ijazah </td>
								<td>
									<a href="{{ $permohonanSertifikasi->fileIjazahLink() }}" download> Klik Disini </a>
								</td>
							</tr>

							<tr>
								<td> File Scan Permohonan Sertifikasi  Pernyataan Kebenaran Data Dokumen </td>
								<td>
									<a href="{{ $permohonanSertifikasi->fileSuratPermohonanLink() }}" download> Klik Disini </a>
								</td>
							</tr>

							<tr>
								<td> File Scan KTP </td>
								<td>
									<a href="{{ $permohonanSertifikasi->fileKtpLink() }}" download> Klik Disini </a>
								</td>
							</tr>

							<tr>
								<td> File Surat Pernyataan Kebenaran Data Dokumen </td>
								<td>
									<a href="{{ $permohonanSertifikasi->fileSuratPernyataanKebenaranDataLink() }}" download> Klik Disini </a>
								</td>
							</tr>

							<tr>
								<td> File Pas Foto </td>
								<td>
									<a href="{{ $permohonanSertifikasi->filePasFotoLink() }}" download> Klik Disini </a>
								</td>
							</tr>
						</tbody>

					</table>
				</div>

				<hr>

				@if($permohonanSertifikasi->status == 'Menunggu')
				<div class="form-group">
					<label> Catatan (Opsional) </label>
					<textarea class="form-control" name="tindak_lanjut" rows="3" placeholder="Pesan untuk pemohon" id="tindak_lanjut"></textarea>
				</div>

				<button class="btn btn-success approve-btn">
					<i class="mdi mdi-check mr-2"></i> Setujui
				</button>

				<button class="btn btn-danger reject-btn">
					<i class="mdi mdi-close mr-2"></i> Tolak
				</button>
				@else
				<div class="form-group">
					<label> Catatan </label>
					<textarea class="form-control" name="tindak_lanjut" rows="3" readonly="">{{ $permohonanSertifikasi->tindak_lanjut }}</textarea>
				</div>
				@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui permohonan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: '{{ route("permohonan_sertifikasi.approve", $permohonanSertifikasi->id) }}',
					dataType: 'json',
					method: 'post',
					data: {
						'tindak_lanjut': $('#tindak_lanjut').val()
					}
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak permohonan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: '{{ route("permohonan_sertifikasi.reject", $permohonanSertifikasi->id) }}',
					dataType: 'json',
					method: 'post',
					data: {
						'tindak_lanjut': $('#tindak_lanjut').val()
					}
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	})

</script>
@endsection