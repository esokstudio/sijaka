@extends('admin.layouts.templates')


@section('action')
<button class="btn btn-success" id="export-btn">
	<i class="mdi mdi-download"></i> Export ke Excel
</button>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Tgl Pengajuan </th>
								<th> Pemohon </th>
								<th> Perusahaan </th>
								<th> Klasifikasi </th>
								<th> Sub Klasifikasi </th>
								<th> Kualifikasi </th>
								<th> Status </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="export-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="export-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Export ke Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Tanggal Awal (Tgl Pendaftaran) </label>
						<input type="date" name="start_date" class="form-control">
					</div>

					<div class="form-group">
						<label> Tanggal Akhir (Tgl Pendaftaran) </label>
						<input type="date" name="end_date" class="form-control">
					</div>

					<div class="form-group">
						<label> Status </label>
						<select class="form-control" style="width: 100%;" name="status">
							<option value="all"> - Semua - </option>
							<option value="Menunggu"> Menunggu </option>
							<option value="Disetujui"> Disetujui </option>
							<option value="Ditolak"> Ditolak </option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-download"></i> Export
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('permohonan_sertifikasi') }}"
			},
			columns : [
				{
					data : 'created_at',
					name : 'created_at'
				},
				{
					data : 'nama_pemohon',
					name : 'nama_pemohon'
				},
				{
					data : 'nama_perusahaan',
					name : 'nama_perusahaan'
				},
				{
					data : 'klasifikasi',
					name : 'klasifikasi'
				},
				{
					data : 'sub_klasifikasi',
					name : 'sub_klasifikasi'
				},
				{
					data : 'kualifikasi',
					name : 'kualifikasi'
				},
				{
					data : 'status',
					name : 'status'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			},
			order:['0', 'desc']
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

		const $exportModal = $('#export-modal');
		const $exportForm = $('#export-form');
		const $exportSubmitBtn = $exportForm.find(`[type="submit"]`).ladda()

		$('#export-btn').on('click', function(){
			$('#export-modal').modal('show')
		})

		$exportForm.on('submit', function(e){
			e.preventDefault();

			$exportSubmitBtn.ladda('start')
			let formData = $(this).serialize()

			ajaxSetup()
			$.ajax({
				url: `{{ route('permohonan_sertifikasi.export_to_excel') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$exportSubmitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$exportSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $exportForm)
			})
		})

		$exportForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

	})

</script>
@endsection