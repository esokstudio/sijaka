@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('pelatihan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Tahun Anggaran </th>
								<th> Sumber Dana </th>
								<th> Nama Pelatihan </th>
								<th> Penanggung Jawab </th>
								<th> Jenis Keahlian </th>
								<th> Tingkat Keahlian </th>
								<th> Klasifikasi </th>
								<th> Sub Klasifikasi </th>
								<th> Metode </th>
								<th> Waktu Pelaksanaan </th>
								<th> Peserta Pria </th>
								<th> Peserta Wanita </th>
								<th> Lokasi </th>
								<th> Keterangan </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('pelatihan') }}"
			},
			columns : [
				{
					data : 'tahun_anggaran',
					name : 'tahun_anggaran'
				},
				{
					data : 'sumber_dana.sumber_dana',
					name : 'sumber_dana.sumber_dana'
				},
				{
					data : 'nama_pelatihan',
					name : 'nama_pelatihan'
				},
				{
					data : 'penanggung_jawab',
					name : 'penanggung_jawab'
				},
				{
					data : 'jenis_keahlian.nama_jenis_keahlian',
					name : 'jenis_keahlian.nama_jenis_keahlian'
				},
				{
					data : 'tingkat_keahlian.nama_tingkat_keahlian',
					name : 'tingkat_keahlian.nama_tingkat_keahlian'
				},
				{
					data : 'klasifikasi.nama_klasifikasi',
					name : 'klasifikasi.nama_klasifikasi'
				},
				{
					data : 'sub_klasifikasi.nama_sub_klasifikasi',
					name : 'sub_klasifikasi.nama_sub_klasifikasi'
				},
				{
					data : 'metode_pelatihan.nama_metode_pelatihan',
					name : 'metode_pelatihan.nama_metode_pelatihan'
				},
				{
					data : 'waktu_pelaksanaan',
					name : 'waktu_pelaksanaan'
				},
				{
					data : 'jumlah_peserta_pria',
					name : 'jumlah_peserta_pria'
				},
				{
					data : 'jumlah_peserta_wanita',
					name : 'jumlah_peserta_wanita'
				},
				{
					data : 'lokasi_pelaksanaan',
					name : 'lokasi_pelaksanaan'
				},
				{
					data : 'keterangan',
					name : 'keterangan'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection