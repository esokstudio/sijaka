@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}

					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tahun Anggaran {!! Template::required() !!} </label>
								<select name="tahun_anggaran" style="width: 100%;" required>
									@foreach(\App\Models\TahunAnggaran::all() as $tahunAnggaran)
									<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Sumber Dana {!! Template::required() !!} </label>
								<select name="id_sumber_dana" style="width: 100%;" required>
									@foreach(\App\Models\SumberDana::all() as $sumberDana)
									<option value="{{ $sumberDana->id }}"> {{ $sumberDana->sumber_dana }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama Pelatihan {!! Template::required() !!} </label>
								<input type="text" name="nama_pelatihan" class="form-control" placeholder="Nama Pelatihan" value="{{ $pelatihan->nama_pelatihan }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Penanggung Jawab {!! Template::required() !!} </label>
								<input type="text" name="penanggung_jawab" class="form-control" placeholder="Penanggung Jawab" value="{{ $pelatihan->penanggung_jawab }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Jenis Keahlian {!! Template::required() !!} </label>
								<select name="id_jenis_keahlian" style="width: 100%;" required>
									@foreach(\App\Models\JenisKeahlian::all() as $jenisKeahlian)
									<option value="{{ $jenisKeahlian->id }}"> {{ $jenisKeahlian->nama_jenis_keahlian }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tingkat Keahlian {!! Template::required() !!} </label>
								<select name="id_tingkat_keahlian" style="width: 100%;" required>
									@foreach(\App\Models\JenisKeahlian::find($pelatihan->id_jenis_keahlian)->tingkatKeahlian as $tingkatKeahlian)
									<option value="{{ $tingkatKeahlian->id }}"> {{ $tingkatKeahlian->nama_tingkat_keahlian }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Klasifikasi {!! Template::required() !!} </label>
								<select name="id_klasifikasi" style="width: 100%;" required>
									@foreach(\App\Models\Klasifikasi::all() as $klasifikasi)
									<option value="{{ $klasifikasi->id }}"> {{ $klasifikasi->nama_klasifikasi }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Sub Klasifikasi {!! Template::required() !!} </label>
								<select name="id_sub_klasifikasi" style="width: 100%;" required>
									@foreach(\App\Models\Klasifikasi::find($pelatihan->id_klasifikasi)->subKlasifikasi as $subKlasifikasi)
									<option value="{{ $subKlasifikasi->id }}"> {{ $subKlasifikasi->nama_sub_klasifikasi }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Metode Pelatihan {!! Template::required() !!} </label>
								<select name="id_metode_pelatihan" style="width: 100%;" required>
									@foreach(\App\Models\MetodePelatihan::all() as $metode)
									<option value="{{ $metode->id }}"> {{ $metode->nama_metode_pelatihan }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Waktu Pelaksanaan {!! Template::required() !!} </label>
								<input type="date" name="waktu_pelaksanaan" class="form-control" placeholder="Waktu Pelaksanaan" value="{{ $pelatihan->waktu_pelaksanaan }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Lokasi {!! Template::required() !!} </label>
								<input type="text" name="lokasi_pelaksanaan" class="form-control" placeholder="Lokasi" value="{{ $pelatihan->lokasi_pelaksanaan }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Keterangan </label>
								<input type="text" name="keterangan" class="form-control" placeholder="Keterangan" value="{{ $pelatihan->keterangan }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>
					

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="tahun_anggaran"]`).select2({
			'placeholder': '- Pilih Tahun Anggaran -'
		})

		$form.find(`[name="id_sumber_dana"]`).select2({
			'placeholder': '- Pilih Sumber Dana -'
		})

		$form.find(`[name="id_jenis_keahlian"]`).select2({
			'placeholder': '- Pilih Jenis Keahlian -'
		})

		$form.find(`[name="id_tingkat_keahlian"]`).select2({
			'placeholder': '- Pilih Tingkat Keahlian -'
		})

		$form.find(`[name="id_klasifikasi"]`).select2({
			'placeholder': '- Pilih Klasifikasi -'
		})

		$form.find(`[name="id_sub_klasifikasi"]`).select2({
			'placeholder': '- Pilih Sub Klasifikasi -'
		})

		$form.find(`[name="id_metode_pelatihan"]`).select2({
			'placeholder': '- Pilih Metode Pelatihan -'
		})

		const resetForm = () => {
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('pelatihan.update', $pelatihan->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

		$form.find(`[name="tahun_anggaran"]`).val(`{{ $pelatihan->tahun_anggaran }}`).trigger('change')

		$form.find(`[name="id_sumber_dana"]`).val(`{{ $pelatihan->id_sumber_dana }}`).trigger('change')

		$form.find(`[name="id_jenis_keahlian"]`).val(`{{ $pelatihan->id_jenis_keahlian }}`).trigger('change')

		$form.find(`[name="id_tingkat_keahlian"]`).val(`{{ $pelatihan->id_tingkat_keahlian }}`).trigger('change')

		$form.find(`[name="id_klasifikasi"]`).val(`{{ $pelatihan->id_klasifikasi }}`).trigger('change')

		$form.find(`[name="id_sub_klasifikasi"]`).val(`{{ $pelatihan->id_sub_klasifikasi }}`).trigger('change')

		$form.find(`[name="id_metode_pelatihan"]`).val(`{{ $pelatihan->id_metode_pelatihan }}`).trigger('change')


		$form.find(`[name="id_jenis_keahlian"]`).on('change', function(){
			let jenisKeahlianID = $(this).val();

			if(jenisKeahlianID != "" && jenisKeahlianID != null) {
				$.get({
					url: `{{ route('jenis_keahlian') }}/${jenisKeahlianID}/get-tingkat-keahlian`,
					dataType: 'json'
				})
				.done(response => {
					let { tingkatKeahlian } = response
					let html = '';
					tingkatKeahlian.forEach(tingkat => {
						html += `<option value="${tingkat.id}"> ${tingkat.nama_tingkat_keahlian} </option>`;
					})

					$form.find(`[name="id_tingkat_keahlian"]`).html(html);
					$form.find(`[name="id_tingkat_keahlian"]`).val('').trigger('change')
					$form.find(`[name="id_tingkat_keahlian"]`).prop('disabled', false)
				})
			} else {
				$form.find(`[name="id_tingkat_keahlian"]`).val('').trigger('change')
				$form.find(`[name="id_tingkat_keahlian"]`).prop('disabled', true)
			}
		});


		$form.find(`[name="id_klasifikasi"]`).on('change', function(){
			let klasifikasiID = $(this).val();

			if(klasifikasiID != "" && klasifikasiID != null) {
				$.get({
					url: `{{ route('klasifikasi') }}/${klasifikasiID}/get-sub-klasifikasi`,
					dataType: 'json'
				})
				.done(response => {
					let { subKlasifikasi } = response
					let html = '';
					subKlasifikasi.forEach(sub => {
						html += `<option value="${sub.id}"> ${sub.nama_sub_klasifikasi} </option>`;
					})

					$form.find(`[name="id_sub_klasifikasi"]`).html(html);
					$form.find(`[name="id_sub_klasifikasi"]`).val('').trigger('change')
					$form.find(`[name="id_sub_klasifikasi"]`).prop('disabled', false)
				})
			} else {
				$form.find(`[name="id_sub_klasifikasi"]`).val('').trigger('change')
				$form.find(`[name="id_sub_klasifikasi"]`).prop('disabled', true)
			}
		});

	})

</script>
@endsection