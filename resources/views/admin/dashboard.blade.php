@extends('admin.layouts.templates')

@section('content')

@if(auth()->user()->isAdmin())
@include('admin.dashboard.dashboard_admin')
@endif

@endsection


@section('scripts')
<script type="text/javascript">

	const dynamicColors = () => {
		const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52);
		const randomRGB = () => `${randomNum()}, ${randomNum()}, ${randomNum()}`;
		return randomRGB();
	};

	Chart.defaults.global.defaultFontFamily = "Calibri";
	Chart.defaults.global.defaultFontSize = 18;


	// Statistik
	const statisticLabels = [];
	const statisticDatapoints = [];
	const lastYearStatisticLabels = [];
	const lastYearStatisticDatapoints = [];
	let statisticMaxSuggest = 0;

	@foreach(\App\Models\ViewStatistic::resumeOfViewYearly(date('Y')) as $date => $amount)
	statisticLabels.push(`{{ $date }}`)
	statisticDatapoints.push(parseInt(`{{ $amount }}`))
	if(parseInt(`{{ $amount }}`) >= statisticMaxSuggest) {
		statisticMaxSuggest = parseInt(`{{ $amount }}`);
	}
	@endforeach

	@foreach(\App\Models\ViewStatistic::resumeOfViewYearly(date('Y') - 1) as $date => $amount)
	lastYearStatisticLabels.push(`{{ $date }}`)
	lastYearStatisticDatapoints.push(parseInt(`{{ $amount }}`))
	if(parseInt(`{{ $amount }}`) >= statisticMaxSuggest) {
		statisticMaxSuggest = parseInt(`{{ $amount }}`);
	}
	@endforeach

	if(statisticMaxSuggest == 0) statisticMaxSuggest = 10;

	const statisticData = {
		labels: statisticLabels,
		datasets: [
			{
				label: 'Tahun Ini',
				data: statisticDatapoints,
				borderColor: '#36a2eb',
				fill: false
			},
			{
				label: 'Tahun Kemarin',
				data: lastYearStatisticDatapoints,
				borderColor: '#ff5d5d',
				fill: false
			}
		]
	};

	const statisticConfig = {
		type: 'line',
		data: statisticData,
		options: {
			responsive: true,
			plugins: {
				title: {
					display: true,
					text: 'Statistik Pengunjung Tahun Ini'
				},
			},
			interaction: {
				intersect: false,
			},
			scales: {
				x: {
					display: true,
					title: {
						display: true
					}
				},
				y: {
					display: true,
					title: {
						display: true,
						text: 'Value'
					},
					suggestedMin: 0,
					suggestedMax: statisticMaxSuggest,
				},
				yAxes: [{
				    ticks: {
				        beginAtZero: true,
				        userCallback: function(value, index, values) {
				            value = value.toString();
				            value = value.split(/(?=(?:...)*$)/);
				            value = value.join('.');
				            return  value;
				        }
				    }
				}]
			},
			tooltips: {
			    callbacks: {
			    	label: function (tooltipItem, data) {
						var tooltipValue = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						return numberFormat(tooltipValue);
					}
			    }
			}
		},
	};

	const statisticChart = new Chart(
		document.getElementById('statisticChart'),
		statisticConfig
	);



	// Chart Paket Pekerjaan
	let paketPekerjaanChartElem = document.getElementById("paket-pekerjaan-chart");
	let datasets = []
	let dataset = null
	let data = null;
	let tahun = [];
	let color = null
	let paketPekerjaanPerKategori = []
	let paguAnggaranPerKategori = []
	let realisasiAnggaranPerKategori = []
	let i = 0;

	@foreach(\App\Models\PaketPekerjaan::jumlahPaketPekerjaanDataChart() as $ins)
		data = []
		tahun = []
		@foreach($ins->data as $tahun => $jumlah)
		data.push(parseInt(`{{ $jumlah }}`))
		tahun.push(parseInt(`{{ $tahun }}`))
		if(!paketPekerjaanPerKategori.hasOwnProperty(`{{ $tahun }}`)) {
			paketPekerjaanPerKategori[`{{ $tahun }}`] = []
		}
		@endforeach
		color = dynamicColors()
		dataset = {
			label: '{{ $ins->instansi->nama_instansi }}',
			data: data,
			backgroundColor: `rgba(${color}, 0.6)`,
			borderColor: `rgba(${color}, 1)`,
		};
		datasets.push(dataset)

		@foreach($ins->data_per_kategori as $tahun => $dataKategori)
			@foreach($dataKategori as $namaKategori => $jml)
			if(!paketPekerjaanPerKategori[`{{ $tahun }}`].hasOwnProperty(i)) {
				paketPekerjaanPerKategori[`{{ $tahun }}`][i] = []
			}
			paketPekerjaanPerKategori[`{{ $tahun }}`][i][`{{ $namaKategori }}`] = parseInt(`{{ $jml }}`)
			@endforeach
		@endforeach
		
		i++;
	@endforeach
	
 
	let jumlahPaketPekerjaanData = {
		'labels': tahun,
		'datasets': datasets
	};

	let chartOptions = {
		scales: {
			xAxes: [{
				barPercentage: 1,
				categoryPercentage: 0.6
			}],
			yAxes: [{
				ticks: {
					callback: function(label, index, labels) {
						return numberFormat(label);
					}
				},
			}]
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					return numberFormat(tooltipItem.yLabel)
				},
				title: function(tooltipItem, data) {
					const tahun = tooltipItem[0].xLabel
					return datasets[tooltipItem[0].datasetIndex].label + ` (${tahun})`
				},
				footer: function(tooltipItem, data) {
					let dataPerKategori = []
					let kategori = paketPekerjaanPerKategori[tooltipItem[0].xLabel][tooltipItem[0].datasetIndex]
					Object.keys(kategori).forEach(function(key) {
						dataPerKategori.push(`${key} : ${kategori[key]}`)
					});
					return dataPerKategori;
				}
			}
		}
	};
	 
	let paketPekerjaanChart = new Chart(paketPekerjaanChartElem, {
		type: 'bar',
		data: jumlahPaketPekerjaanData,
		options: chartOptions
	});




	// Chart Pagu Anggaran
	let paguAnggaranChartElem = document.getElementById("pagu-anggaran-chart");
	datasets = [];
	tahun = [];
	i = 0;
	@foreach(\App\Models\PaketPekerjaan::paguAnggaranDataChart() as $ins)
		data = []
		tahun = []
		@foreach($ins->data as $tahun => $jumlah)
		data.push(parseInt(`{{ $jumlah }}`))
		tahun.push(parseInt(`{{ $tahun }}`))
		if(!paguAnggaranPerKategori.hasOwnProperty(`{{ $tahun }}`)) {
			paguAnggaranPerKategori[`{{ $tahun }}`] = []
		}
		@endforeach
		color = dynamicColors()
		dataset = {
			label: '{{ $ins->instansi->nama_instansi }}',
			data: data,
			backgroundColor: `rgba(${color}, 0.6)`,
			borderColor: `rgba(${color}, 1)`,
		};
		datasets.push(dataset)

		@foreach($ins->data_per_kategori as $tahun => $dataKategori)
			@foreach($dataKategori as $namaKategori => $jml)
			if(!paguAnggaranPerKategori[`{{ $tahun }}`].hasOwnProperty(i)) {
				paguAnggaranPerKategori[`{{ $tahun }}`][i] = []
			}
			paguAnggaranPerKategori[`{{ $tahun }}`][i][`{{ $namaKategori }}`] = parseInt(`{{ $jml }}`)
			@endforeach
		@endforeach
		i++;
	@endforeach
 
	let paguAnggaranData = {
		'labels': tahun,
		'datasets': datasets
	};

	chartOptions = {
		scales: {
			xAxes: [{
				barPercentage: 1,
				categoryPercentage: 0.6
			}],
			yAxes: [{
				ticks: {
					callback: function(label, index, labels) {
						return numberFormat(label);
					}
				},
			}]
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					return `Rp. ${numberFormat(tooltipItem.yLabel)}`
				},
				title: function(tooltipItem, data) {
					const tahun = tooltipItem[0].xLabel
					return datasets[tooltipItem[0].datasetIndex].label + ` (${tahun})`
				},
				footer: function(tooltipItem, data) {
					let dataPerKategori = []
					let kategori = paguAnggaranPerKategori[tooltipItem[0].xLabel][tooltipItem[0].datasetIndex]
					Object.keys(kategori).forEach(function(key) {
						dataPerKategori.push(`${key} : Rp. ${numberFormat(kategori[key])}`)
					});
					return dataPerKategori;
				}
			}
		}
	};
	 
	let paguAnggaranChart = new Chart(paguAnggaranChartElem, {
		type: 'bar',
		data: paguAnggaranData,
		options: chartOptions
	});


	// Chart Realisasi Anggaran
	let realisasiAnggaranChartElem = document.getElementById("realisasi-anggaran-chart");
	datasets = [];
	tahun = [];
	i = 0;
	@foreach(\App\Models\PaketPekerjaan::realisasiAnggaranDataChart() as $ins)
		data = []
		tahun = []
		@foreach($ins->data as $tahun => $jumlah)
		data.push(parseInt(`{{ $jumlah }}`))
		tahun.push(parseInt(`{{ $tahun }}`))
		if(!realisasiAnggaranPerKategori.hasOwnProperty(`{{ $tahun }}`)) {
			realisasiAnggaranPerKategori[`{{ $tahun }}`] = []
		}
		@endforeach
		color = dynamicColors()
		dataset = {
			label: '{{ $ins->instansi->nama_instansi }}',
			data: data,
			backgroundColor: `rgba(${color}, 0.6)`,
			borderColor: `rgba(${color}, 1)`,
		};
		datasets.push(dataset)

		@foreach($ins->data_per_kategori as $tahun => $dataKategori)
			@foreach($dataKategori as $namaKategori => $jml)
			if(!realisasiAnggaranPerKategori[`{{ $tahun }}`].hasOwnProperty(i)) {
				realisasiAnggaranPerKategori[`{{ $tahun }}`][i] = []
			}
			realisasiAnggaranPerKategori[`{{ $tahun }}`][i][`{{ $namaKategori }}`] = parseInt(`{{ $jml }}`)
			@endforeach
		@endforeach
		i++;
	@endforeach
 
	let realisasiAnggaranData = {
		'labels': tahun,
		'datasets': datasets
	};

	chartOptions = {
		scales: {
			xAxes: [{
				barPercentage: 1,
				categoryPercentage: 0.6
			}],
			yAxes: [{
				ticks: {
					callback: function(label, index, labels) {
						return numberFormat(label);
					}
				},
			}]
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					return `Rp. ${numberFormat(tooltipItem.yLabel)}`
				},
				title: function(tooltipItem, data) {
					const tahun = tooltipItem[0].xLabel
					return datasets[tooltipItem[0].datasetIndex].label + ` (${tahun})`
				},
				footer: function(tooltipItem, data) {
					let dataPerKategori = []
					let kategori = realisasiAnggaranPerKategori[tooltipItem[0].xLabel][tooltipItem[0].datasetIndex]
					Object.keys(kategori).forEach(function(key) {
						dataPerKategori.push(`${key} : Rp. ${numberFormat(kategori[key])}`)
					});
					return dataPerKategori;
				}
			}
		}
	};
	 
	let realisasiAnggaranChart = new Chart(realisasiAnggaranChartElem, {
		type: 'bar',
		data: realisasiAnggaranData,
		options: chartOptions
	});


	// Chart Tenaga Ahli
	let tenagaAhliChartElem = document.getElementById("tenaga-ahli-chart");
	let tenagaAhliDatasets = [];
	let tenagaAhliTahun = [];
	i = 0;
	@foreach(\App\Models\TenagaAhli::tenagaAhliDataChart() as $tahun => $jml)
		data = [ `{{ $jml }}` ]
		color = dynamicColors()
		dataset = {
			label: '{{ $tahun }}',
			data: data,
			backgroundColor: `rgba(${color}, 0.6)`,
			borderColor: `rgba(${color}, 1)`,
		};
		tenagaAhliTahun.push(parseInt(`{{ $tahun }}`))
		tenagaAhliDatasets.push(dataset)
	@endforeach
 
	let tenagaAhliData = {
		'labels': tenagaAhliTahun,
		'datasets': tenagaAhliDatasets
	};

	chartOptions = {
		'scales' : {
			'min' : 0,
		}
	};
	 
	let tenagaAhliChart = new Chart(tenagaAhliChartElem, {
		type: 'bar',
		data: tenagaAhliData,
		options: chartOptions
	});
</script>
@endsection