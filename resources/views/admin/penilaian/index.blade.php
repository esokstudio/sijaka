@extends('admin.layouts.templates')


@section('action')
<button class="btn btn-success" id="export-btn">
	<i class="mdi mdi-download"></i> Export ke Excel
</button>
<button class="btn btn-primary" id="filter-btn">
	<i class="mdi mdi-filter"></i> Filter
</button>
<div class="dropdown d-inline-block">
	<button class="btn btn-success dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<i class="mdi mdi-plus-thick"></i> Buat Penilaian
	</button>
	<div class="dropdown-menu">
		@forelse(App\Models\SkemaPenilaian::all() as $skemaPenilaian)
		<a class="dropdown-item" href="{{ route('penilaian.create') }}?id_skema={{ $skemaPenilaian->id }}" title="{{ $skemaPenilaian->nama_skema_penilaian }}">
			{{ $skemaPenilaian->nama_skema_penilaian }} 
		</a>
		@empty
		<a class="dropdown-item" href="javascript:void(0);" title="Tidak Memiliki Skema Penilaian"> - Tidak Memiliki Skema Penilaian - </a>
		@endforelse
	</div>
</div>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Tanggal Kontrak </th>
								<th> Tahun Anggaran </th>
								<th> Nomor Kontrak </th>
								<th> Perusahaan </th>
								<th> Paket Pekerjaan </th>
								<th> Nilai Kinerja </th>
								<th> Nilai Keterangan </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="filter-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="filter-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Filter </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label> Tahun Anggaran </label>
						<select name="tahun_anggaran" style="width: 100%;" required>
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
							<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="id_perusahaan">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\Perusahaan::all() as $perusahaan)
							<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Awal (Tgl Kontrak) </label>
								<input type="date" name="start_date" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Akhir (Tgl Kontrak) </label>
								<input type="date" name="end_date" class="form-control">
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-filter"></i> Filter
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="export-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="export-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Export ke Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Tahun Anggaran </label>
						<select name="tahun_anggaran" style="width: 100%;" required>
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
							<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>
					
					<div class="form-group">
						<label> Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="id_perusahaan">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\Perusahaan::all() as $perusahaan)
							<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Awal (Tgl Kontrak) </label>
								<input type="date" name="start_date" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Akhir (Tgl Kontrak) </label>
								<input type="date" name="end_date" class="form-control">
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-download"></i> Export
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('penilaian') }}"
			},
			columns : [
				{
					data : 'paket_pekerjaan.tanggal_kontrak',
					name : 'paket_pekerjaan.tanggal_kontrak'
				},
				{
					data : 'tahun_anggaran',
					name : 'tahun_anggaran'
				},
				{
					data : 'paket_pekerjaan.nomor_kontrak',
					name : 'paket_pekerjaan.nomor_kontrak'
				},
				{
					data : 'perusahaan.nama_perusahaan',
					name : 'perusahaan.nama_perusahaan'
				},
				{
					data : 'paket_pekerjaan.nama_pekerjaan',
					name : 'paket_pekerjaan.nama_pekerjaan'
				},
				{
					data : 'nilai_kinerja',
					name : 'nilai_kinerja'
				},
				{
					data : 'nilai_keterangan',
					name : 'nilai_keterangan'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			},
			preDrawCallback: settings => {
				let $filter = $('#filter-form');
				let query = '';

				if($filter.find(`[name="tahun_anggaran"]`).val() != 'all') query += `&tahun_anggaran=${$filter.find(`[name="tahun_anggaran"]`).val()}`
				if($filter.find(`[name="id_perusahaan"]`).val() != 'all') query += `&id_perusahaan=${$filter.find(`[name="id_perusahaan"]`).val()}`
				if($filter.find(`[name="start_date"]`).val() != null) query += `&start_date=${$filter.find(`[name="start_date"]`).val()}`
				if($filter.find(`[name="end_date"]`).val() != null) query += `&end_date=${$filter.find(`[name="end_date"]`).val()}`

				if(query.length > 0) query = query.substr(1);

				settings.ajax.url = `{{ route('penilaian') }}?${query}`
			},
			order:['0', 'desc']
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}


		const $filterModal = $('#filter-modal');
		const $filterForm = $('#filter-form');
		const $filterSubmitBtn = $filterForm.find(`[type="submit"]`).ladda()

		$('#filter-btn').on('click', function(){
			$('#filter-modal').modal('show')
		})

		$('#filter-form').on('submit', function(e){
			e.preventDefault();

			$filterModal.modal('hide')
			dtReload()
		})

		$filterForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})



		const $exportModal = $('#export-modal');
		const $exportForm = $('#export-form');
		const $exportSubmitBtn = $exportForm.find(`[type="submit"]`).ladda()

		$('#export-btn').on('click', function(){
			$('#export-modal').modal('show')
		})

		$('#export-form').on('submit', function(e){
			e.preventDefault();

			$exportSubmitBtn.ladda('start')
			let formData = $(this).serialize()

			ajaxSetup()
			$.ajax({
				url: `{{ route('penilaian.export') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$exportSubmitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$exportSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $importForm)
			})
		})

		$exportForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

	})

</script>
@endsection