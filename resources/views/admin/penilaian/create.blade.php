@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}

					<input type="hidden" name="nama_skema_penilaian" value="{{ $skema->nama_skema_penilaian }}" required>

					<div class="row">

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tahun Anggaran {!! Template::required() !!} </label>
								<select name="tahun_anggaran" style="width: 100%;" required>
									@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
									<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Unit Kerja / Perangkat Daerah {!! Template::required() !!} </label>
								<input type="text" name="unit_kerja" class="form-control" placeholder="Unit Kerja" value="{{ auth()->user()->namaInstansi() }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Paket Pekerjaan {!! Template::required() !!} </label>
								<select name="id_paket_pekerjaan" style="width: 100%;" required>
									@if(auth()->user()->isPengguna())

									@foreach(\App\Models\PaketPekerjaan::doesntHave('penilaian')->where('id_instansi', auth()->user()->id_instansi)->get() as $paketPekerjaan)
									<option value="{{ $paketPekerjaan->id }}">
										{{ $paketPekerjaan->nomor_kontrak }} - {{ $paketPekerjaan->nama_pekerjaan }} - {{ $paketPekerjaan->namaPerusahaan() }}
									</option>
									@endforeach

									@else

									@foreach(\App\Models\PaketPekerjaan::doesntHave('penilaian')->get() as $paketPekerjaan)
									<option value="{{ $paketPekerjaan->id }}">
										{{ $paketPekerjaan->nomor_kontrak }} - {{ $paketPekerjaan->nama_pekerjaan }} - {{ $paketPekerjaan->namaPerusahaan() }}
									</option>
									@endforeach

									@endif
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Metode Pemilihan Penyedia {!! Template::required() !!} </label>
								<select name="metode_pemilihan_penyedia" class="form-control" required>
									<option value="" selected disabled> - Pilih - </option>
									<option value="Tender"> Tender </option>
									<option value="Tender Cepat"> Tender Cepat </option>
									<option value="Pengadaan Langsung"> Pengadaan Langsung </option>
									<option value="Penunjukan Langsung"> Penunjukan Langsung </option>
									<option value="E-Purchasing"> E-Purchasing </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tempat Penilaian {!! Template::required() !!} </label>
								<input type="text" name="tempat_penilaian" class="form-control" placeholder="Tempat Penilaian" value="Cirebon" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<p>
						<b> Panduan Pengisian Skor : </b> <br>
						<ul>
							<li> 0 - 50 (Sangat Kurang) </li>
							<li> 51 - 60 (Kurang) </li>
							<li> 61 - 70 (Cukup) </li>
							<li> 71 - 80 (Baik) </li>
							<li> 81 - 100 (Sangat Baik) </li>
						</ul>
					</p>

					<table class="table table-bordered">
						<thead>
							<tr>
								<th> No </th>
								<th> Aspek </th>
								<th> Indikator </th>
								<th> Bobot </th>
								<th width="150"> Skor </th>
							</tr>
						</thead>
						<tbody>
							<?php $row = 0; ?>
							@foreach($skema->poinPenilaian() as $data)
							<tr>
								<td rowspan="{{ count($data['list_indikator']) }}"> {{ $loop->iteration }} </td>
								<td rowspan="{{ count($data['list_indikator']) }}"> {{ $data['aspek_kinerja'] }} </td>

								<td> {{ $data['list_indikator'][0]['indikator'] }} </td>
								<td> {{ $data['list_indikator'][0]['bobot'] }} % </td>
								<td>
									<input type="hidden" name="poin[{{ $row }}][aspek_kinerja]" value="{{ $data['aspek_kinerja'] }}">
									<input type="hidden" name="poin[{{ $row }}][indikator]" value="{{ $data['list_indikator'][0]['indikator'] }}">
									<input type="hidden" name="poin[{{ $row }}][bobot]" value="{{ $data['list_indikator'][0]['bobot'] }}">
									<input type="number" class="form-control" name="poin[{{ $row++ }}][skor]" placeholder="Skor (0-100)" min="0" max="100" required>
									<!-- <select class="form-control" name="poin[{{ $row++ }}][skor]" required>
										<option value="" disabled selected> - Pilih - </option>
										<option value="1"> Cukup </option>
										<option value="2"> Baik </option>
										<option value="3"> Sangat Baik </option>
									</select> -->
								</td>
							</tr>
								@foreach($data['list_indikator'] as $item)
								@if($loop->iteration > 1)
								<tr>
									<td> {{ $item['indikator'] }} </td>		
									<td> {{ $item['bobot'] }} % </td>		
									<td>
										<input type="hidden" name="poin[{{ $row }}][aspek_kinerja]" value="{{ $data['aspek_kinerja'] }}">
										<input type="hidden" name="poin[{{ $row }}][indikator]" value="{{ $item['indikator'] }}">
										<input type="hidden" name="poin[{{ $row }}][bobot]" value="{{ $item['bobot'] }}">
										<input type="number" class="form-control" name="poin[{{ $row++ }}][skor]" placeholder="Skor (0-100)" min="0" max="100" required>
										<!-- <select class="form-control" name="poin[{{ $row++ }}][skor]" required>
											<option value="" disabled selected> - Pilih - </option>
											<option value="1"> Cukup </option>
											<option value="2"> Baik </option>
											<option value="3"> Sangat Baik </option>
										</select> -->
									</td>
								</tr>
								@endif
								@endforeach
							@endforeach
						</tbody>
					</table>

					<hr>

					<div class="row">

						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Penilai 1 {!! Template::required() !!} </label>
								<input type="text" name="nama_penilai_1" class="form-control" placeholder="Nama Penilai 1" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Penilai 1 {!! Template::required() !!} </label>
								<input type="text" name="jabatan_penilai_1" class="form-control" placeholder="Jabatan Penilai 1" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Penilai 1 </label>
								<input type="text" name="nip_penilai_1" class="form-control" placeholder="NIP Penilai 1">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Penilai 2 {!! Template::required() !!} </label>
								<input type="text" name="nama_penilai_2" class="form-control" placeholder="Nama Penilai 2" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Penilai 2 {!! Template::required() !!} </label>
								<input type="text" name="jabatan_penilai_2" class="form-control" placeholder="Jabatan Penilai 2" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Penilai 2 </label>
								<input type="text" name="nip_penilai_2" class="form-control" placeholder="NIP Penilai 2">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Yang Mengetahui {!! Template::required() !!} </label>
								<input type="text" name="nama_mengetahui" class="form-control" placeholder="Nama Yang Mengetahui" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Yang Mengetahui {!! Template::required() !!} </label>
								<input type="text" name="jabatan_mengetahui" class="form-control" placeholder="Jabatan Yang Mengetahui" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Yang Mengetahui </label>
								<input type="text" name="nip_mengetahui" class="form-control" placeholder="NIP Yang Mengetahui">
								<span class="invalid-feedback"></span>
							</div>
						</div>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			$form.find(`[name="id_paket_pekerjaan"]`).val('').trigger('change')
			clearInvalid();
			$form.find(`[name="unit_kerja"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('penilaian.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				// $submitBtn.ladda('stop');
				// resetForm();
				setTimeout(() => {
					window.location.href = `{{ route('penilaian') }}`
				})
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		$form.find(`[name="tahun_anggaran"]`).select2({
			'placeholder': '- Pilih Tahun -'
		})

		$form.find(`[name="id_paket_pekerjaan"]`).select2({
			'placeholder': '- Pilih Paket Pekerjaan -'
		})

		resetForm();

	})

</script>
@endsection