@extends('admin.layouts.templates')


@section('content')
<form id="form">
	<div class="row">
		<div class="col-md-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<h6 class="card-title"> {{ $title }} </h6>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tahun Anggaran {!! Template::required() !!} </label>
								<select name="tahun_anggaran" style="width: 100%;" required>
									@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
									<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>
							
						<div class="col-lg-6">
							<div class="form-group">
								<label> Unit Kerja / Perangkat Daerah {!! Template::required() !!} </label>
								<input type="text" name="unit_kerja" class="form-control" placeholder="Unit Kerja" value="{{ $penilaian->unit_kerja }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Paket Pekerjaan {!! Template::required() !!} </label>
								<select name="id_paket_pekerjaan" style="width: 100%;" required>

									@foreach($dataPaket as $paketPekerjaan)
									<option value="{{ $paketPekerjaan->id }}">
										{{ $paketPekerjaan->nomor_kontrak }} - {{ $paketPekerjaan->nama_pekerjaan }} - {{ $paketPekerjaan->namaPerusahaan() }}
									</option>
									@endforeach
									
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Metode Pemilihan Penyedia {!! Template::required() !!} </label>
								<select name="metode_pemilihan_penyedia" class="form-control" required>
									<option value="Tender"> Tender </option>
									<option value="Tender Cepat"> Tender Cepat </option>
									<option value="Pengadaan Langsung"> Pengadaan Langsung </option>
									<option value="Penunjukan Langsung"> Penunjukan Langsung </option>
									<option value="E-Purchasing"> E-Purchasing </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tempat Penilaian {!! Template::required() !!} </label>
								<input type="text" name="tempat_penilaian" class="form-control" placeholder="Tempat Penilaian" value="{{ $penilaian->tempat_penilaian }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>
					</div>

					<p>
						<b> Panduan Pengisian Skor : </b> <br>
						<ul>
							<li> 0 - 50 (Sangat Kurang) </li>
							<li> 51 - 60 (Kurang) </li>
							<li> 61 - 70 (Cukup) </li>
							<li> 71 - 80 (Baik) </li>
							<li> 81 - 100 (Sangat Baik) </li>
						</ul>
					</p>

					<table class="table table-bordered">
						<thead>
							<tr>
								<th> Aspek </th>
								<th> Indikator </th>
								<th> Bobot </th>
								<th width="150"> Skor </th>
							</tr>
						</thead>
						<tbody>
							@foreach($penilaian->poinPenilaianWithGrouping() as $group)
							<tr>
								<td rowspan="{{ count($group) }}"> {{ $group[0]->aspek_kinerja }} </td>
								<td> {{ $group[0]->indikator }} </td>
								<td> {{ $group[0]->bobot }} % </td>
								<td>
									<input type="number" class="form-control" name="poin[{{ $group[0]->id }}]" placeholder="Skor (0-100)" min="0" max="100" required>
								</td>
							</tr>
								@foreach($group as $item)
								@if($loop->iteration > 1)
								<tr>
									<td> {{ $item->indikator }} </td>
									<td> {{ $item->bobot }} % </td>
									<td>
										<input type="number" class="form-control" name="poin[{{ $item->id }}]" placeholder="Skor (0-100)" min="0" max="100" required>
									</td>
								</tr>
								@endif
								@endforeach
							@endforeach
						</tbody>
					</table>

					<hr>

					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Penilai 1 {!! Template::required() !!} </label>
								<input type="text" name="nama_penilai_1" class="form-control" placeholder="Nama Penilai 1" value="{{ $penilaian->nama_penilai_1 }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Penilai 1 {!! Template::required() !!} </label>
								<input type="text" name="jabatan_penilai_1" class="form-control" placeholder="Jabatan Penilai 1" value="{{ $penilaian->jabatan_penilai_1 }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Penilai 1 </label>
								<input type="text" name="nip_penilai_1" class="form-control" placeholder="NIP Penilai 1" value="{{ $penilaian->nip_penilai_1 }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Penilai 2 {!! Template::required() !!} </label>
								<input type="text" name="nama_penilai_2" class="form-control" placeholder="Nama Penilai 2" value="{{ $penilaian->nama_penilai_2 }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Penilai 2 {!! Template::required() !!} </label>
								<input type="text" name="jabatan_penilai_2" class="form-control" placeholder="Jabatan Penilai 2" value="{{ $penilaian->jabatan_penilai_2 }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Penilai 2 </label>
								<input type="text" name="nip_penilai_2" class="form-control" placeholder="NIP Penilai 2" value="{{ $penilaian->nip_penilai_2 }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Nama Yang Mengetahui {!! Template::required() !!} </label>
								<input type="text" name="nama_mengetahui" class="form-control" placeholder="Nama Yang Mengetahui" value="{{ $penilaian->nama_mengetahui }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> Jabatan Yang Mengetahui {!! Template::required() !!} </label>
								<input type="text" name="jabatan_mengetahui" class="form-control" placeholder="Jabatan Yang Mengetahui" value="{{ $penilaian->jabatan_mengetahui }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label> NIP Yang Mengetahui </label>
								<input type="text" name="nip_mengetahui" class="form-control" placeholder="NIP Yang Mengetahui" value="{{ $penilaian->nip_mengetahui }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			// $form.find(`[name="id_paket_pekerjaan"]`).val('').trigger('change')
			clearInvalid();
			$form.find(`[name="unit_kerja"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('penilaian.update', $penilaian->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				$submitBtn.ladda('stop');
				ajaxSuccessHandling(response);
				// resetForm();
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		$form.find(`[name="tahun_anggaran"]`).select2({
			'placeholder': '- Pilih Tahun -'
		})
		$form.find(`[name="tahun_anggaran"]`).val(`{{ $penilaian->tahun_anggaran }}`).trigger('change')

		$form.find(`[name="id_paket_pekerjaan"]`).select2({
			'placeholder': '- Pilih Paket Pekerjaan -'
		})
		$form.find(`[name="id_paket_pekerjaan"]`).val(`{{ $penilaian->id_paket_pekerjaan }}`).trigger('change')
		$form.find(`[name="metode_pemilihan_penyedia"]`).val(`{{ $penilaian->metode_pemilihan_penyedia }}`).trigger('change')

		resetForm();

		@foreach($penilaian->poinPenilaian as $poin)
		$('[name="poin[{{ $poin->id }}]"]').val(`{{ $poin->skor }}`).trigger('change')
		@endforeach

	})

</script>
@endsection