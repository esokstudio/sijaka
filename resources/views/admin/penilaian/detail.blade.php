@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>

				<hr>

				<div class="row">
					<div class="col-lg-6">
						<div class="table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<td> Tahun Anggaran </td>
										<td> : </td>
										<td> {{ $penilaian->tahun_anggaran ?? '-' }} </td>
									</tr>
									<tr>
										<td> Unit Kerja </td>
										<td> : </td>
										<td> {{ $penilaian->unit_kerja ?? '-' }} </td>
									</tr>
									<tr>
										<td> Paket Pekerjaan </td>
										<td> : </td>
										<td> {{ $penilaian->namaPekerjaan() }} </td>
									</tr>
									<tr>
										<td> Perusahaan </td>
										<td> : </td>
										<td> {{ $penilaian->namaPerusahaan() }} </td>
									</tr>
									<tr>
										<td> Lokasi Pekerjaan </td>
										<td> : </td>
										<td> {{ $penilaian->lokasiPekerjaan() }} </td>
									</tr>
									<tr>
										<td> Nomor Kontrak </td>
										<td> : </td>
										<td> {{ $penilaian->nomorKontrak() }} </td>
									</tr>
									<tr>
										<td> Tanggal Kontrak </td>
										<td> : </td>
										<td> {{ $penilaian->tanggalKontrak() }} </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<td> Jangka Waktu Pelaksanaan </td>
										<td> : </td>
										<td> {{ $penilaian->jangka_waktu_pelaksanaan }} hari </td>
									</tr>
									<tr>
										<td> Metode Pemilihan Penyedia </td>
										<td> : </td>
										<td> {{ $penilaian->metode_pemilihan_penyedia }} hari </td>
									</tr>
									<tr>
										<td> Nilai Kinerja </td>
										<td> : </td>
										<td> {{ $penilaian->nilai_kinerja }} </td>
									</tr>
									<tr>
										<td> Nilai Keterangan </td>
										<td> : </td>
										<td> {{ $penilaian->nilai_keterangan }} </td>
									</tr>
									<tr>
										<td> Skema Penilaian </td>
										<td> : </td>
										<td> {{ $penilaian->nama_skema_penilaian }} </td>
									</tr>
									<tr>
										<td> Tempat Penilaian </td>
										<td> : </td>
										<td> {{ $penilaian->tempat_penilaian }} </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<hr>

				<p>
					<b> Keterangan Skor : </b> <br>
					0 - 50 (Sangat Kurang), 51 - 60 (Kurang), 61 - 70 (Cukup), 71 - 80 (Baik), 81 - 100 (Sangat Baik)
				</p>

				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th> Aspek </th>
								<th> Indikator </th>
								<th> Bobot </th>
								<th> Skor </th>
							</tr>
						</thead>
						<tbody>
							@foreach($penilaian->poinPenilaianWithGrouping() as $group)
							<tr>
								<td rowspan="{{ count($group) }}"> {{ $group[0]->aspek_kinerja }} </td>
								<td> {{ $group[0]->indikator }} </td>
								<td> {{ $group[0]->bobot }} % </td>
								<td> {{ $group[0]->skor }} </td>
							</tr>
								@foreach($group as $item)
								@if($loop->iteration > 1)
								<tr>
									<td> {{ $item->indikator }} </td>
									<td> {{ $item->bobot }} % </td>
									<td> {{ $item->skor }} </td>
								</tr>
								@endif
								@endforeach
							@endforeach
						</tbody>
					</table>
				</div>

				<hr>

				<h5 class="mb-4"> Yang bertanda tangan </h5>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th> Nama </th>
							<th> Jabatan </th>
							<th> NIP </th>
							<th> Sebagai </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> {{ $penilaian->nama_penilai_1 ?? '-' }} </td>
							<td> {{ $penilaian->jabatan_penilai_1 ?? '-' }} </td>
							<td> {{ $penilaian->nip_penilai_1 ?? '-' }} </td>
							<td> Penilai </td>
						</tr>
						<tr>
							<td> {{ $penilaian->nama_penilai_2 ?? '-' }} </td>
							<td> {{ $penilaian->jabatan_penilai_2 ?? '-' }} </td>
							<td> {{ $penilaian->nip_penilai_2 ?? '-' }} </td>
							<td> Penilai </td>
						</tr>
						<tr>
							<td> {{ $penilaian->nama_mengetahui ?? '-' }} </td>
							<td> {{ $penilaian->jabatan_mengetahui ?? '-' }} </td>
							<td> {{ $penilaian->nip_mengetahui ?? '-' }} </td>
							<td> Yang Mengetahui </td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>
@endsection