@extends('admin.layouts.templates')


@section('styles')
<style type="text/css">
.dataTables_wrapper .dataTable .btn, .dataTables_wrapper .dataTable .ajax-upload-dragdrop .ajax-file-upload, .ajax-upload-dragdrop .dataTables_wrapper .dataTable .ajax-file-upload, .dataTables_wrapper .dataTable .swal2-modal .swal2-buttonswrapper .swal2-styled, .swal2-modal .swal2-buttonswrapper .dataTables_wrapper .dataTable .swal2-styled, .dataTables_wrapper .dataTable .wizard > .actions a, .wizard > .actions .dataTables_wrapper .dataTable a {
    padding: 0.5rem 1rem!important;
}
</style>
@endsection


@section('content')
<div class="row">

	@if(count($perusahaanList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Profil Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Nama Perusahaan </th>
								<th> Alamat </th>
								<th> Kota </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($perusahaanList as $perusahaan)
							<tr>
								<th> {!! tmpText($perusahaan, 'nama_perusahaan') !!} </th>
								<th> {!! tmpText($perusahaan, 'alamat') !!} </th>
								<th> {!! tmpText($perusahaan, 'kota') !!} </th>
								<th> 
									<a href="{{ route('perusahaan.detail', $perusahaan->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif

	@if(count($kualifikasiList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Kualifikasi Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Sub Bidang Klasifikasi </th>
								<th> Nomor Kode </th>
								<th> Kualifikasi </th>
								<th> Tahun </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($kualifikasiList as $kualifikasi)
							<tr>
								<th> {!! $kualifikasi->namaPerusahaan() !!} </th>
								<th> {!! tmpText($kualifikasi, 'sub_bidang_klasifikasi') !!} </th>
								<th> {!! tmpText($kualifikasi, 'nomor_kode') !!} </th>
								<th> {!! tmpText($kualifikasi, 'kualifikasi') !!} </th>
								<th> {!! tmpText($kualifikasi, 'tahun') !!} </th>
								<th> 
									<a href="{{ route('kualifikasi_perusahaan.detail', $kualifikasi->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif

	@if(count($aktePendirianList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Akte Pendirian Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> No Akte </th>
								<th> Tgl Akte </th>
								<th> Nama Notaris </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($aktePendirianList as $aktePendirian)
							<tr>
								<th> {!! $aktePendirian->namaPerusahaan() !!} </th>
								<th> {!! tmpText($aktePendirian, 'no_akte') !!} </th>
								<th> {!! tmpText($aktePendirian, 'tanggal_akte') !!} </th>
								<th> {!! tmpText($aktePendirian, 'nama_notaris') !!} </th>
								<th> 
									<a href="{{ route('perusahaan.detail', $aktePendirian->id_perusahaan) }}?tab=akte-pendirian" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif

	@if(count($pengesahanList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Pengesahan Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> No Kemenkumham </th>
								<th> Tgl Kemenkumham </th>
								<th> No Pengadilan Negeri </th>
								<th> Tgl Pengadilan Negeri </th>
								<th> No Lembar Negara </th>
								<th> Tgl Lembar Negara </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($pengesahanList as $pengesahan)
							<tr>
								<th> {!! $pengesahan->namaPerusahaan() !!} </th>
								<th> {!! tmpText($pengesahan, 'menkumham_no') !!} </th>
								<th> {!! tmpText($pengesahan, 'menkumham_tgl') !!} </th>
								<th> {!! tmpText($pengesahan, 'pengadilan_negeri_no') !!} </th>
								<th> {!! tmpText($pengesahan, 'pengadilan_negeri_tgl') !!} </th>
								<th> {!! tmpText($pengesahan, 'lembar_negara_no') !!} </th>
								<th> {!! tmpText($pengesahan, 'lembar_negara_tgl') !!} </th>
								<th> 
									<a href="{{ route('perusahaan.detail', $pengesahan->id_perusahaan) }}?tab=pengesahan" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(count($aktePerubahanList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Akte Perubahan Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> No Akte </th>
								<th> Nama Notaris </th>
								<th> Alamat </th>
								<th> Kota </th>
								<th> Provinsi </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($aktePerubahanList as $aktePerubahan)
							<tr>
								<th> {!! $aktePerubahan->namaPerusahaan() !!} </th>
								<th> {!! tmpText($aktePerubahan, 'no_akte') !!} </th>
								<th> {!! tmpText($aktePerubahan, 'nama_notaris') !!} </th>
								<th> {!! tmpText($aktePerubahan, 'alamat') !!} </th>
								<th> {!! tmpText($aktePerubahan, 'kota') !!} </th>
								<th> {!! tmpText($aktePerubahan, 'provinsi') !!} </th>
								<th> 
									<a href="{{ route('akte_perubahan_perusahaan.detail', $aktePerubahan->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(count($pengurusList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Pengurus Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Nama </th>
								<th> Tgl Lahir </th>
								<th> Alamat </th>
								<th> Jabatan </th>
								<th> Pendidikan </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($pengurusList as $pengurus)
							<tr>
								<th> {!! $pengurus->namaPerusahaan() !!} </th>
								<th> {!! tmpText($pengurus, 'nama') !!} </th>
								<th> {!! tmpText($pengurus, 'tgl_lahir') !!} </th>
								<th> {!! tmpText($pengurus, 'alamat') !!} </th>
								<th> {!! tmpText($pengurus, 'jabatan') !!} </th>
								<th> {!! tmpText($pengurus, 'pendidikan') !!} </th>
								<th> 
									<a href="{{ route('pengurus_perusahaan.detail', $pengurus->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(count($keuanganList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Keuangan Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Nama </th>
								<th> Alamat </th>
								<th> Jumlah Saham </th>
								<th> Modal Dasar </th>
								<th> Modal Disetor </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($keuanganList as $keuangan)
							<tr>
								<th> {!! $keuangan->namaPerusahaan() !!} </th>
								<th> {!! tmpText($keuangan, 'nama') !!} </th>
								<th> {!! tmpText($keuangan, 'alamat') !!} </th>
								<th> {!! tmpText($keuangan, 'jumlah_saham') !!} </th>
								<th> {!! tmpText($keuangan, 'modal_dasar') !!} </th>
								<th> {!! tmpText($keuangan, 'modal_disetor') !!} </th>
								<th> 
									<a href="{{ route('keuangan_perusahaan.detail', $keuangan->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(count($tenagaKerjaList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Tenaga Kerja Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Nama </th>
								<th> Tgl Lahir </th>
								<th> Pendidikan </th>
								<th> No Registrasi </th>
								<th> Jenis Sertifikat </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($tenagaKerjaList as $tenagaKerja)
							<tr>
								<th> {!! $tenagaKerja->namaPerusahaan() !!} </th>
								<th> {!! tmpText($tenagaKerja, 'nama') !!} </th>
								<th> {!! tmpText($tenagaKerja, 'tgl_lahir') !!} </th>
								<th> {!! tmpText($tenagaKerja, 'pendidikan') !!} </th>
								<th> {!! tmpText($tenagaKerja, 'no_registrasi') !!} </th>
								<th> {!! tmpText($tenagaKerja, 'jenis_sertifikat') !!} </th>
								<th> 
									<a href="{{ route('tenaga_kerja_perusahaan.detail', $tenagaKerja->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(count($pengalamanList) > 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Perubahan Pengalaman Perusahaan </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Nama Pekerjaan </th>
								<th> Lokasi </th>
								<th> Nomor Kontrak </th>
								<th> Tgl Kontrak </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>

						<tbody>
							@foreach($pengalamanList as $pengalaman)
							<tr>
								<th> {!! $pengalaman->namaPerusahaan() !!} </th>
								<th> {!! tmpText($pengalaman, 'nama_pekerjaan') !!} </th>
								<th> {!! tmpText($pengalaman, 'lokasi_pekerjaan') !!} </th>
								<th> {!! tmpText($pengalaman, 'nomor_kontrak') !!} </th>
								<th> {!! tmpText($pengalaman, 'tanggal_kontrak') !!} </th>
								<th> 
									<a href="{{ route('pengalaman_perusahaan.detail', $pengalaman->id) }}" class="btn btn-info">
										<i class="mdi mdi-magnify"></i> Telusuri
									</a>
								</th>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif


	@if(SijakdaHelper::permintaanPerubahanCount() == 0)
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				
				<p align="center" class="mb-0">
					<i> Tidak ada permintaan </i>
				</p>

			</div>
		</div>
	</div>
	@endif
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('.dataTable').DataTable({
			autoWidth : false,
		});

	})

</script>
@endsection