@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-12">
							<input type="hidden" name="id_paket_pekerjaan" value="{{ $paketPekerjaan->id }}">

							<div class="form-group">
								<label> Tahun {!! Template::required() !!} </label>
								<select name="tahun" style="width: 100%;" required>
									@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
									<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Bulan {!! Template::required() !!} </label>
								<select name="bulan" style="width: 100%;" required>
									<option value="Januari"> Januari </option>
									<option value="Februari"> Februari </option>
									<option value="Maret"> Maret </option>
									<option value="April"> April </option>
									<option value="Mei"> Mei </option>
									<option value="Juni"> Juni </option>
									<option value="Juli"> Juli </option>
									<option value="Agustus"> Agustus </option>
									<option value="September"> September </option>
									<option value="Oktober"> Oktober </option>
									<option value="November"> November </option>
									<option value="Desember"> Desember </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Progress Keuangan (Rp) {!! Template::required() !!} </label>
								<input type="number" name="progress_keuangan" class="form-control" placeholder="Progress Keuangan" min="0" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Progress Fisik (%) {!! Template::required() !!} </label>
								<input type="number" name="progress_fisik" class="form-control" placeholder="Progress Fisik" min="0" max="100" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Foto Progress Fisik {!! Template::required() !!} </label>
								<input type="file" name="foto" class="form-control" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="tahun"]`).select2({
			'placeholder' : '- Pilih Tahun -'
		})
		$form.find(`[name="bulan"]`).select2({
			'placeholder' : '- Pilih Bulan -'
		})

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('paket_pekerjaan.realisasi.store', $paketPekerjaan->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				setTimeout(() => {
					window.location.href = `{{ route('paket_pekerjaan.realisasi', $paketPekerjaan->id) }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

		$form.find(`[name="tahun"]`).val('').trigger('change')
		$form.find(`[name="bulan"]`).val('').trigger('change')

	})

</script>
@endsection