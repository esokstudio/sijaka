@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('paket_pekerjaan.realisasi.create', $paketPekerjaan->id) }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} [{{ $paketPekerjaan->nama_pekerjaan }} - {{ $paketPekerjaan->namaPerusahaan() }}] </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Tahun </th>
								<th> Bulan </th>
								<th> Progress Keuangan </th>
								<th> Progress Fisik </th>
								<th> Foto Progress </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('paket_pekerjaan.realisasi', $paketPekerjaan->id) }}"
			},
			columns : [
				{
					data : 'tahun',
					name : 'tahun'
				},
				{
					data : 'bulan',
					name : 'bulan'
				},
				{
					data : 'progress_keuangan',
					name : 'progress_keuangan'
				},
				{
					data : 'progress_fisik',
					name : 'progress_fisik'
				},
				{
					data : 'foto_progress_fisik',
					name : 'foto_progress_fisik'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection