@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>

				<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group">
							<label class="d-block mb-1 font-weight-bold"> Nama Lengkap </label>
							<label> {{ $message->full_name }} </label>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<label class="d-block mb-1 font-weight-bold"> Email </label>
							<label> {!! $message->emailHtml() !!} </label>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="form-group">
							<label class="d-block mb-1 font-weight-bold"> Subjek </label>
							<label> {{ $message->subject }} </label>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="form-group">
							<label class="d-block mb-1 font-weight-bold"> Isi Pesan </label>
							<label> {{ $message->message }} </label>
						</div>
					</div>

				</div>
				
			</div>
		</div>
	</div>
</div>
@endsection