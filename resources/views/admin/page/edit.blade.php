@extends('admin.layouts.templates')


@section('content')
<form id="form">
	@method('PUT')
	<div class="row">

		<div class="col-md-12">
			{!! Template::requiredBanner() !!}
		</div>
			
		<div class="col-md-9">

			<div class="form-group">
				<label> Judul {!! Template::required() !!} </label>
				<input type="text" name="title" class="form-control" placeholder="Judul" value="{{ $page->title }}" required>
				<small class="invalid-feedback"></small>
			</div>

			<div id="content-area">
				<textarea id="tiny" name="content"> {!! $page->content !!} </textarea>
			</div>

			<div id="custom-page-area">
				
			</div>

		</div>

		<div class="col-md-3 grid-margin">
			<div class="card">
				<div class="card-body">

					@if(\Setting::getValue('show_page_thumbnail_field', 'no') == 'yes')
					<div class="form-group">
						<label class="d-block"> Thumbnail </label>
						<img src="#" style="width: 100%; height: auto; display: none;" class="thumbnail-preview mb-2">
						<input type="file" name="thumbnail" class="form-control">
						<small class="invalid-feedback"></small>
					</div>
					@endif

					<div class="form-group">
						<label class="d-block"> Slug {!! Template::required() !!} </label>
						<input type="text" name="slug" class="form-control" placeholder="Slug" value="{{ $page->slug }}" required>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Laman Custom {!! Template::required() !!} </label>
						<select class="form-control" name="is_custom_page" required>
							<option value="no"> Tidak </option>
							<option value="yes"> Ya </option>
						</select>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Publikasi {!! Template::required() !!} </label>
						<select class="form-control" name="is_published" required>
							<option value="yes"> Ya </option>
							<option value="no"> Tidak </option>
						</select>
						<small class="invalid-feedback"></small>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</div>
			</div>
		</div>

	</div>
</form>
@endsection


@section('scripts')
<script src="{{ url('vendors/tinymce/tinymce.min.js') }}"></script>
<script src="{{ url('vendors/tinymce/jquery.tinymce.min.js') }}"></script>

<script>
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			clearInvalid();
			$form.find(`[name="title"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			formData.set('content', $form.find(`[name="content"]`).val());
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('page.update', $page->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				$submitBtn.ladda('stop')
				ajaxSuccessHandling(response);
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})


		$form.find('textarea#tiny').tinymce({
			height: 400,
			menubar: false,
			plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table paste code help wordcount'
			],
			toolbar: 'undo redo | formatselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table link image media code | preview removeformat help',
			images_upload_url: `{{ url('api/uploads/image') }}`,
		});


		@if(\Setting::getValue('show_page_thumbnail_field', 'no') == 'yes')
		previewImageAfterChange({
			fieldSelector: `[name="thumbnail"]`,
			previewSelector: `.thumbnail-preview`,
			defaultSource: `{{ $page->thumbnailLink() }}`,
		})
		@endif

		$form.find(`[name="is_custom_page"]`).on('change', function(){
			let val = $(this).val();

			if(val == 'no') {
				$('#content-area').show();
				$('#custom-page-area').html('')
			} else {
				$('#content-area').hide();
				$('#custom-page-area').html($('#customPageAreaTemplate').text())
			}
		})

		$form.find(`[name="is_published"]`).val(`{{ $page->is_published }}`).trigger('change')
		$form.find(`[name="is_custom_page"]`).val(`{{ $page->is_custom_page }}`).trigger('change')

		const initCustomPageFile = (value = '') => {
			$form.find(`[name="custom_page_file"]`).select2({
				'placeholder': '- Pilih File -',
			})
			$form.find(`[name="custom_page_file"]`).val(value).trigger('change')
		}
		
		if($form.find(`[name="is_custom_page"]`).val() == 'yes') {
			let interval = setInterval(() => {
				if($form.find(`[name="custom_page_file"]`).length == 1) {
					initCustomPageFile(`{{ $page->custom_page_file }}`)
					clearInterval(interval)
				}
			}, 100)
		}

		resetForm();

	})
</script>

<script type="text/html" id="customPageAreaTemplate">
	<label> File View Laman Custom {!! Template::required() !!} </label>
	<select name="custom_page_file" class="form-control" required>
		@foreach(\App\MyClass\Helper::getCustomPagesAvailable() as $page)
		<option value="{{ $page->filename }}"> {{ $page->aliasname }} ({{ $page->filename }}) </option>
		@endforeach
	</select>
	<p class="mt-2">
		* File di simpan di dalam folder pages di dalam folder tema
	</p>
</script>
@endsection