@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> No KTP {!! Template::required() !!} </label>
								<input type="text" name="no_ktp" class="form-control" placeholder="No KTP" value="{{ $tenagaTerampil->no_ktp }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama (Tanpa Gelar) {!! Template::required() !!} </label>
								<input type="text" name="nama" class="form-control" placeholder="Nama (Tanpa Gelar)" value="{{ $tenagaTerampil->nama }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Lahir {!! Template::required() !!} </label>
								<input type="date" name="tgl_lahir" class="form-control" value="{{ $tenagaTerampil->tgl_lahir }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tempat Lahir {!! Template::required() !!} </label>
								<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="{{ $tenagaTerampil->tempat_lahir }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Jenis Kelamin {!! Template::required() !!} </label>
								<select class="form-control" name="jenis_kelamin" required>
									<option value="" disabled selected> - Pilih Jenis Kelamin -</option>
									<option value="PRIA"> Pria </option>
									<option value="WANITA"> Wanita </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Alamat {!! Template::required() !!} </label>
								<textarea name="alamat" class="form-control" placeholder="Alamat" required>{{ $tenagaTerampil->alamat }}</textarea>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Kab/Kota {!! Template::required() !!} </label>
								<input type="text" name="kota" class="form-control" placeholder="Kab/Kota" value="{{ $tenagaTerampil->kota }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Provinsi {!! Template::required() !!} </label>
								<input type="text" name="provinsi" class="form-control" placeholder="Provinsi" value="{{ $tenagaTerampil->provinsi }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Email </label>
								<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $tenagaTerampil->email }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> No HP {!! Template::required() !!} </label>
								<input type="text" name="no_hp" class="form-control" placeholder="No HP" value="{{ $tenagaTerampil->no_hp }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Pendidikan (Tingkat dan Bidang Jurusan) {!! Template::required() !!} </label>
								<input type="text" name="pendidikan" class="form-control" placeholder="Pendidikan (Tingkat dan Bidang Jurusan)" value="{{ $tenagaTerampil->pendidikan }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Klasifikasi Keterampilan Tenaga Kerja Kontruksi {!! Template::required() !!} </label>
								<input type="text" name="klasifikasi" class="form-control" placeholder="Klasifikasi Keterampilan Tenaga Kerja Kontruksi" value="{{ $tenagaTerampil->klasifikasi }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Dinas/Asosiasi/Perusahaan {!! Template::required() !!} </label>
								<input type="text" name="instansi" class="form-control" placeholder="Dinas/Asosiasi/Perusahaan" value="{{ $tenagaTerampil->instansi }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Nomor Sertifikat {!! Template::required() !!} </label>
								<input type="text" name="no_sertifikat" class="form-control" placeholder="Nomor Sertifikat" value="{{ $tenagaTerampil->no_sertifikat }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Awal Berlaku Sertifikat {!! Template::required() !!} </label>
								<input type="date" name="tgl_awal_berlaku_sertifikat" class="form-control" value="{{ $tenagaTerampil->tgl_awal_berlaku_sertifikat }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Akhir Berlaku Sertifikat {!! Template::required() !!} </label>
								<input type="date" name="tgl_akhir_berlaku_sertifikat" class="form-control" value="{{ $tenagaTerampil->tgl_akhir_berlaku_sertifikat }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>
							
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="no_ktp"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('tenaga_terampil.update', $tenagaTerampil->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		$form.find(`[name="jenis_kelamin"]`).val(`{{ $tenagaTerampil->jenis_kelamin }}`)


	})

</script>
@endsection