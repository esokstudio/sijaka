@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				<div class="table-responsive mt-3">
						
					<table class="table table-hover">
						<tr>
							<th> No KTP </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->no_ktp }} </td>
							<th> Nama </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->nama }} </td>
						</tr>

						<tr>
							<th> Tanggal Lahir </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->tgl_lahir }} </td>
							<th> Tempat Lahir </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->tempat_lahir }} </td>
						</tr>

						<tr>
							<th> Jenis Kelamin </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->jenis_kelamin }} </td>
							<th> Alamat </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->alamat }} </td>
						</tr>

						<tr>
							<th> Kab/Kota </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->kota }} </td>
							<th> Provinsi </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->provinsi }} </td>
						</tr>

						<tr>
							<th> Email </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->email }} </td>
							<th> No HP </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->no_hp }} </td>
						</tr>

						<tr>
							<th> Pendidikan </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->pendidikan }} </td>
							<th> Klasifikasi </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->klasifikasi }} </td>
						</tr>

						<tr>
							<th> Dinas/Asosiasi/Perusahaan </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->instansi }} </td>
							<th> Nomor Sertifikat </th>
							<td> :  </td>
							<td> {{ $tenagaTerampil->no_sertifikat }} </td>
						</tr>
						<tr>
							<th> Tanggal Awal Berlaku Sertifikat </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->tgl_awal_berlaku_sertifikat }} </td>
							<th> Tanggal Akhir Berlaku Sertifikat </th>
							<td> : </td>
							<td> {{ $tenagaTerampil->tgl_akhir_berlaku_sertifikat }} </td>
						</tr>
						<tr>
							<th> Status Sertifikat </th>
							<td> : </td>
							<td> {!! $tenagaTerampil->statusSertifikatHtml() !!} </td>
							<th> </th>
							<td> </td>
							<td> </td>
						</tr>
					</table>

				</div>

			</div>
		</div>
	</div>
</div>
@endsection