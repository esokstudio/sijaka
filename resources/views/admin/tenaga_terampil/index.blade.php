@extends('admin.layouts.templates')


@section('action')
<button class="btn btn-success" id="import-btn">
	<i class="mdi mdi-upload"></i> Import dari Excel
</button>
<button class="btn btn-success" id="export-btn">
	<i class="mdi mdi-download"></i> Export ke Excel
</button>
<button class="btn btn-primary" id="filter-btn">
	<i class="mdi mdi-filter"></i> Filter
</button>
<a class="btn btn-success" href="{{ route('tenaga_terampil.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> No KTP </th>
								<th> Nama </th>
								<th> Kab/Kota </th>
								<th> Provinsi </th>
								<th> Klasifikasi </th>
								<th> Dinas/Asosiasi/Perusahaan </th>
								<th> Status Sertifikat </th>
								<th> Dibuat Pada </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="filter-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="filter-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Filter </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Kota </label>
						<select class="form-control" style="width: 100%;" name="kota">
							<option value="all"> - Semua Kota - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('kota') as $kota)
							<option value="{{ $kota->kota }}"> {{ $kota->kota }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Provinsi </label>
						<select class="form-control" style="width: 100%;" name="provinsi">
							<option value="all"> - Semua Provinsi - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('provinsi') as $provinsi)
							<option value="{{ $provinsi->provinsi }}"> {{ $provinsi->provinsi }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Klasifikasi </label>
						<select class="form-control" style="width: 100%;" name="klasifikasi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('klasifikasi') as $klasifikasi)
							<option value="{{ $klasifikasi->klasifikasi }}"> {{ $klasifikasi->klasifikasi }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Dinas/Asosiasi/Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="instansi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('instansi') as $instansi)
							<option value="{{ $instansi->instansi }}"> {{ $instansi->instansi }} </option>
							@endforeach
						</select>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Awal (Tgl Dibuat) </label>
								<input type="date" name="start_date" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Akhir (Tgl Dibuat) </label>
								<input type="date" name="end_date" class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label> Status Sertifikat </label>
						<select class="form-control" style="width: 100%;" name="status_sertifikat">
							<option value="all"> - Semua - </option>
							<option value="Aktif"> Aktif </option>
							<option value="Tidak Aktif"> Tidak Aktif </option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-filter"></i> Filter
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="import-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="import-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Import Dari Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					{!! Template::requiredBanner() !!}
					<p> 
						Silahkan download template pengisian dengan klik <a href="{{ url('docs/Template_Import_Tenaga_Ahli_Terampil.xlsx') }}" download> disini </a> <br>
						Catatan :
						<ul>
							<li> Import wajib menggunakan template yang sudah disediakan </li>
							<li> Kolom berwarna merah wajib diisi </li>
							<li> Contoh pengisian bisa <a href="{{ url('docs/Sample_Template_Import_Tenaga_Ahli_Terampil.xlsx') }}" download> lihat disini </a> </li>
						</ul>
					</p>
					<div class="form-group">
						<label> File {!! Template::required() !!} </label>
						<input type="file" name="file" class="form-control" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-upload"></i> Upload
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>


<div class="modal fade" id="export-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="export-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Export ke Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Kota </label>
						<select class="form-control" style="width: 100%;" name="kota">
							<option value="all"> - Semua Kota - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('kota') as $kota)
							<option value="{{ $kota->kota }}"> {{ $kota->kota }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Provinsi </label>
						<select class="form-control" style="width: 100%;" name="provinsi">
							<option value="all"> - Semua Provinsi - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('provinsi') as $provinsi)
							<option value="{{ $provinsi->provinsi }}"> {{ $provinsi->provinsi }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Klasifikasi </label>
						<select class="form-control" style="width: 100%;" name="klasifikasi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('klasifikasi') as $klasifikasi)
							<option value="{{ $klasifikasi->klasifikasi }}"> {{ $klasifikasi->klasifikasi }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Dinas/Asosiasi/Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="instansi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TenagaTerampil::grouping('instansi') as $instansi)
							<option value="{{ $instansi->instansi }}"> {{ $instansi->instansi }} </option>
							@endforeach
						</select>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Awal (Tgl Dibuat) </label>
								<input type="date" name="start_date" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tanggal Akhir (Tgl Dibuat) </label>
								<input type="date" name="end_date" class="form-control">
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-download"></i> Export
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('tenaga_terampil') }}"
			},
			columns : [
				{
					data : 'no_ktp',
					name : 'no_ktp'
				},
				{
					data : 'nama',
					name : 'nama'
				},
				{
					data : 'kota',
					name : 'kota'
				},
				{
					data : 'provinsi',
					name : 'provinsi'
				},
				{
					data : 'klasifikasi',
					name : 'klasifikasi'
				},
				{
					data : 'instansi',
					name : 'instansi'
				},
				{
					data : 'status_sertifikat',
					name : 'status_sertifikat'
				},
				{
					data : 'created_at',
					name : 'created_at'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			order: [[ '6', 'desc' ]],
			drawCallback : settings => {
				renderedEvent();
			},
			preDrawCallback: settings => {
				let $filter = $('#filter-form');
				let query = '';

				if($filter.find(`[name="kota"]`).val() != 'all') query += `&kota=${$filter.find(`[name="kota"]`).val()}`
				if($filter.find(`[name="provinsi"]`).val() != 'all') query += `&provinsi=${$filter.find(`[name="provinsi"]`).val()}`
				if($filter.find(`[name="klasifikasi"]`).val() != 'all') query += `&klasifikasi=${$filter.find(`[name="klasifikasi"]`).val()}`
				if($filter.find(`[name="instansi"]`).val() != 'all') query += `&instansi=${$filter.find(`[name="instansi"]`).val()}`
				if($filter.find(`[name="start_date"]`).val() != null) query += `&start_date=${$filter.find(`[name="start_date"]`).val()}`
				if($filter.find(`[name="end_date"]`).val() != null) query += `&end_date=${$filter.find(`[name="end_date"]`).val()}`
				if($filter.find(`[name="status_sertifikat"]`).val() != null) query += `&status_sertifikat=${$filter.find(`[name="status_sertifikat"]`).val()}`

				if(query.length > 0) query = query.substr(1);

				settings.ajax.url = `{{ route('tenaga_terampil') }}?${query}`
			},
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}


		const $importModal = $('#import-modal');
		const $importForm = $('#import-form');
		const $importSubmitBtn = $importForm.find(`[type="submit"]`).ladda()

		$('#import-btn').on('click', function(){
			$('#import-modal').modal('show')
		})

		$('#import-form').on('submit', function(e){
			e.preventDefault();

			$importSubmitBtn.ladda('start')
			let formData = new FormData(this)

			ajaxSetup()
			$.ajax({
				url: `{{ route('tenaga_terampil.import') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false
			})
			.done(response => {
				$importModal.modal('hide')
				$importSubmitBtn.ladda('stop')
				ajaxSuccessHandling(response)
				$importForm[0].reset()
				dtReload()
			})
			.fail(error => {
				$importSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $importForm)
			})
		})



		const $filterModal = $('#filter-modal');
		const $filterForm = $('#filter-form');
		const $filterSubmitBtn = $filterForm.find(`[type="submit"]`).ladda()

		$('#filter-btn').on('click', function(){
			$('#filter-modal').modal('show')
		})

		$('#filter-form').on('submit', function(e){
			e.preventDefault();

			$filterModal.modal('hide')
			dtReload()
		})

		$filterForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

		const $exportModal = $('#export-modal');
		const $exportForm = $('#export-form');
		const $exportSubmitBtn = $exportForm.find(`[type="submit"]`).ladda()

		$('#export-btn').on('click', function(){
			$('#export-modal').modal('show')
		})

		$exportForm.on('submit', function(e){
			e.preventDefault();

			$exportSubmitBtn.ladda('start')
			let formData = $(this).serialize()

			ajaxSetup()
			$.ajax({
				url: `{{ route('tenaga_terampil.export') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$exportSubmitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$exportSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $exportForm)
			})
		})

		$exportForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

	})

</script>
@endsection