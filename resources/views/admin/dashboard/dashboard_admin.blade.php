<div class="row">
	@if(($count = SijakdaHelper::permintaanPerubahanCount()) > 0)
	<div class="col-lg-12">
		<div class="alert alert-info border-0">
			Sejumlah {{ $count }} permintaan perubahan data penyedia jasa/perusahaan belum di verifikasi. Silahkan klik <a href="{{ route('permintaan_perubahan') }}"> disini. </a>
		</div>
	</div>
	@endif

	<div class="col-md-3 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Jumlah Perusahaan </h5>
					<i class="mdi mdi-office-building"></i>
				</div>
				<div class="d-flex justify-content-between">
					<div class="h2">
						{{ App\Models\Perusahaan::count() }}
					</div>
					<p class="text-muted"></p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-3 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Jumlah Instansi/SKPD </h5>
					<i class="icon-people"></i>
				</div>
				<div class="d-flex justify-content-between">
					<div class="h2">
						{{ \App\Models\Instansi::count() }}
					</div>
					<p class="text-muted"></p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-3 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Jumlah Paket Pekerjaan ({{ date('Y') }}) </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<div class="h2">
						{{ \App\Models\PaketPekerjaan::where('tahun_anggaran', date('Y'))->count() }}
					</div>
					<p class="text-muted"></p>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-3 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Jumlah Paket Pekerjaan Telah Dinilai ({{ date('Y') }}) </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<div class="h2">
						{{ \App\Models\PaketPekerjaan::where('tahun_anggaran', date('Y'))->has('penilaian')->count() }}
					</div>
					<p class="text-muted"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Statistik Pengunjung </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<canvas id="statisticChart"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Grafik Jumlah Paket Pekerjaan Jasa Kontruksi </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<canvas id="paket-pekerjaan-chart" style="min-height: 500px;"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Grafik Pagu Anggaran Jasa Kontruksi </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<canvas id="pagu-anggaran-chart" style="min-height: 500px;"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Realisasi Anggaran Jasa Kontruksi </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<canvas id="realisasi-anggaran-chart" style="min-height: 500px;"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="d-flex justify-content-between pb-2">
					<h5> Tenaga Ahli Jasa Kontruksi </h5>
					<i class="mdi mdi-chart-line"></i>
				</div>
				<div class="d-flex justify-content-between">
					<canvas id="tenaga-ahli-chart" style="min-height: 500px;"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>