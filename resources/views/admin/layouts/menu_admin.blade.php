<li class="nav-item nav-title">
	<span> SIJAKDA </span>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#master-data-dropdown" aria-expanded="false" aria-controls="master-data-dropdown">
		<i class="menu-icon mdi mdi-database mdi-24px"></i>
		<span class="menu-title"> Master Data </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="master-data-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('kategori_peraturan') }}"> Kategori Peraturan </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('tahun_anggaran') }}"> Tahun Anggaran </a>
			</li>
			<!-- <li class="nav-item">
				<a class="nav-link" href="{{ route('sumber_dana') }}"> Sumber Dana </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('jenis_keahlian') }}"> Jenis Keahlian </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('tingkat_keahlian') }}"> Tingkat Keahlian </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('klasifikasi') }}"> Klasifikasi </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('sub_klasifikasi') }}"> Sub Klasifikasi </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('metode_pelatihan') }}"> Metode Pelatihan </a>
			</li> -->
			<li class="nav-item">
				<a class="nav-link" href="{{ route('asosiasi') }}"> Asosiasi </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('instansi') }}"> Instansi </a>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('peraturan') }}">
	<i class="menu-icon mdi mdi-book-multiple mdi-24px"></i>
	<span class="menu-title"> Peraturan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('kecelakaan') }}">
	<i class="menu-icon mdi mdi-alert-box-outline mdi-24px"></i>
	<span class="menu-title"> Kecelakaan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#pelatihan-data-dropdown" aria-expanded="false" aria-controls="pelatihan-data-dropdown">
		<i class="menu-icon mdi mdi-star mdi-24px"></i>
		<span class="menu-title"> Pelatihan </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="pelatihan-data-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('tenaga_terampil') }}"> Tenaga Terampil </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('tenaga_ahli') }}"> Tenaga Ahli </a>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('permohonan_sertifikasi') }}">
		<i class="menu-icon mdi mdi-account-group mdi-24px"></i>
		<span class="menu-title"> Permohonan Sertifikasi </span>
		@if(($count = \App\Models\PermohonanSertifikasi::where('status', 'Menunggu')->count()) > 0)
		<span class="badge badge-warning ml-2"> {{ $count }} </span>
		</span>
		@endif
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('perusahaan') }}">
	<i class="menu-icon mdi mdi-office-building mdi-24px"></i>
	<span class="menu-title"> Perusahaan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('permintaan_perubahan') }}">
		<i class="menu-icon mdi mdi-update mdi-24px"></i>
		<span class="menu-title"> Permintaan Perubahan
		@if(($count = SijakdaHelper::permintaanPerubahanCount()) > 0)
		<span class="badge badge-warning ml-2"> {{ $count }} </span>
		</span>
		@endif
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('paket_pekerjaan') }}">
	<i class="menu-icon mdi mdi-account-tie"></i>
	<span class="menu-title"> Paket Pekerjaan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('laporan_fisik_keuangan') }}">
	<i class="menu-icon mdi mdi-file-chart mdi-24px"></i>
	<span class="menu-title"> Laporan Fisik Keuangan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#penilaian-data-dropdown" aria-expanded="false" aria-controls="penilaian-data-dropdown">
		<i class="menu-icon mdi mdi-star mdi-24px"></i>
		<span class="menu-title"> Penilaian Paket Pekerjaan </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="penilaian-data-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('skema_penilaian') }}"> Skema Penilaian </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('penilaian') }}"> Penilaian </a>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('rantai_pasok') }}">
	<i class="menu-icon mdi mdi-dolly mdi-24px"></i>
	<span class="menu-title"> Rantai Pasok </span>
	</a>
</li>


<!-- <li class="nav-item">
	<a class="nav-link" href="#">
	<i class="menu-icon mdi mdi-file-multiple mdi-24px"></i>
	<span class="menu-title"> Berita Acara </span>
	</a>
</li> -->

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#user-dropdown" aria-expanded="false" aria-controls="user-dropdown">
		<i class="menu-icon mdi mdi-account mdi-24px"></i>
		<span class="menu-title"> User </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="user-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('user.pengguna') }}"> Pengguna </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('user.penyedia_jasa') }}"> Penyedia Jasa </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('user.penilai') }}"> Penilai Paket Pekerjaan </a>
			</li>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item nav-title">
	<span> CMS </span>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#postingan-dropdown" aria-expanded="false" aria-controls="postingan-dropdown">
		<i class="menu-icon icon-paper-plane"></i>
		<span class="menu-title"> Postingan </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="postingan-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('tag') }}"> Tag </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('post_category') }}"> Kategori </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('post') }}"> Postingan </a>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('page') }}">
		<i class="menu-icon icon-globe"></i>
		<span class="menu-title"> Laman </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#navigasi-dropdown" aria-expanded="false" aria-controls="navigasi-dropdown">
		<i class="menu-icon icon-list"></i>
		<span class="menu-title"> Navigasi </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="navigasi-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('nav_menu') }}"> Menu </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('link_group') }}"> Link </a>
			</li>
		</ul>
	</div>
</li>

@if(\Setting::getValue('image_slider_available', 'yes') == 'yes')
<li class="nav-item">
	<a class="nav-link" href="{{ route('image_slider') }}">
		<i class="menu-icon icon-picture"></i>
		<span class="menu-title"> Slide Foto </span>
	</a>
</li>
@endif