<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="_token" content="{{ csrf_token() }}">

		<title> SIJAKDA | {{ $title ?? 'Judul' }} </title>
		<!-- plugins:css -->
		<link rel="stylesheet" href="{{ url('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/css/vendor.bundle.base.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/css/vendor.bundle.addons.css') }}">

		<link rel="stylesheet" href="{{ url('vendors/pace/green/pace-theme-flash.css') }}">

		<link rel="stylesheet" href="{{ url('vendors/swal2/sweetalert.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/toastr/toastr.min.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/ladda/ladda-themeless.min.css') }}">
		<link rel="stylesheet" href="{{ url('vendors/jquery-confirm/jquery-confirm.css') }}">
		<!-- endinject -->
		<!-- plugin css for this page -->
		<!-- End plugin css for this page -->
		<!-- inject:css -->
		<link rel="stylesheet" href="{{ url('css/style.css') }}">
		<!-- endinject -->
		<!-- <link rel="shortcut icon" href="{{ url('images/favicon.png') }}" /> -->

		<style type="text/css">
			
			.nav-item .submenu-arrow-icon::before,
			.nav-item .nav-link[data-toggle="collapse"][aria-expanded="false"] .submenu-arrow-icon::before {
				transition: 0.25s;
			}

			.nav-item.active .submenu-arrow-icon::before,
			.nav-item .nav-link[data-toggle="collapse"][aria-expanded="true"] .submenu-arrow-icon::before {
				transform: rotate(-90deg);
			}

			.table td, .jsgrid .jsgrid-table td, .table th, .jsgrid .jsgrid-table th {
				white-space: unset !important;
				padding: 15px;
			}

			table.dataTable {
				border-collapse: collapse !important;
			}

			.select2-container .select2-search--inline .select2-search__field {
				padding: 0 10px;
			}

			.select2-container .select2-selection--single .select2-selection__rendered {
				padding-left: 0px;
			}

			.nav-item.nav-title {
				margin-top: 20px;
				margin-bottom: 10px;
				text-align: center;
			}

			.nav-item.nav-title span {
				padding: 16px 35px;
				color: white;
				font-weight: bolder;
			}

			.sidebar-icon-only .nav-item.nav-title span {
				padding: 16px;
			}

			.dataTables_wrapper .dataTable thead .sorting:after, .dataTables_wrapper .dataTable thead .sorting_asc:after, .dataTables_wrapper .dataTable thead .sorting_asc_disabled:after, .dataTables_wrapper .dataTable thead .sorting_desc:after, .dataTables_wrapper .dataTable thead .sorting_desc_disabled:after {
				content: "\F0045";
				right: 0.2em;
			}

			.dataTables_wrapper .dataTable thead .sorting:before, .dataTables_wrapper .dataTable thead .sorting_asc:before, .dataTables_wrapper .dataTable thead .sorting_asc_disabled:before, .dataTables_wrapper .dataTable thead .sorting_desc:before, .dataTables_wrapper .dataTable thead .sorting_desc_disabled:before {
				content: "\F005D";
				right: 1.2em;
			}


			:root {
				--bg-active-color: #1a5441;
				--bg-normal-color: #318a6d;
				--text-normal-color: #ffffff;
			}


			.navbar.default-layout .navbar-brand-wrapper {
				background: var(--bg-normal-color);
			}


			.sidebar {
				background: var(--bg-normal-color);
			}

			.sidebar .nav .nav-item .nav-link[aria-expanded="true"] {
				background: var(--bg-active-color);
			}


			.sidebar .nav:not(.sub-menu) > .nav-item:hover:not(.nav-profile) > .nav-link {
				background: var(--bg-active-color);
				color: var(--text-normal-color);
			}

			.sidebar .nav .nav-item .collapse.show, .sidebar .nav .nav-item .collapsing {
				background: var(--bg-active-color);
			}

			.sidebar .nav .nav-item .nav-link,
			.sidebar .nav .nav-item .nav-link .menu-icon,
			.sidebar .nav .nav-item .nav-link:hover,
			.sidebar .nav:not(.sub-menu) > .nav-item:hover:not(.nav-profile) > .nav-link .menu-icon,
			.sidebar .nav .nav-item.active > .nav-link,
			.sidebar .nav .nav-item.active > .nav-link .menu-icon,
			.sidebar .nav.sub-menu .nav-item .nav-link,
			.nav-item.nav-title span {
				color: var(--text-normal-color);
			}

		</style>

		@yield('styles')
	</head>
	<body>
		<div class="container-scroller">
			<!-- partial:partials/_navbar.html -->
			<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
				<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
					<a class="navbar-brand brand-logo" href="{{ route('dashboard') }}">
						<img src="{{ url('images/sijakda.png') }}" alt="logo" style="height: 50px;" />
					</a>
					<a class="navbar-brand brand-logo-mini" href="{{ route('dashboard') }}">
					<!-- <img src="{{ url('images/logo-mini.svg') }}" alt="logo" /> -->
					</a>
				</div>
				<div class="navbar-menu-wrapper d-flex align-items-center">
					<button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
					<span class="mdi mdi-menu"></span>
					</button>
					<ul class="navbar-nav navbar-nav-right">
						<li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
							<a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
								<div class="dropdown-toggle-wrapper">
									<div class="inner">
										<img class="img-xs rounded-circle" src="{{ auth()->user()->avatarLink() }}" alt="Profile image">
									</div>
									<div class="inner">
										<div class="inner">
											<span class="profile-text font-weight-bold"> {{ auth()->user()->name }} </span>
											<small class="profile-text small"> {{ auth()->user()->roleText() }} </small>
										</div>
										<div class="inner">
											<div class="icon-wrapper">
												<i class="mdi mdi-chevron-down"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
							<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
								<a class="dropdown-item mt-2" href="{{ route('setting.profile') }}">
									Atur Profil
								</a>
								<a class="dropdown-item" href="{{ route('setting.change_password') }}">
									Ganti Password
								</a>
								<a class="dropdown-item" onclick="$('#logoutForm').submit()" href="javascript:void(0);">
									Log Out
								</a>
							</div>
						</li>
						<!-- <li class="nav-item dropdown">
							<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
							<i class="mdi mdi-bell-outline"></i>
							<span class="count">4</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
								<a class="dropdown-item">
									<p class="mb-0 font-weight-normal float-left">You have 4 new notifications
									</p>
									<span class="badge badge-pill badge-warning float-right">View all</span>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<div class="preview-icon bg-success">
											<i class="mdi mdi-alert-circle-outline mx-0"></i>
										</div>
									</div>
									<div class="preview-item-content">
										<h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>
										<p class="font-weight-light small-text">
											Just now
										</p>
									</div>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<div class="preview-icon bg-warning">
											<i class="mdi mdi-comment-text-outline mx-0"></i>
										</div>
									</div>
									<div class="preview-item-content">
										<h6 class="preview-subject font-weight-medium text-dark">Settings</h6>
										<p class="font-weight-light small-text">
											Private message
										</p>
									</div>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<div class="preview-icon bg-info">
											<i class="mdi mdi-email-outline mx-0"></i>
										</div>
									</div>
									<div class="preview-item-content">
										<h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>
										<p class="font-weight-light small-text">
											2 days ago
										</p>
									</div>
								</a>
							</div>
						</li> -->
						<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle count-indicator" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
							<i class="mdi mdi-email-outline"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
								<div class="dropdown-item">
									<p class="mb-0 font-weight-normal float-left">You have 7 unread mails
									</p>
									<span class="badge badge-info badge-pill float-right">View all</span>
								</div>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<img src="{{ url('images/faces/face4.jpg') }}" alt="image" class="profile-pic">
									</div>
									<div class="preview-item-content flex-grow">
										<h6 class="preview-subject ellipsis font-weight-medium text-dark">David Grey
											<span class="float-right font-weight-light small-text">1 Minutes ago</span>
										</h6>
										<p class="font-weight-light small-text">
											The meeting is cancelled
										</p>
									</div>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<img src="{{ url('images/faces/face2.jpg') }}" alt="image" class="profile-pic">
									</div>
									<div class="preview-item-content flex-grow">
										<h6 class="preview-subject ellipsis font-weight-medium text-dark">Tim Cook
											<span class="float-right font-weight-light small-text">15 Minutes ago</span>
										</h6>
										<p class="font-weight-light small-text">
											New product launch
										</p>
									</div>
								</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item preview-item">
									<div class="preview-thumbnail">
										<img src="{{ url('images/faces/face3.jpg') }}" alt="image" class="profile-pic">
									</div>
									<div class="preview-item-content flex-grow">
										<h6 class="preview-subject ellipsis font-weight-medium text-dark"> Johnson
											<span class="float-right font-weight-light small-text">18 Minutes ago</span>
										</h6>
										<p class="font-weight-light small-text">
											Upcoming board meeting
										</p>
									</div>
								</a>
							</div>
						</li>
						<li class="nav-item dropdown color-setting d-none d-md-block">
							<a class="nav-link count-indicator" href="#">
							<i class="mdi mdi-invert-colors"></i>
							</a>
						</li> -->
					</ul>
					<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
					<span class="mdi mdi-menu"></span>
					</button>
				</div>
			</nav>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- partial:partials/_settings-panel.html -->
				<div class="theme-setting-wrapper">
					<div id="theme-settings" class="settings-panel">
						<i class="settings-close mdi mdi-close"></i>
						<div class="d-flex align-items-center justify-content-between border-bottom">
							<p class="settings-heading font-weight-bold border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Template Skins</p>
						</div>
						<div class="sidebar-bg-options" id="sidebar-light-theme">
							<div class="img-ss rounded-circle bg-light border mr-3"></div>
							Light
						</div>
						<div class="sidebar-bg-options selected" id="sidebar-dark-theme">
							<div class="img-ss rounded-circle bg-dark border mr-3"></div>
							Dark
						</div>
						<p class="settings-heading font-weight-bold mt-2">Header Skins</p>
						<div class="color-tiles mx-0 px-4">
							<div class="tiles primary"></div>
							<div class="tiles success"></div>
							<div class="tiles warning"></div>
							<div class="tiles danger"></div>
							<div class="tiles pink"></div>
							<div class="tiles info"></div>
							<div class="tiles dark"></div>
							<div class="tiles default"></div>
						</div>
					</div>
				</div>
				<!-- partial -->
				<!-- partial:partials/_sidebar.html -->
				<nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
					<ul class="nav">
						<li class="nav-item nav-profile">
							<img src="{{ auth()->user()->avatarLink() }}" alt="profile image">
							<p class="text-center font-weight-medium"> {{ auth()->user()->name }} </p>
						</li>
						
						@include('admin.layouts.menu')

					</ul>
				</nav>
				<!-- partial -->
				<div class="main-panel">
					<div class="content-wrapper">
						<div class="row mb-4">
							<div class="col-12 d-flex align-items-center justify-content-between">
								<h4 class="page-title"> {{ $title }} </h4>
								<div class="d-flex align-self-start">
									<div class="wrapper">
										@yield('action')
									</div>
								</div>
							</div>


							<?php 
								$bread = $breadcrumbs ?? [];
								$breadcrumbs = [];
								$breadcrumbs[] = [
									'title'		=> 'Dashboard',
									'link'		=> url('dashboard')
								];
								$breadcrumbs = array_merge($breadcrumbs, $bread);
							?>

							<div class="col-12">
								<nav aria-label="breadcrumb">
									<ol class="breadcrumb border-0 px-0">

										@foreach($breadcrumbs as $breadcrumb)

										@if($loop->iteration == count($breadcrumbs))
										<li class="breadcrumb-item active" aria-current="page">
											{{ $breadcrumb['title'] }}
										</li>
										@else
										<li class="breadcrumb-item">
											<a href="{{ $breadcrumb['link'] }}"> {{ $breadcrumb['title'] }} </a>
										</li>
										@endif

										@endforeach

									</ol>
								</nav>
							</div>
						</div>
						

						@yield('content')

					</div>
					<!-- content-wrapper ends -->
					<!-- partial:partials/_footer.html -->
					<footer class="footer">
						<div class="container-fluid clearfix">
							<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; 2022 |
							<a href="https://esokstudio.com" target="_blank">Esok Studio</a>. All rights reserved.</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
								Version {{ Setting::getValue('app_version') }} | 
								Hand-crafted & made with
								<i class="mdi mdi-heart-outline text-danger"></i>
							</span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		

		<form id="logoutForm" method="post" action="{{ route('logout') }}">
			@csrf
		</form>

		@yield('modal')

		<script src="{{ url('vendors/js/vendor.bundle.base.js') }}"></script>
		<script src="{{ url('vendors/js/vendor.bundle.addons.js') }}"></script>

		<script src="{{ url('vendors/pace/pace.min.js') }}"></script>
		<script src="{{ url('vendors/swal2/sweetalert.min.js') }}"></script>
		<script src="{{ url('vendors/toastr/toastr.min.js') }}"></script>
		<script src="{{ url('vendors/ladda/spin.min.js') }}"></script>
		<script src="{{ url('vendors/ladda/ladda.min.js') }}"></script>
		<script src="{{ url('vendors/ladda/ladda.jquery.min.js') }}"></script>
		<script src="{{ url('vendors/jquery-confirm/jquery-confirm.js') }}"></script>
		<!-- endinject -->
		<!-- Plugin js for this page-->
		<!-- End plugin js for this page-->
		<!-- inject:js -->
		<script src="{{ url('js/off-canvas.js') }}"></script>
		<script src="{{ url('js/hoverable-collapse.js') }}"></script>
		<script src="{{ url('js/misc.js') }}"></script>
		<script src="{{ url('js/settings.js') }}"></script>
		<script src="{{ url('js/todolist.js') }}"></script>
		<script src="{{ url('js/myJs.js') }}"></script>

		<script type="text/javascript">

			$(function(){
				const setActiveMenu = () => {
					$('#sidebar').find('.active').removeClass('active')
					$('#sidebar').find(`[data-toggle="collapse"]`).removeClass('collapsed').attr('aria-expanded', 'false')
					$('#sidebar').find(`[data-toggle="collapse"]`).siblings('.collapse').removeClass('show')

					let isFoundLink = false;
					let path = [];
					window.location.pathname.split("/").forEach(item => {
						if(item !== "") path.push(item);
					})
					let lengthPath = path.length;
					let lengthUse = lengthPath;
					let origin = window.location.origin;

					while(lengthUse >= 1) {
						let link = '';
						for (let i = 0; i < lengthUse; i++) {
							link += `/${path[i]}`;
						}
						$.each($('#sidebar').find('a'), (i, elem) => {
							if($(elem).attr('href') == `${origin}${link}`) {
								$(elem).addClass('active')
								$(elem).parent('li').addClass('active')
								$(elem).parents('li.nav-item').addClass('active').addClass('submenu')
								$(elem).parents('li.nav-item').find(`.collapse`).addClass('show')
							}
						})

						if(isFoundLink) break;
						lengthUse--;
					}
				}

				setActiveMenu();

			})
			

		</script>

		@yield('scripts')
	</body>
</html>