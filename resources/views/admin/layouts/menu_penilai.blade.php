<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#penilaian-data-dropdown" aria-expanded="false" aria-controls="penilaian-data-dropdown">
		<i class="menu-icon mdi mdi-star mdi-24px"></i>
		<span class="menu-title"> Penilaian Paket Pekerjaan </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="penilaian-data-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('skema_penilaian') }}"> Skema Penilaian </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('penilaian') }}"> Penilaian </a>
			</li>
		</ul>
	</div>
</li>