<li class="nav-item">
	<a class="nav-link" href="{{ route('dashboard') }}">
	<i class="menu-icon icon-home"></i>
	<span class="menu-title"> Dashboard </span>
	</a>
</li>

@if(auth()->user()->isAdmin())
	@include('admin.layouts.menu_admin')
@endif

@if(auth()->user()->isPerusahaan())
	@include('admin.layouts.menu_perusahaan')
@endif

@if(auth()->user()->isPengguna())
	@include('admin.layouts.menu_pengguna')
@endif

@if(auth()->user()->isPenilai())
	@include('admin.layouts.menu_penilai')
@endif

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#pengaturan-dropdown" aria-expanded="false" aria-controls="pengaturan-dropdown">
		<i class="menu-icon icon-wrench"></i>
		<span class="menu-title"> Pengaturan </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="pengaturan-dropdown">
		<ul class="nav flex-column sub-menu">

			@if(auth()->user()->isAdmin())
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.website') }}"> Situs </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.theme') }}"> Tema </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.api_key') }}"> API Key </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.email') }}"> Notifikasi Email </a>
			</li>
			@endif
			
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.profile') }}"> Atur Profil </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('setting.change_password') }}"> Ganti Password </a>
			</li>
		</ul>
	</div>
</li>