<li class="nav-item">
	<a class="nav-link" href="{{ route('p.edit_perusahaan') }}">
	<i class="menu-icon mdi mdi-office-building mdi-24px"></i>
	<span class="menu-title"> Edit Perusahaan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('p.kualifikasi') }}">
	<i class="menu-icon mdi mdi-clipboard mdi-24px"></i>
	<span class="menu-title"> Kualifikasi </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" data-toggle="collapse" href="#administrasi-dropdown" aria-expanded="false" aria-controls="administrasi-dropdown">
		<i class="menu-icon mdi mdi-file mdi-24px"></i>
		<span class="menu-title"> Administrasi </span>
		<div class="badge">
			<i class="mdi mdi-chevron-left mdi-18px submenu-arrow-icon"></i>
		</div>
	</a>
	<div class="collapse" id="administrasi-dropdown">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('p.edit_akte_pendirian') }}"> Akte Pendirian </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('p.edit_pengesahan') }}"> Pengesahan </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('p.akte_perubahan') }}"> Akte Perubahan </a>
			</li>
		</ul>
	</div>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('p.pengurus') }}">
	<i class="menu-icon mdi mdi-account-group mdi-24px"></i>
	<span class="menu-title"> Pengurus </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('p.keuangan') }}">
	<i class="menu-icon mdi mdi-cash"></i>
	<span class="menu-title"> Keuangan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('p.tenaga_kerja') }}">
	<i class="menu-icon mdi mdi-account-tie"></i>
	<span class="menu-title"> Tenaga Kerja </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('p.pengalaman') }}">
	<i class="menu-icon mdi mdi-account-tie"></i>
	<span class="menu-title"> Pengalaman </span>
	</a>
</li>