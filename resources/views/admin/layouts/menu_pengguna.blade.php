<li class="nav-item">
	<a class="nav-link" href="{{ route('paket_pekerjaan') }}">
	<i class="menu-icon mdi mdi-account-tie"></i>
	<span class="menu-title"> Paket Pekerjaan </span>
	</a>
</li>

<li class="nav-item">
	<a class="nav-link" href="{{ route('laporan_fisik_keuangan') }}">
	<i class="menu-icon mdi mdi-file-chart mdi-24px"></i>
	<span class="menu-title"> Laporan Fisik Keuangan </span>
	</a>
</li>