@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama Sub Klasifikasi {!! Template::required() !!} </label>
						<input type="text" name="nama_sub_klasifikasi" class="form-control" placeholder="Nama Sub Klasifikasi" value="{{ $subKlasifikasi->nama_sub_klasifikasi }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Klasifikasi {!! Template::required() !!} </label>
						<select name="id_klasifikasi" style="width: 100%;" required>
							@foreach(\App\Models\Klasifikasi::all() as $klasifikasi)
							<option value="{{ $klasifikasi->id }}"> {{ $klasifikasi->nama_klasifikasi }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="id_klasifikasi"]`).select2({
			'placeholder': '- Pilih Klasifikasi -'
		})
		$form.find(`[name="id_klasifikasi"]`).val(`{{ $subKlasifikasi->id_klasifikasi }}`).trigger('change')

		const resetForm = () => {
			clearInvalid();
			$form.find(`[name="nama_sub_klasifikasi"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('sub_klasifikasi.update', $subKlasifikasi->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection