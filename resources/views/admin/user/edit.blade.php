@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama User {!! Template::required() !!} </label>
						<input type="text" name="name" class="form-control" placeholder="Nama User" value="{{ $user->name }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Username {!! Template::required() !!} </label>
						<input type="text" name="username" class="form-control" placeholder="Username" value="{{ $user->username }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Password </label>
						<input type="password" name="password" class="form-control" placeholder="Isi Jika Ingin Ganti Password">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Role {!! Template::required() !!} </label>
						<select class="form-control" name="role" required>
							<option value="" selected disabled> - Pilih Role - </option>
							@foreach(\App\User::availableRolesForInput() as $value => $label)
							<option value="{{ $value }}"> {{ $label }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Email {!! Template::required() !!} </label>
						<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('user.update', $user->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				// resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		$form.find(`[name="role"]`).val(`{{ $user->role }}`)

	})

</script>
@endsection