@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('user.create') }}">
	<i class="mdi mdi-filter"></i> Filter
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama User </th>
								<th> Email </th>
								<th> Perusahaan </th>
								<!-- <th> Role </th> -->
								<th> Status Verifikasi </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('user') }}"
			},
			columns : [
				{
					data : 'name',
					name : 'name'
				},
				{
					data : 'email',
					name : 'email'
				},
				{
					data : 'perusahaan.nama_perusahaan',
					name : 'perusahaan.nama_perusahaan',
					searchable : false,
				},
				{
					data : 'status_verifikasi',
					name : 'status_verifikasi'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.approve-btn').off('click')
			$('.approve-btn').on('click', function(){
				let href = $(this).data('href')
				confirmation('Yakin ingin disetujui?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'post',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})

			$('.reject-btn').off('click')
			$('.reject-btn').on('click', function(){
				let href = $(this).data('href')
				confirmation('Yakin ingin ditolak?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'post',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection