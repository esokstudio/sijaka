@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama User {!! Template::required() !!} </label>
						<input type="text" name="name" class="form-control" placeholder="Nama User" value="{{ $user->name }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Email {!! Template::required() !!} </label>
						<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Password </label>
						<input type="password" name="password" class="form-control" placeholder="Isi Jika Ingin Ganti Password">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Instansi {!! Template::required() !!} </label>
						<select class="form-control" name="id_instansi" required>
							<option value="" selected disabled> - Pilih Instansi - </option>
							@foreach(\App\Models\Instansi::all() as $instansi)
							<option value="{{ $instansi->id }}"> {{ $instansi->nama_instansi }} </option>
							@endforeach
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="id_instansi"]`).select2({
			'placeholder': '- Pilih Instansi -'
		})

		const initForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="name"]`).focus();
			// $form.find(`[name="id_instansi"]`).val('').trigger('change')
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('user.pengguna.update', $user->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData
			})
			.done(response => {
				$submitBtn.ladda('stop');
				ajaxSuccessHandling(response);
				initForm();
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		initForm();
		$form.find(`[name="id_instansi"]`).val(`{{ $user->id_instansi }}`).trigger('change')

	})

</script>
@endsection