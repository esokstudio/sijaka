@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('user.pengguna.create') }}">
	<i class="mdi mdi-plus-thick"></i> Tambah
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama User </th>
								<th> Instansi </th>
								<th> Email </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('user.pengguna') }}"
			},
			columns : [
				{
					data : 'name',
					name : 'name'
				},
				{
					data : 'instansi.nama_instansi',
					name : 'instansi.nama_instansi'
				},
				{
					data : 'email',
					name : 'email'
				},
				{
					data : 'pengguna_action',
					name : 'pengguna_action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.approve-btn').off('click')
			$('.approve-btn').on('click', function(){
				let href = $(this).data('href')
				confirmation('Yakin ingin disetujui?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'post',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})

			$('.reject-btn').off('click')
			$('.reject-btn').on('click', function(){
				let href = $(this).data('href')
				confirmation('Yakin ingin ditolak?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'post',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})

			$('.delete-btn').off('click')
			$('.delete-btn').on('click', function(){
				let href = $(this).data('href')
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection