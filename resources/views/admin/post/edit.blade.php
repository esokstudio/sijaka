@extends('admin.layouts.templates')


@section('content')
<form id="form">
	@method('PUT')
	<div class="row">

		<div class="col-md-12">
			{!! Template::requiredBanner() !!}
		</div>
			
		<div class="col-md-9">

			<div class="form-group">
				<label> Judul {!! Template::required() !!} </label>
				<input type="text" name="title" class="form-control" placeholder="Judul" value="{{ $post->title }}" required>
				<small class="invalid-feedback"></small>
			</div>

			<textarea id="tiny" name="content"> {!! $post->content !!} </textarea>
		</div>

		<div class="col-md-3 grid-margin">
			<div class="card">
				<div class="card-body">

					<div class="form-group">
						<label class="d-block"> Thumbnail </label>
						<img src="#" style="width: 100%; height: auto; display: none;" class="thumbnail-preview mb-2">
						<input type="file" name="thumbnail" class="form-control">
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Slug {!! Template::required() !!} </label>
						<input type="text" name="slug" class="form-control" placeholder="Slug" value="{{ $post->slug }}" required>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Kategori </label>
						<select class="form-control" name="id_category">
							<option value=""> - Tanpa Kategori - </option>
							@foreach(\App\Models\PostCategory::all() as $category)
							<option value="{{ $category->id }}"> {{ $category->category_name }} </option>
							@endforeach
						</select>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Tag </label>
						<select class="form-control tags" name="tags[]" multiple>
							<option value=""> - Tanpa Tag - </option>
							@foreach(\App\Models\Tag::all() as $tag)
							<option value="{{ $tag->id }}"> {{ $tag->tag_name }} </option>
							@endforeach
						</select>
						<small class="invalid-feedback"></small>
					</div>

					<div class="form-group">
						<label class="d-block"> Publikasi {!! Template::required() !!} </label>
						<select class="form-control" name="is_published" required>
							<option value="yes"> Ya </option>
							<option value="no"> Tidak </option>
						</select>
						<small class="invalid-feedback"></small>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</div>
			</div>
		</div>

	</div>
</form>
@endsection


@section('scripts')
<script src="{{ url('vendors/tinymce/tinymce.min.js') }}"></script>
<script src="{{ url('vendors/tinymce/jquery.tinymce.min.js') }}"></script>

<script>
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			clearInvalid();
			$form.find(`[name="title"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			formData.set('content', $form.find(`[name="content"]`).val());
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('post.update', $post->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				$submitBtn.ladda('stop')
				ajaxSuccessHandling(response);
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})


		$form.find('textarea#tiny').tinymce({
			height: 400,
			menubar: false,
			plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table paste code help wordcount'
			],
			toolbar: 'undo redo | formatselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table link image media code | preview removeformat help',
			images_upload_url: `{{ url('api/uploads/image') }}`,
		});


		previewImageAfterChange({
			fieldSelector: `[name="thumbnail"]`,
			previewSelector: `.thumbnail-preview`,
			defaultSource: `{{ $post->thumbnailLink() }}`,
		})


		$form.find(`[name="id_category"]`).select2()
		$form.find(`.tags`).select2({
			'placeholder': 'Pilih Tag'
		})

		$form.find(`[name="id_category"]`).val(`{{ $post->id_category }}`).trigger('change')
		$form.find(`[name="is_published"]`).val(`{{ $post->is_published }}`)
		$form.find(`.tags`).val({{ $post->tagsToArrayAsString() }}).trigger('change')

		resetForm();

	})
</script>
@endsection