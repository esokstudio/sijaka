@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Judul {!! Template::required() !!} </label>
						<input type="text" name="title" class="form-control" placeholder="Judul" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Sumber Link {!! Template::required() !!} </label>
						<select name="source" style="width: 100%;" required>
							<option value="post"> Postingan </option>
							<option value="page"> Laman </option>
							<option value="web"> Website Ini </option>
							<option value="url"> URL </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<div id="addedInput"></div>

					<div class="form-group">
						<label> Buka Di Tab Baru {!! Template::required() !!} </label>
						<select name="is_open_new_tab" class="form-control" required>
							<option value="no"> Tidak </option>
							<option value="yes"> Ya </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="source"]`).select2({
			'placeholder': '- Pilih Sumber -'
		})

		const resetForm = () => {
			$form[0].reset()
			$form.find(`[name="source"]`).val('').trigger('change')
			clearInvalid();
			$form.find(`[name="title"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('nav_submenu.store', $navMenu->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		$form.find(`[name="source"]`).on('change', function(){
			let source = $(this).val();
			let html = '';

			if(source == 'post') {
				html = $('#postLinkTemplate').text();
			} else if(source == 'page') {
				html = $('#pageLinkTemplate').text();
			} else if(source == 'web') {
				html = $('#webLinkTemplate').text();
			} else if(source == 'url') {
				html = $('#urlLinkTemplate').text();
			}

			$('#addedInput').html(html);

			if(source == 'post') {
				$form.find(`[name="link"]`).select2({
					'placeholder': '- Pilih Postingan -'
				})
				$form.find(`[name="link"]`).val('').trigger('change')
			}

			if(source == 'page') {
				$form.find(`[name="link"]`).select2({
					'placeholder': '- Pilih Laman -'
				})
				$form.find(`[name="link"]`).val('').trigger('change')
			}
		})

		resetForm();

	})

</script>

<script type="text/html" id="postLinkTemplate">
	<div class="form-group">
		<label> Postingan {!! Template::required() !!} </label>
		<select style="width: 100%;" name="link" required>
			@foreach(\App\Models\Post::all() as $post)
			<option value="{{ $post->slug }}"> {{ $post->title }} </option>
			@endforeach
		</select>
	</div>
</script>

<script type="text/html" id="pageLinkTemplate">
	<div class="form-group">
		<label> Laman {!! Template::required() !!} </label>
		<select style="width: 100%;" name="link" required>
			@foreach(\App\Models\Page::all() as $page)
			<option value="{{ $page->slug }}"> {{ $page->title }} </option>
			@endforeach
		</select>
	</div>
</script>

<script type="text/html" id="webLinkTemplate">
	<div class="form-group">
		<label> Link </label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> {{ url('') }}/ </span>
			</div>
			<input type="text" name="link" class="form-control" placeholder="Link">
		</div>
	</div>
</script>

<script type="text/html" id="urlLinkTemplate">
	<div class="form-group">
		<label> Link {!! Template::required() !!} </label>
		<input type="text" name="link" class="form-control" value="https://" placeholder="Link" required>
	</div>
</script>
@endsection