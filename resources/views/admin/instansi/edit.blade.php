@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama Instansi {!! Template::required() !!} </label>
						<input type="text" name="nama_instansi" class="form-control" placeholder="Nama Instansi" value="{{ $instansi->nama_instansi }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Tampilkan Di Grafik </label>
						<select class="form-control" name="tampilkan_grafik" required>
							<option value="yes"> Ya </option>
							<option value="no"> Tidak </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="nama_instansi"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('instansi.update', $instansi->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		$form.find(`[name="tampilkan_grafik"]`).val(`{{ $instansi->tampilkan_grafik }}`)

		resetForm();

	})

</script>
@endsection