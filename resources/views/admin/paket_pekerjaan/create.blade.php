@extends('admin.layouts.templates')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ url('vendors/datepicker/css/datepicker.css') }}">
<style type="text/css">
	.form-control[readonly] {
		background: #ffffff;
	}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Tahun Anggaran {!! Template::required() !!} </label>
								<select name="tahun_anggaran" style="width: 100%;" required>
									@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
									<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Perusahaan {!! Template::required() !!} </label>
								<select name="id_perusahaan" style="width: 100%;" required>
									@foreach(\App\Models\Perusahaan::all() as $perusahaan)
									<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nama Pekerjaan {!! Template::required() !!} </label>
								<input type="text" name="nama_pekerjaan" class="form-control" placeholder="Nama Pekerjaan" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Lokasi Pekerjaan {!! Template::required() !!} </label>
								<input type="text" name="lokasi_pekerjaan" class="form-control" placeholder="Lokasi Pekerjaan" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nomor Kontrak {!! Template::required() !!} </label>
								<input type="text" name="nomor_kontrak" class="form-control" placeholder="Nomor Kontrak" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Kontrak {!! Template::required() !!} </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="mdi mdi-calendar"></i>
										</span>
									</div>
									<input type="text" readonly placeholder="Pilih Tanggal" name="tanggal_kontrak" class="form-control" required>
								</div>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Pagu Anggaran {!! Template::required() !!} </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											Rp
										</span>
									</div>
									<input type="number" name="pagu_anggaran" class="form-control" placeholder="Pagu Anggaran" required>
								</div>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nilai Kontrak {!! Template::required() !!} </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											Rp
										</span>
									</div>
									<input type="number" name="nilai_kontrak" class="form-control" placeholder="Nilai Kontrak" required>
								</div>
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> Nilai Volume {!! Template::required() !!} </label>
								<input type="number" name="nilai_target" class="form-control" placeholder="Nilai Volume" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Satuan Volume {!! Template::required() !!} </label>
								<input type="text" name="satuan_target" class="form-control" placeholder="Satuan Volume" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Kategori / Skema Pekerjaan {!! Template::required() !!} </label>
								<select class="form-control" name="nama_skema_pekerjaan" required>
									<option selected disabled> - Pilih - </option>
									<option value="Pelaksana Kontruksi"> Pelaksana Kontruksi </option>
									<option value="Penyedia Barang"> Penyedia Barang </option>
									<option value="Konsultan Pengawas"> Konsultan Pengawas </option>
									<option value="Konsultan Perencanaan"> Konsultan Perencanaan </option>
									<option value="Konsultan Perencanaan Non Kons"> Konsultan Perencanaan Non Kons </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Status {!! Template::required() !!} </label>
								<select class="form-control" name="status" required>
									<option value="Pengerjaan"> Pengerjaan </option>
									<option value="Selesai"> Selesai </option>
								</select>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Awal Pelaksanaan {!! Template::required() !!} </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="mdi mdi-calendar"></i>
										</span>
									</div>
									<input type="text" readonly placeholder="Pilih Tanggal" name="awal_pelaksanaan" class="form-control" required>
								</div>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Akhir Pelaksanaan {!! Template::required() !!} </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="mdi mdi-calendar"></i>
										</span>
									</div>
									<input type="text" readonly placeholder="Pilih Tanggal" name="akhir_pelaksanaan" class="form-control" required>
								</div>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nama Pelaksana Kegiatan {!! Template::required() !!} </label>
								<input type="text" name="nama_pelaksana_kegiatan" class="form-control" placeholder="Nama Pelaksana Kegiatan" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nama Petugas / Ahli K3 {!! Template::required() !!} </label>
								<input type="text" name="nama_petugas" class="form-control" placeholder="Nama Petugas / Ahli K3" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript" src="{{ url('vendors/datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="tanggal_kontrak"], [name="awal_pelaksanaan"], [name="akhir_pelaksanaan"]`).datepicker({
			'format': 'dd/mm/yyyy'
		})

		$form.find(`[name="tahun_anggaran"]`).select2({
			'placeholder': '- Pilih Tahun -'
		})

		$form.find(`[name="id_perusahaan"]`).select2({
			'placeholder': '- Pilih Perusahaan -'
		})

		const resetForm = () => {
			$form[0].reset()
			$form.find(`[name="id_perusahaan"]`).val('').trigger('change')
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('paket_pekerjaan.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				$submitBtn.ladda('stop')
				ajaxSuccessHandling(response);
				resetForm()
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		resetForm();

	})

</script>
@endsection