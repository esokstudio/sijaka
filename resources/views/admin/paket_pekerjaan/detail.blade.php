@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>

				<div class="row">
					<div class="col-lg-6">
						<div class="table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<td> Tahun Anggaran </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->tahun_anggaran ?? '-' }} </td>
									</tr>
									<tr>
										<td> Perusahaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->namaPerusahaan() }} </td>
									</tr>
									<tr>
										<td> Nama Pekerjaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nama_pekerjaan }} </td>
									</tr>
									<tr>
										<td> Lokasi Pekerjaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->lokasi_pekerjaan }} </td>
									</tr>
									<tr>
										<td> Nomor Kontrak </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nomor_kontrak }} </td>
									</tr>
									<tr>
										<td> Tanggal Kontrak </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->tanggal_kontrak }} </td>
									</tr>
									<tr>
										<td> Pagu Anggaran </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->paguAnggaranText() }} </td>
									</tr>
									<tr>
										<td> Nilai Kontrak </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nilaiKontrakText() }} </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<td> Nilai Volume </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nilai_target }} </td>
									</tr>
									<tr>
										<td> Satuan Volume </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->satuan_target }} </td>
									</tr>
									<tr>
										<td> Kategori / Skema Pekerjaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nama_skema_pekerjaan ?? '-' }} </td>
									</tr>
									<tr>
										<td> Status </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->status }} </td>
									</tr>
									<tr>
										<td> Awal Pelaksanaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->awal_pelaksanaan }} </td>
									</tr>
									<tr>
										<td> Akhir Pelaksanaan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->akhir_pelaksanaan }} </td>
									</tr>
									<tr>
										<td> Nama Pelaksana Kegiatan </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nama_pelaksana_kegiatan }} </td>
									</tr>
									<tr>
										<td> Nama Petugas / Ahli K3 </td>
										<td width="10"> : </td>
										<td> {{ $paketPekerjaan->nama_petugas }} </td>
									</tr>
									<tr>
										<td> </td>
										<td width="10"> </td>
										<td> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
