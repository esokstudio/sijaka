@extends('admin.layouts.templates')


@section('action')
<button class="btn btn-success" id="import-btn">
	<i class="mdi mdi-upload"></i> Import dari Excel
</button>
<button class="btn btn-success" id="export-btn">
	<i class="mdi mdi-download"></i> Export ke Excel
</button>
<button class="btn btn-primary" id="filter-btn">
	<i class="mdi mdi-filter"></i> Filter
</button>
<a class="btn btn-success" href="{{ route('paket_pekerjaan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Tanggal Kontrak </th>
								<th> Tahun Anggaran </th>
								<th> Nomor Kontrak </th>
								<th> Nama Pekerjaan </th>
								<th> Lokasi </th>
								<th> Perusahaan </th>
								<th> Pagu </th>
								<th> Nilai Kontrak </th>
								<th> Volume </th>
								<th> Pelaksanaan </th>
								<th> Status </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="filter-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="filter-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Filter </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Tahun Anggaran </label>
						<select class="form-control" style="width: 100%;" name="tahun_anggaran">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
							<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="id_perusahaan">
							<option value="all"> - Semua Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::all() as $perusahaan)
							<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					@if(auth()->user()->isAdmin())
					<div class="form-group">
						<label> Instansi </label>
						<select class="form-control" style="width: 100%;" name="id_instansi">
							<option value="all"> - Semua Instansi - </option>
							@foreach(\App\Models\Instansi::all() as $instansi)
							<option value="{{ $instansi->id }}"> {{ $instansi->nama_instansi }} </option>
							@endforeach
						</select>
					</div>
					@endif

					<div class="form-group">
						<label> Status </label>
						<select class="form-control" style="width: 100%;" name="status">
							<option value="all"> - Semua - </option>
							<option value="Pengerjaan"> Pengerjaan </option>
							<option value="Selesai"> Selesai </option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-filter"></i> Filter
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>


<div class="modal fade" id="export-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="export-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Export ke Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Tahun Anggaran </label>
						<select class="form-control" style="width: 100%;" name="tahun_anggaran">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\TahunAnggaran::orderBy('tahun', 'desc')->get() as $tahunAnggaran)
							<option value="{{ $tahunAnggaran->tahun }}"> {{ $tahunAnggaran->tahun }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="id_perusahaan">
							<option value="all"> - Semua Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::all() as $perusahaan)
							<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					@if(auth()->user()->isAdmin())
					<div class="form-group">
						<label> Instansi </label>
						<select class="form-control" style="width: 100%;" name="id_instansi">
							<option value="all"> - Semua Instansi - </option>
							@foreach(\App\Models\Instansi::all() as $instansi)
							<option value="{{ $instansi->id }}"> {{ $instansi->nama_instansi }} </option>
							@endforeach
						</select>
					</div>
					@endif

					<div class="form-group">
						<label> Status </label>
						<select class="form-control" style="width: 100%;" name="status">
							<option value="all"> - Semua - </option>
							<option value="Pengerjaan"> Pengerjaan </option>
							<option value="Selesai"> Selesai </option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-download"></i> Export
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="import-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="import-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Import Dari Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					{!! Template::requiredBanner() !!}
					<p> 
						Silahkan download template pengisian dengan klik <a href="{{ url('docs/Template_Import_Paket_Pekerjaan.xlsx') }}" download> disini </a> <br>
						Catatan :
						<ul>
							<li> Import wajib menggunakan template yang sudah disediakan </li>
							<li> Kolom berwarna merah wajib diisi </li>
							<li> Contoh pengisian bisa <a href="{{ url('docs/Sample_Template_Import_Paket_Pekerjaan.xlsx') }}" download> lihat disini </a> </li>
						</ul>
					</p>
					<div class="form-group">
						<label> File {!! Template::required() !!} </label>
						<input type="file" name="file" class="form-control" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-upload"></i> Upload
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('paket_pekerjaan') }}"
			},
			columns : [
				{
					data : 'tanggal_kontrak',
					name : 'tanggal_kontrak'
				},
				{
					data : 'tahun_anggaran',
					name : 'tahun_anggaran'
				},
				{
					data : 'nomor_kontrak',
					name : 'nomor_kontrak'
				},
				{
					data : 'nama_pekerjaan',
					name : 'nama_pekerjaan'
				},
				{
					data : 'lokasi_pekerjaan',
					name : 'lokasi_pekerjaan'
				},
				{
					data : 'perusahaan.nama_perusahaan',
					name : 'perusahaan.nama_perusahaan'
				},
				{
					data : 'pagu_anggaran',
					name : 'pagu_anggaran'
				},
				{
					data : 'nilai_kontrak',
					name : 'nilai_kontrak'
				},
				{
					data : 'nilai_target',
					name : 'nilai_target'
				},
				{
					data : 'awal_pelaksanaan',
					name : 'awal_pelaksanaan'
				},
				{
					data : 'status',
					name : 'status'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			},

			preDrawCallback: settings => {
				let $filter = $('#filter-form');
				let query = '';

				if($filter.find(`[name="tahun_anggaran"]`).val() != 'all') query += `&tahun_anggaran=${$filter.find(`[name="tahun_anggaran"]`).val()}`
				if($filter.find(`[name="id_perusahaan"]`).val() != 'all') query += `&id_perusahaan=${$filter.find(`[name="id_perusahaan"]`).val()}`
				@if(auth()->user()->isAdmin())
				if($filter.find(`[name="id_instansi"]`).val() != 'all') query += `&id_instansi=${$filter.find(`[name="id_instansi"]`).val()}`
				@endif
				if($filter.find(`[name="status"]`).val() != 'all') query += `&status=${$filter.find(`[name="status"]`).val()}`

				if(query.length > 0) query = query.substr(1);

				settings.ajax.url = `{{ route('paket_pekerjaan') }}?${query}`
			},
			order: ['0', 'desc']
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

		const $importModal = $('#import-modal');
		const $importForm = $('#import-form');
		const $importSubmitBtn = $importForm.find(`[type="submit"]`).ladda()

		$('#import-btn').on('click', function(){
			$('#import-modal').modal('show')
		})

		$('#import-form').on('submit', function(e){
			e.preventDefault();

			$importSubmitBtn.ladda('start')
			let formData = new FormData(this)

			ajaxSetup()
			$.ajax({
				url: `{{ route('paket_pekerjaan.import') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false
			})
			.done(response => {
				$importModal.modal('hide')
				$importSubmitBtn.ladda('stop')
				ajaxSuccessHandling(response)
				$importForm[0].reset()
				dtReload()
				setTimeout(() => {
					window.location.reload()
				}, 2000)
			})
			.fail(error => {
				$importSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $importForm)
			})
		})


		const $filterModal = $('#filter-modal');
		const $filterForm = $('#filter-form');
		const $filterSubmitBtn = $filterForm.find(`[type="submit"]`).ladda()

		$('#filter-btn').on('click', function(){
			$('#filter-modal').modal('show')
		})

		$('#filter-form').on('submit', function(e){
			e.preventDefault();

			$filterModal.modal('hide')
			dtReload()
		})

		$filterForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})



		const $exportModal = $('#export-modal');
		const $exportForm = $('#export-form');
		const $exportSubmitBtn = $exportForm.find(`[type="submit"]`).ladda()

		$('#export-btn').on('click', function(){
			$('#export-modal').modal('show')
		})

		$exportForm.on('submit', function(e){
			e.preventDefault();

			$exportSubmitBtn.ladda('start')
			let formData = $(this).serialize()

			ajaxSetup()
			$.ajax({
				url: `{{ route('paket_pekerjaan.export_to_excel') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$exportSubmitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$exportSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $exportForm)
			})
		})

		$exportForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

	})

</script>
@endsection