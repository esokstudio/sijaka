@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama Badan Usaha {!! Template::required() !!} </label>
						<input type="text" name="nama_badan_usaha" class="form-control" placeholder="Nama Badan Usaha" value="{{ $rantaiPasok->nama_badan_usaha }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Jenis Rantai Pasok {!! Template::required() !!} </label>
						<select class="form-control" name="jenis_rantai_pasok" required>
							<option disabled selected> - Pilih - </option>
							<option value="Material Kontruksi"> Material Kontruksi </option>
							<option value="Peralatan Kontruksi"> Peralatan Kontruksi </option>
						</select>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Jenis Usaha {!! Template::required() !!} </label>
						<input type="text" name="jenis_usaha" class="form-control" placeholder="Jenis Usaha" value="{{ $rantaiPasok->jenis_usaha }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Alamat {!! Template::required() !!} </label>
						<input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ $rantaiPasok->alamat }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nomor Telepon (Opsional) </label>
						<input type="text" name="nomor_telepon" class="form-control" placeholder="Nomor Telepon" value="{{ $rantaiPasok->nomor_telepon }}">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Email (Opsional) </label>
						<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $rantaiPasok->email }}">
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					{!! Template::infoBanner('Koordinat/Sematan Peta untuk di tampilkan di laman rantai pasok. Akan memprioritaskan menampilkan sematan peta (Jika diisi).') !!}

					<div class="form-group">
						<label> Sematan Peta (Opsional) </label>
						<textarea class="form-control" name="embedded_maps" rows="3" placeholder="<iframe></iframe>">{{ $rantaiPasok->embedded_maps }}</textarea>
					</div>

					<div class="form-group">
						<label> Koordinat (Opsional) </label>
						<input type="text" name="koordinat" class="form-control" placeholder="Contoh : -6.743638, 108.543766" value="{{ $rantaiPasok->koordinat }}">
					</div>

					<hr>

					<div class="form-group">
						<label> No SBU </label>
						<input type="text" name="no_sbu" class="form-control" placeholder="No SBU" value="{{ $rantaiPasok->no_sbu }}">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Tanggal SBU </label>
						<input type="date" name="tgl_sbu" class="form-control" placeholder="Tanggal SBU" value="{{ $rantaiPasok->tgl_sbu }}">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Akhir Berlaku SBU </label>
						<input type="date" name="berlaku_sbu" class="form-control" placeholder="Akhir Berlaku SBU" value="{{ $rantaiPasok->berlaku_sbu }}">
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<div class="form-group">
						<label> No NIB {!! Template::required() !!} </label>
						<input type="text" name="no_nib" class="form-control" placeholder="No NIB" value="{{ $rantaiPasok->no_nib }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Tanggal NIB {!! Template::required() !!} </label>
						<input type="date" name="tgl_nib" class="form-control" placeholder="Tanggal NIB" value="{{ $rantaiPasok->tgl_nib }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Akhir Berlaku NIB {!! Template::required() !!} </label>
						<input type="date" name="berlaku_nib" class="form-control" placeholder="Akhir Berlaku NIB" value="{{ $rantaiPasok->berlaku_nib }}" required>
						<span class="invalid-feedback"></span>
					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.find(`[name="jenis_rantai_pasok"]`).val(`{{ $rantaiPasok->jenis_rantai_pasok }}`)

		const resetForm = () => {
			clearInvalid();
			$form.find(`[name="nama_badan_usaha"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('rantai_pasok.update', $rantaiPasok->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection