@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>

				<div class="table-responsive">
					<table class="table">
						<tr>
							<th> Nama Badan Usaha </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->nama_badan_usaha }} </td>
							<th> Jenis Usaha </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->jenis_usaha }} </td>
						</tr>
						<tr>
							<th> Alamat </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->alamat }} </td>
							<th> Jenis Rantai Pasok </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->jenis_rantai_pasok }} </td>
						</tr>
						<tr>
							<th> Nomor Telepon </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->nomor_telepon ?? '-' }} </td>
							<th> Email </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->email ?? '-' }} </td>
						</tr>
						<tr>
							<th> No SBU </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->no_sbu ?? '-' }} </td>
							<th> Tanggal SBU </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->tgl_sbu ?? '-' }} </td>
						</tr>
						<tr>
							<th> Akhir Berlaku SBU </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->berlaku_sbu ?? '-' }} </td>
							<th> No NIB </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->no_nib ?? '-' }} </td>
						</tr>
						<tr>
							<th> Tanggal NIB </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->tgl_nib ?? '-' }} </td>
							<th> Akhir Berlaku NIB </th>
							<td width="10"> : </td>
							<td> {{ $rantaiPasok->berlaku_nib ?? '-' }} </td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	@if($rantaiPasok->jenis_rantai_pasok == 'Material Kontruksi')
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Item Material Kontruksi </h6>

				<div class="text-right mb-3">
					<button class="btn btn-primary" data-toggle="modal" data-target="#create-material-modal">
						<i class="mdi mdi-plus"></i> Tambah
					</button>
				</div>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama Produk </th>
								<th> Nama Sub Produk </th>
								<th> Merk Produk </th>
								<th> Sertifikat TKDN </th>
								<th> Sertifikat SNI </th>
								<th> Foto </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
	@elseif($rantaiPasok->jenis_rantai_pasok == 'Peralatan Kontruksi')
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> Item Peralatan Kontruksi </h6>

				<div class="text-right mb-3">
					<button class="btn btn-primary" data-toggle="modal" data-target="#create-peralatan-modal">
						<i class="mdi mdi-plus"></i> Tambah
					</button>
				</div>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama Peralatan </th>
								<th> Nama Sub Peralatan </th>
								<th> Merk Peralatan </th>
								<th> Jumlah Unit </th>
								<th> Surat Keterangan Memenuhi Syarat K3 </th>
								<th> Bukti Kepemilikan </th>
								<th> Foto </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
	@endif
</div>
@endsection


@section('scripts')
@if($rantaiPasok->jenis_rantai_pasok == 'Material Kontruksi')
<script type="text/javascript">
	
	$(function(){

		const $createForm = $('#create-material-form')
		const $createSubmitBtn = $createForm.find(`[type="submit"]`).ladda()
		const $updateForm = $('#update-material-form')
		const $updateSubmitBtn = $updateForm.find(`[type="submit"]`).ladda()

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('rantai_pasok_material_kontruksi') }}?id_rantai_pasok={{ $rantaiPasok->id }}"
			},
			columns : [
				{
					data : 'nama_produk',
					name : 'nama_produk'
				},
				{
					data : 'nama_sub_produk',
					name : 'nama_sub_produk'
				},
				{
					data : 'merk_produk',
					name : 'merk_produk'
				},
				{
					data : 'file_sertifikat_tkdn',
					name : 'file_sertifikat_tkdn'
				},
				{
					data : 'file_sertifikat_sni',
					name : 'file_sertifikat_sni'
				},
				{
					data : 'file_foto',
					name : 'file_foto'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})

			$('.edit').off('click')
			$('.edit').on('click', function(){
				const { getHref, updateHref } = $(this).data()
				$.get({
					url: getHref,
					dataType: 'json'
				})
				.done(response => {
					const { rantaiPasokMaterialKontruksi } = response
					$updateForm.attr('action', updateHref)
					$updateForm.find(`[name="nama_produk"]`).val(rantaiPasokMaterialKontruksi.nama_produk)
					$updateForm.find(`[name="nama_sub_produk"]`).val(rantaiPasokMaterialKontruksi.nama_sub_produk)
					$updateForm.find(`[name="merk_produk"]`).val(rantaiPasokMaterialKontruksi.merk_produk)
					$('#update-material-modal').modal('show')
				})
			})
		}

		$createForm.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$createSubmitBtn.ladda('start')

			formData.append('id_rantai_pasok', `{{ $rantaiPasok->id }}`)

			ajaxSetup();
			$.ajax({
				url: `{{ route('rantai_pasok_material_kontruksi.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$createSubmitBtn.ladda('stop');
				$createForm[0].reset()
				$('#create-material-modal').modal('hide')
				dtReload()
			})
			.fail(error => {
				ajaxErrorHandling(error, $createForm)
				$createSubmitBtn.ladda('stop')
			})
		})

		$updateForm.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			let href = $(this).attr('action')
			$updateSubmitBtn.ladda('start')
			formData.append('_method', `PUT`)

			ajaxSetup();
			$.ajax({
				url: href,
				method: 'post',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$updateSubmitBtn.ladda('stop');
				$('#update-material-modal').modal('hide')
				dtReload()
			})
			.fail(error => {
				ajaxErrorHandling(error, $updateForm)
				$updateSubmitBtn.ladda('stop')
			})
		})

	})

</script>
@else
<script type="text/javascript">
	
	$(function(){

		const $createForm = $('#create-peralatan-form')
		const $createSubmitBtn = $createForm.find(`[type="submit"]`).ladda()
		const $updateForm = $('#update-peralatan-form')
		const $updateSubmitBtn = $updateForm.find(`[type="submit"]`).ladda()

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('rantai_pasok_peralatan_kontruksi') }}?id_rantai_pasok={{ $rantaiPasok->id }}"
			},
			columns : [
				{
					data : 'nama_peralatan',
					name : 'nama_peralatan'
				},
				{
					data : 'nama_sub_peralatan',
					name : 'nama_sub_peralatan'
				},
				{
					data : 'merk_peralatan',
					name : 'merk_peralatan'
				},
				{
					data : 'jumlah_unit',
					name : 'jumlah_unit'
				},
				{
					data : 'file_surat_keterangan_k3',
					name : 'file_surat_keterangan_k3'
				},
				{
					data : 'file_bukti_kepemilikan',
					name : 'file_bukti_kepemilikan'
				},
				{
					data : 'file_foto',
					name : 'file_foto'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})

			$('.edit').off('click')
			$('.edit').on('click', function(){
				const { getHref, updateHref } = $(this).data()
				$.get({
					url: getHref,
					dataType: 'json'
				})
				.done(response => {
					const { rantaiPasokPeralatanKontruksi } = response
					$updateForm.attr('action', updateHref)
					$updateForm.find(`[name="nama_peralatan"]`).val(rantaiPasokPeralatanKontruksi.nama_peralatan)
					$updateForm.find(`[name="nama_sub_peralatan"]`).val(rantaiPasokPeralatanKontruksi.nama_sub_peralatan)
					$updateForm.find(`[name="merk_peralatan"]`).val(rantaiPasokPeralatanKontruksi.merk_peralatan)
					$updateForm.find(`[name="jumlah_unit"]`).val(rantaiPasokPeralatanKontruksi.jumlah_unit)
					$('#update-peralatan-modal').modal('show')
				})
			})
		}

		$createForm.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$createSubmitBtn.ladda('start')

			formData.append('id_rantai_pasok', `{{ $rantaiPasok->id }}`)

			ajaxSetup();
			$.ajax({
				url: `{{ route('rantai_pasok_peralatan_kontruksi.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$createSubmitBtn.ladda('stop');
				$createForm[0].reset()
				$('#create-peralatan-modal').modal('hide')
				dtReload()
			})
			.fail(error => {
				ajaxErrorHandling(error, $createForm)
				$createSubmitBtn.ladda('stop')
			})
		})

		$updateForm.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			let href = $(this).attr('action')
			$updateSubmitBtn.ladda('start')
			formData.append('_method', `PUT`)

			ajaxSetup();
			$.ajax({
				url: href,
				method: 'post',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$updateSubmitBtn.ladda('stop');
				$('#update-peralatan-modal').modal('hide')
				dtReload()
			})
			.fail(error => {
				ajaxErrorHandling(error, $updateForm)
				$updateSubmitBtn.ladda('stop')
			})
		})

	})

</script>
@endif
@endsection


@section('modal')
<div class="modal fade" id="create-material-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="create-material-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Tambah Item Material </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Nama Produk {!! Template::required() !!} </label>
						<input type="text" name="nama_produk" class="form-control" placeholder="Nama Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nama Sub Produk {!! Template::required() !!} </label>
						<input type="text" name="nama_sub_produk" class="form-control" placeholder="Nama Sub Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Merk Produk {!! Template::required() !!} </label>
						<input type="text" name="merk_produk" class="form-control" placeholder="Merk Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Sertifikat TKDN {!! Template::required() !!} </label>
						<input type="file" name="file_upload_sertifikat_tkdn" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Sertifikat SNI {!! Template::required() !!} </label>
						<input type="file" name="file_upload_sertifikat_sni" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Foto {!! Template::required() !!} </label>
						<input type="file" name="file_upload_foto" accept="image/*" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="update-material-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="update-material-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Edit Item Material </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Nama Produk {!! Template::required() !!} </label>
						<input type="text" name="nama_produk" class="form-control" placeholder="Nama Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nama Sub Produk {!! Template::required() !!} </label>
						<input type="text" name="nama_sub_produk" class="form-control" placeholder="Nama Sub Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Merk Produk {!! Template::required() !!} </label>
						<input type="text" name="merk_produk" class="form-control" placeholder="Merk Produk" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Sertifikat TKDN (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_sertifikat_tkdn" class="form-control">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Sertifikat SNI (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_sertifikat_sni" class="form-control">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Foto (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_foto" accept="image/*" class="form-control">
						<span class="invalid-feedback"></span>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="create-peralatan-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="create-peralatan-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Tambah Item Peralatan </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Nama Peralatan {!! Template::required() !!} </label>
						<input type="text" name="nama_peralatan" class="form-control" placeholder="Nama Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nama Sub Peralatan {!! Template::required() !!} </label>
						<input type="text" name="nama_sub_peralatan" class="form-control" placeholder="Nama Sub Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Merk Peralatan {!! Template::required() !!} </label>
						<input type="text" name="merk_peralatan" class="form-control" placeholder="Merk Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Jumlah Unit {!! Template::required() !!} </label>
						<input type="number" name="jumlah_unit" class="form-control" placeholder="Jumlah Unit" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Surat Keterangan Memenuhi Syarat K3 {!! Template::required() !!} </label>
						<input type="file" name="file_upload_surat_keterangan_k3" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Bukti Kepemilikan {!! Template::required() !!} </label>
						<input type="file" name="file_upload_bukti_kepemilikan" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Foto {!! Template::required() !!} </label>
						<input type="file" name="file_upload_foto" accept="image/*" class="form-control" required>
						<span class="invalid-feedback"></span>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="update-peralatan-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="update-peralatan-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Edit Item Peralatan </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Nama Peralatan {!! Template::required() !!} </label>
						<input type="text" name="nama_peralatan" class="form-control" placeholder="Nama Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Nama Sub Peralatan {!! Template::required() !!} </label>
						<input type="text" name="nama_sub_peralatan" class="form-control" placeholder="Nama Sub Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Merk Peralatan {!! Template::required() !!} </label>
						<input type="text" name="merk_peralatan" class="form-control" placeholder="Merk Peralatan" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> Jumlah Unit {!! Template::required() !!} </label>
						<input type="number" name="jumlah_unit" class="form-control" placeholder="Jumlah Unit" required>
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Surat Keterangan Memenuhi Syarat K3 (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_surat_keterangan_k3" class="form-control">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Bukti Kepemilikan (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_bukti_kepemilikan" class="form-control">
						<span class="invalid-feedback"></span>
					</div>

					<div class="form-group">
						<label> File Foto (Isi Jika Ingin Diganti) </label>
						<input type="file" name="file_upload_foto" accept="image/*" class="form-control">
						<span class="invalid-feedback"></span>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection()