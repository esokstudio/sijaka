@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('rantai_pasok.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama Badan Usaha </th>
								<th> Jenis Usaha </th>
								<th> Alamat </th>
								<th> No NIB </th>
								<th> Jenis Rantai Pasok </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('rantai_pasok') }}"
			},
			columns : [
				{
					data : 'nama_badan_usaha',
					name : 'nama_badan_usaha'
				},
				{
					data : 'jenis_usaha',
					name : 'jenis_usaha'
				},
				{
					data : 'alamat',
					name : 'alamat'
				},
				{
					data : 'no_nib',
					name : 'no_nib'
				},
				{
					data : 'jenis_rantai_pasok',
					name : 'jenis_rantai_pasok'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection