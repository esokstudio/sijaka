@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">

				<ul class="nav nav-tabs tab-solid tab-solid-danger" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tab-profil" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">
							Profil
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-kualifikasi" data-toggle="tab" href="#kualifikasi" role="tab" aria-controls="kualifikasi" aria-selected="false">
							Kualifikasi
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-akte-pendirian" data-toggle="tab" href="#akte-pendirian" role="tab" aria-controls="akte-pendirian" aria-selected="false">
							Akte Pendirian
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-pengesahan" data-toggle="tab" href="#pengesahan" role="tab" aria-controls="pengesahan" aria-selected="false">
							Pengesahan
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-akte-perubahan" data-toggle="tab" href="#akte-perubahan" role="tab" aria-controls="akte-perubahan" aria-selected="false">
							Akte Perubahan
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-pengurus" data-toggle="tab" href="#pengurus" role="tab" aria-controls="pengurus" aria-selected="false">
							Pengurus
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-keuangan" data-toggle="tab" href="#keuangan" role="tab" aria-controls="keuangan" aria-selected="false">
							Keuangan
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-tenaga-kerja" data-toggle="tab" href="#tenaga-kerja" role="tab" aria-controls="tenaga-kerja" aria-selected="false">
							Tenaga Kerja
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-pengalaman" data-toggle="tab" href="#pengalaman" role="tab" aria-controls="pengalaman" aria-selected="false">
							Pengalaman
						</a>
					</li>
				</ul>

				<hr>

				<div class="tab-content tab-content-solid">


					<!-- PROFIL -->
					<div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil">
						<div class="table-responsive mt-3">
						
							<table class="table table-hover">
								<tr>
									<th> Nama Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'nama_perusahaan') !!} </td>
									<th> Direktur </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'direktur') !!} </td>
								</tr>

								<tr>
									<th> Alamat </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'alamat') !!} </td>
									<th> Kota </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'kota') !!} </td>
								</tr>

								<tr>
									<th> Kodepos </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'kodepos') !!} </td>
									<th> Nomor Telepon </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'nomor_telepon') !!} </td>
								</tr>

								<tr>
									<th> Fax </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'fax') !!} </td>
									<th> Email </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'email') !!} </td>
								</tr>

								<tr>
									<th> Website </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'website') !!} </td>
									<th> Bentuk Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'bentuk_perusahaan') !!} </td>
								</tr>

								<tr>
									<th> Jenis Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'jenis_perusahaan') !!} </td>
									<th> Asosiasi </th>
									<td width="10"> : </td>
									<td> {!! tmpText($perusahaan, 'asosiasi', 'namaAsosiasi', 'newNamaAsosiasi') !!} </td>
								</tr>

								@if(!empty($perusahaan->nilai_kinerja) && !empty($perusahaan->nilai_keterangan))
								<tr>
									<th> Nilai Kinerja </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->nilai_kinerja !!} </td>
									<th> Nilai Keterangan </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->nilai_keterangan !!} </td>
								</tr>
								@endif

								<tr>
									<th> SBU </th>
									<td> : </td>
									<td>
										@if($perusahaan->isHasFileSbu())
										<a href="{{ $perusahaan->fileSbuLink() }}"> Klik disini </a>
										@else
										Belum Upload
										@endif
									</td>
									<th> Nomor NIB </th>
									<td> : </td>
									<td> {!! tmpText($perusahaan, 'nib') !!} </td>
								</tr>
							</table>

						</div>
						

						@if($perusahaan->isStatusWait())
						<hr>

						<div>
							<p class="text-primary font-weight-bold"> * [Perubahan] </p>
							<button class="btn btn-success approve-btn mb-2" data-href="{{ route('perusahaan.approve_change', $perusahaan->id) }}">
								<i class="mdi mdi-check"></i> Setujui Perubahan
							</button>
							<button class="btn btn-danger reject-btn mb-2" data-href="{{ route('perusahaan.reject_change', $perusahaan->id) }}">
								<i class="mdi mdi-close"></i> Tolak Perubahan
							</button>
						</div>
						@endif
					</div>
					<!-- END PROFIL -->


					<!-- KUALIFIKASI -->
					<div class="tab-pane fade" id="kualifikasi" role="tabpanel" aria-labelledby="kualifikasi">
						<div class="table-responsive">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> Sub Bidang Klasifikasi </th>
										<th> Nomor Kode </th>
										<th> Kualifikasi </th>
										<th> Tahun </th>
										<th width="100"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->kualifikasiPerusahaan as $kualifikasi)
									<tr>
										<td> {!! tmpText($kualifikasi, 'sub_bidang_klasifikasi') !!} </td>
										<td> {!! tmpText($kualifikasi, 'nomor_kode') !!} </td>
										<td> {!! tmpText($kualifikasi, 'kualifikasi') !!} </td>
										<td> {!! tmpText($kualifikasi, 'tahun') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('kualifikasi_perusahaan.detail', $kualifikasi->id) }}" title="Detail Kualifikasi Perusahaan">
														<i class="mdi mdi-magnify"></i> Detail 
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>

						</div>
					</div>
					<!-- END KUALIFIKASI -->



					<!-- AKTE PENDIRIAN -->
					<div class="tab-pane fade" id="akte-pendirian" role="tabpanel" aria-labelledby="akte-pendirian">
						@if($pendirian = $perusahaan->aktePendirianPerusahaan)
						<div class="table-responsive mt-3">
							<table class="table table-hover">
								<tr>
									<th> No Akte </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'no_akte') !!} </td>
									<th> Nama Notaris </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'nama_notaris') !!} </td>
								</tr>

								<tr>
									<th> Alamat </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'alamat') !!} </td>
									<th> Kota </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'kota') !!} </td>
								</tr>

								<tr>
									<th> Provinsi </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'provinsi') !!} </td>
									<th> Tgl Akte </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pendirian, 'tanggal_akte') !!} </td>
								</tr>
							</table>
						</div>

							@if($pendirian->isStatusWait())
							<hr>

							<div>
								<p class="text-primary font-weight-bold"> * [Perubahan] </p>
								<button class="btn btn-success approve-btn mb-2" data-href="{{ route('akte_pendirian_perusahaan.approve_change', $pendirian->id) }}">
									<i class="mdi mdi-check"></i> Setujui Perubahan
								</button>
								<button class="btn btn-danger reject-btn mb-2" data-href="{{ route('akte_pendirian_perusahaan.reject_change', $pendirian->id) }}">
									<i class="mdi mdi-close"></i> Tolak Perubahan
								</button>
							</div>
							@endif

						@else
						<p align="center">
							<i> Belum mengisi </i>
						</p>
						@endif
					</div>
					<!-- END AKTE PENDIRIAN -->



					<!-- PENGESAHAN -->
					<div class="tab-pane fade" id="pengesahan" role="tabpanel" aria-labelledby="pengesahan">
						@if($pengesahan = $perusahaan->pengesahanPerusahaan)
						<div class="table-responsive mt-3">
						
							<table class="table table-hover">
								<tr>
									<th> No Kemenkumham </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'menkumham_no') !!} </td>
									<th> Tgl Kemenkumham </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'menkumham_tgl') !!} </td>
								</tr>

								<tr>
									<th> No Pengadilan Negeri </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'pengadilan_negeri_no') !!} </td>
									<th> Tgl Pengadilan Negeri </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'pengadilan_negeri_tgl') !!} </td>
								</tr>

								<tr>
									<th> No Lembar Negara </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'lembar_negara_no') !!} </td>
									<th> Tgl Lembar Negara </th>
									<td width="10"> : </td>
									<td> {!! tmpText($pengesahan, 'lembar_negara_tgl') !!} </td>
								</tr>
							</table>
						</div>
							@if($pengesahan->isStatusWait())
							<hr>

							<div>
								<p class="text-primary font-weight-bold"> * [Perubahan] </p>
								<button class="btn btn-success approve-btn mb-2" data-href="{{ route('pengesahan_perusahaan.approve_change', $pengesahan->id) }}">
									<i class="mdi mdi-check"></i> Setujui Perubahan
								</button>
								<button class="btn btn-danger reject-btn mb-2" data-href="{{ route('pengesahan_perusahaan.reject_change', $pengesahan->id) }}">
									<i class="mdi mdi-close"></i> Tolak Perubahan
								</button>
							</div>
							@endif
						@else
						<p align="center">
							<i> Belum mengisi </i>
						</p>
						@endif
					</div>
					<!-- END PENGESAHAN -->



					<!-- AKTE PERUBAHAN -->
					<div class="tab-pane fade" id="akte-perubahan" role="tabpanel" aria-labelledby="akte-perubahan">
						<div class="table-responsive mt-3">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> No Akte </th>
										<th> Nama Notaris </th>
										<th> Alamat </th>
										<th> Kota </th>
										<th> Provinsi </th>
										<th width="70"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->aktePerubahanPerusahaan as $aktePerubahan)
									<tr>
										<td> {!! tmpText($aktePerubahan, 'no_akte') !!} </td>
										<td> {!! tmpText($aktePerubahan, 'nama_notaris') !!} </td>
										<td> {!! tmpText($aktePerubahan, 'alamat') !!} </td>
										<td> {!! tmpText($aktePerubahan, 'kota') !!} </td>
										<td> {!! tmpText($aktePerubahan, 'provinsi') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('akte_perubahan_perusahaan.detail', $aktePerubahan->id) }}" title="Detail Akte Perubahan">
														<i class="mdi mdi-magnify"></i> Detail 
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END AKTE PERUBAHAN -->



					<!-- PENGURUS -->
					<div class="tab-pane fade" id="pengurus" role="tabpanel" aria-labelledby="pengurus">
						<div class="table-responsive mt-3">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Tgl Lahir </th>
										<th> Alamat </th>
										<th> Jabatan </th>
										<th> Pendidikan </th>
										<th width="70"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->pengurusPerusahaan as $pengurus)
									<tr>
										<td> {!! tmpText($pengurus, 'nama') !!} </td>
										<td> {!! tmpText($pengurus, 'tgl_lahir') !!} </td>
										<td> {!! tmpText($pengurus, 'alamat') !!} </td>
										<td> {!! tmpText($pengurus, 'jabatan') !!} </td>
										<td> {!! tmpText($pengurus, 'pendidikan') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('pengurus_perusahaan.detail', $pengurus->id) }}" title="Detail Pengurus Perusahaan">
														<i class="mdi mdi-magnify"></i> Detail 
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END PENGURUS -->



					<!-- KEUANGAN -->
					<div class="tab-pane fade" id="keuangan" role="tabpanel" aria-labelledby="keuangan">
						<div class="table-responsive mt-3">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Alamat </th>
										<th> Jumlah Saham </th>
										<th> Nilai Satuan Saham </th>
										<th> Modal Dasar </th>
										<th> Modal Disetor </th>
										<th width="70"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->keuanganPerusahaan as $keuangan)
									<tr>
										<td> {!! tmpText($keuangan, 'nama') !!} </td>
										<td> {!! tmpText($keuangan, 'alamat') !!} </td>
										<td> {!! tmpText($keuangan, 'jumlah_saham') !!} </td>
										<td> {!! tmpText($keuangan, 'nilai_satuan_saham') !!} </td>
										<td> {!! tmpText($keuangan, 'modal_dasar') !!} </td>
										<td> {!! tmpText($keuangan, 'modal_disetor') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('keuangan_perusahaan.detail', $keuangan->id) }}" title="Detail Keuangan">
														<i class="mdi mdi-magnify"></i> Detail 
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END KEUANGAN -->



					<!-- TENAGA KERJA -->
					<div class="tab-pane fade" id="tenaga-kerja" role="tabpanel" aria-labelledby="tenaga-kerja">
						<div class="table-responsive mt-3">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Tgl Lahir </th>
										<th> Pendidikan </th>
										<th> No Registrasi </th>
										<th> Jenis Sertifikat </th>
										<th width="70"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->tenagaKerjaPerusahaan as $tenagaKerja)
									<tr>
										<td> {!! tmpText($tenagaKerja, 'nama') !!} </td>
										<td> {!! tmpText($tenagaKerja, 'tgl_lahir') !!} </td>
										<td> {!! tmpText($tenagaKerja, 'pendidikan') !!} </td>
										<td> {!! tmpText($tenagaKerja, 'no_registrasi') !!} </td>
										<td> {!! tmpText($tenagaKerja, 'jenis_sertifikat') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('tenaga_kerja_perusahaan.detail', $tenagaKerja->id) }}" title="Detail Tenaga Kerja">
														<i class="mdi mdi-magnify"></i> Detail 
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TENAGA KERJA -->



					<!-- PENGALAMAN -->
					<div class="tab-pane fade" id="pengalaman" role="tabpanel" aria-labelledby="pengalaman">
						<div class="table-responsive mt-3">
							<table class="table table-bordered table-hover dataTable">
								<thead>
									<tr>
										<th> Tanggal Kontrak </th>
										<th> Nomor Kontrak </th>
										<th> Nama Pekerjaan </th>
										<th> Lokasi </th>
										<th width="70"> Aksi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->pengalamanPerusahaan as $pengalaman)
									<tr>
										<td> {!! tmpText($pengalaman, 'tanggal_kontrak') !!} </td>
										<td> {!! tmpText($pengalaman, 'nomor_kontrak') !!} </td>
										<td> {!! tmpText($pengalaman, 'nama_pekerjaan') !!} </td>
										<td> {!! tmpText($pengalaman, 'lokasi_pekerjaan') !!} </td>
										<td>
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Aksi
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="{{ route('pengalaman_perusahaan.detail', $pengalaman->id) }}" title="Detail Pengalaman Perusahaan">
														<i class="mdi mdi-magnify"></i> Detail
													</a>
												</div>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END PENGALAMAN -->

				</div>

			</div>
		</div>
	</div>

</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			let { href } = $(this).data()
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: href,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			let { href } = $(this).data()
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: href,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

		$('.dataTable').DataTable({
			autoWidth: false,
		})

		@if(isset($_GET['tab']))
		$(`#tab-{{ $_GET['tab'] }}`).click()
		@endif
	})

</script>
@endsection