@extends('admin.layouts.templates')


@section('action')
<button class="btn btn-success" id="import-btn">
	<i class="mdi mdi-upload"></i> Import dari Excel
</button>
<button class="btn btn-success" id="export-btn">
	<i class="mdi mdi-download"></i> Export ke Excel
</button>
<button class="btn btn-primary" id="filter-btn">
	<i class="mdi mdi-filter"></i> Filter
</button>
<a class="btn btn-success" href="{{ route('perusahaan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Perusahaan </th>
								<th> Alamat </th>
								<th> Kota </th>
								<th> Kodepos </th>
								<th> Asosiasi </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="filter-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="filter-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Filter </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Kota </label>
						<select class="form-control" style="width: 100%;" name="kota">
							<option value="all"> - Semua Kota - </option>
							@foreach(\App\Models\Perusahaan::grouping('kota') as $kota)
							<option value="{{ $kota->kota }}"> {{ $kota->kota }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Bentuk Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="bentuk_perusahaan">
							<option value="all"> - Semua Bentuk Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::grouping('bentuk_perusahaan') as $bentuk)
							<option value="{{ $bentuk->bentuk_perusahaan }}"> {{ $bentuk->bentuk_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Jenis Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="jenis_perusahaan">
							<option value="all"> - Semua Jenis Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::grouping('jenis_perusahaan') as $jenis)
							<option value="{{ $jenis->jenis_perusahaan }}"> {{ $jenis->jenis_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Asosiasi </label>
						<select class="form-control" style="width: 100%;" name="id_asosiasi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\Asosiasi::all() as $asosiasi)
							<option value="{{ $asosiasi->id }}"> {{ $asosiasi->nama_asosiasi }} </option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-filter"></i> Filter
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="import-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="import-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Import Dari Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					{!! Template::requiredBanner() !!}
					<p> 
						Silahkan download template pengisian dengan klik <a href="{{ url('docs/Template_Import_Perusahaan.xlsx') }}" download> disini </a> <br>
						Catatan :
						<ul>
							<li> Import wajib menggunakan template yang sudah disediakan </li>
							<li> Kolom berwarna merah wajib diisi </li>
						</ul>
					</p>
					<div class="form-group">
						<label> File {!! Template::required() !!} </label>
						<input type="file" name="file" class="form-control" required>
						<small class="invalid-feedback"></small>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-upload"></i> Upload
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>

<div class="modal fade" id="export-modal">
	<div class="modal-dialog mt-4">
		<div class="modal-content">

			<form id="export-form">
				
				<div class="modal-header">
					<h5 class="modal-title"> Export ke Excel </h5>
					<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label> Kota </label>
						<select class="form-control" style="width: 100%;" name="kota">
							<option value="all"> - Semua Kota - </option>
							@foreach(\App\Models\Perusahaan::grouping('kota') as $kota)
							<option value="{{ $kota->kota }}"> {{ $kota->kota }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Bentuk Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="bentuk_perusahaan">
							<option value="all"> - Semua Bentuk Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::grouping('bentuk_perusahaan') as $bentuk)
							<option value="{{ $bentuk->bentuk_perusahaan }}"> {{ $bentuk->bentuk_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Jenis Perusahaan </label>
						<select class="form-control" style="width: 100%;" name="jenis_perusahaan">
							<option value="all"> - Semua Jenis Perusahaan - </option>
							@foreach(\App\Models\Perusahaan::grouping('jenis_perusahaan') as $jenis)
							<option value="{{ $jenis->jenis_perusahaan }}"> {{ $jenis->jenis_perusahaan }} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label> Asosiasi </label>
						<select class="form-control" style="width: 100%;" name="id_asosiasi">
							<option value="all"> - Semua - </option>
							@foreach(\App\Models\Asosiasi::all() as $asosiasi)
							<option value="{{ $asosiasi->id }}"> {{ $asosiasi->nama_asosiasi }} </option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-download"></i> Export
					</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">
						<i class="mdi mdi-close"></i> Tutup
					</button>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('perusahaan') }}"
			},
			columns : [
				{
					data : 'nama_perusahaan',
					name : 'nama_perusahaan'
				},
				{
					data : 'alamat',
					name : 'alamat'
				},
				{
					data : 'kota',
					name : 'kota'
				},
				{
					data : 'kodepos',
					name : 'kodepos'
				},
				{
					data : 'asosiasi.nama_asosiasi',
					name : 'asosiasi.nama_asosiasi'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			},
			preDrawCallback: settings => {
				let $filter = $('#filter-form');
				let query = '';

				if($filter.find(`[name="kota"]`).val() != 'all') query += `&kota=${$filter.find(`[name="kota"]`).val()}`
				if($filter.find(`[name="bentuk_perusahaan"]`).val() != 'all') query += `&bentuk_perusahaan=${$filter.find(`[name="bentuk_perusahaan"]`).val()}`
				if($filter.find(`[name="jenis_perusahaan"]`).val() != 'all') query += `&jenis_perusahaan=${$filter.find(`[name="jenis_perusahaan"]`).val()}`
				if($filter.find(`[name="id_asosiasi"]`).val() != 'all') query += `&id_asosiasi=${$filter.find(`[name="id_asosiasi"]`).val()}`

				if(query.length > 0) query = query.substr(1);

				settings.ajax.url = `{{ route('perusahaan') }}?${query}`
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}


		const $importModal = $('#import-modal');
		const $importForm = $('#import-form');
		const $importSubmitBtn = $importForm.find(`[type="submit"]`).ladda()

		$('#import-btn').on('click', function(){
			$('#import-modal').modal('show')
		})

		$('#import-form').on('submit', function(e){
			e.preventDefault();

			$importSubmitBtn.ladda('start')
			let formData = new FormData(this)

			ajaxSetup()
			$.ajax({
				url: `{{ route('perusahaan.import') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false
			})
			.done(response => {
				$importModal.modal('hide')
				$importSubmitBtn.ladda('stop')
				ajaxSuccessHandling(response)
				$importForm[0].reset()
				dtReload()
				setTimeout(() => {
					window.location.reload()
				}, 2000)
			})
			.fail(error => {
				$importSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $importForm)
			})
		})



		const $filterModal = $('#filter-modal');
		const $filterForm = $('#filter-form');
		const $filterSubmitBtn = $filterForm.find(`[type="submit"]`).ladda()

		$('#filter-btn').on('click', function(){
			$('#filter-modal').modal('show')
		})

		$('#filter-form').on('submit', function(e){
			e.preventDefault();

			$filterModal.modal('hide')
			dtReload()
		})

		$filterForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})



		const $exportModal = $('#export-modal');
		const $exportForm = $('#export-form');
		const $exportSubmitBtn = $exportForm.find(`[type="submit"]`).ladda()

		$('#export-btn').on('click', function(){
			$('#export-modal').modal('show')
		})

		$('#export-form').on('submit', function(e){
			e.preventDefault();

			$exportSubmitBtn.ladda('start')
			let formData = $(this).serialize()

			ajaxSetup()
			$.ajax({
				url: `{{ route('perusahaan.export') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				let { filedata, filemime, filename } = response;
				ajaxSuccessHandling(response);
				$exportSubmitBtn.ladda('stop');
				downloadFromBase64(filedata, filemime, filename)
			})
			.fail(error => {
				$exportSubmitBtn.ladda('stop')
				ajaxErrorHandling(error, $importForm)
			})
		})

		$exportForm.find('select').select2({
			'placeholder': '- Pilih Semua -'
		})

	})

</script>
@endsection