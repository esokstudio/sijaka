@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}

					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama Perusahaan {!! Template::required() !!} </label>
								<input type="text" name="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" value="{{ $perusahaan->nama_perusahaan }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Direktur {!! Template::required() !!} </label>
								<input type="text" name="direktur" class="form-control" placeholder="Direktur" value="{{ $perusahaan->direktur }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Alamat {!! Template::required() !!} </label>
								<input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ $perusahaan->alamat }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Email </label>
								<input type="email" name="email" class="form-control" placeholder="Email" value="{{ $perusahaan->email }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Kota </label>
								<input type="text" name="kota" class="form-control" placeholder="Kota" value="{{ $perusahaan->kota }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Kode Pos </label>
								<input type="text" name="kodepos" class="form-control" placeholder="Kode Pos" value="{{ $perusahaan->kode_pos }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Nomor Telepon </label>
								<input type="text" name="nomor_telepon" class="form-control" placeholder="Nomor Telepon" value="{{ $perusahaan->nomor_telepon }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Fax </label>
								<input type="text" name="fax" class="form-control" placeholder="Fax" value="{{ $perusahaan->fax }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Website </label>
								<input type="text" name="website" class="form-control" placeholder="Website" value="{{ $perusahaan->website }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Bentuk Perusahaan </label>
								<input type="text" name="bentuk_perusahaan" class="form-control" placeholder="Bentuk Perusahaan" value="{{ $perusahaan->bentuk_perusahaan }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Jenis Perusahaan </label>
								<input type="text" name="jenis_perusahaan" class="form-control" placeholder="Jenis Perusahaan" value="{{ $perusahaan->jenis_perusahaan }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Nomor NIB {!! Template::required() !!} </label>
								<input type="text" name="nib" class="form-control" placeholder="Nomor NIB" value="{{ $perusahaan->nib }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Asosiasi </label>
								<select name="id_asosiasi" style="width: 100%;">
									@foreach(\App\Models\Asosiasi::all() as $asosiasi)
									<option value="{{ $asosiasi->id }}"> {{ $asosiasi->nama_asosiasi }} </option>
									@endforeach
								</select>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> File SBU </label>
								<input type="file" name="file_sbu" class="form-control">
								@if($perusahaan->isHasFileSbu())
								<p class="mt-2">
									File SBU yang sudah diupload klik <a href="{{ $perusahaan->fileSbuLink() }}"> disini </a>
								</p>
								@endif
								<span class="invalid-feedback"></span>
							</div>
						</div>

						@method('put')

					</div>


					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			// $form.find(`[name="id_asosiasi"]`).val('').trigger('change');
			clearInvalid();
			$form.find(`[name="nama_perusahaan"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('perusahaan.update', $perusahaan->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})
		
		$form.find(`[name="id_asosiasi"]`).select2({
			'placeholder': '- Pilih Asosiasi -'
		})

		$form.find(`[name="id_asosiasi"]`).val(`{{ $perusahaan->id_asosiasi }}`).trigger('change');

		resetForm();


	})

</script>
@endsection