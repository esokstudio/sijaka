@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('kecelakaan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama Pekerjaan </th>
								<th> Perusahaan </th>
								<th> Lokasi Kecelakaan </th>
								<th> Waktu Kejadian </th>
								<th> Deskripsi Kecelakaan </th>
								<th> Deskripsi Kerugian </th>
								<th> Sumber Masalah </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('kecelakaan') }}"
			},
			columns : [
				{
					data : 'nama_pekerjaan',
					name : 'nama_pekerjaan'
				},
				{
					data : 'perusahaan',
					name : 'perusahaan'
				},
				{
					data : 'lokasi_kecelakaan',
					name : 'lokasi_kecelakaan'
				},
				{
					data : 'waktu_kejadian',
					name : 'waktu_kejadian'
				},
				{
					data : 'deskripsi_kecelakaan',
					name : 'deskripsi_kecelakaan'
				},
				{
					data : 'deskripsi_kerugian',
					name : 'deskripsi_kerugian'
				},
				{
					data : 'sumber_masalah',
					name : 'sumber_masalah'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection