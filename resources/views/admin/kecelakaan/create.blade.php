@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}

					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama Pekerjaan {!! Template::required() !!} </label>
								<input type="text" name="nama_pekerjaan" class="form-control" placeholder="Nama Pekerjaan" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Perusahaan {!! Template::required() !!} </label>
								<input type="text" name="perusahaan" class="form-control" placeholder="Perusahaan" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Lokasi Kecelakaan {!! Template::required() !!} </label>
								<input type="text" name="lokasi_kecelakaan" class="form-control" placeholder="Lokasi Kecelakaan" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Waktu Kejadian {!! Template::required() !!} </label>
								<input type="date" name="waktu_kejadian" class="form-control" placeholder="Waktu Kejadian" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Deskripsi Kecelakaan {!! Template::required() !!} </label>
								<textarea class="form-control" name="deskripsi_kecelakaan" rows="4" placeholder="Deskripsi Kecelakaan" required></textarea>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Deskripsi Kerugian {!! Template::required() !!} </label>
								<textarea class="form-control" name="deskripsi_kerugian" rows="4" placeholder="Deskripsi Kerugian" required></textarea>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Sumber Masalah {!! Template::required() !!} </label>
								<textarea class="form-control" name="sumber_masalah" rows="4" placeholder="Sumber Masalah" required></textarea>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>
					

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="nama_pekerjaan"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('kecelakaan.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop');
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection