@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="form-group">
						<label> Nama Kategori {!! Template::required() !!} </label>
						<input type="text" name="category_name" class="form-control" placeholder="Nama Kategori" required>
						<span class="invalid-feedback"></span>
					</div>

					@if(\Setting::getValue('show_post_category_thumbnail_field', 'no') == 'yes')
					<div class="form-group">
						<label> Thumbnail </label> <br>
						<img src="#" style="max-width: 100px; max-height: 100px; display: none;" class="thumbnail-preview mb-2">
						<input type="file" name="thumbnail" class="form-control">
						<small class="invalid-feedback"></small>
					</div>
					@endif

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="category_name"]`).focus();
			$('.thumbnail-preview').hide();
			$('.thumbnail-preview').attr('src', '#')
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('post_category.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				$submitBtn.ladda('stop');
				ajaxSuccessHandling(response);
				resetForm();
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		@if(\Setting::getValue('show_post_category_thumbnail_field', 'no') == 'yes')
		previewImageAfterChange({
			fieldSelector: `[name="thumbnail"]`,
			previewSelector: `.thumbnail-preview`,
		})
		@endif

		resetForm();

	})

</script>
@endsection