<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="_token" content="{{ csrf_token() }}">
	<title> SIJAKDA | @yield('title') </title>

	<link rel="stylesheet" href="{{ url('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/css/vendor.bundle.base.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/css/vendor.bundle.addons.css') }}">
	<link rel="stylesheet" href="{{ url('css/style.css') }}">
	
	<link rel="stylesheet" href="{{ url('vendors/pace/blue/pace-theme-flash.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/ladda/ladda-themeless.min.css') }}">
	<script src="{{ url('vendors/pace/pace.min.js') }}"></script>
	<link rel="stylesheet" href="{{ url('vendors/toastr/toastr.min.css') }}">

	<link rel="shortcut icon" href="{{ url('') }}images/favicon.png" />

	<style type="text/css">
		.auth.theme-one .auto-form-wrapper {
			background: #ffffff;
			padding: 40px 40px 10px;
			border-radius: 4px;
			box-shadow: 0 6px 22.7px 13.3px rgb(112 112 112 / 7%);
		}
	</style>

	@yield('style')

</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div class="content-wrapper d-flex align-items-center auth theme-one" style="background: #dddddd;">

				@yield('content')

			</div>

		</div>

	</div>

	@yield('modal')

	<script src="{{ url('vendors/js/vendor.bundle.base.js') }}"></script>
	<script src="{{ url('vendors/js/vendor.bundle.addons.js') }}"></script>
	<script src="{{ url('js/myJs.js') }}"></script>
	<script src="{{ url('js/off-canvas.js') }}"></script>
	<script src="{{ url('js/hoverable-collapse.js') }}"></script>
	<script src="{{ url('js/misc.js') }}"></script>
	<script src="{{ url('js/settings.js') }}"></script>
	<script src="{{ url('js/todolist.js') }}"></script>
	<script src="{{ url('vendors/ladda/spin.min.js') }}"></script>
	<script src="{{ url('vendors/ladda/ladda.min.js') }}"></script>
	<script src="{{ url('vendors/ladda/ladda.jquery.min.js') }}"></script>
	<script src="{{ url('vendors/toastr/toastr.min.js') }}"></script>

	@yield('script')
</body>

</html>