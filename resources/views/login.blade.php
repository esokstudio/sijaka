@extends('layouts.auth')


@section('title', 'Login')


@section('content')
<div class="row w-100 mx-auto">
	<div class="col-lg-4 mx-auto px-4">
		<div class="auto-form-wrapper py-5">
			<figure class="text-center">
				<img src="{{ url('images/sijakda.png') }}" style="width: 150px; height: auto;">
			</figure>
			<h5 align="center" class="mb-3"> Login </h5>
			<form id="loginForm">

				<div class="form-group">
					<label class="label"> Email </label>
					<div class="input-group">
						<input type="email" class="form-control loginBorder" placeholder="Email" name="email" autocomplete="off" required>
						<div class="input-group-append">
							<span class="input-group-text loginBorder loginText">
							</span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="label"> Password </label>
					<div class="input-group">
						<input type="password" class="form-control loginBorder" placeholder="Password" name="password" required>
						<div class="input-group-append">
							<span class="input-group-text loginBorder loginText">
							</span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary submit-btn btn-block" type="submit">
					<i class="mdi mdi-login"></i> Login
					</button>
				</div>

				<p class="text-center textMsg"><br></p>

				<div class="text-block text-center">
					<span class="text-small font-weight-semibold"> Belum memiliki akun ?</span>
					<a href="{{ route('website.daftar') }}" class="text-black text-small"> Daftar sekarang </a>
				</div>

			</form>
		</div>
		<p class="footer-text text-center text-dark mt-3">
			&copy; {{ date('Y') }} | Esok Studio. All rights reserved.
		</p>
	</div>
</div>
@endsection


@section('script')
<script>
	$(function(){
		let form = $('#loginForm');
		let submitBtn = form.find(`[type="submit"]`).ladda();

		const clearError = () => {
			$('.loginBorder').removeClass('border-danger');
			$('.loginBorder').removeClass('text-danger');
			$('.loginBorder').removeClass('border-success');
			$('.loginBorder').removeClass('text-success');
			$('.loginText').html('<i class="mdi mdi-check-circle-outline"></i>');
		}

		form.on('submit', function(e) {
			e.preventDefault();
			clearError();

			submitBtn.ladda('start');

			let formData = $(this).serialize();

			ajaxSetup();
			$.ajax({
				url : `{{ route('login') }}`,
				method : "post",
				data : formData,
			})
			.done(response => {
				$('.loginBorder').addClass('border-success');
				$('.loginText').addClass('text-success');
				$('.loginText').html('<i class="mdi mdi-check-circle-outline"></i>');
				$('.textMsg').removeClass("text-danger");
				$('.textMsg').html("Login berhasil, laman sedang dialihkan..");
				$('.textMsg').addClass("text-success");
				setTimeout(() => {
					window.location.replace("{{ route('dashboard') }}");
				}, 1000)
			})
			.fail(error => {
				submitBtn.ladda('stop')
				$('.loginBorder').addClass('border-danger');
				$('.loginText').addClass('text-danger');
				$('.loginText').html('<i class="mdi mdi-close-circle-outline"></i>');
				$('.textMsg').addClass("text-danger");

				let { status, responseJSON } = error
				let { message } = responseJSON
				if(status == 403) {
					$('.textMsg').html(message);
				} else {
					$('.textMsg').html("Username/Password salah");
				}
			});
		});

		$('.form-control').on('change, keyup', function(){
			clearError();
		});
	})
</script>
@endsection