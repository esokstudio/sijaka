@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Sub Bidang Klasifikasi {!! Template::required() !!} </label>
								<input type="text" name="sub_bidang_klasifikasi" class="form-control" placeholder="Sub Bidang Klasifikasi" value="{{ tmp($kualifikasi, 'sub_bidang_klasifikasi') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nomor Kode {!! Template::required() !!} </label>
								<input type="text" name="nomor_kode" class="form-control" placeholder="Nomor Kode" value="{{ tmp($kualifikasi, 'nomor_kode') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Kualifikasi {!! Template::required() !!} </label>
								<input type="text" name="kualifikasi" class="form-control" placeholder="Kualifikasi" value="{{ tmp($kualifikasi, 'kualifikasi') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tahun {!! Template::required() !!} </label>
								<input type="text" name="tahun" class="form-control" placeholder="Tahun" value="{{ tmp($kualifikasi, 'tahun') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nilai {!! Template::required() !!} </label>
								<input type="number" name="nilai" class="form-control" placeholder="Nilai" value="{{ tmp($kualifikasi, 'nilai') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> Asosiasi {!! Template::required() !!} </label>
								<input type="text" name="asosiasi" class="form-control" placeholder="Asosiasi" value="{{ tmp($kualifikasi, 'asosiasi') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Permohonan {!! Template::required() !!} </label>
								<input type="date" name="tgl_permohonan" class="form-control" placeholder="Tanggal Permohonan" value="{{ tmp($kualifikasi, 'tgl_permohonan') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Cetak Pertama {!! Template::required() !!} </label>
								<input type="date" name="tgl_cetak_pertama" class="form-control" placeholder="Tanggal Cetak Pertama" value="{{ tmp($kualifikasi, 'tgl_cetak_pertama') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Cetak Perubahan Terakhir {!! Template::required() !!} </label>
								<input type="date" name="tgl_cetak_perubahan_terakhir" class="form-control" placeholder="Tanggal Cetak Perubahan Terakhir" value="{{ tmp($kualifikasi, 'tgl_cetak_perubahan_terakhir') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="sub_bidang_klasifikasi"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.kualifikasi.update', $kualifikasi->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				setTimeout(() => {
					window.location.href = `{{ route('p.kualifikasi') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection