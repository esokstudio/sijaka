@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }} @if($aktePendirian->isStatusWait()) ({!! $aktePendirian->statusPerubahanHtml() !!}) @endif
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> No Akte {!! Template::required() !!} </label>
								<input type="text" name="no_akte" class="form-control" placeholder="No Akte" value="{{ tmp($aktePendirian, 'no_akte') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nama Notaris {!! Template::required() !!} </label>
								<input type="text" name="nama_notaris" class="form-control" placeholder="Nama Notaris" value="{{ tmp($aktePendirian, 'nama_notaris') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Alamat </label>
								<input type="text" name="alamat" class="form-control" placeholder="Alamat" value="{{ tmp($aktePendirian, 'alamat') }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> Kota </label>
								<input type="text" name="kota" class="form-control" placeholder="Kota" value="{{ tmp($aktePendirian, 'kota') }}">
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Provinsi </label>
								<input type="text" name="provinsi" class="form-control" placeholder="Provinsi" value="{{ tmp($aktePendirian, 'provinsi') }}">
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Akte </label>
								<input type="date" name="tanggal_akte" class="form-control" placeholder="Tanggal Akte" value="{{ tmp($aktePendirian, 'tanggal_pendirian') }}">
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="no_akte"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.edit_akte_pendirian_save') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop')
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection