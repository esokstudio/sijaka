@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				@if($kualifikasi->isStatusWait())
				<div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $kualifikasi->aksiPerubahanText() }}</span>  </div>
				@endif

					<div class="table-responsive mt-3">
						
						<table class="table table-hover">
							<tr>
								<th> Perusahaan </th>
								<td> : </td>
								<td> 
									@if(auth()->user()->isAdmin())
									<a href="{{ route('perusahaan.detail', $kualifikasi->id_perusahaan) }}">
										{{ $kualifikasi->namaPerusahaan() }}
									</a>
									@else
									{{ $kualifikasi->namaPerusahaan() }}
									@endif
								</td>
								<th> Sub Bidang Kualifikasi </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'sub_bidang_klasifikasi') !!} </td>
							</tr>

							<tr>
								<th> Nomor Kode </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'nomor_kode') !!} </td>
								<th> Kualifikasi </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'kualifikasi') !!} </td>
							</tr>

							<tr>
								<th> Tahun </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'tahun') !!} </td>
								<th> Nilai </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'nilai') !!} </td>
							</tr>

							<tr>
								<th> Asosiasi </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'asosiasi') !!} </td>
								<th> Tgl Permohonan </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'tgl_permohonan') !!} </td>
							</tr>

							<tr>
								<th> Tgl Cetak Pertama </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'tgl_cetak_pertama') !!} </td>
								<th> Tgl Cetak Perubahan Terakhir </th>
								<td> : </td>
								<td> {!! tmpText($kualifikasi, 'tgl_cetak_perubahan_terakhir') !!} </td>
							</tr>
						</table>

					</div>
					

					@if($kualifikasi->isStatusWait())
					<hr>

					<div>
						<p class="text-primary font-weight-bold"> * [Perubahan] </p>
						@if(auth()->user()->isAdmin())
						<button class="btn btn-success approve-btn mb-2">
							<i class="mdi mdi-check"></i> Setujui Perubahan
						</button>
						<button class="btn btn-danger reject-btn mb-2">
							<i class="mdi mdi-close"></i> Tolak Perubahan
						</button>
						@endif
					</div>
					@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('kualifikasi_perusahaan.approve_change', $kualifikasi->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('kualifikasi_perusahaan.reject_change', $kualifikasi->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	});
</script>
@endsection