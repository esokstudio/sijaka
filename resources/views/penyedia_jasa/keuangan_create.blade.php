@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama {!! Template::required() !!} </label>
								<input type="text" name="nama" class="form-control" placeholder="Nama" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Alamat {!! Template::required() !!} </label>
								<input type="text" name="alamat" class="form-control" placeholder="Alamat" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Jumlah Saham {!! Template::required() !!} </label>
								<input type="number" name="jumlah_saham" class="form-control" placeholder="Jumlah Saham" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> Nilai Satuan Saham {!! Template::required() !!} </label>
								<input type="number" name="nilai_satuan_saham" class="form-control" placeholder="Nilai Satuan Saham" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Modal Dasar {!! Template::required() !!} </label>
								<input type="number" name="modal_dasar" class="form-control" placeholder="Modal Dasar" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Modal Disetor {!! Template::required() !!} </label>
								<input type="number" name="modal_disetor" class="form-control" placeholder="Modal Disetor" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="nama"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.keuangan.store') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				setTimeout(() => {
					window.location.href = `{{ route('p.keuangan') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection