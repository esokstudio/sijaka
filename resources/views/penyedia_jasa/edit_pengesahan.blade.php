@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }} @if($pengesahan->isStatusWait()) ({!! $pengesahan->statusPerubahanHtml() !!}) @endif
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> No Kemenkumham {!! Template::required() !!} </label>
								<input type="text" name="menkumham_no" class="form-control" placeholder="No Kemenkumham" value="{{ tmp($pengesahan, 'menkumham_no') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tgl Kemenkumham {!! Template::required() !!} </label>
								<input type="date" name="menkumham_tgl" class="form-control" placeholder="Tgl Kemenkumham" value="{{ tmp($pengesahan, 'menkumham_tgl') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> No Pengadilan Negeri {!! Template::required() !!} </label>
								<input type="text" name="pengadilan_negeri_no" class="form-control" placeholder="No Pengadilan Negeri" value="{{ tmp($pengesahan, 'pengadilan_negeri_no') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tgl Pengadilan Negeri {!! Template::required() !!} </label>
								<input type="date" name="pengadilan_negeri_tgl" class="form-control" placeholder="Tgl Pengadilan Negeri" value="{{ tmp($pengesahan, 'pengadilan_negeri_tgl') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> No Lembar Negara {!! Template::required() !!} </label>
								<input type="text" name="lembar_negara_no" class="form-control" placeholder="No Lembar Negara" value="{{ tmp($pengesahan, 'lembar_negara_no') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="form-group">
								<label> Tgl Lembar Negara {!! Template::required() !!} </label>
								<input type="date" name="lembar_negara_tgl" class="form-control" placeholder="Tgl Lembar Negara" value="{{ tmp($pengesahan, 'lembar_negara_tgl') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
			$form.find(`[name="kemenkumham_no"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.edit_pengesahan_save') }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				$submitBtn.ladda('stop')
				resetForm();
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection