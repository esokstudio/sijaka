@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('p.pengurus.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama </th>
								<th> Tgl Lahir </th>
								<th> Alamat </th>
								<th> Jabatan </th>
								<th> Pendidikan </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('p.pengurus') }}"
			},
			columns : [
				{
					data : 'nama',
					name : 'nama'
				},
				{
					data : 'tgl_lahir',
					name : 'tgl_lahir'
				},
				{
					data : 'alamat',
					name : 'alamat'
				},
				{
					data : 'jabatan',
					name : 'jabatan'
				},
				{
					data : 'pendidikan',
					name : 'pendidikan'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection