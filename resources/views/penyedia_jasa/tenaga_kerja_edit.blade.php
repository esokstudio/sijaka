@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">
							<div class="form-group">
								<label> Nama {!! Template::required() !!} </label>
								<input type="text" name="nama" class="form-control" placeholder="Nama" value="{{ tmp($tenagaKerja, 'nama') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Lahir {!! Template::required() !!} </label>
								<input type="date" name="tgl_lahir" class="form-control" placeholder="Tanggal Lahir" value="{{ tmp($tenagaKerja, 'tgl_lahir') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Pendidikan {!! Template::required() !!} </label>
								<input type="text" name="pendidikan" class="form-control" placeholder="Pendidikan" value="{{ tmp($tenagaKerja, 'pendidikan') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> No Registrasi {!! Template::required() !!} </label>
								<input type="text" name="no_registrasi" class="form-control" placeholder="No Registrasi" value="{{ tmp($tenagaKerja, 'no_registrasi') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Jenis Sertifikat {!! Template::required() !!} </label>
								<input type="text" name="jenis_sertifikat" class="form-control" placeholder="Jenis Sertifikat" value="{{ tmp($tenagaKerja, 'jenis_sertifikat') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="nama"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = $(this).serialize();
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.tenaga_kerja.update', $tenagaKerja->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				setTimeout(() => {
					window.location.href = `{{ route('p.tenaga_kerja') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection