@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('p.keuangan.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Nama </th>
								<th> Alamat </th>
								<th> Jumlah Saham </th>
								<th> Nilai Satuan Saham </th>
								<th> Modal Dasar </th>
								<th> Modal Disetor </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('p.keuangan') }}"
			},
			columns : [
				{
					data : 'nama',
					name : 'nama'
				},
				{
					data : 'alamat',
					name : 'alamat'
				},
				{
					data : 'jumlah_saham',
					name : 'jumlah_saham'
				},
				{
					data : 'nilai_satuan_saham',
					name : 'nilai_satuan_saham'
				},
				{
					data : 'modal_dasar',
					name : 'modal_dasar'
				},
				{
					data : 'modal_disetor',
					name : 'modal_disetor'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection