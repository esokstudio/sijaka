@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-12">
							<div class="form-group">
								<label> Bulan {!! Template::required() !!} </label>
								<input type="text" name="bulan" class="form-control" placeholder="Bulan" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Progress keuangan {!! Template::required() !!} </label>
								<input type="text" name="progress_keuangan" class="form-control" placeholder="Progress Keuangan" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Progress Fisik {!! Template::required() !!} </label>
								<input type="text" name="progress_fisik" class="form-control" placeholder="Progress Fisik" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Foto Progress Fisik {!! Template::required() !!} </label>
								<input type="file" name="foto" class="form-control" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			$form[0].reset()
			clearInvalid();
			$form.find(`[name="tahun_anggaran"]`).focus();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.paket_pekerjaan.realisasi.store', $paketPekerjaan->id) }}`,
				method: 'post',
				dataType: 'json',
				data: formData,
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response);
				setTimeout(() => {
					window.location.href = `{{ route('p.paket_pekerjaan.realisasi', $paketPekerjaan->id) }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		})

		resetForm();

	})

</script>
@endsection