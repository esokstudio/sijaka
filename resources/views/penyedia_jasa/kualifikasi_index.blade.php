@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('p.kualifikasi.create') }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> Sub Bidang Klasifikasi </th>
								<th> Nomor Kode </th>
								<th> Kualifikasi </th>
								<th> Tahun </th>
								<th> Aksi Perubahan </th>
								<th> Status Perubahan </th>
								<th width="70"> Aksi </th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('p.kualifikasi') }}"
			},
			columns : [
				{
					data : 'sub_bidang_klasifikasi',
					name : 'sub_bidang_klasifikasi'
				},
				{
					data : 'nomor_kode',
					name : 'nomor_kode'
				},
				{
					data : 'kualifikasi',
					name : 'kualifikasi'
				},
				{
					data : 'tahun',
					name : 'tahun'
				},
				{
					data : 'aksi_perubahan',
					name : 'aksi_perubahan'
				},
				{
					data : 'status_perubahan',
					name : 'status_perubahan'
				},
				{
					data : 'action',
					name : 'action',
					orderable : false,
					searchable : false,
				}
			],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			$('#dataTable').DataTable().ajax.reload();
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection