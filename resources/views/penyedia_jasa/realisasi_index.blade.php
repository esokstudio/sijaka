@extends('admin.layouts.templates')


@section('action')
<a class="btn btn-success" href="{{ route('p.paket_pekerjaan.realisasi.create', $paketPekerjaan->id) }}">
	<i class="mdi mdi-plus-thick"></i> Buat
</a>
@endsection


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> {{ $title }} </h6>
				
				<div class="table-responsive">
					
					<table class="table table-bordered table-hover" id="dataTable">
						<thead>
							<tr>
								<th> No </th>
								<th> Bulan </th>
								<th> Progres Keuangan </th>
								<th> Progres Fisik </th>
								<th> Foto </th>
								<!-- <th width="70"> Aksi </th> -->
							</tr>
						</thead>

						<tbody>
							@foreach($paketPekerjaan->realisasiPaketPekerjaan as $realisasi)
							<tr>
								<td> {{ $loop->iteration }} </td>
								<td> {{ $realisasi->bulan }} </td>
								<td> {{ $realisasi->progress_keuangan }} </td>
								<td> {{ $realisasi->progress_fisik }} </td>
								<td>
									<a href="{{ $realisasi->fotoLink() }}" target="_blank">
										Klik Disini
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$('#dataTable').DataTable({
			// processing : true,
			// serverSide : true,
			// autoWidth : false,
			// ajax : {
			// 	url : "{{ url('') }}"
			// },
			// columns : [
			// 	{
			// 		data : 'tahun_anggaran',
			// 		name : 'tahun_anggaran'
			// 	},
			// 	{
			// 		data : 'nama_pekerjaan',
			// 		name : 'nama_pekerjaan'
			// 	},
			// 	{
			// 		data : 'nilai_kontrak',
			// 		name : 'nilai_kontrak'
			// 	},
			// 	{
			// 		data : 'status',
			// 		name : 'status'
			// 	},
			// 	{
			// 		data : 'progress_fisik',
			// 		name : 'progress_fisik'
			// 	},
			// 	{
			// 		data : 'progress_fisik_pada',
			// 		name : 'progress_fisik_pada'
			// 	},
			// 	{
			// 		data : 'progress_keuangan',
			// 		name : 'progress_keuangan'
			// 	},
			// 	{
			// 		data : 'progress_keuangan_pada',
			// 		name : 'progress_keuangan_pada'
			// 	},
			// 	{
			// 		data : 'mulai_pada',
			// 		name : 'mulai_pada'
			// 	},
			// 	{
			// 		data : 'selesai_pada',
			// 		name : 'selesai_pada'
			// 	},
			// 	{
			// 		data : 'action',
			// 		name : 'action',
			// 		orderable : false,
			// 		searchable : false,
			// 	}
			// ],
			drawCallback : settings => {
				renderedEvent();
			}
		});


		const dtReload = () => {
			// $('#dataTable').DataTable().ajax.reload();
			window.location.reload()
		}


		const renderedEvent = () => {
			$('.delete').off('click')
			$('.delete').on('click', function(){
				let { href } = $(this).data();
				confirmation('Yakin ingin dihapus?', () => {
					ajaxSetup();
					$.ajax({
						url: href,
						method: 'delete',
						dataType: 'json'
					})
					.done(response => {
						ajaxSuccessHandling(response);
						dtReload();
					})
					.fail(error => {
						ajaxErrorHandling(error);
					})
				})
			})
		}

	})

</script>
@endsection