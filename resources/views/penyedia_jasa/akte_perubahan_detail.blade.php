@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				@if($aktePerubahan->isStatusWait())
				<div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $aktePerubahan->aksiPerubahanText() }}</span>  </div>
				@endif

					<div class="table-responsive mt-3">
						
						<table class="table table-hover">
							<tr>
								<th> Perusahaan </th>
								<td> : </td>
								<td> 
									@if(auth()->user()->isAdmin())
									<a href="{{ route('perusahaan.detail', $aktePerubahan->id_perusahaan) }}">
										{{ $aktePerubahan->namaPerusahaan() }}
									</a>
									@else
									{{ $aktePerubahan->namaPerusahaan() }}
									@endif
								</td>
								<th> No Akte </th>
								<td> : </td>
								<td> {!! tmpText($aktePerubahan, 'no_akte') !!} </td>
							</tr>

							<tr>
								<th> Nama Notaris </th>
								<td> : </td>
								<td> {!! tmpText($aktePerubahan, 'nama_notaris') !!} </td>
								<th> Alamat </th>
								<td> : </td>
								<td> {!! tmpText($aktePerubahan, 'alamat') !!} </td>
							</tr>

							<tr>
								<th> Kota </th>
								<td> : </td>
								<td> {!! tmpText($aktePerubahan, 'kota') !!} </td>
								<th> Provinsi </th>
								<td> : </td>
								<td> {!! tmpText($aktePerubahan, 'provinsi') !!} </td>
							</tr>
						</table>

					</div>
					

					@if($aktePerubahan->isStatusWait())
					<hr>

					<div>
						<p class="text-primary font-weight-bold"> * [Perubahan] </p>
						@if(auth()->user()->isAdmin())
						<button class="btn btn-success approve-btn mb-2">
							<i class="mdi mdi-check"></i> Setujui Perubahan
						</button>
						<button class="btn btn-danger reject-btn mb-2">
							<i class="mdi mdi-close"></i> Tolak Perubahan
						</button>
						@endif
					</div>
					@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('akte_perubahan_perusahaan.approve_change', $aktePerubahan->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('akte_perubahan_perusahaan.reject_change', $aktePerubahan->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	});
</script>
@endsection