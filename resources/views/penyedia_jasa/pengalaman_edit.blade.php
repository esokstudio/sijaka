@extends('admin.layouts.templates')


@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h6 class="card-title"> 
					{{ $title }}
				</h6>
				
				<form id="form">

					{!! Template::requiredBanner() !!}
					
					<div class="row">
						
						<div class="col-lg-6">

							<div class="form-group">
								<label> Nama Pekerjaan {!! Template::required() !!} </label>
								<input type="text" name="nama_pekerjaan" class="form-control" placeholder="Nama Pekerjaan" value="{{ tmp($pengalaman, 'nama_pekerjaan') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Lokasi Pekerjaan {!! Template::required() !!} </label>
								<input type="text" name="lokasi_pekerjaan" class="form-control" placeholder="Lokasi Pekerjaan" value="{{ tmp($pengalaman, 'lokasi_pekerjaan') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nomor Kontrak {!! Template::required() !!} </label>
								<input type="text" name="nomor_kontrak" class="form-control" placeholder="Nomor Kontrak" value="{{ tmp($pengalaman, 'nomor_kontrak') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Tanggal Kontrak {!! Template::required() !!} </label>
								<input type="date" name="tanggal_kontrak" class="form-control" value="{{ tmp($pengalaman, 'tanggal_kontrak') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Pagu Anggaran {!! Template::required() !!} </label>
								<input type="number" name="pagu_anggaran" class="form-control" placeholder="Pagu Anggaran" value="{{ tmp($pengalaman, 'pagu_anggaran') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Nilai Kontrak {!! Template::required() !!} </label>
								<input type="number" name="nilai_kontrak" class="form-control" placeholder="Nilai Kontrak" value="{{ tmp($pengalaman, 'nilai_kontrak') }}" required>
								<span class="invalid-feedback"></span>
							</div>
						</div>


						<div class="col-lg-6">
							<div class="form-group">
								<label> Nilai Volume {!! Template::required() !!} </label>
								<input type="number" name="nilai_target" class="form-control" placeholder="Nilai Volume" value="{{ tmp($pengalaman, 'nilai_target') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Satuan Volume {!! Template::required() !!} </label>
								<input type="text" name="satuan_target" class="form-control" placeholder="Satuan Volume" value="{{ tmp($pengalaman, 'satuan_target') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Awal Pelaksanaan {!! Template::required() !!} </label>
								<input type="date" name="awal_pelaksanaan" class="form-control" value="{{ tmp($pengalaman, 'awal_pelaksanaan') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> Akhir Pelaksanaan {!! Template::required() !!} </label>
								<input type="date" name="akhir_pelaksanaan" class="form-control" value="{{ tmp($pengalaman, 'akhir_pelaksanaan') }}" required>
								<span class="invalid-feedback"></span>
							</div>

							<div class="form-group">
								<label> File Bukti Fisik Pengalaman </label>
								<input type="file" name="file_upload_bukti_fisik_pengalaman" class="form-control">
								<span class="invalid-feedback"></span>
							</div>
						</div>

					</div>

					<hr>

					<button type="submit" class="btn btn-success">
						<i class="mdi mdi-check"></i> Simpan
					</button>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){

		$form = $('#form');
		$submitBtn = $form.find(`[type="submit"]`).ladda();

		const resetForm = () => {
			// $form[0].reset()
			clearInvalid();
		}

		$form.on('submit', function(e){
			e.preventDefault();
			clearInvalid();

			let formData = new FormData(this);
			$submitBtn.ladda('start')

			ajaxSetup();
			$.ajax({
				url: `{{ route('p.pengalaman.update', $pengalaman->id) }}`,
				method: 'put',
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false
			})
			.done(response => {
				$submitBtn.ladda('stop')
				ajaxSuccessHandling(response);
			})
			.fail(error => {
				$submitBtn.ladda('stop')
				ajaxErrorHandling(error, $form)
			})
		})

		resetForm();

	})

</script>
@endsection