@extends('admin.layouts.templates')

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="card-title h6 font-weight-bold"> {{ $title }} </div>

        @if($pengalaman->isStatusWait())
        <div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $pengalaman->aksiPerubahanText() }}</span>  </div>
        @endif

          <div class="table-responsive mt-3">
            
            <table class="table table-hover">
              <tr>
                <th> Perusahaan </th>
                <td> : </td>
                <td> 
                  @if(auth()->user()->isAdmin())
                  <a href="{{ route('perusahaan.detail', $pengalaman->id_perusahaan) }}">
                    {{ $pengalaman->namaPerusahaan() }}
                  </a>
                  @else
                  {{ $pengalaman->namaPerusahaan() }}
                  @endif
                </td>
                <th> Nama Pekerjaan </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'nama_pekerjaan') !!} </td>
              </tr>

              <tr>
                <th> Lokasi Pekerjaan </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'lokasi_pekerjaan') !!} </td>
                <th> Nomor Kontrak </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'nomor_kontrak') !!} </td>
              </tr>

              <tr>
                <th> Tgl Kontrak </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'tanggal_kontrak') !!} </td>
                <th> Pagu Anggaran </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'pagu_anggaran') !!} </td>
              </tr>

              <tr>
                <th> Nilai Kontrak </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'nilai_kontrak') !!} </td>
                <th> Nilai Target </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'nilai_target') !!} </td>
              </tr>

              <tr>
                <th> Satuan Target </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'satuan_target') !!} </td>
                <th> Awal Pelaksanaan </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'awal_pelaksanaan') !!} </td>
              </tr>

              <tr>
                <th> Akhir Pelaksanaan </th>
                <td> : </td>
                <td> {!! tmpText($pengalaman, 'akhir_pelaksanaan') !!} </td>
                <th> File Bukti Pengalaman Kerja </th>
                <td> : </td>
                <td>
                  <a href="javascript:void(0);">
                    Klik Disini
                  </a>
                </td>
              </tr>
            </table>

          </div>
          

          @if($pengalaman->isStatusWait())
          <hr>

          <div>
            <p class="text-primary font-weight-bold"> * [Perubahan] </p>
            @if(auth()->user()->isAdmin())
            <button class="btn btn-success approve-btn mb-2">
              <i class="mdi mdi-check"></i> Setujui Perubahan
            </button>
            <button class="btn btn-danger reject-btn mb-2">
              <i class="mdi mdi-close"></i> Tolak Perubahan
            </button>
            @endif
          </div>
          @endif

      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
  
  $(function(){


    $('.approve-btn').on('click', function(){
      confirmation('Yakin ingin menyetujui perubahan?', () => {
        $(this).ladda();
        $(this).ladda('start');
        ajaxSetup()
        $.ajax({
          url: `{{ route('pengalaman_perusahaan.approve_change', $pengalaman->id) }}`,
          dataType: 'json',
          method: 'post'
        })
        .done(response => {
          ajaxSuccessHandling(response);
          setTimeout(() => {
            window.location.reload();
          }, 1000)
        })
        .fail(error => {
          $(this).ladda('stop');
          ajaxErrorHandling(error)
        })
      })
    })


    $('.reject-btn').on('click', function(){
      confirmation('Yakin ingin menolak perubahan?', () => {
        $(this).ladda();
        $(this).ladda('start');
        ajaxSetup()
        $.ajax({
          url: `{{ route('pengalaman_perusahaan.reject_change', $pengalaman->id) }}`,
          dataType: 'json',
          method: 'post'
        })
        .done(response => {
          ajaxSuccessHandling(response);
          setTimeout(() => {
            window.location.reload();
          }, 1000)
        })
        .fail(error => {
          $(this).ladda('stop');
          ajaxErrorHandling(error)
        })
      })
    })

  });
</script>
@endsection