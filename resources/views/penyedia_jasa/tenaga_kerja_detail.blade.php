@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				@if($tenagaKerja->isStatusWait())
				<div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $tenagaKerja->aksiPerubahanText() }}</span>  </div>
				@endif

					<div class="table-responsive mt-3">
						
						<table class="table table-hover">
							<tr>
								<th> Perusahaan </th>
								<td> : </td>
								<td> 
									@if(auth()->user()->isAdmin())
									<a href="{{ route('perusahaan.detail', $tenagaKerja->id_perusahaan) }}">
										{{ $tenagaKerja->namaPerusahaan() }}
									</a>
									@else
									{{ $tenagaKerja->namaPerusahaan() }}
									@endif
								</td>
								<th> Nama </th>
								<td> : </td>
								<td> {!! tmpText($tenagaKerja, 'nama') !!} </td>
							</tr>

							<tr>
								<th> Tgl Lahir </th>
								<td> : </td>
								<td> {!! tmpText($tenagaKerja, 'tgl_lahir') !!} </td>
								<th> Pendidikan </th>
								<td> : </td>
								<td> {!! tmpText($tenagaKerja, 'pendidikan') !!} </td>
							</tr>

							<tr>
								<th> No Registrasi </th>
								<td> : </td>
								<td> {!! tmpText($tenagaKerja, 'no_registrasi') !!} </td>
								<th> Jenis Sertifikat </th>
								<td> : </td>
								<td> {!! tmpText($tenagaKerja, 'jenis_sertifikat') !!} </td>
							</tr>
						</table>

					</div>
					

					@if($tenagaKerja->isStatusWait())
					<hr>

					<div>
						<p class="text-primary font-weight-bold"> * [Perubahan] </p>
						@if(auth()->user()->isAdmin())
						<button class="btn btn-success approve-btn mb-2">
							<i class="mdi mdi-check"></i> Setujui Perubahan
						</button>
						<button class="btn btn-danger reject-btn mb-2">
							<i class="mdi mdi-close"></i> Tolak Perubahan
						</button>
						@endif
					</div>
					@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('tenaga_kerja_perusahaan.approve_change', $tenagaKerja->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('tenaga_kerja_perusahaan.reject_change', $tenagaKerja->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	});
</script>
@endsection