@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				@if($keuangan->isStatusWait())
				<div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $keuangan->aksiPerubahanText() }}</span>  </div>
				@endif

					<div class="table-responsive mt-3">
						
						<table class="table table-hover">
							<tr>
								<th> Perusahaan </th>
								<td> : </td>
								<td> 
									@if(auth()->user()->isAdmin())
									<a href="{{ route('perusahaan.detail', $keuangan->id_perusahaan) }}">
										{{ $keuangan->namaPerusahaan() }}
									</a>
									@else
									{{ $keuangan->namaPerusahaan() }}
									@endif
								</td>
								<th> Nama </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'nama') !!} </td>
							</tr>

							<tr>
								<th> Alamat </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'alamat') !!} </td>
								<th> Jumlah Saham </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'jumlah_saham') !!} </td>
							</tr>

							<tr>
								<th> Nilai Satuan Saham </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'nilai_satuan_saham') !!} </td>
								<th> Modal Dasar </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'modal_dasar') !!} </td>
							</tr>

							<tr>
								<th> Modal Disetor </th>
								<td> : </td>
								<td> {!! tmpText($keuangan, 'modal_disetor') !!} </td>
								<th> </th>
								<td> </td>
								<td> </td>
							</tr>
						</table>

					</div>
					

					@if($keuangan->isStatusWait())
					<hr>

					<div>
						<p class="text-primary font-weight-bold"> * [Perubahan] </p>
						@if(auth()->user()->isAdmin())
						<button class="btn btn-success approve-btn mb-2">
							<i class="mdi mdi-check"></i> Setujui Perubahan
						</button>
						<button class="btn btn-danger reject-btn mb-2">
							<i class="mdi mdi-close"></i> Tolak Perubahan
						</button>
						@endif
					</div>
					@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('keuangan_perusahaan.approve_change', $keuangan->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('keuangan_perusahaan.reject_change', $keuangan->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	});
</script>
@endsection