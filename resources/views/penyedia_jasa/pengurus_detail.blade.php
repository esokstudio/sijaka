@extends('admin.layouts.templates')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<div class="card-title h6 font-weight-bold"> {{ $title }} </div>

				@if($pengurus->isStatusWait())
				<div class="mt-3 text-right"> Aksi Perubahan : <span class="font-weight-bold">{{ $pengurus->aksiPerubahanText() }}</span>  </div>
				@endif

					<div class="table-responsive mt-3">
						
						<table class="table table-hover">
							<tr>
								<th> Perusahaan </th>
								<td> : </td>
								<td> 
									@if(auth()->user()->isAdmin())
									<a href="{{ route('perusahaan.detail', $pengurus->id_perusahaan) }}">
										{{ $pengurus->namaPerusahaan() }}
									</a>
									@else
									{{ $pengurus->namaPerusahaan() }}
									@endif
								</td>
								<th> Nama </th>
								<td> : </td>
								<td> {!! tmpText($pengurus, 'nama') !!} </td>
							</tr>

							<tr>
								<th> Tanggal Lahir </th>
								<td> : </td>
								<td> {!! tmpText($pengurus, 'tgl_lahir') !!} </td>
								<th> Alamat </th>
								<td> : </td>
								<td> {!! tmpText($pengurus, 'alamat') !!} </td>
							</tr>

							<tr>
								<th> Jabatan </th>
								<td> : </td>
								<td> {!! tmpText($pengurus, 'jabatan') !!} </td>
								<th> Pendidikan </th>
								<td> : </td>
								<td> {!! tmpText($pengurus, 'pendidikan') !!} </td>
							</tr>
						</table>

					</div>
					

					@if($pengurus->isStatusWait())
					<hr>

					<div>
						<p class="text-primary font-weight-bold"> * [Perubahan] </p>
						@if(auth()->user()->isAdmin())
						<button class="btn btn-success approve-btn mb-2">
							<i class="mdi mdi-check"></i> Setujui Perubahan
						</button>
						<button class="btn btn-danger reject-btn mb-2">
							<i class="mdi mdi-close"></i> Tolak Perubahan
						</button>
						@endif
					</div>
					@endif

			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){


		$('.approve-btn').on('click', function(){
			confirmation('Yakin ingin menyetujui perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('pengurus_perusahaan.approve_change', $pengurus->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})


		$('.reject-btn').on('click', function(){
			confirmation('Yakin ingin menolak perubahan?', () => {
				$(this).ladda();
				$(this).ladda('start');
				ajaxSetup()
				$.ajax({
					url: `{{ route('pengurus_perusahaan.reject_change', $pengurus->id) }}`,
					dataType: 'json',
					method: 'post'
				})
				.done(response => {
					ajaxSuccessHandling(response);
					setTimeout(() => {
						window.location.reload();
					}, 1000)
				})
				.fail(error => {
					$(this).ladda('stop');
					ajaxErrorHandling(error)
				})
			})
		})

	});
</script>
@endsection