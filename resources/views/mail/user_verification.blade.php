<!DOCTYPE html>
<html>
<head>
	<title> Invoice </title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel="stylesheet" href="{{ url('css/custom/invoice-mail.css') }}">
	<style type="text/css">
		.wrapper {

		}

		.header {
			background: #fbc769;
			padding: 15px;
		}

		.logo {
			position: absolute;
			top: 0px;
			left: 0px;
			width: 100px !important;
			height: auto;
		}

		.brand * {
			text-align: center;
			color: black;
			margin-bottom: 10px !important;
		}

		.table {
			width: 100%;
			border-collapse: collapse;
		}

		.table td,
		.table th {
			padding: 5px;
			text-align: center;
		}
		.status-text {
			display: inline; 
			padding: 20px; 
			border: 2px solid red;
			color: red;
			font-weight: 700;
			text-transform: uppercase;
		}

		.status-success {
			border-color: green !important;
			color: green !important;
		}
	</style>
</head>
<body>

	<div class="wrapper">
		
		<center>
			<img src="https://sijakda.putr.cirebonkab.go.id/web-asset/img/pemda-kabcirebon.png" class="logo">
		</center>
		<h1 align="center"> Verifikasi User </h1>

		<hr>

		<p>
			Anda telah mendaftarkan email anda untuk membuat akun di Web Sijakda Kabupaten Cirebon. Silahkan klik tombol di bawah untuk verifikasi akun anda agar dapat login. Abaikan email ini jika anda merasa tidak melakukan pendaftaran akun.
		</p>

		<center>
			<a href="{{ route('website.user_verification', $user->verification_code) }}" style="padding:15px 2rem;background:#deae22;border-radius:30px;font-weight:bold;color:white">
				Verifikasi Akun
			</a>
		</center>
		<br><br>

		<hr>

		<p> Klik link dibawah jika tombol verifikasi tidak berfungsi. </p>

		<a href="{{ route('website.user_verification', $user->verification_code) }}">{{ route('website.user_verification', $user->verification_code) }}</a>

	</div>
</body>
</html>