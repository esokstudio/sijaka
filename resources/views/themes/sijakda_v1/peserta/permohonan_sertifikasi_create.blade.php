@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('permohonan-sertifikasi') }}"> Permohonan Sertifikasi </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Buat </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Buat Permohonan Sertifikasi </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-lg-6 col-lg-offset-3">

					<form id="form">
						
						<div class="form-group">
							<label> Nama Pemohon </label>
							<input type="text" name="nama_pemohon" class="form-control" placeholder="Nama Pemohon" value="{{ auth()->user()->name }}" readonly required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Pribadi / Perusahaan </label>
							<select class="form-control" name="tipe" required>
								<option value="pribadi"> Pribadi </option>
								<option value="perusahaan"> Perusahaan </option>
							</select>
						</div>

						<div id="input-nama-perusahaan"></div>

						<div class="form-group">
							<label> Klasifikasi </label>
							<select class="form-control" name="klasifikasi" required>
								<option value="Arsitektur"> Arsitektur </option>
								<option value="Sipil"> Sipil </option>
								<option value="Mekanikal"> Mekanikal </option>
								<option value="Elektrikal"> Elektrikal </option>
								<option value="Tata Lingkungan"> Tata Lingkungan </option>
								<option value="Lain-lain"> Lain-lain </option>
							</select>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Sub Klasifikasi </label>
							<input type="text" name="sub_klasifikasi" class="form-control" placeholder="Sub Klasifikasi" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Kualifikasi </label>
							<select class="form-control" name="kualifikasi">
								<option value="Kelas 1"> Kelas 1 </option>
								<option value="Kelas 2"> Kelas 2 </option>
								<option value="Kelas 3"> Kelas 3 </option>
							</select>
							<small class="invalid-feedback"></small>
						</div>

						<input type="hidden" name="tahun_anggaran" value="{{ date('Y') }}">
						
						<div class="form-group">
							<label> Nomor Whatsapp </label>
							<input type="text" name="nomor_telepon" class="form-control" placeholder="Nomor Whatsapp" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Email </label>
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{ auth()->user()->email }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> File Scan Ijazah </label>
							<input type="file" name="file_ijazah" class="form-control" accept=".docx, .pdf, .doc" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> File Scan Permohonan Sertifikasi </label>
							<input type="file" name="file_surat_permohonan" class="form-control" accept=".docx, .pdf, .doc" required>
							<p class="text-info">
								Download template berkas dengan klik <a href="{{ url('docs/surat_permohonan.docx') }}" download> Disini </a>
							</p>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> File Scan KTP </label>
							<input type="file" name="file_ktp" class="form-control" accept=".docx, .pdf, .doc, image/*" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> File Surat Pernyataan Kebenaran Data Dokumen </label>
							<input type="file" name="file_pernyataan_kebenaran_data" class="form-control" accept=".docx, .pdf, .doc" required>
							<p class="text-info">
								Download template berkas dengan klik <a href="{{ url('docs/surat_pernyataan_kebenaran_data.docx') }}" download> Disini </a>
							</p>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> File Pas Foto </label>
							<input type="file" name="file_pas_foto" class="form-control" accept="image/*" required>
							<small class="invalid-feedback"></small>
						</div>

						<button type="submit" class="btn btn-success">
							Buat
						</button>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$form = $('#form')
		$submitBtn = $form.find(`[type="submit"]`).ladda()

		$form.on('submit', function(e){
			clearInvalid()
			e.preventDefault();
			$submitBtn.ladda('start')

			let formData = new FormData(this);

			ajaxSetup();
			$.ajax({
				url: `{{ route('website.permohonan_sertifikasi.store') }}`,
				data: formData,
				dataType: 'json',
				method: 'post',
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response)
				$form[0].reset()

				setTimeout(() => {
					window.location.href = `{{ route('website.permohonan_sertifikasi') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		});

		$form.find(`[name="tipe"]`).on('change', function(){
			let tipe = $(this).val();

			if(tipe == 'perusahaan') {
				$('#input-nama-perusahaan').html($('#namaPerusahaanTemplate').text())
			} else {
				$('#input-nama-perusahaan').html('')
			}
		})
	})

</script>

<script type="text/html" id="namaPerusahaanTemplate">
	<div class="form-group">
		<label> Nama Perusahaan </label>
		<input type="text" name="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" required>
	</div>
</script>
@endsection