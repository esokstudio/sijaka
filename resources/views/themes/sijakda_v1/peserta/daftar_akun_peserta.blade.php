@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('daftar-akun-peserta') }}" class="active"> Daftar Akun Peserta </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Daftar Akun Peserta </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-lg-6 col-lg-offset-3">

					<h2> Daftar </h2>

					<form id="form">
						
						<div class="form-group">
							<label> Nama Lengkap </label>
							<input type="text" name="name" class="form-control" placeholder="Nama Lengkap" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Email </label>
							<input type="email" name="email" class="form-control" placeholder="Email" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Password </label>
							<input type="password" name="password" class="form-control" placeholder="Password" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Konfirmasi Password </label>
							<input type="password" name="confirm_password" class="form-control" placeholder="Konfirmasi Password" required>
							<small class="invalid-feedback"></small>
						</div>

						<button type="submit" class="btn btn-success">
							Daftar
						</button>

						<hr>

						<p align="center">
							Telah memiliki akun? 
							<a href="{{ route('website.login_akun_peserta') }}">
								Login disini
							</a>
						</p>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$form = $('#form')
		$submitBtn = $form.find(`[type="submit"]`).ladda()

		$form.on('submit', function(e){
			clearInvalid()
			e.preventDefault();
			$submitBtn.ladda('start')

			let formData = $(this).serialize();

			ajaxSetup();
			$.ajax({
				url: `{{ route('website.daftar_akun_peserta_save') }}`,
				data: formData,
				dataType: 'json',
				method: 'post'
			})
			.done(response => {
				ajaxSuccessHandling(response)
				setTimeout(() => {
					$submitBtn.ladda('stop')
					window.location.href = `{{ route('website.login_akun_peserta') }}`
				}, 1000)
				// $form[0].reset()
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		});
	})

</script>
@endsection