@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('pendidikan-peserta') }}"> Pendidikan Peserta </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Tambah </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Tambah Pendidikan Peserta </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-lg-6 col-lg-offset-3">
					<form id="form">
						
						<div class="form-group">
							<label> Jenjang Pendidikan </label>
							<input type="text" name="jenjang_pendidikan" class="form-control" placeholder="Jenjang Pendidikan" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Nama Sekolah </label>
							<input type="text" name="nama_sekolah" class="form-control" placeholder="Nama Sekolah" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Jurusan </label>
							<input type="text" name="jurusan" class="form-control" placeholder="Jurusan" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Kota / Kab </label>
							<input type="text" name="kota" class="form-control" placeholder="Kota" required>
							<small class="invalid-feedback"></small>
						</div>
						
						<div class="form-group">
							<label> Tahun Lulus </label>
							<input type="number" name="tahun_lulus" class="form-control" placeholder="Tahun Lulus" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> No Ijazah </label>
							<input type="text" name="no_ijazah" class="form-control" placeholder="No Ijazah" required>
							<small class="invalid-feedback"></small>
						</div>

						<button type="submit" class="btn btn-success">
							Buat
						</button>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$form = $('#form')
		$submitBtn = $form.find(`[type="submit"]`).ladda()

		$form.on('submit', function(e){
			clearInvalid()
			e.preventDefault();
			$submitBtn.ladda('start')

			let formData = new FormData(this);

			ajaxSetup();
			$.ajax({
				url: `{{ route('website.pendidikan_peserta.store') }}`,
				data: formData,
				dataType: 'json',
				method: 'post',
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response)
				$form[0].reset()

				setTimeout(() => {
					window.location.href = `{{ route('website.pendidikan_peserta') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		});
	})

</script>
@endsection