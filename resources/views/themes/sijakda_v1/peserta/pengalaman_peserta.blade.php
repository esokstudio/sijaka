@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Pengalaman Kerja </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
				</ul>

				<h1 class="section-header"><b> Pengalaman Kerja </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-md-10 col-md-offset-1">

					<div class="text-right" style="margin-bottom: 1rem;">
						<a href="{{ route('website.pengalaman_peserta.create') }}" class="btn btn-success">
							Tambah
						</a>
					</div>

					<div class="box-body table-responsive no-padding">
						<table class="table table-striped table-custom table-bordered" id="dataTable">
							<thead>
								<tr>
									<th> Nama Pekerjaan </th>
									<th> Lokasi </th>
									<th> Nilai Kontrak </th>
									<th> Mulai </th>
									<th> Selesai </th>
									<th> Jabatan </th>
									<th> Aksi </th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('website.pengalaman_peserta') }}"
			},
			columns : [
				{
					data : 'nama_pekerjaan',
					name : 'nama_pekerjaan'
				},
				{
					data : 'lokasi_pekerjaan',
					name : 'lokasi_pekerjaan'
				},
				{
					data : 'nilai_kontrak',
					name : 'nilai_kontrak'
				},
				{
					data : 'mulai',
					name : 'mulai'
				},
				{
					data : 'selesai',
					name : 'selesai'
				},
				{
					data : 'jabatan',
					name : 'jabatan'
				},
				{
					data : 'peserta_action',
					name : 'peserta_action',
					orderable : false,
					searchable : false,
				}
			],
		})
	})

</script>
@endsection