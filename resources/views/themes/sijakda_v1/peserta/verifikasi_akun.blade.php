@extends('themes.sijakda_v1.layouts')


@section('styles')
<style type="text/css">
    .message-box {
        background-color: white;
        margin: 3rem 5rem;
        padding: 3rem;
        text-align: center;
    }

    .icon {
            width: 100px;
            height: 100px;
    }

    @media screen and (max-width: 567px) {
        .message-box {
            margin: 1rem 1.5rem;
        }
    }
</style>
@endsection


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb-custom">
                    <li>
                        <a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="active"> Verifikasi Email </a>
                    </li>
                </ul>

                <h1 class="section-header"><b> Verifikasi Email </b></h1>
                <br>
            </div>
        </div>
    </div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
    <div class="container">
        <div class="row">
            <div class="blog-items">
                <div class="blog-content col-lg-6 col-lg-offset-3">

                    <div class="message-box">
                        @if($isSuccess)
                        <img src="{{ url('images/checklist.png') }}" alt="" class="icon">
                        @else
                        <img src="{{ url('images/close.png') }}" alt="" class="icon">
                        @endif
                        <br>
                        @if($isSuccess)
                        <h1> Berhasil </h1>
                        @else
                        <h1> Gagal </h1>
                        @endif
                        <p> {{ $message }} </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection