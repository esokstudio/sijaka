@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area shadow dark bg-fixed text-center text-light"
	style="background-image: url(https://sipjaki.pu.go.id/img/banner/4.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<h1> Permohonan Sertifikasi </h1>
				<ul class="breadcrumb">
					<li><a href="{{ url('/') }}"><i class="fas fa-home"></i> Home</a></li>
					<li><a href="{{ route('website.dashboard_peserta') }}"> Dashboard</a></li>
					<li class="active"> Permohonan Sertifikasi </li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Permohonan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Permohonan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-md-10 col-md-offset-1">

					<div class="text-right">
						<a href="{{ route('website.permohonan_sertifikasi.create') }}" class="btn btn-success">
							Buat Permohonan
						</a>
						<button class="btn btn-danger" onclick="$('#logout-form').submit()">
							Logout
						</button>
					</div>

					<hr>

					<div class="box-body table-responsive no-padding">
						<table class="table table-striped" id="dataTable">
							<thead>
								<tr>
									<th> Tgl Pengajuan </th>
									<th> Pemohon </th>
									<th> Perusahaan </th>
									<th> Klasifikasi </th>
									<th> Sub Klasifikasi </th>
									<th> Kualifikasi </th>
									<th> Status </th>
									<th> Catatan </th>
									<!-- <th> Aksi </th> -->
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('website.permohonan_sertifikasi') }}"
			},
			columns : [
				{
					data : 'created_at',
					name : 'created_at'
				},
				{
					data : 'nama_pemohon',
					name : 'nama_pemohon'
				},
				{
					data : 'nama_perusahaan',
					name : 'nama_perusahaan'
				},
				{
					data : 'klasifikasi',
					name : 'klasifikasi'
				},
				{
					data : 'sub_klasifikasi',
					name : 'sub_klasifikasi'
				},
				{
					data : 'kualifikasi',
					name : 'kualifikasi'
				},
				{
					data : 'status',
					name : 'status'
				},
				{
					data : 'tindak_lanjut',
					name : 'tindak_lanjut'
				},
				// {
				// 	data : 'peserta_action',
				// 	name : 'peserta_action',
				// 	orderable : false,
				// 	searchable : false,
				// }
			],
		})
	})

</script>
@endsection