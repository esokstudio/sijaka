@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Edit Profil </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Edit Profil </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-lg-6 col-lg-offset-3">
					<form id="form">
						
						<div class="form-group">
							<label> Nama Peserta </label>
							<input type="text" name="nama_peserta" class="form-control" placeholder="Jenjang Pendidikan" value="{{ $profil->nama_peserta }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Tempat Lahir </label>
							<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="{{ $profil->tempat_lahir }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Tanggal Lahir </label>
							<input type="date" name="tgl_lahir" class="form-control" placeholder="Tanggal Lahir" value="{{ $profil->tgl_lahir }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Kelurahan </label>
							<input type="text" name="kelurahan" class="form-control" placeholder="Kelurahan" value="{{ $profil->kelurahan }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Kecamatan </label>
							<input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" value="{{ $profil->kecamatan }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Kota / Kab </label>
							<input type="text" name="kota" class="form-control" placeholder="Kota" value="{{ $profil->kota }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> No KTP </label>
							<input type="text" name="no_ktp" class="form-control" placeholder="No KTP" value="{{ $profil->no_ktp }}" required>
							<small class="invalid-feedback"></small>
						</div>

						<button type="submit" class="btn btn-success">
							Update
						</button>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$form = $('#form')
		$submitBtn = $form.find(`[type="submit"]`).ladda()

		$form.on('submit', function(e){
			clearInvalid()
			e.preventDefault();
			$submitBtn.ladda('start')

			let formData = new FormData(this);

			ajaxSetup();
			$.ajax({
				url: `{{ route('website.profil_peserta.save') }}`,
				data: formData,
				dataType: 'json',
				method: 'post',
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response)
				$submitBtn.ladda('stop')
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		});
	})

</script>
@endsection