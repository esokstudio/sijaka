@extends('themes.sijakda_v1.layouts')


@section('styles')
<style type="text/css">
	a.menu-peserta-link:hover {
		background-color: lightgray;
	}
	a.menu-peserta-link {
		display: block;
		text-align: center;
		padding: 15px;
		font-size: 14pt;
		border-style: solid;
		border-color: lightgray;
		border-width: 1px 0px 1px 0px;
		transition: .1s;
	}
</style>
@endsection


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('dashboard-peserta') }}" class="active"> Dashboard Peserta </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Dashboard Peserta </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-md-10 col-md-offset-1">

					<div class="text-right">
						<button class="btn btn-danger" onclick="$('#logout-form').submit()">
							Logout
						</button>
					</div>

					<hr>

					<a class="menu-peserta-link" href="{{ route('website.profil_peserta') }}">
						<i class="fa fa-user"></i> Edit Profil
					</a>

					<a class="menu-peserta-link" href="{{ route('website.pendidikan_peserta') }}">
						<i class="fa fa-graduation-cap"></i> Edit Pendidikan
						<span class="badge ml-2">
							{{ auth()->user()->countPendidikanPeserta() }}
						</span>
					</a>

					<a class="menu-peserta-link" href="{{ route('website.pengalaman_peserta') }}">
						<i class="fa fa-user-tie"></i> Edit Pengalaman Kerja
						<span class="badge ml-2">
							{{ auth()->user()->countPengalamanPeserta() }}
						</span>
					</a>

					<a class="menu-peserta-link" href="{{ route('website.permohonan_sertifikasi') }}">
						<i class="fa fa-check"></i> Permohonan Sertifikasi
						<span class="badge ml-2">
							{{ auth()->user()->countPermohonanSertifikasi() }}
						</span>
					</a>

					<a class="menu-peserta-link" href="{{ route('website.permohonan_sertifikasi.create') }}">
						<i class="fa fa-edit"></i> Buat Permohonan Sertifikasi
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('website.permohonan_sertifikasi') }}"
			},
			columns : [
				{
					data : 'created_at',
					name : 'created_at'
				},
				{
					data : 'nama_pemohon',
					name : 'nama_pemohon'
				},
				{
					data : 'instansi',
					name : 'instansi'
				},
				{
					data : 'klasifikasi',
					name : 'klasifikasi'
				},
				{
					data : 'sub_klasifikasi',
					name : 'sub_klasifikasi'
				},
				{
					data : 'kualifikasi',
					name : 'kualifikasi'
				},
			],
		})
	})

</script>
@endsection