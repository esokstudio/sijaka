@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Pendidikan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Pendidikan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-md-10 col-md-offset-1">

					<div class="text-right" style="margin-bottom: 1rem;">
						<a href="{{ route('website.pendidikan_peserta.create') }}" class="btn btn-success">
							Tambah
						</a>
					</div>

					<div class="box-body table-responsive no-padding">
						<table class="table table-striped table-custom table-bordered" id="dataTable">
							<thead>
								<tr>
									<th> Jenjang Pendidikan </th>
									<th> Nama Sekolah </th>
									<th> Jurusan </th>
									<th> Kota </th>
									<th> Tahun Lulus </th>
									<th> No Ijazah </th>
									<th> Aksi </th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable({
			processing : true,
			serverSide : true,
			autoWidth : false,
			ajax : {
				url : "{{ route('website.pendidikan_peserta') }}"
			},
			columns : [
				{
					data : 'jenjang_pendidikan',
					name : 'jenjang_pendidikan'
				},
				{
					data : 'nama_sekolah',
					name : 'nama_sekolah'
				},
				{
					data : 'jurusan',
					name : 'jurusan'
				},
				{
					data : 'kota',
					name : 'kota'
				},
				{
					data : 'tahun_lulus',
					name : 'tahun_lulus'
				},
				{
					data : 'no_ijazah',
					name : 'no_ijazah'
				},
				{
					data : 'peserta_action',
					name : 'peserta_action',
					orderable : false,
					searchable : false,
				}
			],
		})
	})

</script>
@endsection