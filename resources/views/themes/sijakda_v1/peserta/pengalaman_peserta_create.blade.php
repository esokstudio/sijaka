@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.dashboard_peserta') }}"> Dashboard </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('pengalaman-peserta') }}"> Pengalaman Peserta </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);" class="active"> Tambah </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Tambah Pengalaman Peserta </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-lg-6 col-lg-offset-3">

					<form id="form">
						
						<div class="form-group">
							<label> Nama Pekerjaan </label>
							<input type="text" name="nama_pekerjaan" class="form-control" placeholder="Nama Pekerjaan" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Lokasi Pekerjaan </label>
							<input type="text" name="lokasi_pekerjaan" class="form-control" placeholder="Lokasi Pekerjaan" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Nilai Kontrak </label>
							<input type="number" name="nilai_kontrak" class="form-control" placeholder="Nilai Kontrak" min="0" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Mulai </label>
							<input type="date" name="mulai" class="form-control" required>
							<small class="invalid-feedback"></small>
						</div>
						
						<div class="form-group">
							<label> Selesai </label>
							<input type="date" name="selesai" class="form-control" required>
							<small class="invalid-feedback"></small>
						</div>

						<div class="form-group">
							<label> Jabatan </label>
							<input type="text" name="jabatan" class="form-control" placeholder="Jabatan" required>
							<small class="invalid-feedback"></small>
						</div>

						<button type="submit" class="btn btn-success">
							Buat
						</button>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$form = $('#form')
		$submitBtn = $form.find(`[type="submit"]`).ladda()

		$form.on('submit', function(e){
			clearInvalid()
			e.preventDefault();
			$submitBtn.ladda('start')

			let formData = new FormData(this);

			ajaxSetup();
			$.ajax({
				url: `{{ route('website.pengalaman_peserta.store') }}`,
				data: formData,
				dataType: 'json',
				method: 'post',
				contentType : false,
				processData : false,
			})
			.done(response => {
				ajaxSuccessHandling(response)
				$form[0].reset()

				setTimeout(() => {
					window.location.href = `{{ route('website.pengalaman_peserta') }}`
				}, 1000)
			})
			.fail(error => {
				ajaxErrorHandling(error, $form)
				$submitBtn.ladda('stop')
			})
		});
	})

</script>
@endsection