
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- ========== Meta Tags ========== -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="SIJAKDA">
	<meta name="_token" content="{{ csrf_token() }}">

	<!-- ========== Page Title ========== -->
	<title> SIJAKDA </title>

	<!-- ========== Favicon Icon ========== -->
	<link rel="shortcut icon" href="{{ url('web-asset/img/favicon.ico') }}" type="image/x-icon">

	<!-- ========== Start Stylesheet ========== -->
	<link href="{{ url('web-asset/css/bootstrap.min.css?v=1648655218') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/fontawesome/css/all.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/flaticon-set.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/tooltipster.css?v=1648655218') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/magnific-popup.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/owl.carousel.min.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/owl.theme.default.min.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/animate.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/bootsnav.css') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/style.css?v=1648655218') }}" rel="stylesheet">

	<link href="{{ url('web-asset/css/datatables.min.css?v=1648655218') }}" rel="stylesheet" />
	<link href="{{ url('web-asset/css/responsive.css') }}" rel="stylesheet" />
	<!-- ========== End Stylesheet ========== -->

	<!-- ========== Google Fonts ========== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('web-asset/DataTables/datatables.css') }}"/>
	<link rel="stylesheet" href="{{ url('vendors/toastr/toastr.min.css') }}">
	<link rel="stylesheet" href="{{ url('vendors/ladda/ladda-themeless.min.css') }}">

	<link href="{{ url('web-asset/css/custom.css') }}" rel="stylesheet">
	<link href="{{ url('web-asset/css/sijakda-v2.css') }}" rel="stylesheet">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Nunito&family=Plus+Jakarta+Sans&display=swap" rel="stylesheet">

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

	<style>
		body {
			font-family: 'Poppins', 'Calibri', 'Arial', sans-serif;
		}

		:root {
			--blue-main: #215ea2;
			--yellow-main: #fecc2a;
		}
		.our-client{
			height: 98px;
			margin-left: auto;
			margin-right: auto;
		}
		.my-auto{
			margin-top: auto;
			margin-bottom: auto;
		}

		.owl-item .thumb img{
			height: 288px;
		}

		.owl-item .info{
			height: 231px;
		}

		.blog-img-fit{
			object-fit: cover;
			object-position: center;
			width: 100%;
			height: 240px;
		}
		.excerpt{
			height: 200px;
		}

		.top-bar-area.bg-light .info.box li .icon i {
			color: var(--yellow-main);
		}

		.login-btn-box {
			float: right;
			padding: 30px;
		}

		.invalid-feedback {
			color: red;
		}

		hr {
			margin: 1.5rem 0;
		}
	</style>

	@yield('styles')

</head>

<body>

<!-- Header
============================================= -->
	<header id="home">

		<!-- Start Navigation -->
		<nav class="navbar navbar-default navbar-sticky bootsnav">

			<!-- Start Top Search -->
			<div class="container">
				<div class="row">
					<div class="top-search">
						<div class="input-group">
							<form action="#">
								<input type="text" name="text" class="form-control" placeholder="Search">
								<button type="submit">
									<i class="fas fa-search"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- End Top Search -->

			<div class="container">

			

			<!-- Start Header Navigation -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand" href="{{ url('/') }}">
						<img src="{{ url('images/sijakda-pemkab.png') }}" class="logo" alt="Logo">
					</a>


				</div>
				<div class="login-btn-box">
					@if(auth()->guest())
					<a href="{{ route('login') }}">
						<i class="fa fa-sign-in-alt" style="margin-right: 5px;"></i> Log in
					</a>
					@else
						@if(auth()->user()->isPerusahaan() || auth()->user()->isAdmin())
						<a href="{{ route('dashboard') }}">
							<i class="fa fa-user" style="margin-right: 5px;"></i> Dashboard
						</a>
						@elseif(auth()->user()->isPeserta())
						<a href="{{ route('website.dashboard_peserta') }}">
							<i class="fa fa-user" style="margin-right: 5px;"></i> Dashboard
						</a>
						@endif
					<!-- <a href="javascript:void(0);" onclick="$('#logout-form').submit()">
						<i class="fa fa-sign-out-alt" style="margin-right: 5px;"></i> Log out
					</a> -->
					@endif
				</div>
				<!-- End Header Navigation -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="navbar-menu" style="width: 100%;">
					<ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
						<li>
							<a href="{{ url('/') }}">Beranda</a>
						</li>

						@foreach(getMenus() as $menu)
						<li class="dropdown">

							@if($menu->isHasSubmenu())
							<a href="javascript:void(0);" class="dropdown-toggle active" data-toggle="dropdown"> {{ $menu->title }} </a>
							<ul class="dropdown-menu">
								@foreach($menu->navSubmenus as $submenu)
								<li>
									<a href="{{ $submenu->getLink() }}"> {{ $submenu->title }} </a>
								</li>
								@endforeach
							</ul>
							@else
							<a href="{{ $menu->getLink() }}"> {{ $menu->title }} </a>
							@endif

						</li>
						@endforeach

					</ul>
				</div><!-- /.navbar-collapse -->
			</div>

		</nav>
		<!-- End Navigation -->

	</header>

	@yield('content')

	<footer>
		
		
		<!-- Start Footer Bottom -->
		<div class="footer-bottom" id="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div style="margin-bottom:5rem;">
							<h5 class="section-header"> Tentang </h5>
							<p>
								Ini adalah website Sistem Informasi Jasa Kontruksi Dinas PUTR Kabupaten Cirebon.
							</p>
							<br>
							<b> Alamat </b> <br>
							<i class="fas fa-map-marker-alt" style="padding-right: 10px;"></i> {!! str_replace("\n", "<br>", \Setting::getValue('sijakda_alamat', 'Alamat')) !!} <br>
							<i class="fas fa-phone" style="padding-right: 10px;"></i> Telp {{ \Setting::getValue('sijakda_nomor_telepon', 'No Telepon') }}<br>
							<i class="fas fa-envelope" style="padding-right: 10px;"></i>{{ \Setting::getValue('sijakda_email', 'Email') }}
						</div>
					</div>
					@if($linkGroup = \App\Models\LinkGroup::orderBy('position', 'asc')->first())
					<div class="col-lg-6">
						<div style="margin-bottom:5rem;">
							<h5 class="section-header"> {{ $linkGroup->title }} </h5>

							<ul class="links">
								@foreach($linkGroup->links as $link)
								<li>
									<a href="{{ $link->getLink() }}"> {{ $link->title }} </a>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
				</div>

				<p class="copyright">
					&copy; 2023, <b style="color: #000;"> Dinas PUTR Kab Cirebon </b>
				</p>

			</div>
		</div>
		<!-- End Footer Bottom -->
	</footer>

	<form action="{{ route('logout') }}" method="post" id="logout-form">
		@csrf
	</form>
	<!-- End Footer -->
	<!-- jQuery Frameworks
	============================================= -->
	<script src="{{ url('web-asset/js/jquery-1.12.4.min.js') }}"></script>
	<!-- <script src="{{ url('web-asset/js/datatables.min.js') }}"></script> -->
	<script src="{{ url('web-asset/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('web-asset/DataTables/datatables.js') }}"></script>
	<script src="{{ url('web-asset/js/equal-height.min.js') }}"></script>
	<script src="{{ url('web-asset/js/jquery.appear.js') }}"></script>
	<script src="{{ url('web-asset/js/jquery.easing.min.js') }}"></script>
	<script src="{{ url('web-asset/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ url('web-asset/js/modernizr.custom.13711.js') }}"></script>
	<script src="{{ url('web-asset/js/owl.carousel.min.js') }}"></script>
	<script src="{{ url('web-asset/js/wow.min.js') }}"></script>
	<script src="{{ url('web-asset/js/progress-bar.min.js') }}"></script>
	<script src="{{ url('web-asset/js/isotope.pkgd.min.js') }}"></script>
	<script src="{{ url('web-asset/js/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ url('web-asset/js/count-to.js') }}"></script>
	<script src="{{ url('web-asset/js/YTPlayer.min.js') }}"></script>
	<script src="{{ url('web-asset/js/bootsnav.js') }}"></script>
	<script src="{{ url('web-asset/js/main.js') }}"></script>
	<script src="{{ url('web-asset/js/jquery.tooltipster.min.js') }}"></script>
	<script src="{{ url('vendors/toastr/toastr.min.js') }}"></script>
	<script src="{{ url('vendors/ladda/spin.min.js') }}"></script>
	<script src="{{ url('vendors/ladda/ladda.min.js') }}"></script>
	<script src="{{ url('vendors/ladda/ladda.jquery.min.js') }}"></script>
	<script src="{{ url('js/myJs.js') }}"></script>

	@yield('scripts')
	

	</body>
</html>