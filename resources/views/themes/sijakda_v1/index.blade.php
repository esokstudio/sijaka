@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="banner-area" >
	<div class="row">
		<div class="col-md-12">
			<div id="bootcarousel" class="carousel inc-top-heading slide carousel-fade animate_text" data-ride="carousel">
					<!-- Wrapper for slides -->
					<!-- <video width="100%" height="90%" controls autoplay muted>
						<source src="{{ url('web-asset/video/SIPJAKI FINAL.mp4') }}" type="video/mp4">
						Browser anda tidak mendukung html5. --> <!-- Text to be shown incase browser doesnt support html5 -->
					<!-- </video> -->
				<div class="carousel-inner bg-gray pb-4">
					@foreach(getImageSliders() as $imageSlider)

					@if($loop->iteration == 1)
					<div class="item active">
						<div class="" >
							<img src="{{ $imageSlider->imageLink() }}" style="width: 100%;" />
						</div>
							 
					</div>
					@else
					<div class="item">
						<div class="" >
							<img src="{{ $imageSlider->imageLink() }}" style="width: 100%;" />
						</div>
							 
					</div>
					@endif

					@endforeach
				</div>
				<!-- End Wrapper for slides -->
				<!-- Left and right controls -->
				<a class="left carousel-control shadow" href="#bootcarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control shadow" href="#bootcarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</div>


<div class="blog-area section">
	<div class="container">
		<h2 class="section-header"> Terkini </h2>
		<div class="row">
			@foreach(\App\Models\Post::publishedPosts(8) as $post)
			<div class="col-lg-6">
				<div class="single-item-custom">
					<div class="thumb">
						<a href="{{ $post->getLink() }}">
							<img src="{{ $post->thumbnailLink() }}">
						</a>
					</div>
					<div class="content">
						<div class="tag-container">
							@foreach($post->tags() as $tag)
							<span class="tag">
								{{ $tag->tag_name }}
							</span>
							@endforeach
						</div>
						<h5 class="title">
							<a href="{{ $post->getLink() }}">
								{{ $post->title }}
							</a>
						</h5>
						<span class="date">
							{{ $post->created_at->isoFormat('dddd, D MMMM Y') }}
						</span>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endsection