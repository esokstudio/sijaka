@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li class="active"> Berita </li>
				</ul>
				<h1 class="section-header"> Berita </h1>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12">
				<?php
					$activePage = isset($_GET['page']) ? $_GET['page'] : 1;
					$posts = getPosts($activePage);
				?>
				@foreach($posts as $post)
				<div class="single-item-custom single-item-custom-2">
					<div class="thumb">
						<a href="{{ $post->getLink() }}">
							<img src="{{ $post->thumbnailLink() }}">
						</a>
					</div>
					<div class="content">
						<div class="tag-container">
							@foreach($post->tags() as $tag)
							<span class="tag">
								{{ $tag->tag_name }}
							</span>
							@endforeach
						</div>
						<h3 class="title">
							<a href="{{ $post->getLink() }}">
								{{ $post->title }}
							</a>
						</h3>
						<span class="date">
							{{ $post->created_at->isoFormat('dddd, D MMMM Y') }}
						</span>
					</div>
				</div>
				@endforeach

				<div class="row">
					<div class="col-md-12 pagi-area">
						<nav aria-label="navigation">
							<ul class="pagination-custom">
								<?php
									$limit = setting('posts_per_page', 5);
									$amountOfPosts = \App\Models\Post::count();
									$config = [
										'limit_per_page'    => $limit,
										'amount_of_items'   => $amountOfPosts,
										'active_page'       => $activePage,
										'link'              => route('website.posts').'?page={number}',
										'open_tag'          => '<li><a href="{link}">',
										'close_tag'         => '</a></li>',
										'open_tag_on_active'=> '<li class="active"><a style="cursor: pointer">',
										'prev_button_text'  => '<i class="fas fa-arrow-left"></i>',
										'next_button_text'  => '<i class="fas fa-arrow-right"></i>',

									];
									$paginationHtml = pagination($config);
								?>
								{!! $paginationHtml !!}
							</ul>
						</nav>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
@endsection