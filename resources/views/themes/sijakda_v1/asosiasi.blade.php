@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('asosiasi') }}" class="active"> Asosiasi </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Asosiasi </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">
			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> Nama Asosiasi </th>
							<th width="100"> Aksi </th>
						</tr>
					</thead>
					<tbody>
						@foreach(\App\Models\Asosiasi::all() as $asosiasi)
						<tr>
							<td>
								<a href="{{ route('website.asosiasi.detail', $asosiasi->id) }}">
									{{ $asosiasi->nama_asosiasi }}
								</a>
							</td>
							<td>
								<a href="{{ route('website.asosiasi.detail', $asosiasi->slug) }}" class="badge-primary-btn">
									Lihat Anggota
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection