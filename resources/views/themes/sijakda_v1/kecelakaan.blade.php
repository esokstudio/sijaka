@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="javascript:void(0);"> Pengawasan </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('kecelakaan') }}" class="active"> Kecelakaan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Kecelakaan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">
			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> Nama Pekerjaan </th>
							<th> Perusahaan </th>
							<th> Lokasi Kecelakaan </th>
							<th> Waktu Kejadian </th>
							<th> Deskripsi Kecelakaan </th>
							<th> Deskripsi Kerugian </th>
							<th> Sumber Masalah </th>
						</tr>
					</thead>
					<tbody>
						@foreach(\App\Models\Kecelakaan::all() as $kecelakaan)
						<tr>
							<td> {{ $kecelakaan->nama_pekerjaan }} </td>
							<td> {{ $kecelakaan->perusahaan }} </td>
							<td> {{ $kecelakaan->lokasi_kecelakaan }} </td>
							<td> {{ $kecelakaan->waktu_kejadian }} </td>
							<td> {{ $kecelakaan->deskripsi_kecelakaan }} </td>
							<td> {{ $kecelakaan->deskripsi_kerugian }} </td>
							<td> {{ $kecelakaan->sumber_masalah }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection