@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('rantai-pasok-material') }}" class="active"> Rantai Pasok Material </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Rantai Pasok Material </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">

		<?php
			$activePage = isset($_GET['page']) ? $_GET['page'] : 1;
			$search = isset($_GET['search']) ? $_GET['search'] : '';
			$limit = 9;
			$rantaiPasok = \App\Models\RantaiPasokMaterialKontruksi::orderBy('rantai_pasok_material_kontruksi.created_at', 'desc')
											->has('rantaiPasok')
											->leftJoin('rantai_pasok', 'rantai_pasok.id', '=', 'rantai_pasok_material_kontruksi.id_rantai_pasok');
			if(!empty($search)) {
				$rantaiPasok = $rantaiPasok->where('rantai_pasok_material_kontruksi.nama_produk', 'like', '%'.$search.'%')
										   ->orWhere('rantai_pasok.nama_badan_usaha', 'like', '%'.$search.'%');
			}

			$rantaiPasok = $rantaiPasok->take($limit)
							 ->offset(($activePage - 1) * $limit)
							 ->get();
			?>

			<form method="get" action="">
				<div class="row">
					<div class="col-lg-3"></div>
					<div class="col-lg-6">
						<div class="input-group">
							<input type="text" name="search" class="form-control" placeholder="Cari disini.." value="{{ $search }}">
							<div class="input-group-addon">
								<i class="fas fa-search"></i>
							</div>
						</div>
					</div>
				</div>
			</form>
			<br><br>
			<div class="row">
				<div class="col-lg-12">
					<div class="row">
						
						@foreach($rantaiPasok as $p)
						<div class="col-lg-4">
							<div class="image-item">
								<div class="image-body">
									<a href="{{ $p->fileFotoLink() }}" class="image" title="{{ $p->nama_produk }}">
										<img src="{{ $p->fileFotoLink() }}">
									</a>
								</div>
								<div class="image-description">
									{{ $p->nama_produk }} <br>
									{{ $p->merk_produk }}
								</div>
								<div class="image-meta">
									{{ $p->alamat() }} <br>
									<div style="margin-bottom: 1rem;">
										<a href="{{ url('rantai-pasok/'.$p->id_rantai_pasok) }}" class="shop-link">
											<i class="fas fa-store"></i> {{ $p->namaBadanUsaha() }}
										</a>
									</div>
									<a class="btn btn-primary" href="{{ url('rantai-pasok/'.$p->id_rantai_pasok) }}" style="border-radius: 5px;">
										<i class="bi bi-whatsapp"></i> Lihat Toko
									</a>
								</div>
							</div>
						</div>
						@endforeach

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12" style="text-align: center;">
					<nav aria-label="navigation">
						<ul class="pagination" style="place-content: center;">
							<?php

								if(!empty($search)) {
									$amountOfPosts = \App\Models\RantaiPasokMaterialKontruksi::orderBy('rantai_pasok_material_kontruksi.created_at', 'desc')
											->has('rantaiPasok')
											->leftJoin('rantai_pasok', 'rantai_pasok.id', '=', 'rantai_pasok_material_kontruksi.id_rantai_pasok')
											->count();

									$link = url('rantai-pasok-material').'?page={number}&search='.$search;
								} else {
									$amountOfPosts = \App\Models\RantaiPasokMaterialKontruksi::count();
									$link = url('rantai-pasok-material').'?page={number}';
								}

								$config = [
									'limit_per_page'    => $limit,
									'amount_of_items'   => $amountOfPosts,
									'active_page'       => $activePage,
									'link'              => $link,
									'open_tag'          => '<li class="page-item"><a class="page-link" href="{link}">',
									'close_tag'         => '</a></li>',
									'open_tag_on_active'=> '<li class="page-item active"><a class="page-link" href="javascript:void(0);">',
									'prev_button_text'  => 'Previous',
									'next_button_text'  => 'Next',

								];
								$paginationHtml = \App\MyClass\Pagination::makeAndGenerate($config);
							?>
							{!! $paginationHtml !!}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection


@section('styles')
<style type="text/css">
	
	.image-item {
		width: 100%;
		box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
		border-radius: 10px;
		overflow: hidden;
		margin-bottom: 2rem;
	}

	.image-item .image-body {
		height: 200px;
		overflow: hidden;
		cursor: pointer;
	}

	.image-body img {
		min-height: 200px;
		width: 100%;
		object-fit: cover;
		object-position: center;
		transition: .5s;
	}

	.image-body img:hover {
		transform: scale(1.25);
	}

	.image-item .image-description {
		padding: 1rem;
		padding-bottom: 0.5rem;
		font-weight: bold;
		text-align: center;
	}

	.image-item .image-meta {
		padding: 0 20px 10px;
		text-align: center;
		font-size: 10pt;
	}

	.image-item .image-meta .shop-link {
		color: black !important;
	}

</style>
@endsection