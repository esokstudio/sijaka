@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<li class="active"> {{ $page->title }} </li>
					</li>
				</ul>

				<h2><b> {{ $page->title }} </b></h2>
				<p class="text-muted">
					{{ $page->created_at->isoFormat('dddd, D MMMM Y HH:mm') }} WIB <br>
					Diposting oleh <b>{{ $page->createdByName() }}</b>
				</p>
				<br>

				@if($page->isHasThumbnail())
				<img src="{{ $page->thumbnailLink() }}" alt="{{ $page->title }}" class="img-post-custom">
				<p class="small" align="center"> {{ $page->title }} </p>
				<br>
				@endif

				<div class="content-post-custom">
					{!! $page->content !!}
				</div>
			</div>
				
			<div class="col-lg-12">
				<div class="sidebar-item recent-post">
					<div class="title">
						<h4 class="section-header"> Berita Terkini </h4>
					</div>
					<div class="row">
						@foreach(\App\Models\Post::where('is_published', 'yes')->orderBy('created_at', 'desc')->take(3)->get() as $populerPost)
						<div class="col-lg-4">
							<div class="single-item-custom">
								<div class="thumb">
									<a href="{{ $populerPost->getLink() }}">
										<img src="{{ $populerPost->thumbnailLink() }}">
									</a>
								</div>
								<div class="content">
									<div class="tag-container">
										@foreach($populerPost->tags() as $tag)
										<span class="tag">
											{{ $tag->tag_name }}
										</span>
										@endforeach
									</div>
									<h5 class="title">
										<a href="{{ $populerPost->getLink() }}">
											{{ $populerPost->title }}
										</a>
									</h5>
									<span class="date">
										{{ $populerPost->created_at->isoFormat('dddd, D MMMM Y') }}
									</span>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('styles')
<style type="text/css">
	.blog-area img {
		width: 100%;
		height: auto;
	}
</style>
@endsection