@extends('themes.sijakda_v1.layouts')

@section('styles')
<style type="text/css">
	.pdf-preview {
		width: 100%;
		min-height: calc(100vh - 100px);
	}

	@media screen and (max-width: 567px) {
		.pdf-preview {
			min-height: calc(100vh - 250px);
		}
	}
</style>
@endsection


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('peraturan') }}" class="active"> Peraturan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Lihat Peraturan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body">
			<div class="row">
				<div class="col-lg-6">
					<table class="table">
						<tbody>
							<tr>
								<th width="150"> Nomor </th>
								<th width="10">:</th>
								<th width="50%"> {{ $peraturan->nomor }} </th>
							</tr>
							<tr>
								<th> Tahun </th>
								<th width="10">:</th>
								<th> {{ $peraturan->tahun }} </th>
							</tr>
							<tr>
								<th> Link Download </th>
								<th width="10">:</th>
								<th>
									<a class="badge-primary-btn bg-primary" href="{{ $peraturan->fileLink() }}">
										<i class="fa fa-download"></i> Download
									</a>
								</th>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="col-lg-6">
					<table class="table">
						<tbody>
							<tr>
								<th width="150"> Kategori </th>
								<th width="10">:</th>
								<th width="50%"> {{ $peraturan->namaKategori() ?? '-' }} </th>
							</tr>
							<tr>
								<th> Judul </th>
								<th width="10">:</th>
								<th colspan="4"> {{ $peraturan->judul }} </th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<br>

	<div class="container">
		<iframe class="pdf-preview" src="https://docs.google.com/viewer?url={{ $peraturan->fileLink() }}&embedded=true"></iframe>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection