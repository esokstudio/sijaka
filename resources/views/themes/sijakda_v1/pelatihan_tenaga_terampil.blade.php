@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('pelatihan-tenaga-terampil') }}" class="active"> Tenaga Terampil </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Tenaga Terampil </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">
			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> No </th>
							<th> Nama </th>
							<th> Kota </th>
							<th> Provinsi </th>
							<th> Pendidikan </th>
							<th> Klasifikasi </th>
							<th> Dinas/Instansi/Perusahaan </th>
							<th> Nomor Sertifikat </th>
							<th> Tgl Berlaku Sertifikat </th>
						</tr>
					</thead>
					<tbody>
						@foreach(\App\Models\TenagaTerampil::where('status_sertifikat', 'Aktif')->get() as $tenaga)
						<tr>
							<td> {{ $loop->iteration }} </td>
							<td> {{ $tenaga->nama }} </td>
							<td> {{ $tenaga->kota }} </td>
							<td> {{ $tenaga->provinsi }} </td>
							<td> {{ $tenaga->pendidikan }} </td>
							<td> {{ $tenaga->klasifikasi }} </td>
							<td> {{ $tenaga->instansi }} </td>
							<td> {{ $tenaga->no_sertifikat }} </td>
							<td> {{ $tenaga->tgl_berlaku_sertifikat }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection