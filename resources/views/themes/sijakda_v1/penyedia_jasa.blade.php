@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('penyedia-jasa') }}" class="active"> Penyedia Jasa </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Penyedia Jasa </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">

			<div class="row" style="margin-bottom: 1rem;">
				<div class="col-lg-4">
					<p>
						Filter Asosiasi :
					</p>

					<select class="form-control" name="id_asosiasi">
						<option value="all"> - Semua Asosiasi - </option>
						@foreach(\App\Models\Asosiasi::all() as $asosiasi)
						<option value="{{ $asosiasi->id }}"> {{ $asosiasi->nama_asosiasi }} </option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> Perusahaan </th>
							<th> Alamat </th>
							<th> Kota </th>
							<th> Kodepos </th>
							<th> Nomor Telepon </th>
							<th> Fax </th>
							<th> Website </th>
							<th> Bentuk Perusahaan </th>
							<th> Jenis Perusahaan </th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if(isset($_GET['id_asosiasi'])) {
								if($_GET['id_asosiasi'] != 'all') {
									$dataPerusahaan = \App\Models\Perusahaan::where('id_asosiasi', $_GET['id_asosiasi'])->get();
								} else {
									$dataPerusahaan = \App\Models\Perusahaan::all();
								}
							} else {
								$dataPerusahaan = \App\Models\Perusahaan::all();
							}
						?>
						@foreach($dataPerusahaan as $perusahaan)
						<tr>
							<td>
								<a href="{{ route('website.penyedia_jasa.detail', $perusahaan->id) }}">
									{{ $perusahaan->nama_perusahaan }}
								</a>
							</td>
							<td> {{ $perusahaan->alamat }} </td>
							<td> {{ $perusahaan->kota }} </td>
							<td> {{ $perusahaan->kodepos }} </td>
							<td> {{ $perusahaan->nomor_telepon }} </td>
							<td> {{ $perusahaan->fax }} </td>
							<td> {{ $perusahaan->website }} </td>
							<td> {{ $perusahaan->bentuk_perusahaan }} </td>
							<td> {{ $perusahaan->jenis_perusahaan }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()

		$(`[name="id_asosiasi"]`).val(`{{ isset($_GET['id_asosiasi']) ? $_GET['id_asosiasi'] : 'all' }}`)


		$(`[name="id_asosiasi"]`).on('change', function(){
			let value = $(this).val()

			window.location.href = `{{ route('website.penyedia_jasa') }}?id_asosiasi=${value}`
		})
	})

</script>
@endsection