@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('rantai-pasok-material') }}" class="active"> Rantai Pasok Material </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Rantai Pasok Material </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">

		<div class="row">
			<div class="col-lg-6 offset-lg-1">
				<h2 class="mb-3"> <b>{{ $rantaiPasok->nama_badan_usaha }}</b> </h2>
				<p style="font-size: 100%;">
					<b>Alamat</b><br> {!! str_replace("\n", '<br>', $rantaiPasok->alamat) !!} <br>
					<b>Jenis Usaha</b><br> {{ $rantaiPasok->jenis_usaha ?? '-' }} <br>
					<b>Telepon</b><br> <a href="tel:{{ $rantaiPasok->nomor_telepon }}">{{ $rantaiPasok->nomor_telepon ?? '-' }}</a> <br>
					<b>Email</b><br> <a href="mailto:{{ $rantaiPasok->email }}">{{ $rantaiPasok->email ?? '-' }}</a> <br>
				</p>
				<p style="font-size: 90%;">
				</p>
					
				<p style="font-size: 90%;"> {!! str_replace("\n", '<br>', $rantaiPasok->deskripsi) !!} </p>

				<br>
				<br>

				@if(!empty($rantaiPasok->embedded_maps))
					{!! $rantaiPasok->embedded_maps !!}
				@else
					@if(!empty($rantaiPasok->latitude) && !empty($rantaiPasok->longitude))
					{!! \App\MyClass\Location::make($rantaiPasok->latitude, $rantaiPasok->longitude)->embeddedMap() !!}
					@elseif(!empty($rantaiPasok->koordinat) && count(explode(',', $rantaiPasok->koordinat)))
					{!! \App\MyClass\Location::make(explode(',', $rantaiPasok->koordinat)[0], explode(',', $rantaiPasok->koordinat)[1])->embeddedMap() !!}
					@endif
				@endif

			</div>

			<div class="col-lg-4">
				<h3 class="mb-3"><b> Produk </b></h3>
				@foreach($rantaiPasok->rantaiPasokMaterialKontruksi as $p)
				<div class="produk-item">
					<div class="produk-image">
						<a href="{{ $p->fileFotoLink() }}" class="image" title="{{ $p->nama_produk }}" data-fancybox-group="gallery" target="_blank">
							<img src="{{ $p->fileFotoLink() }}">
						</a>
					</div>
					<div class="produk-content">
						<b> {{ $p->nama_produk }} </b> <br>
						Merk {{ $p->merk_produk }}
					</div>
				</div>
				@endforeach

				@foreach($rantaiPasok->rantaiPasokPeralatanKontruksi as $p)
				<div class="produk-item">
					<div class="produk-image">
						<a href="{{ $p->fileFotoLink() }}" class="image" title="{{ $p->nama_peralatan }}" data-fancybox-group="gallery" target="_blank">
							<img src="{{ $p->fileFotoLink() }}">
						</a>
					</div>
					<div class="produk-content">
						<b> {{ $p->nama_peralatan }} </b> <br>
						Merk {{ $p->merk_peralatan }}
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection


@section('styles')
<style type="text/css">
	
	.produk-item {
		display: flex;
		width: 100%;
		box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
		border-radius: 10px;
		overflow: hidden;
		margin-bottom: 2rem;
		background: white;
	}

	.produk-item .produk-image {
		width: 120px;
	}

	.produk-item .produk-image {
		height: 100px;
		overflow: hidden;
		cursor: pointer;
	}

	.produk-item .produk-image img {
		min-height: 100px;
		width: 100%;
		object-fit: cover;
		object-position: center;
		transition: .5s;
	}

	.produk-item .produk-image img:hover {
		transform: scale(1.25);
	}

	.produk-item .produk-content {
		padding: 1rem;
	}

	iframe {
		max-width: 100%;
	}

</style>
@endsection