@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('peraturan') }}" class="active"> Peraturan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Peraturan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>


<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">
			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> No </th>
							<th> Kategori </th>
							<th> Nomor </th>
							<th> Tahun </th>
							<th> Judul </th>
							<th width="50"> Aksi </th>
						</tr>
					</thead>
					<tbody>
						@foreach(\App\Models\Peraturan::orderBy('tahun', 'desc')->get() as $peraturan)
						<tr>
							<td> {{ $loop->iteration }} </td>
							<td> {{ $peraturan->namaKategori() }} </td>
							<td> {{ $peraturan->nomor }} </td>
							<td> {{ $peraturan->tahun }} </td>
							<td> {{ $peraturan->judul }} </td>
							<td> 
								<a class="badge-primary-btn bg-primary" href="{{ route('website.peraturan_detail', $peraturan->id) }}">
									<i class="fa fa-search mr-2"></i> Lihat
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection