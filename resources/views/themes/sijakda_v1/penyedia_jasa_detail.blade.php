@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ route('website.penyedia_jasa') }}"> Penyedia Jasa </a>
					</li>
					<li>
						<span class="active"> {{ $perusahaan->nama_perusahaan }} </span>
					</li>
				</ul>

				<h1 class="section-header"><b> {{ $perusahaan->nama_perusahaan }} </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area full-blog blog-standard full-blog default-padding section">
	<div class="container">
		<div class="row">
			<div class="blog-items">
				<div class="blog-content col-md-10 col-md-offset-1">

					<div class="box-body table-responsive no-padding">

						<h4 class="section-header"> Detail Perusahaan </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-hover table-bordered table-custom">
								<tr>
									<th> Nama Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->nama_perusahaan !!} </td>
									<th> Direktur </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->direktur !!} </td>
								</tr>

								<tr>
									<th> Alamat </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->alamat !!} </td>
									<th> Kota </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->kota !!} </td>
								</tr>

								<tr>
									<th> Kodepos </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->kodepos !!} </td>
									<th> Nomor Telepon </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->nomor_telepon !!} </td>
								</tr>

								<tr>
									<th> Fax </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->fax !!} </td>
									<th> Email </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->email !!} </td>
								</tr>

								<tr>
									<th> Website </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->website !!} </td>
									<th> Bentuk Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->bentuk_perusahaan !!} </td>
								</tr>

								<tr>
									<th> Jenis Perusahaan </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->jenis_perusahaan !!} </td>
									<th> Asosiasi </th>
									<td width="10"> : </td>
									<td> {!! $perusahaan->namaAsosiasi() !!} </td>
								</tr>

								<tr>
									<th> SBU </th>
									<td> : </td>
									<td>
										@if($perusahaan->isHasFileSbu())
										<a href="{{ $perusahaan->fileSbuLink() }}"> Klik disini </a>
										@else
										Belum Upload
										@endif
									</td>
									<th> </th>
									<td> </td>
									<td> </td>
								</tr>
							</table>

						</div>

						<hr>

						<h4 class="section-header"> Kualifikasi </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-custom dataTable">
								<thead>
									<tr>
										<th> Sub Bidang Klasifikasi </th>
										<th> Nomor Kode </th>
										<th> Kualifikasi </th>
										<th> Tahun </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->kualifikasiPerusahaan as $kualifikasi)
									<tr>
										<td> {!! $kualifikasi->sub_bidang_klasifikasi !!} </td>
										<td> {!! $kualifikasi->nomor_kode !!} </td>
										<td> {!! $kualifikasi->kualifikasi !!} </td>
										<td> {!! $kualifikasi->tahun !!} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


						<hr>

						<h4 class="section-header"> Akte Pendirian </h4>

						@if($pendirian = $perusahaan->aktePendirianPerusahaan)
						<div class="table-responsive mt-3">
						
							<table class="table table-hover table-bordered table-custom">
								<tr>
									<th> No Akte </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->no_akte !!} </td>
									<th> Nama Notaris </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->nama_notaris !!} </td>
								</tr>

								<tr>
									<th> Alamat </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->alamat !!} </td>
									<th> Kota </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->kota !!} </td>
								</tr>

								<tr>
									<th> Provinsi </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->provinsi !!} </td>
									<th> Tgl Akte </th>
									<td width="10"> : </td>
									<td> {!! $pendirian->tanggal_akte !!} </td>
								</tr>
							</table>
						</div>
						@else
						<p align="center">
							<i> Belum mengisi </i>
						</p>

						@endif


						<hr>

						<h4 class="section-header"> Pengesahan </h4>

						@if($pengesahan = $perusahaan->pengesahanPerusahaan)
						<div class="table-responsive mt-3">
						
							<table class="table table-hover table-bordered table-custom">
								<tr>
									<th> No Kemenkumham </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->menkumham_no !!} </td>
									<th> Tgl Kemenkumham </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->menkumham_tgl !!} </td>
								</tr>

								<tr>
									<th> No Pengadilan Negeri </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->pengadilan_negeri_no !!} </td>
									<th> Tgl Pengadilan Negeri </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->pengadilan_negeri_tgl !!} </td>
								</tr>

								<tr>
									<th> No Lembar Negara </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->lembar_negara_no !!} </td>
									<th> Tgl Lembar Negara </th>
									<td width="10"> : </td>
									<td> {!! $pengesahan->lembar_negara_tgl !!} </td>
								</tr>
							</table>
						</div>
						@else
						<p align="center">
							<i> Belum mengisi </i>
						</p>
						@endif


						<hr>

						<h4 class="section-header"> Akte Perubahan </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-bordered table-custom dataTable">
								<thead>
									<tr>
										<th> No Akte </th>
										<th> Nama Notaris </th>
										<th> Alamat </th>
										<th> Kota </th>
										<th> Provinsi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->aktePerubahanPerusahaan as $aktePerubahan)
									<tr>
										<td> {!! $aktePerubahan->no_akte !!} </td>
										<td> {!! $aktePerubahan->nama_notaris !!} </td>
										<td> {!! $aktePerubahan->alamat !!} </td>
										<td> {!! $aktePerubahan->kota !!} </td>
										<td> {!! $aktePerubahan->provinsi !!} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


						<hr>

						<h4 class="section-header"> Pengurus </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-bordered table-custom dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Tgl Lahir </th>
										<th> Alamat </th>
										<th> Jabatan </th>
										<th> Pendidikan </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->pengurusPerusahaan as $pengurus)
									<tr>
										<td> {!! $pengurus->nama !!} </td>
										<td> {!! $pengurus->tgl_lahir !!} </td>
										<td> {!! $pengurus->alamat !!} </td>
										<td> {!! $pengurus->jabatan !!} </td>
										<td> {!! $pengurus->pendidikan !!} </td>
										<td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


						<hr>

						<h4 class="section-header"> Keuangan </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-bordered table-custom dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Alamat </th>
										<th> Jumlah Saham </th>
										<th> Nilai Satuan Saham </th>
										<th> Modal Dasar </th>
										<th> Modal Disetor </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->keuanganPerusahaan as $keuangan)
									<tr>
										<td> {!! $keuangan->nama !!} </td>
										<td> {!! $keuangan->alamat !!} </td>
										<td> {!! $keuangan->jumlah_saham !!} </td>
										<td> {!! $keuangan->nilai_satuan_saham !!} </td>
										<td> {!! $keuangan->modal_dasar !!} </td>
										<td> {!! $keuangan->modal_disetor !!} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


						<hr>

						<h4 class="section-header"> Tenaga Kerja </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-bordered table-custom dataTable">
								<thead>
									<tr>
										<th> Nama </th>
										<th> Tgl Lahir </th>
										<th> Pendidikan </th>
										<th> No Registrasi </th>
										<th> Jenis Sertifikat </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->tenagaKerjaPerusahaan as $tenagaKerja)
									<tr>
										<td> {!! $tenagaKerja->nama !!} </td>
										<td> {!! $tenagaKerja->tgl_lahir !!} </td>
										<td> {!! $tenagaKerja->pendidikan !!} </td>
										<td> {!! $tenagaKerja->no_registrasi !!} </td>
										<td> {!! $tenagaKerja->jenis_sertifikat !!} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


						<hr>

						<h4 class="section-header"> Pengalaman </h4>

						<div class="table-responsive mt-3">
						
							<table class="table table-bordered table-hover table-bordered table-custom dataTable">
								<thead>
									<tr>
										<th> Tanggal Kontrak </th>
										<th> Nomor Kontrak </th>
										<th> Nama Pekerjaan </th>
										<th> Lokasi </th>
									</tr>
								</thead>

								<tbody>
									@foreach($perusahaan->pengalamanPerusahaan as $pengalaman)
									<tr>
										<td> {!! $pengalaman->tanggal_kontrak !!} </td>
										<td> {!! $pengalaman->nomor_kontrak !!} </td>
										<td> {!! $pengalaman->nama_pekerjaan !!} </td>
										<td> {!! $pengalaman->lokasi_pekerjaan !!} </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('.dataTable').DataTable({
			autoWidth: false,
		})
	})

</script>
@endsection