@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">

		<div class="row">
			<div class="col-lg-8">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('berita') }}"> Berita </a>
					</li>
				</ul>

				<h2><b> {{ $post->title }} </b></h2>
				<p class="text-muted">
					{{ $post->created_at->isoFormat('dddd, D MMMM Y HH:mm') }} WIB <br>
					Diposting oleh <b>{{ $post->createdByName() }}</b>
				</p>
				<br>

				@if($post->isHasThumbnail())
				<img src="{{ $post->thumbnailLink() }}" alt="{{ $post->title }}" class="img-post-custom">
				<p class="small" align="center"> {{ $post->title }} </p>
				<br>
				@endif

				<div class="content-post-custom">
					{!! $post->content !!}
				</div>
			</div>
				
			<div class="col-lg-4">
				<div class="sidebar-item recent-post">
					<div class="title">
						<h4 class="section-header"> Berita Terkini </h4>
					</div>
					@foreach(\App\Models\Post::where('is_published', 'yes')->orderBy('created_at', 'desc')->take(5)->get() as $populerPost)
					<div class="single-item-custom">
						<div class="thumb">
							<a href="{{ $populerPost->getLink() }}">
								<img src="{{ $populerPost->thumbnailLink() }}">
							</a>
						</div>
						<div class="content">
							<div class="tag-container">
								@foreach($populerPost->tags() as $tag)
								<span class="tag">
									{{ $tag->tag_name }}
								</span>
								@endforeach
							</div>
							<h5 class="title">
								<a href="{{ $populerPost->getLink() }}">
									{{ $populerPost->title }}
								</a>
							</h5>
							<span class="date">
								{{ $populerPost->created_at->isoFormat('dddd, D MMMM Y') }}
							</span>
						</div>
					</div>
					@endforeach
				</div>
			</div>

		</div>
	</div>
</div>
@endsection