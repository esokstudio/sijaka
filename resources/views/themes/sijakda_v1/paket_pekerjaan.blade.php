@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('paket-pekerjaan') }}" class="active"> Paket Pekerjaan </a>
					</li>
				</ul>

				<h1 class="section-header"><b> Paket Pekerjaan </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">

			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> Tahun Anggaran </th>
							<th> Nama Pekerjaan </th>
							<th> Perusahaan </th>
							<th> Nilai Kontrak </th>
							<th> Status </th>
						</tr>
					</thead>
					<tbody>
						@foreach(\App\Models\PaketPekerjaan::where('status', 'Selesai')->get() as $paketPekerjaan)
						<tr>
							<td> {{ $paketPekerjaan->tahun_anggaran }} </td>
							<td> {{ $paketPekerjaan->nama_pekerjaan }} </td>
							<td> {{ $paketPekerjaan->namaPerusahaan() }} </td>
							<td> {{ number_format($paketPekerjaan->nilai_kontrak) }} </td>
							<td> {{ $paketPekerjaan->status }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<br>
		<br>

		<h2 align="center"> Grafik Paket Pekerjaan </h2>

		<div class="table-responsive">
			<canvas id="paket-pekerjaan-chart" style="min-height: 500px; width: 100%;"></canvas>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.0/chart.min.js" integrity="sha512-JxJpoAvmomz0MbIgw9mx+zZJLEvC6hIgQ6NcpFhVmbK1Uh5WynnRTTSGv3BTZMNBpPbocmdSJfldgV5lVnPtIw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable({
			order: [['0', 'desc']]
		})
	})

</script>

<script type="text/javascript">

	const dynamicColors = () => {
		const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52);
		const randomRGB = () => `${randomNum()}, ${randomNum()}, ${randomNum()}`;
		return randomRGB();
	};

	// Chart.defaults.global.defaultFontFamily = "Calibri";
	// Chart.defaults.global.defaultFontSize = 18;



	// Chart Paket Pekerjaan
	let paketPekerjaanChartElem = document.getElementById("paket-pekerjaan-chart");
	let datasets = []
	let dataset = null
	let data = null;
	let tahun = [];
	let color = null
	@foreach(\App\Models\PaketPekerjaan::jumlahPaketPekerjaanDataChart() as $ins)
	data = []
	tahun = []
	@foreach($ins->data as $tahun => $jumlah)
	data.push(parseInt(`{{ $jumlah }}`))
	tahun.push(parseInt(`{{ $tahun }}`))
	@endforeach
	color = dynamicColors()
	dataset = {
		label: '{{ $ins->instansi->nama_instansi }}',
		data: data,
		backgroundColor: `rgba(${color}, 0.6)`,
		borderColor: `rgba(${color}, 1)`,
	};
	datasets.push(dataset)
	@endforeach
 
	let jumlahPaketPekerjaanData = {
		'labels': tahun,
		'datasets': datasets
	};

	let chartOptions = {
		scales: {
			xAxes: [{
				barPercentage: 1,
				categoryPercentage: 0.6
			}],
			yAxes: [{
				ticks: {
					callback: function(label, index, labels) {
						return numberFormat(label);
					}
				},
			}]
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					console.log(tooltipItem.yLabel)
					return numberFormat(tooltipItem.yLabel)
				},
				title: function(tooltipItem, data) {
					const tahun = tooltipItem[0].xLabel
					return datasets[tooltipItem[0].datasetIndex].label + ` (${tahun})`
				}
			}
		}
	};
	 
	let paketPekerjaanChart = new Chart(paketPekerjaanChartElem, {
		type: 'bar',
		data: jumlahPaketPekerjaanData,
		options: chartOptions
	});




	// Chart Pagu Anggaran
	let paguAnggaranChartElem = document.getElementById("pagu-anggaran-chart");
	datasets = [];
	tahun = [];

	@foreach(\App\Models\PaketPekerjaan::paguAnggaranDataChart() as $ins)
	data = []
	tahun = []
	@foreach($ins->data as $tahun => $jumlah)
	data.push(parseInt(`{{ $jumlah }}`))
	tahun.push(parseInt(`{{ $tahun }}`))
	@endforeach
	color = dynamicColors()
	dataset = {
		label: '{{ $ins->instansi->nama_instansi }}',
		data: data,
		backgroundColor: `rgba(${color}, 0.6)`,
		borderColor: `rgba(${color}, 1)`,
	};
	datasets.push(dataset)
	@endforeach
 
	let paguAnggaranData = {
		'labels': tahun,
		'datasets': datasets
	};

	chartOptions = {
		scales: {
			xAxes: [{
				barPercentage: 1,
				categoryPercentage: 0.6
			}],
			yAxes: [{
				ticks: {
					callback: function(label, index, labels) {
						return numberFormat(label);
					}
				},
			}]
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					return numberFormat(tooltipItem.yLabel)
				},
				title: function(tooltipItem, data) {
					const tahun = tooltipItem[0].xLabel
					return datasets[tooltipItem[0].datasetIndex].label + ` (${tahun})`
				}
			}
		}
	};
	 
	let paguAnggaranChart = new Chart(paguAnggaranChartElem, {
		type: 'bar',
		data: paguAnggaranData,
		options: chartOptions
	});
</script>
@endsection