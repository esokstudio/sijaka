@extends('themes.sijakda_v1.layouts')


@section('content')
<div class="breadcrumb-area breadcrumb-area-custom">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb-custom">
					<li>
						<a href="{{ url('/') }}"> Beranda </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<a href="{{ url('asosiasi') }}"> Asosiasi </a> <i class="fas fa-chevron-right small mx-1 d-inline-block"></i>
					</li>
					<li>
						<span class="active"> {{ $asosiasi->nama_asosiasi }} </span>
					</li>
				</ul>

				<h1 class="section-header"><b> {{ $asosiasi->nama_asosiasi }} </b></h1>
				<br>
			</div>
		</div>
	</div>
</div>

<div class="blog-area single full-blog right-sidebar full-blog default-padding section">
	<div class="container">
		<div class="box-body table-responsive no-padding">
			<div class="table-responsive px-2">
				<table class="table table-bordered table-hover table-custom" id="dataTable">
					<thead>
						<tr>
							<th> Perusahaan </th>
							<th> Alamat </th>
							<th> Kota </th>
							<th> Kodepos </th>
							<th> Nomor Telepon </th>
							<th> Fax </th>
							<th> Website </th>
							<th> Bentuk Perusahaan </th>
							<th> Jenis Perusahaan </th>
						</tr>
					</thead>
					<tbody>
						@foreach($asosiasi->perusahaan as $perusahaan)
						<tr>
							<td> {{ $perusahaan->nama_perusahaan }} </td>
							<td> {{ $perusahaan->alamat }} </td>
							<td> {{ $perusahaan->kota }} </td>
							<td> {{ $perusahaan->kodepos }} </td>
							<td> {{ $perusahaan->nomor_telepon }} </td>
							<td> {{ $perusahaan->fax }} </td>
							<td> {{ $perusahaan->website }} </td>
							<td> {{ $perusahaan->bentuk_perusahaan }} </td>
							<td> {{ $perusahaan->jenis_perusahaan }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
	
	$(function(){
		$('#dataTable').DataTable()
	})

</script>
@endsection