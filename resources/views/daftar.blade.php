@extends('layouts.auth')


@section('title', 'Pendaftaran')


@section('content')
<div class="row w-100 mx-auto">
	<div class="col-lg-4 mx-auto px-4">
		<div class="auto-form-wrapper py-5">
			<figure class="text-center">
				<img src="{{ url('images/sijakda.png') }}" style="width: 150px; height: auto;">
			</figure>
			<h5 align="center" class="mb-3"> Pendaftaran </h5>
			<form id="form">

				{!! Template::requiredBanner() !!}

				<div class="form-group">
					<label> Nama Lengkap {!! Template::required() !!} </label>
					<input type="text" class="form-control" placeholder="Nama Lengkap" name="name" autocomplete="off" required>
					<small class="invalid-feedback"></small>
				</div>

				<div class="form-group">
					<label> Email {!! Template::required() !!} </label>
					<input type="email" class="form-control" placeholder="Email" name="email" autocomplete="off" required>
					<small class="invalid-feedback"></small>
				</div>

				<div class="form-group">
					<label> Password {!! Template::required() !!} </label>
					<input type="password" class="form-control" placeholder="Password" name="password" required>
					<small class="invalid-feedback"></small>
				</div>

				<div class="form-group">
					<label> Konfirmasi Password {!! Template::required() !!} </label>
					<input type="password" class="form-control" placeholder="Konfirmasi Password" name="confirm_password" required>
					<small class="invalid-feedback"></small>
				</div>

				<div class="form-group">
					<label> Perusahaan {!! Template::required() !!} </label>
					<select style="width: 100%;" name="id_perusahaan" required>
						@foreach(\App\Models\Perusahaan::all() as $perusahaan)
						<option value="{{ $perusahaan->id }}"> {{ $perusahaan->nama_perusahaan }} </option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<button class="btn btn-primary submit-btn btn-block" type="submit">
					<i class="mdi mdi-account-edit"></i> Daftar
					</button>
				</div>

				<p class="text-center textMsg"><br></p>

				<div class="text-block text-center">
					<span class="text-small font-weight-semibold"> Sudah memiliki akun ?</span>
					<a href="{{ route('login') }}" class="text-black text-small"> Login disini </a>
				</div>

			</form>
		</div>
		<p class="footer-text text-center text-dark mt-3">
			&copy; {{ date('Y') }} | Esok Studio. All rights reserved.
		</p>
	</div>
</div>
@endsection


@section('script')
<script>
	$(function(){
		let $form = $('#form');
		let $submitBtn = $form.find(`[type="submit"]`).ladda();

		$form.on('submit', function(e) {
			e.preventDefault();
			clearInvalid()

			$submitBtn.ladda('start');
			let formData = $(this).serialize();

			ajaxSetup();
			$.ajax({
				url : `{{ route('website.daftar_save') }}`,
				method : "post",
				data : formData,
			})
			.done(response => {
				// $submitBtn.ladda('stop')
				let { message } = response

				toastrAlert();
				toastr.success(message, 'Berhasil')

				setTimeout(() => {
					window.location.href = `{{ route('login') }}`
				}, 2000)
			})
			.fail(error => {
				$submitBtn.ladda('stop')

				let { status, responseJSON } = error;
				let { message } = responseJSON

				if(status == 422) {
					let { errors } = responseJSON
					invalidResponse($form, errors)
				}

				toastrAlert();

				toastr.warning(message, 'Peringatan')
			});
		});

		$form.find(`[name="id_perusahaan"]`).select2({
			'placeholder': '- Pilih Perusahaan -',
			'minimumInputLength': 2
		})
		$form.find(`[name="id_perusahaan"]`).val('').trigger('change')

		
	})
</script>
@endsection