<?php

return [
	'cms_version'		=> '1.0.0',
	'updated_at'		=> '2022-05-25 15:54',
	'developer_url'		=> 'https://esokstudio.com',
	'developer_name'	=> 'Esok Studio',
];