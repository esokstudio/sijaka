<?php 

return [

	'title'	=> [
		'default'		=> 'Judul',
		'label'			=> 'Judul',
		'description'	=> 'Judul utama website.',
		'type'			=> 'text',
	],
	'tagline'	=> [
		'default'		=> null,
		'label'			=> 'Slogan',
		'description'	=> 'Slogan website.',
		'type'			=> 'longtext',
		'nullable'		=> true,
	],
	'description'	=> [
		'default'		=> null,
		'label'			=> 'Deskripsi',
		'description'	=> 'Deskripsi website.',
		'type'			=> 'longtext',
		'nullable'		=> true,
	],
	'keywords'	=> [
		'default'		=> null,
		'label'			=> 'Kata Kunci',
		'description'	=> 'Kata kunci yang relevan dengan website. Pisahkan setiap kata kunci dengan koma (,) .',
		'type'			=> 'longtext',
		'nullable'		=> true,
	],
	'website_theme'	=> [
		'default'		=> 'sijakda_v1',
		'label'			=> 'Tema',
		'description'	=> 'Tema yang diterapkan pada tampilan depan website.',
		'type'			=> 'option',
		'options'		=> [
			'sijakda_v1'	=> 'Sijakda',
		]
	],
	'show_page_thumbnail_field'	=> [
		'default'		=> 'no',
		'label'			=> 'Thumbnail Laman',
		'description'	=> 'Penyediaan thumbnail untuk laman',
		'type'			=> 'option',
		'options'		=> [
			'yes'	=> 'Ya',
			'no'	=> 'Tidak'
		]
	],
	'show_post_category_thumbnail_field'	=> [
		'default'		=> 'no',
		'label'			=> 'Thumbnail Kategori Postingan',
		'description'	=> 'Penyediaan thumbnail untuk kategori postingan',
		'type'			=> 'option',
		'options'		=> [
			'yes'	=> 'Ya',
			'no'	=> 'Tidak'
		]
	],
	'posts_per_page'	=> [
		'default'		=> '5',
		'label'			=> 'Tampilan Postingan Per Halaman',
		'description'	=> 'Jumlah maksimal postingan yang tampil pada setiap halaman di list postingan',
		'type'			=> 'number',
		'min'			=> 1,
		'max'			=> 100,
	],
	'image_slider_available'	=> [
		'default'		=> 'yes',
		'label'			=> 'Penyediaan Slide Foto',
		'description'	=> 'Penyediaan slide foto untuk website',
		'type'			=> 'option',
		'options'		=> [
			'yes'	=> 'Ya',
			'no'	=> 'Tidak'
		]
	],
	'message_available'	=> [
		'default'		=> 'yes',
		'label'			=> 'Penyediaan Pesan',
		'description'	=> 'Penyediaan pesan dari pengunjung untuk website',
		'type'			=> 'option',
		'options'		=> [
			'yes'	=> 'Ya',
			'no'	=> 'Tidak'
		]
	],
	'message_receiver_email'	=> [
		'default'		=> 'sample-user@esokstudio.com',
		'label'			=> 'Alamat Email Penerima Pesan',
		'description'	=> 'Alamat email yang akan dikirimkan jika ada pengunjung yang mengirimkan pesan melalui form pesan di website. (Jika lebih dari 1 alamat email pisahkan dengan koma(,).',
		'type'			=> 'text',
		'nullable'		=> true,
	],

];