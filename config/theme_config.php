<?php

return [

	'sijakda_v1'	=> [
		'sijakda_email'	=> [
			'default'		=> 'name@example.com',
			'label'			=> 'Alamat Email',
			'description'	=> 'Alamat email yang akan dicantumkan di website.',
			'type'			=> 'text',
		],
		'sijakda_nomor_telepon' => [
			'default'		=> '08xxxxxxxx',
			'label'			=> 'Nomor Telepon',
			'description'	=> 'Nomor Telepon yang akan dicantumkan di website.',
			'type'			=> 'text',
		],
		'sijakda_alamat'	=> [
			'default'		=> 'Alamat',
			'label'			=> 'Alamat',
			'description'	=> 'Alamat yang akan dicantumkan di webiste',
			'type'			=> 'longtext',
		]
	],

];