<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenagaAhli extends Model
{
	protected $table = 'tenaga_ahli';
	protected $fillable = [ 'no_ktp', 'nama', 'tgl_lahir', 'tempat_lahir', 'jenis_kelamin', 'alamat', 'kota', 'provinsi', 'email', 'no_hp', 'pendidikan', 'klasifikasi', 'instansi', 'id_tenaga_kerja_perusahaan', 'no_sertifikat', 'tgl_awal_berlaku_sertifikat', 'tgl_akhir_berlaku_sertifikat', 'status_sertifikat' ];


	public static function createTenagaAhli(array $request)
	{
		$tenagaAhli = self::create($request);
		self::refreshStatus();
		return $tenagaAhli;
	}


	public function updateTenagaAhli(array $request)
	{
		$this->update($request);
		self::refreshStatus();
		return $this;
	}


	public function deleteTenagaAhli()
	{
		return $this->delete();
	}


	public function createdAtText()
	{
		return date('Y-m-d', strtotime($this->created_at));
	}

	public function statusSertifikatHtml()
	{
		return $this->status_sertifikat == 'Aktif' ? '<span class="text-success"> Aktif </span>' : '<span class="text-danger"> Tidak Aktif </span>';
	}


	public static function dt($request)
	{
		$data = self::select([ 'tenaga_ahli.*' ]);

		if(!empty($request->kota)) {
			$data = $data->where('kota', $request->kota);
		}

		if(!empty($request->provinsi)) {
			$data = $data->where('provinsi', $request->provinsi);
		}

		if(!empty($request->klasifikasi)) {
			$data = $data->where('klasifikasi', $request->klasifikasi);
		}

		if(!empty($request->instansi)) {
			$data = $data->where('instansi', $request->instansi);
		}

		if(!empty($request->start_date)) {
			$data = $data->where('created_at', '>=', $request->start_date . ' 00:00:00');
		}

		if(!empty($request->end_date)) {
			$data = $data->where('created_at', '<=', $request->end_date . ' 23:59:59');
		}

		if(!empty($request->status_sertifikat)) {
			if($request->status_sertifikat != "all") {
				$data = $data->where('status_sertifikat', $request->status_sertifikat);
			}
		}

		return \DataTables::eloquent($data)
			->editColumn('created_at', function($data){
				return $data->created_at->format('Y-m-d H:i');
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('tenaga_ahli.edit', $data->id).'" title="Edit Tenaga Ahli">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item" href="'.route('tenaga_ahli.detail', $data->id).'" title="Detail Tenaga Ahli">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('tenaga_ahli.destroy', $data->id).'" title="Hapus Tenaga Ahli">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->editColumn('status_sertifikat', function($data){
				return $data->statusSertifikatHtml();
			})
			->rawColumns([ 'action', 'status_sertifikat', ])
			->make(true);
	}


	public static function importFromExcel($request)
	{
		if(!empty($request->file))
		{
			$file = $request->file('file');
			$ext = $file->getClientOriginalExtension();

			if(in_array($ext, [ 'xlsx', 'xls' ]))
			{
				$filename = 'tenaga_ahli_'.date('Ymdhis') . '.'.$ext;
				$filepath = \Helper::temps($filename);
				$path = \Helper::temps('');
				$file->move($path, $filename);

				if($xlsx = \App\MyClass\SimpleXLSX::parse($filepath)) {

					$iter = 1;
					$count = 0;
					foreach($xlsx->rows() as $tenaga) {
						if($iter > 1) {
							$noKtp = $tenaga[0];
							$nama = $tenaga[1];
							$klasifikasi = $tenaga[11];

							$cek = self::where('no_ktp', $noKtp)
									   ->where('nama', $nama)
									   ->where('klasifikasi', $klasifikasi)
									   ->first();
							if(!$cek) {
								try {
									self::create([
										'no_ktp'		=> $tenaga[0],
										'nama'			=> $tenaga[1],
										'tgl_lahir'		=> $tenaga[2],
										'tempat_lahir'	=> $tenaga[3],
										'jenis_kelamin'	=> $tenaga[4],
										'alamat'		=> $tenaga[5],
										'kota'			=> $tenaga[6],
										'provinsi'		=> $tenaga[7],
										'email'			=> $tenaga[8],
										'no_hp'			=> $tenaga[9],
										'pendidikan'	=> $tenaga[10],
										'klasifikasi'	=> $tenaga[11],
										'instansi'		=> $tenaga[12],
										'no_sertifikat' => $tenaga[13],
										'tgl_awal_berlaku_sertifikat' => $tenaga[14],
										'tgl_akhir_berlaku_sertifikat' => $tenaga[15],
									]);
									$count++;
								} catch (Exception $e) {}
							}
						}

						$iter++;
					}
					self::refreshStatus();

					return $count;

				} else {
					throw new \Exception("Pembacaan File Gagal");
				}
			}
			else
			{
				throw new \Exception("Ekstensi tidak didukung");
			}
		} 
		else 
		{
			throw new \Exception("File Kosong");
		}
	}


	public static function exportToExcel($request)
	{
		$data = self::where('created_at', '!=', null);

		if($request->kota != 'all') {
			$data = $data->where('kota', $request->kota);
		}

		if($request->provinsi != 'all') {
			$data = $data->where('provinsi', $request->provinsi);
		}

		if($request->klasifikasi != 'all') {
			$data = $data->where('klasifikasi', $request->klasifikasi);
		}

		if($request->instansi != 'all') {
			$data = $data->where('instansi', $request->instansi);
		}

		if(!empty($request->start_date)) {
			$data = $data->where('created_at', '>=', $request->start_date . ' 00:00:00');
		}

		if(!empty($request->end_date)) {
			$data = $data->where('created_at', '<=', $request->end_date . ' 23:59:59');
		}

		$data = $data->get();

		$filename = 'Tenaga_Ahli.xlsx';

		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 20 ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			'No' => 'string',
			'No. KTP' => 'string',
			'Nama (Tanpa Gelar)' => 'string',
			'Tanggal Lahir (YYYY-MM-DD)' => 'string',
			'Tempat Lahir' => 'string',
			'Jenis Kelamin (P/W)' => 'string',
			'Alamat Sesuai KTP' => 'string',
			'Kabupaten / Kota' => 'string',
			'Provinsi' => 'string',
			'Email' => 'string',
			'No. HP' => 'string',
			'Pendidikan (Tingkat dan Bidang jurusan)' => 'string',
			'Klasifikasi Keterampilan Tenaga Kerja Konstruksi' => 'string',
			'Dinas/Asosiasi/Perusahaan' => 'string',
			'Nomor Sertifikat' => 'string',
			'Tgl Awal Berlaku Sertifikat' => 'string',
			'Tgl Akhir Berlaku Sertifikat' => 'string',
			'Status Sertifikat' => 'string',
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,25,20,20,20,10,25,10,20,20,20,20,25,20,20,20,20,10],
			'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 5, 'wrap_text' => true
		]);

		$iter = 1;
		foreach($data as $tenaga)
		{
			$writer->writeSheetRow('Sheet1', [
				$iter++,
				$tenaga->no_ktp,
				$tenaga->nama,
				$tenaga->tgl_lahir,
				$tenaga->tempat_lahir,
				$tenaga->jenis_kelamin,
				$tenaga->alamat,
				$tenaga->kota,
				$tenaga->provinsi,
				$tenaga->email,
				$tenaga->no_hp,
				$tenaga->pendidikan,
				$tenaga->klasifikasi,
				$tenaga->instansi,
				$tenaga->no_sertifikat,
				$tenaga->tgl_awal_berlaku_sertifikat,
				$tenaga->tgl_akhir_berlaku_sertifikat,
				$tenaga->status_sertifikat,
			], $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}


	public static function grouping($column)
	{
		return self::select($column)
				 ->groupBy($column)
				 ->get();
	}


	public static function jumlahTenagaAhliDataChart()
	{
		$listTahun = [];

		for ($tahun = date('Y'); $tahun >= (date('Y') - 2); $tahun--) { 
			$jumlahData = TenagaAhli::where('created_at', 'like', $tahun.'-')->count();
			$listTahun[(int) $tahun] = $jumlahData;
		}

		return $results;
	}

	public static function tenagaAhliDataChart()
	{
		$results = [];

		for ($tahun = date('Y'); $tahun >= (date('Y') - 2); $tahun--) { 
			$jumlahData = TenagaAhli::where('created_at', 'like', '%'.$tahun.'%')->count();
			if($jumlahData > 0) {
				$results[$tahun] = (int) $jumlahData;
			}
		}

		return $results;
	}

	public static function refreshStatus()
	{
		self::where('tgl_akhir_berlaku_sertifikat', '>=', date('Y-m-d'))->update([
			'status_sertifikat'	=> 'Aktif',
		]);
		self::where('tgl_akhir_berlaku_sertifikat', '<', date('Y-m-d'))->update([
			'status_sertifikat'	=> 'Tidak Aktif',
		]);
	}
}
