<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
	protected $fillable = [ 'category_name', 'slug', 'thumbnail', 'created_by', 'updated_by' ];


	public function posts()
	{
		return $this->hasMany('App\Models\Post', 'id_category');
	}


	public function amountOfPosts()
	{
		return count($this->posts);
	}


	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}


	public function createdByName()
	{
		return $this->createdBy ? $this->createdBy->name : '-';
	}


	public function updatedBy()
	{
		return $this->belongsTo('App\User', 'updated_by');
	}


	public function updatedByName()
	{
		return $this->updatedBy ? $this->updatedBy->name : '-';
	}


	public function generateSlug()
	{
		$base = \Str::of($this->category_name)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$postCategory = self::where('slug', $slug)
								->where('id', '!=', $this->id)
								->first();

			if($postCategory) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public function thumbnailPath()
	{
		return storage_path('app/public/cms/post_categories/'.$this->thumbnail);
	}


	public function thumbnailLink()
	{
		if($this->isHasThumbnail()) {
			return url('storage/cms/post_categories/'.$this->thumbnail);
		} else {
			return url('images/no-image.jpg');
		}
	}


	public function isHasThumbnail()
	{
		if(empty($this->thumbnail)) return false;

		return \File::exists($this->thumbnailPath());
	}


	public function removeThumbnail()
	{
		if($this->isHasThumbnail())
		{
			\File::delete($this->thumbnailPath());
			$this->update([
				'thumbnail'	=> null,
			]);
		}

		return $this;
	}


	public function setThumbnail($request)
	{
		if(!empty($request->thumbnail))
		{
			$this->removeThumbnail();
			$file = $request->file('thumbnail');
			$filename = $this->slug ?? date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/cms/post_categories/'), $filename);
			$this->update([
				'thumbnail'	=> $filename
			]);
		}

		return $this;
	}


	public static function createPostCategory($request)
	{
		$category = self::create([
			'category_name'		=> $request->category_name,
			'created_by'		=> auth()->user()->id,
		]);

		$category->generateSlug();
		$category->setThumbnail($request);

		return $category;
	}


	public function updatePostCategory($request)
	{
		$this->update([
			'category_name'		=> $request->category_name,
			'slug'				=> $request->slug,
			'updated_by'		=> auth()->user()->id,
		]);

		$this->setThumbnail($request);

		return $this;
	}


	public function deletePostCategory()
	{
		$this->removeThumbnail();
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->editColumn('thumbnail', function($data){
				$html = '<img src="'.$data->thumbnailLink().'" style="max-width: 30px; max-height: 30px;">';
				return $html;
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('post_category.edit', $data->id).'" title="Edit Kategori Postingan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('post_category.destroy', $data->id).'" title="Hapus Kategori Postingan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'thumbnail', 'action' ])
			->make(true);
	}
}
