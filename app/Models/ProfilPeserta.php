<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfilPeserta extends Model
{
	protected $table = 'profil_peserta';
	protected $fillable = [ 'id_user', 'nama_peserta', 'tempat_lahir', 'tgl_lahir', 'kelurahan', 'kecamatan', 'kota', 'no_ktp' ];


	public function updateProfile(array $request)
	{
		$this->update($request);
		return $this;
	}
}
