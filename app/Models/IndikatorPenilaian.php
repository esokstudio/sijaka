<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndikatorPenilaian extends Model
{
	protected $table = 'indikator_penilaian';
	protected $fillable = [ 'id_aspek_kinerja', 'nama_indikator_penilaian', 'bobot' ];


	public static function createIndikatorPenilaian(array $request)
	{
		return self::create($request);
	}


	public function updateIndikatorPenilaian(array $request)
	{
		$this->update($request);
		return $this;
	}


	public function deleteIndikatorPenilaian()
	{
		return $this->delete();
	}
}
