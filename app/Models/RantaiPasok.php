<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RantaiPasok extends Model
{
	protected $table = 'rantai_pasok';
	protected $fillable = [ 'nama_badan_usaha', 'alamat', 'jenis_usaha', 'no_sbu', 'tgl_sbu', 'berlaku_sbu', 'no_nib', 'tgl_nib', 'berlaku_nib', 'nomor_telepon', 'email', 'latitude', 'longitude', 'jenis_rantai_pasok', 'koordinat', 'embedded_maps' ];


	/** 
	 * Relationship methods
	 * */
	public function rantaiPasokMaterialKontruksi()
	{
		return $this->hasMany('App\Models\RantaiPasokMaterialKontruksi', 'id_rantai_pasok');
	}

	public function rantaiPasokPeralatanKontruksi()
	{
		return $this->hasMany('App\Models\RantaiPasokPeralatanKontruksi', 'id_rantai_pasok');
	}



	/**
	 * 	CRUD methods
	 * */
	public static function createRantaiPasok($request)
	{
		$rantaiPasok = self::create($request->all());
		return $rantaiPasok;
	}

	public function updateRantaiPasok($request)
	{
		$this->update($request->all());
		return $this;
	}

	public function deleteRantaiPasok()
	{
		return $this->delete();
	}



	/**
	 * 	Static methods
	 * */
	public static function dataTable($request)
	{
		$data = self::select([ 'rantai_pasok.*' ]);

		if(!empty($request->jenis_rantai_pasok)) {
			$data = $data->where('jenis_rantai_pasok', $request->jenis_rantai_pasok);
		}

		if(!empty($request->jenis_usaha)) {
			$data = $data->where('jenis_usaha', $request->jenis_usaha);
		}

		return \DataTables::eloquent($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('rantai_pasok.detail', $data->id).'" title="Detail Rantai Pasok">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item" href="'.route('rantai_pasok.edit', $data->id).'" title="Edit Rantai Pasok">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('rantai_pasok.destroy', $data->id).'" title="Hapus Rantai Pasok">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'status_perubahan', 'action' ])
			->make(true);
	}
}
