<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SumberDana extends Model
{
	protected $table = "sumber_dana";
	protected $fillable = [ 'sumber_dana' ];

	public static function createSumberDana($request)
	{
		$sumberDana = self::create([
			'sumber_dana'	=> $request->sumber_dana,
		]);

		return $sumberDana;
	}


	public function updateSumberDana($request)
	{
		$this->update([
			'sumber_dana'	=> $request->sumber_dana,
		]);

		return $this;
	}


	public function deleteSumberDana()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('sumber_dana.edit', $data->id).'" title="Edit Sumber Dana">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('sumber_dana.destroy', $data->id).'" title="Hapus Sumber Dana">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
