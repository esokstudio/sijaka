<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealisasiPaketPekerjaan extends Model
{
	protected $table = 'realisasi_paket_pekerjaan';
	protected $fillable = [ 'id_paket_pekerjaan', 'tahun', 'bulan', 'progress_keuangan', 'progress_fisik', 'foto_progress_fisik' ];


	public function paketPekerjaan()
	{
		return $this->belongsTo('App\Models\PaketPekerjaan', 'id_paket_pekerjaan');
	}


	public static function createRealisasi($request)
	{
		$realisasi = self::create([
			'id_paket_pekerjaan'=> $request->id_paket_pekerjaan,
			'tahun'				=> $request->tahun,
			'bulan'				=> $request->bulan,
			'progress_keuangan'	=> $request->progress_keuangan,
			'progress_fisik'	=> $request->progress_fisik,
		]);
		$realisasi->setFoto($request);

		return $realisasi;
	}


	public function updateRealisasi($request)
	{
		$this->update([
			'tahun'				=> $request->tahun,
			'bulan'				=> $request->bulan,
			'progress_keuangan'	=> $request->progress_keuangan,
			'progress_fisik'	=> $request->progress_fisik,
		]);
		$this->setFoto($request);

		return $this;
	}


	public function deleteRealisasi()
	{
		$this->removeFoto();
		return $this->delete();
	}


	public function setFoto($request)
	{
		if(!empty($request->foto))
		{
			$file = $request->file('foto');
			$filename = date('Ymd_His').'.'.$file->getClientOriginalExtension();
			$path = storage_path('app/public/realisasi');

			$file->move($path, $filename);
			$this->update([
				'foto_progress_fisik'	=> $filename
			]);
		}

		return $this;
	}


	public function removeFoto()
	{
		if(!empty($this->foto_progress_fisik)) {
			if(\File::exists($this->fotoPath())) {
				\File::delete($this->fotoPath());
				$this->update([
					'foto_progress_fisik'	=> null,
				]);
			}
		}

		return $this;
	}


	public function fotoLink()
	{
		return url('storage/realisasi/'.$this->foto_progress_fisik);
	}


	public function fotoPath()
	{
		return storage_path('app/public/realisasi/'.$this->foto_progress_fisik);
	}


	public static function dt($paketPekerjaanId)
	{
		$data = self::where('id_paket_pekerjaan', $paketPekerjaanId);

		return \DataTables::eloquent($data)
			->editColumn('foto_progress_fisik', function($data){
				return '<a href="'.$data->fotoLink().'" target="_blank"> Klik Disini </a>';
			})
			->editColumn('progress_keuangan', function($data){
				return 'Rp.'. number_format($data->progress_keuangan);
			})
			->editColumn('progress_fisik', function($data){
				return $data->progress_fisik .'%';
			})
			->addColumn('action', function($data) use ($paketPekerjaanId){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('paket_pekerjaan.realisasi.edit', [$paketPekerjaanId, $data->id]).'" title="Edit Realisasi Paket Pekerjaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('paket_pekerjaan.destroy', $data->id).'" title="Hapus Realisasi Paket Pekerjaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'foto_progress_fisik', 'action' ])
			->make(true);
	}
}
