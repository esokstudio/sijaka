<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageSlider extends Model
{
	protected $fillable = [ 'title', 'filename', 'is_published' ];


	public function isPublished()
	{
		return $this->is_published == 'yes';
	}


	public function imagePath()
	{
		return storage_path('app/public/cms/image_sliders/'.$this->filename);
	}


	public function imageLink()
	{
		if($this->isHasImage()) {
			return url('storage/cms/image_sliders/'.$this->filename);
		} else {
			return url('cms/images/no-image.jpg');
		}
	}


	public function isHasImage()
	{
		if(empty($this->filename)) return false;

		return \File::exists($this->imagePath());
	}


	public function setImage($request)
	{
		if(!empty($request->image)) {
			$this->removeImage();
			$file = $request->file('image');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/cms/image_sliders/'), $filename);
			$this->update([
				'filename'	=> $filename
			]);
		}

		return $this;
	}


	public function removeImage()
	{
		if($this->isHasImage())
		{
			\File::delete($this->imagePath());
			$this->update([
				'filename'	=> null,
			]);
		}

		return $this;
	}


	public static function createImageSlider($request)
	{
		$imageSlider = self::create([
			'title'			=> $request->title,
			'is_published'	=> $request->is_published
		]);

		$imageSlider->setImage($request);

		return $imageSlider;
	}


	public function updateImageSlider($request)
	{
		$this->update([
			'title'			=> $request->title,
			'is_published'	=> $request->is_published
		]);

		$this->setImage($request);

		return $this;
	}


	public function deleteImageSlider()
	{
		$this->removeImage();
		return $this->delete();
	}


	public function publishedHtml()
	{
		if($this->isPublished()) {
			return '<span class="text-success"> Ya </span>';
		} else {
			return '<span class="text-danger"> Tidak </span>';
		}
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->editColumn('is_published', function($data){
				return $data->publishedHtml();
			})
			->editColumn('filename', function($data){
				$html = '<img src="'.$data->imageLink().'" style="max-width: 30px; max-height: 30px;">';
				return $html;
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('image_slider.edit', $data->id).'" title="Edit Slide Foto">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('image_slider.destroy', $data->id).'" title="Hapus Slide Foto">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'is_published', 'filename', 'action' ])
			->make(true);
	}
}
