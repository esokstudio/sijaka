<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model
{
	use SoftDeletes;
	
	protected $table = "perusahaan";
	protected $fillable = [ 'nama_perusahaan', 'direktur', 'alamat', 'kota', 'kodepos', 'nomor_telepon', 'fax', 'email', 'website', 'bentuk_perusahaan', 'jenis_perusahaan', 'id_asosiasi', 'file_sbu', 'nib', 'nilai_kinerja', 'nilai_keterangan', 'data_perubahan', 'status_perubahan', 'slug' ];


	/**
	 * 	Relationship
	 * */
	public function aktePendirianPerusahaan()
	{
		return $this->hasOne('App\Models\AktePendirianPerusahaan', 'id_perusahaan');
	}

	public function pengesahanPerusahaan()
	{
		return $this->hasOne('App\Models\PengesahanPerusahaan', 'id_perusahaan');
	}

	public function kualifikasiPerusahaan()
	{
		return $this->hasMany('App\Models\KualifikasiPerusahaan', 'id_perusahaan');
	}

	public function aktePerubahanPerusahaan()
	{
		return $this->hasMany('App\Models\AktePerubahanPerusahaan', 'id_perusahaan');
	}

	public function pengurusPerusahaan()
	{
		return $this->hasMany('App\Models\PengurusPerusahaan', 'id_perusahaan');
	}

	public function keuanganPerusahaan()
	{
		return $this->hasMany('App\Models\KeuanganPerusahaan', 'id_perusahaan');
	}

	public function tenagaKerjaPerusahaan()
	{
		return $this->hasMany('App\Models\TenagaKerjaPerusahaan', 'id_perusahaan');
	}

	public function paketPekerjaanPerusahaan()
	{
		return $this->hasMany('App\Models\PaketPekerjaan', 'id_perusahaan');
	}

	public function pengalamanPerusahaan()
	{
		return $this->hasMany('App\Models\Pengalaman', 'id_perusahaan');
	}

	public function asosiasi()
	{
		return $this->belongsTo('App\Models\Asosiasi', 'id_asosiasi');
	}

	public function namaAsosiasi()
	{
		return $this->asosiasi ? $this->asosiasi->nama_asosiasi : '';
	}

	public function newAsosiasi()
	{
		return Asosiasi::find(tmp($this, 'id_asosiasi'));
	}

	public function newNamaAsosiasi()
	{
		return $this->newAsosiasi() ? $this->newAsosiasi()->nama_asosiasi : '';
	}

	public function penilaian()
	{
		return $this->hasMany('App\Models\Penilaian', 'id_perusahaan');
	}


	public static function createPerusahaan(array $request)
	{
		$request['status_perubahan'] = 'aproved';
		$perusahaan = self::create($request);

		return $perusahaan;
	}


	public function updatePerusahaan(array $request)
	{
		$this->update([
			'data_perubahan'	=> serialize($request),
			'status_perubahan' => 'wait',
		]);

		if(!$this->isChange()) {
			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan' => 'approved',
			]);
		}

		if(auth()->user()->isAdmin() || auth()->user()->isPengguna()) {
			$this->applyUpdate();
		}

		return $this;
	}


	public function deletePerusahaan()
	{
		return $this->delete();
	}


	public static function dt($request)
	{
		$data = self::with([ 'asosiasi' ])
					->select([ 'perusahaan.*' ])
					->leftJoin('asosiasi','perusahaan.id_asosiasi','=', 'asosiasi.id');;

		if(!empty($request->kota)) {
			$data = $data->where('kota', $request->kota);
		}

		if(!empty($request->bentuk_perusahaan)) {
			$data = $data->where('bentuk_perusahaan', $request->bentuk_perusahaan);
		}

		if(!empty($request->jenis_perusahaan)) {
			$data = $data->where('jenis_perusahaan', $request->jenis_perusahaan);
		}

		if(!empty($request->id_asosiasi)) {
			$data = $data->where('id_asosiasi', $request->id_asosiasi);
		}

		return \DataTables::eloquent($data)
			->editColumn('asosiasi.nama_asosiasi', function($data){
				return $data->namaAsosiasi();
			})
			->editColumn('status_perubahan', function($data){
				return $data->statusPerubahanHtml();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>';

				$button .= '
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('perusahaan.detail', $data->id).'" title="Detail Perusahaan">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item" href="'.route('perusahaan.edit', $data->id).'" title="Edit Perusahaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('perusahaan.destroy', $data->id).'" title="Hapus Perusahaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'status_perubahan', 'action' ])
			->make(true);
	}


	public function getDataPerubahan()
	{
		try {
			$result = unserialize($this->data_perubahan);
			if($result) {
				return $result;
			} else {
				return [];
			}
		} catch (\Exception $e) {
			return [];
		}
	}


	public function applyUpdate()
	{
		$this->update($this->getDataPerubahan());
		$this->update([
			'data_perubahan'	=> serialize([]),
			'status_perubahan' => 'approved',
		]);

		return $this;
	}


	public function reverseUpdate()
	{
		$this->update([
			'data_perubahan'	=> serialize([]),
			'status_perubahan' => 'approved',
		]);

		return $this;
	}


	public function isStatusWait()
	{
		return $this->status_perubahan == 'wait';
	}


	public function statusPerubahanHtml()
	{
		if($this->status_perubahan == 'approved') {
			return '<span class="text-success"> Terverifikasi </span>';
		} elseif($this->status_perubahan == 'rejected') {
			return '<span class="text-danger"> Ditolak </span>';
		} elseif($this->status_perubahan == 'wait') {
			return '<span class="text-primary"> Menunggu Verifikasi </span>';
		}
	}


	public function getAktePendirianPerusahaan()
	{
		if($this->aktePendirianPerusahaan) {
			return $this->aktePendirianPerusahaan;
		} else {
			return AktePendirianPerusahaan::create([
				'id_perusahaan'		=> $this->id,
				'status_perubahan'	=> 'approved',
			]);
		}
	}


	public function getPengesahanPerusahaan()
	{
		if($this->pengesahanPerusahaan) {
			return $this->pengesahanPerusahaan;
		} else {
			return PengesahanPerusahaan::create([
				'id_perusahaan'	=> $this->id,
				'status_perubahan'	=> 'approved',
			]);
		}
	}


	public static function getPerusahaanStatusWait()
	{
		return self::where('status_perubahan', 'wait')->get();
	}

	public static function countPerusahaanStatusWait() : int
	{
		return self::where('status_perubahan', 'wait')->count();
	}

	public function isChange()
	{
		try {
			$perubahan = (object) $this->getDataPerubahan();
			if($this->nama_perusahaan != $perubahan->nama_perusahaan) return true;
			if($this->direktur != $perubahan->direktur) return true;
			if($this->alamat != $perubahan->alamat) return true;
			if($this->kota != $perubahan->kota) return true;
			if($this->kodepos != $perubahan->kodepos) return true;
			if($this->nomor_telepon != $perubahan->nomor_telepon) return true;
			if($this->fax != $perubahan->fax) return true;
			if($this->email != $perubahan->email) return true;
			if($this->website != $perubahan->website) return true;
			if($this->bentuk_perusahaan != $perubahan->bentuk_perusahaan) return true;
			if($this->jenis_perusahaan != $perubahan->jenis_perusahaan) return true;
			if($this->id_asosiasi != $perubahan->id_asosiasi) return true;

			return false;
		} catch (\Exception $e) {
			return false;
		}
	}


	public function setFileSbu($request)
	{
		if(!empty($request->file_sbu))
		{
			$file = $request->file('file_sbu');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/sbu/'), $filename);
			$this->update([
				'file_sbu'	=> $filename
			]);
		}

		return $this;
	}


	public function fileSbuLink()
	{
		return url('storage/sbu/'.$this->file_sbu);
	}


	public function fileSbuPath()
	{
		return storage_path('app/public/sbu/'.$this->file_sbu);
	}


	public function isHasFileSbu()
	{
		if(empty($this->file_sbu)) return false;

		return \File::exists($this->fileSbuPath());
	}


	public function generatePenilaian()
	{
		$this->load('penilaian');
		if(count($this->penilaian) > 0)
		{
			$nilai = 0;
			foreach($this->penilaian as $penilaian) {
				$nilai += $penilaian->nilai_kinerja;
			}

			$nilaiKinerja = $nilai / count($this->penilaian);

			$nilaiKeterangan = '';
			if($nilaiKinerja >= 81 && $nilaiKinerja <= 100) $nilaiKeterangan = "Sangat Baik";
			if($nilaiKinerja >= 71 && $nilaiKinerja <= 80) $nilaiKeterangan = "Baik";
			if($nilaiKinerja >= 61 && $nilaiKinerja <= 70) $nilaiKeterangan = "Cukup";
			if($nilaiKinerja >= 51 && $nilaiKinerja <= 60) $nilaiKeterangan = "Kurang";
			if($nilaiKinerja <= 50) $nilaiKeterangan = "Sangat Kurang";

			$this->update([
				'nilai_kinerja'		=> $nilaiKinerja,
				'nilai_keterangan'	=> $nilaiKeterangan,
			]);
		}

		return $this;
	}

	public function generateSlug()
	{
		$base = \Str::of($this->nama_perusahaan)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$post = self::where('slug', $slug)
						->where('id', '!=', $this->id)
						->first();
			if($post) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public static function importFromExcel($request)
	{
		if(!empty($request->file))
		{
			$file = $request->file('file');
			$ext = $file->getClientOriginalExtension();

			if(in_array($ext, [ 'xlsx', 'xls' ]))
			{
				$filename = 'perusahaan_'.date('Ymdhis') . '.'.$ext;
				$filepath = \Helper::temps($filename);
				$path = \Helper::temps('');
				$file->move($path, $filename);

				if($xlsx = \App\MyClass\SimpleXLSX::parse($filepath)) {

					$iter = 1;
					$count = 0;
					foreach($xlsx->rows() as $row) {
						if($iter > 1) {
							$namaAsosiasi = $row[0];
							$namaPerusahaan = $row[1];
							$direktur = $row[2];
							$alamat = $row[3];

							if(!empty($namaAsosiasi) && !empty($namaPerusahaan)
								&& !empty($direktur) && !empty($alamat))
							{
								$asosiasi = Asosiasi::where('nama_asosiasi', $namaAsosiasi)->first();
								if(empty($asosiasi)) {
									$asosiasi = Asosiasi::create([
										'nama_asosiasi'	=> $namaAsosiasi
									]);
								}

								$perusahaan = self::where('nama_perusahaan', $namaPerusahaan)
										   		  ->first();
								if(!$perusahaan) {
									try {
										self::create([
											'id_asosiasi'		=> $asosiasi->id,
											'nama_perusahaan'	=> $namaPerusahaan,
											'direktur'			=> $direktur,
											'alamat'			=> $alamat,
											'kota'				=> $row[4],
											'kodepos'			=> $row[5],
											'nomor_telepon'		=> $row[6],
											'fax'				=> $row[7],
											'email'				=> $row[8],
											'website'			=> $row[9],
											'bentuk_perusahaan'	=> $row[10],
											'jenis_perusahaan'	=> $row[11],
											'nib'				=> $row[12],
										]);
										$count++;
									} catch (Exception $e) {}
								} else {
									try {
										$perusahaan->update([
											'id_asosiasi'		=> $asosiasi->id,
											'nama_perusahaan'	=> $namaPerusahaan,
											'direktur'			=> $direktur,
											'alamat'			=> $alamat,
											'kota'				=> $row[4],
											'kodepos'			=> $row[5],
											'nomor_telepon'		=> $row[6],
											'fax'				=> $row[7],
											'email'				=> $row[8],
											'website'			=> $row[9],
											'bentuk_perusahaan'	=> $row[10],
											'jenis_perusahaan'	=> $row[11],
											'nib'				=> $row[12],
										]);
									} catch (Exception $e) {}
								}
							}

						}

						$iter++;
					}

					return $count;

				} else {
					throw new \Exception("Pembacaan File Gagal");
				}
			}
			else
			{
				throw new \Exception("Ekstensi tidak didukung");
			}
		} 
		else 
		{
			throw new \Exception("File Kosong");
		}
	}


	public static function grouping($column)
	{
		return self::select($column)
				 ->groupBy($column)
				 ->get();
	}


	public static function exportToExcel($request)
	{
		$data = self::where('created_at', '!=', null);

		if(!empty($request->kota)) {
			if($request->kota != 'all') {
				$data = $data->where('kota', $request->kota);
			}
		}

		if(!empty($request->bentuk_perusahaan)) {
			if($request->bentuk_perusahaan != 'all') {
				$data = $data->where('bentuk_perusahaan', $request->bentuk_perusahaan);
			}
		}

		if(!empty($request->jenis_perusahaan)) {
			if($request->jenis_perusahaan != 'all') {
				$data = $data->where('jenis_perusahaan', $request->jenis_perusahaan);
			}
		}

		if(!empty($request->id_asosiasi)) {
			if($request->id_asosiasi != 'all') {
				$data = $data->where('id_asosiasi', $request->id_asosiasi);
			}
		}

		$data = $data->get();

		$filename = 'Perusahaan.xlsx';

		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 20 ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			'No' => 'string',
			'Nama Perusahaan' => 'string',
			'Direktur' => 'string',
			'Alamat' => 'string',
			'Kota' => 'string',
			'Kodepos' => 'string',
			'No Telepon' => 'string',
			'Fax' => 'string',
			'Email' => 'string',
			'Website' => 'string',
			'Bentuk' => 'string',
			'Jenis' => 'string',
			'Asosiasi' => 'string',
			'NIB' => 'string',
			'Nilai Kinerja' => 'string',
			'Nilai Keterangan' => 'string',
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,25,20,25,15,10,20,20,20,20,20,25,20,20,15,15],
			'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 5, 'wrap_text' => true
		]);

		$iter = 1;
		foreach($data as $perusahaan)
		{
			$writer->writeSheetRow('Sheet1', [
				$iter++,
				$perusahaan->nama_perusahaan,
				$perusahaan->direktur,
				$perusahaan->alamat,
				$perusahaan->kota,
				$perusahaan->nomor_telepon,
				$perusahaan->alamat,
				$perusahaan->fax,
				$perusahaan->email,
				$perusahaan->website,
				$perusahaan->bentuk_perusahaan,
				$perusahaan->jenis_perusahaan,
				$perusahaan->namaAsosiasi(),
				$perusahaan->nib,
				$perusahaan->nilai_kinerja,
				$perusahaan->nilai_keterangan,
			], $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}
}
