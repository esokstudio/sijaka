<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
	protected $fillable = [ 'id_link_group', 'title', 'source', 'link', 'position', 'is_open_new_tab' ];


	public function setPosition()
	{
		if(self::count() > 0) {
			$lastPosition = self::orderBy('position', 'desc')
								->where('id_link_group', $this->id_link_group)
								->first()->position;
			$position = (int) $lastPosition + 1;
		} else {
			$position = 1;
		}

		$this->update([
			'position'	=> $position
		]);
		self::autoResetPosition($this->id_link_group);

		return $this;
	}


	public function getLink()
	{
		if ($this->source == 'page') {
			return url($this->link);
		} elseif ($this->source == 'post') {
			return url('post').'/'.$this->link;
		} elseif ($this->source == 'web') {
			return url('').'/'.$this->link;
		} elseif ($this->source == 'url') {
			return $this->link;
		}
	}


	public function sourceText()
	{
		if ($this->source == 'page') {
			return 'Laman';
		} elseif ($this->source == 'post') {
			return 'Postingan';
		} elseif ($this->source == 'web') {
			return 'Website Ini';
		} elseif ($this->source == 'url') {
			return 'URL';
		}
	}


	public static function autoResetPosition($idLinkGroup)
	{
		$links = self::orderBy('position', 'asc')
						->where('id_link_group', $idLinkGroup)
						->get();
		$iter = 1;

		foreach($links as $link)
		{
			if($link->position != $iter) {
				$link->update([
					'position'	=> $iter
				]);
			}
			$iter++;
		}

		return $links;
	}


	public static function createLink(array $request)
	{
		$link = self::create($request);
		$link->setPosition($link->id_link_group);

		return $link;
	}


	public function updateLink(array $request)
	{
		$this->update($request);
		self::autoResetPosition($this->id_link_group);

		return $this;
	}


	public function deleteLink()
	{
		$groupID = $this->id_link_group;
		$result = $this->delete();
		self::autoResetPosition($groupID);

		return $result;
	}


	public static function dt($linkGroupID)
	{
		$data = self::where('id_link_group', $linkGroupID)->get();

		return \DataTables::of($data)
			->addColumn('source', function($data){
				return $data->sourceText();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('link.edit', [$data->id_link_group, $data->id]).'" title="Edit Link">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('link.destroy', [$data->id_link_group, $data->id]).'" title="Hapus Link">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('link.switch_to_up', [$data->id_link_group, $data->id]).'" data-message="Yakin ingin naikan posisi?" title="Naikan Urutan">
							<i class="mdi mdi-arrow-up"></i> Naikan Urutan
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('link.switch_to_down', [$data->id_link_group, $data->id]).'" data-message="Yakin ingin turunkan posisi?" title="Turunkan Urutan">
							<i class="mdi mdi-arrow-down"></i> Turunkan Urutan
						</a>
						<a class="dropdown-item" href="'.$data->getLink().'" title="Kunjungi Link" target="_blank">
							<i class="mdi mdi-web"></i> Kunjungi
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function switchToUp()
	{
		self::autoResetPosition($this->id_link_group);
		if($this->position != 1) {
			$link = self::where('position', ($this->position - 1))
						->where('id_link_group', $this->id_link_group)
						->first();

			$link->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position - 1,
			]);
		}

		return $this;
	}


	public function switchToDown()
	{
		self::autoResetPosition($this->id_link_group);
		$total = self::count();

		if($this->position != $total ) {
			$link = self::where('position', ($this->position + 1))
						->where('id_link_group', $this->id_link_group)
						->first();

			$link->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position + 1,
			]);
		}

		return $this;
	}
}
