<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Klasifikasi extends Model
{
	protected $table = "klasifikasi";
	protected $fillable = [ 'nama_klasifikasi' ];


	public function subKlasifikasi()
	{
		return $this->hasMany('App\Models\SubKlasifikasi', 'id_klasifikasi');
	}


	public static function createKlasifikasi(array $request)
	{
		$klasifikasi = self::create($request);

		return $klasifikasi;
	}


	public function updateKlasifikasi(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteKlasifikasi()
	{
		$this->deleteSubKlasifikasi();
		return $this->delete();
	}


	public function deleteSubKlasifikasi()
	{
		SubKlasifikasi::where('id_klasifikasi', $this->id)->delete();
		return $this;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('klasifikasi.edit', $data->id).'" title="Edit Klasifikasi">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('klasifikasi.destroy', $data->id).'" title="Hapus Klasifikasi">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
