<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $fillable = [ 'title', 'slug', 'content', 'thumbnail', 'custom_page_file', 'is_published', 'is_custom_page', 'created_by', 'updated_by' ];


	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}


	public function createdByName()
	{
		return $this->createdBy ? $this->createdBy->name : '-';
	}


	public function updatedBy()
	{
		return $this->belongsTo('App\User', 'updated_by');
	}


	public function updatedByName()
	{
		return $this->updatedBy ? $this->updatedBy->name : '-';
	}


	public function generateSlug()
	{
		$base = \Str::of($this->title)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$page = self::where('slug', $slug)
						->where('id', '!=', $this->id)
						->first();

			if($page) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public function createdAtText($format = 'd M Y')
	{
		return date($format, strtotime($this->created_at));
	}


	public function thumbnailPath()
	{
		return storage_path('app/public/cms/pages/'.$this->thumbnail);
	}


	public function thumbnailLink()
	{
		if($this->isHasThumbnail()) {
			return url('storage/cms/pages/'.$this->thumbnail);
		} else {
			return url('images/no-image.jpg');
		}
	}


	public function isHasThumbnail()
	{
		if(empty($this->thumbnail)) return false;

		return \File::exists($this->thumbnailPath());
	}


	public function removeThumbnail()
	{
		if($this->isHasThumbnail())
		{
			\File::delete($this->thumbnailPath());
			$this->update([
				'thumbnail'	=> null,
			]);
		}

		return $this;
	}


	public function setThumbnail($request)
	{
		if(!empty($request->thumbnail))
		{
			$this->removeThumbnail();
			$file = $request->file('thumbnail');
			$filename = $this->slug ?? date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/cms/pages/'), $filename);
			$this->update([
				'thumbnail'	=> $filename
			]);
		}

		return $this;
	}


	public static function createPage($request)
	{
		$page = self::create([
			'title'				=> $request->title,
			'content'			=> \Helper::fixContent($request->content),
			'custom_page_file'	=> $request->custom_page_file,
			'is_published'		=> $request->is_published,
			'is_custom_page'	=> $request->is_custom_page,
			'created_by'		=> auth()->user()->id,
		]);

		$page->generateSlug();
		$page->setThumbnail($request);

		return $page;
	}


	public function updatePage($request)
	{
		$this->update([
			'title'				=> $request->title,
			'content'			=> \Helper::fixContent($request->content),
			'custom_page_file'	=> $request->custom_page_file,
			'is_published'		=> $request->is_published,
			'is_custom_page'	=> $request->is_custom_page,
			'slug'				=> $request->slug,
			'updated_by'		=> auth()->user()->id,
		]);

		$this->setThumbnail($request);

		return $this;
	}


	public function deletePage()
	{
		$this->removeThumbnail();
		return $this->delete();
	}


	public function getLink()
	{
		return url($this->fullSlug());
	}


	public function fullSlug()
	{
		return $this->slug;
	}


	public function modifiedAt()
	{
		return $this->updated_at ?? $this->created_at;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->editColumn('is_published', function($data){
				return $data->publishedHtml();
			})
			->editColumn('thumbnail', function($data){
				$html = '<img src="'.$data->thumbnailLink().'" style="max-width: 30px; max-height: 30px;">';
				return $html;
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('page.edit', $data->id).'" title="Edit Laman">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('page.destroy', $data->id).'" title="Hapus Laman">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item" href="'.$data->getLink().'" title="Kunjungi Laman" target="_blank">
							<i class="mdi mdi-web"></i> Kunjungi
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'is_published', 'thumbnail', 'action' ])
			->make(true);
	}


	public function isPublished()
	{
		return $this->is_published == 'yes';
	}


	public function publishedHtml()
	{
		if($this->isPublished()) {
			return '<span class="text-success"> Ya </span>';
		} else {
			return '<span class="text-danger"> Tidak </span>';
		}
	}


	public function isCustomPage()
	{
		return $this->is_custom_page == 'yes';
	}


	public static function getBySlug($slug)
	{
		return self::where('slug', $slug)->first();
	}


	public static function publishedPages()
	{
		$pages = self::where('is_published', 'yes')->get();

		return $pages;
	}

}
