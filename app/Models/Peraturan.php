<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peraturan extends Model
{
	protected $table = "peraturan";
	protected $fillable = [ 'id_kategori_peraturan', 'nomor', 'tahun', 'judul', 'filename', 'is_published' ];


	public function kategoriPeraturan()
	{
		return $this->belongsTo('App\Models\KategoriPeraturan', 'id_kategori_peraturan');
	}


	public function namaKategori()
	{
		return $this->kategoriPeraturan ? $this->kategoriPeraturan->nama_kategori : '-';
	}


	public function filePath()
	{
		return storage_path('app/public/peraturan/'.$this->filename);
	}


	public function fileLink()
	{
		if($this->isHasFile()) {
			return url('storage/peraturan/'.$this->filename);
		} else {
			return url('cms/images/no-image.jpg');
		}
	}


	public function isHasFile()
	{
		if(empty($this->filename)) return false;

		return \File::exists($this->filePath());
	}


	public function removeFile()
	{
		if($this->isHasFile())
		{
			\File::delete($this->filePath());
			$this->update([
				'filename'	=> null,
			]);
		}

		return $this;
	}


	public function saveFile($request)
	{
		if(!empty($request->file))
		{
			$this->removeFile();
			$file = $request->file('file');
			$filename = date('Ymd_His_').\Str::slug($this->judul);
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/peraturan/'), $filename);
			$this->update([
				'filename'	=> $filename
			]);
		}

		return $this;
	}


	public static function createPeraturan($request)
	{
		$peraturan = self::create([
			'id_kategori_peraturan'		=> $request->id_kategori_peraturan,
			'nomor'						=> $request->nomor,
			'tahun'						=> $request->tahun,
			'judul'						=> $request->judul,
			'is_published'				=> $request->is_published,
		]);

		$peraturan->saveFile($request);

		return $peraturan;
	}


	public function updatePeraturan($request)
	{
		$this->update([
			'id_kategori_peraturan'		=> $request->id_kategori_peraturan,
			'nomor'						=> $request->nomor,
			'tahun'						=> $request->tahun,
			'judul'						=> $request->judul,
			'is_published'				=> $request->is_published,
		]);

		$this->saveFile($request);

		return $this;
	}


	public function deletePeraturan()
	{
		$this->removeFile();
		return $this->delete();
	}


	public function createdAtText()
	{
		return date('Y-m-d', strtotime($this->created_at));
	}


	public static function dt()
	{
		$data = self::with([ 'kategoriPeraturan' ]);

		return \DataTables::eloquent($data)
			->editColumn('kategori_peraturan.nama_kategori', function($data){
				return $data->namaKategori();
			})
			->editColumn('created_at', function($data){
				return $data->createdAtText();
			})
			->editColumn('is_published', function($data){
				return $data->publishedHtml();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('peraturan.edit', $data->id).'" title="Edit Peraturan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('peraturan.destroy', $data->id).'" title="Hapus Peraturan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'is_published', 'action' ])
			->make(true);
	}


	public function isPublished()
	{
		return $this->is_published == 'yes';
	}


	public function publishedHtml()
	{
		if($this->isPublished()) {
			return '<span class="text-success"> Ya </span>';
		} else {
			return '<span class="text-danger"> Tidak </span>';
		}
	}
}
