<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RantaiPasokMaterialKontruksi extends Model
{
	protected $table = 'rantai_pasok_material_kontruksi';
	protected $fillable = [ 'id_rantai_pasok', 'nama_produk', 'nama_sub_produk', 'merk_produk', 'file_sertifikat_tkdn', 'file_sertifikat_sni', 'file_foto' ];


	/**
	 * 	Relationship methods
	 * */
	public function rantaiPasok()
	{
		return $this->belongsTo('App\Models\RantaiPasok', 'id_rantai_pasok');
	}


	public static function createRantaiPasokMaterialKontruksi($request)
	{
		$material = self::create($request->all());
		$material->saveFileSertifikatTkdn($request);
		$material->saveFileSertifikatSni($request);
		$material->saveFileFoto($request);
		return $material;
	}

	public function updateRantaiPasokMaterialKontruksi($request)
	{
		$this->update($request->all());
		$this->saveFileSertifikatTkdn($request);
		$this->saveFileSertifikatSni($request);
		$this->saveFileFoto($request);
		return $this;
	}

	public function deleteRantaiPasokMaterialKontruksi()
	{
		$this->removeFileSertifikatTkdn();
		$this->removeFileSertifikatSni();
		$this->removeFileFoto();
		return $this->delete();
	}



	/**
	 * 	Helper methods
	 * */
	public function namaBadanUsaha()
	{
		return $this->rantaiPasok->nama_badan_usaha ?? '-';
	}

	public function alamat()
	{
		return $this->rantaiPasok->alamat ?? '-';
	}

	// Sertifikat TKDN
	public function saveFileSertifikatTkdn($request)
	{
		if(!empty($request->file_upload_sertifikat_tkdn)) {
			$this->removeFileSertifikatTkdn();
			$file = $request->file('file_upload_sertifikat_tkdn');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/sertifikat_tkdn'), $filename);
			$this->update([
				'file_sertifikat_tkdn' => $filename
			]);
		}

		return $this;
	}

	public function removeFileSertifikatTkdn()
	{
		if($this->isHasFileSertifikatTkdn()) {
			\File::delete($this->fileSertifikatTkdnPath());
			$this->update([
				'file_sertifikat_tkdn' => null
			]);
		}

		return $this;
	}

	public function fileSertifikatTkdnPath()
	{
		return storage_path('app/public/sertifikat_tkdn/'.$this->file_sertifikat_tkdn);
	}

	public function fileSertifikatTkdnLink()
	{
		return url('storage/sertifikat_tkdn/'.$this->file_sertifikat_tkdn);
	}

	public function isHasFileSertifikatTkdn()
	{
		if(empty($this->file_sertifikat_tkdn)) return false;
		return \File::exists($this->fileSertifikatTkdnPath());
	}


	// Sertifikat SNI
	public function saveFileSertifikatSni($request)
	{
		if(!empty($request->file_upload_sertifikat_sni)) {
			$this->removeFileSertifikatSni();
			$file = $request->file('file_upload_sertifikat_sni');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/sertifikat_sni'), $filename);
			$this->update([
				'file_sertifikat_sni' => $filename
			]);
		}

		return $this;
	}

	public function removeFileSertifikatSni()
	{
		if($this->isHasFileSertifikatSni()) {
			\File::delete($this->fileSertifikatSniPath());
			$this->update([
				'file_sertifikat_sni' => null
			]);
		}

		return $this;
	}

	public function fileSertifikatSniPath()
	{
		return storage_path('app/public/sertifikat_sni/'.$this->file_sertifikat_sni);
	}

	public function fileSertifikatSniLink()
	{
		return url('storage/sertifikat_sni/'.$this->file_sertifikat_sni);
	}

	public function isHasFileSertifikatSni()
	{
		if(empty($this->file_sertifikat_sni)) return false;
		return \File::exists($this->fileSertifikatSniPath());
	}

	// File Foto
	public function saveFileFoto($request)
	{
		if(!empty($request->file_upload_foto)) {
			$this->removeFileFoto();
			$file = $request->file('file_upload_foto');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/foto_item_rantai_pasok'), $filename);
			$this->update([
				'file_foto' => $filename
			]);
		}

		return $this;
	}

	public function removeFileFoto()
	{
		if($this->isHasFileFoto()) {
			\File::delete($this->fileFotoPath());
			$this->update([
				'file_foto' => null
			]);
		}

		return $this;
	}

	public function fileFotoPath()
	{
		return storage_path('app/public/foto_item_rantai_pasok/'.$this->file_foto);
	}

	public function fileFotoLink()
	{
		return url('storage/foto_item_rantai_pasok/'.$this->file_foto);
	}

	public function isHasFileFoto()
	{
		if(empty($this->file_foto)) return false;
		return \File::exists($this->fileFotoPath());
	}



	/**
	 * 	Static methods
	 * */
	public static function dataTable($request)
	{
		$data = self::select([ 'rantai_pasok_material_kontruksi.*' ]);

		if($request->id_rantai_pasok) {
			$data = $data->where('id_rantai_pasok', $request->id_rantai_pasok);
		}

		return \DataTables::eloquent($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item edit" href="javascript:void(0);" data-update-href="'.route('rantai_pasok_material_kontruksi.update', $data->id).'" data-get-href="'.route('rantai_pasok_material_kontruksi.get', $data->id).'" title="Edit Rantai Pasok">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('rantai_pasok_material_kontruksi.destroy', $data->id).'" title="Hapus Rantai Pasok">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->editColumn('file_sertifikat_tkdn', function($data){
				return '<a href="'.$data->fileSertifikatTkdnLink().'" target="_blank"> Klik Disini </a>';
			})
			->editColumn('file_sertifikat_sni', function($data){
				return '<a href="'.$data->fileSertifikatSniLink().'" target="_blank"> Klik Disini </a>';
			})
			->editColumn('file_foto', function($data){
				return '<img src="'.$data->fileFotoLink().'" style="width: 30px; height: 30px;">';
			})
			->rawColumns([ 'file_sertifikat_tkdn', 'file_sertifikat_sni', 'file_foto', 'status_perubahan', 'action' ])
			->make(true);
	}
}
