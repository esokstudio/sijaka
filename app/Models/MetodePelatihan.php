<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetodePelatihan extends Model
{
	protected $table = "metode_pelatihan";
	protected $fillable = [ 'nama_metode_pelatihan' ];


	public static function createMetodePelatihan(array $request)
	{
		$jenisKeahlian = self::create($request);

		return $jenisKeahlian;
	}


	public function updateMetodePelatihan(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteMetodePelatihan()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('metode_pelatihan.edit', $data->id).'" title="Edit Metode Pelatihan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('metode_pelatihan.destroy', $data->id).'" title="Hapus Metode Pelatihan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
