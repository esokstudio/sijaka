<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisKeahlian extends Model
{
	protected $table = "jenis_keahlian";
	protected $fillable = [ 'nama_jenis_keahlian' ];


	public function tingkatKeahlian()
	{
		return $this->hasMany('App\Models\TingkatKeahlian', 'id_jenis_keahlian');
	}


	public static function createJenisKeahlian(array $request)
	{
		$jenisKeahlian = self::create($request);

		return $jenisKeahlian;
	}


	public function updateJenisKeahlian(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteJenisKeahlian()
	{
		$this->deleteTingkatKeahlian();
		return $this->delete();
	}


	public function deleteTingkatKeahlian()
	{
		TingkatKeahlian::where('id_jenis_keahlian', $this->id)->delete();
		return $this;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('jenis_keahlian.edit', $data->id).'" title="Edit Jenis Keahlian">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('jenis_keahlian.destroy', $data->id).'" title="Hapus Jenis Keahlian">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
