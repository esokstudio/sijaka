<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NavMenu extends Model
{
	protected $fillable = [ 'title', 'source', 'link', 'position', 'is_open_new_tab' ];

	const SOURCE_PAGE		= 'page';
	const SOURCE_POST		= 'post';
	const SOURCE_LINK		= 'link';
	const SOURCE_THIS_WEB	= 'web';

	public function navSubmenus()
	{
		return $this->hasMany('App\Models\NavSubmenu', 'id_nav_menu')
					->orderBy('position', 'asc');
	}


	public function submenus()
	{
		return $this->hasMany('App\Models\NavSubmenu', 'id_nav_menu')
					->orderBy('position', 'asc');
	}


	public function amountOfNavSubmenus()
	{
		return count($this->navSubmenus);
	}


	public function isHasSubmenu()
	{
		return $this->amountOfNavSubmenus() > 0;
	}


	public function setPosition()
	{
		if(self::count() > 0) {
			$lastPosition = self::orderBy('position', 'desc')->first()->position;
			$position = (int) $lastPosition + 1;
		} else {
			$position = 1;
		}

		$this->update([
			'position'	=> $position
		]);
		self::autoResetPosition();

		return $this;
	}


	public function getLink()
	{
		if ($this->source == 'page') {
			return url($this->link);
		} elseif ($this->source == 'post') {
			return url('post').'/'.$this->link;
		} elseif ($this->source == 'web') {
			return url('').'/'.$this->link;
		} elseif ($this->source == 'url') {
			return $this->link;
		}
	}


	public function sourceText()
	{
		if ($this->source == 'page') {
			return 'Laman';
		} elseif ($this->source == 'post') {
			return 'Postingan';
		} elseif ($this->source == 'web') {
			return 'Website Ini';
		} elseif ($this->source == 'url') {
			return 'URL';
		}
	}


	public static function autoResetPosition()
	{
		$menus = self::orderBy('position', 'asc')->get();
		$iter = 1;

		foreach($menus as $menu)
		{
			if($menu->position != $iter) {
				$menu->update([
					'position'	=> $iter
				]);
			}
			$iter++;
		}

		return $menus;
	}


	public static function createNavMenu(array $request)
	{
		$menu = self::create($request);
		$menu->setPosition();

		return $menu;
	}


	public function updateNavMenu(array $request)
	{
		$this->update($request);
		self::autoResetPosition();

		return $this;
	}


	public function deleteNavMenu()
	{
		$result = $this->delete();
		self::autoResetPosition();

		return $result;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('amount_of_nav_submenu', function($data){
				return $data->amountOfNavSubmenus();
			})
			->addColumn('source', function($data){
				return $data->sourceText();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('nav_menu.edit', $data->id).'" title="Edit Menu">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('nav_menu.destroy', $data->id).'" title="Hapus Menu">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('nav_menu.switch_to_up', $data->id).'" data-message="Yakin ingin naikan posisi?" title="Naikan Urutan">
							<i class="mdi mdi-arrow-up"></i> Naikan Urutan
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('nav_menu.switch_to_down', $data->id).'" data-message="Yakin ingin turunkan posisi?" title="Turunkan Urutan">
							<i class="mdi mdi-arrow-down"></i> Turunkan Urutan
						</a>
						<a class="dropdown-item" href="'.$data->getLink().'" title="Kunjungi Link" target="_blank">
							<i class="mdi mdi-web"></i> Kunjungi
						</a>
						<a class="dropdown-item" href="'.route('nav_submenu', $data->id).'" title="Atur Submenu">
							<i class="mdi mdi-menu"></i> Atur Submenu
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function switchToUp()
	{
		self::autoResetPosition();
		if($this->position != 1) {
			$otherMenu = self::where('position', ($this->position - 1))->first();

			$otherMenu->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position - 1,
			]);
		}

		return $this;
	}


	public function switchToDown()
	{
		self::autoResetPosition();
		$total = self::count();

		if($this->position != $total ) {
			$otherMenu = self::where('position', ($this->position + 1))->first();

			$otherMenu->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position + 1,
			]);
		}

		return $this;
	}


	public static function getMenus()
	{
		return self::orderBy('position', 'asc')->get();
	}
}
