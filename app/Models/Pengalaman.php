<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengalaman extends Model
{
	protected $table = 'pengalaman';
	protected $fillable = [ 'id_perusahaan', 'nama_pekerjaan', 'lokasi_pekerjaan', 'nomor_kontrak', 'tanggal_kontrak', 'pagu_anggaran', 'nilai_kontrak', 'nilai_target', 'satuan_target', 'awal_pelaksanaan', 'akhir_pelaksanaan', 'file_bukti_fisik_pengalaman', 'id_paket_pekerjaan', 'data_perubahan', 'status_perubahan', 'aksi_perubahan', 'data_baru' ];


	/**
	 * 	Relationship
	 * */
	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan');
	}

	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '';
	}


	public static function createPengalaman($request)
	{
		return self::create([
			'id_perusahaan'		=> auth()->user()->id_perusahaan,
			'data_perubahan'	=> serialize($request->all()),
			'status_perubahan'	=> 'wait',
			'aksi_perubahan'	=> 'create',
			'data_baru'			=> 'yes',
		]);
	}


	public function updatePengalaman($request)
	{
		$this->update([
			'data_perubahan'	=> serialize($request->all()),
			'status_perubahan'	=> 'wait',
		]);

		if(empty($this->aksi_perubahan)) {
			$this->update([
				'aksi_perubahan'	=> 'update',
				'data_baru'			=> 'no',
			]);
		}

		return $this;
	}


	public function deletePengalaman()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::where('created_at', '!=', null);

		if(auth()->user()->isPerusahaan()) {
			$data = $data->where('id_perusahaan', perusahaan()->id);
		}

		return \DataTables::eloquent($data)
			->editColumn('tanggal_kontrak', function($data){
				return tmpText($data, 'tanggal_kontrak');
			})
			->editColumn('nomor_kontrak', function($data){
				return tmpText($data, 'nomor_kontrak');
			})
			->editColumn('nama_pekerjaan', function($data){
				return tmpText($data, 'nama_pekerjaan');
			})
			->editColumn('lokasi_pekerjaan', function($data){
				return tmpText($data, 'lokasi_pekerjaan');
			})
			->editColumn('aksi_perubahan', function($data){
				return $data->aksiPerubahanHtml();
			})
			->editColumn('status_perubahan', function($data){
				return $data->statusPerubahanHtml();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('p.pengalaman.edit', $data->id).'" title="Edit Pengalaman Perusahaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item" href="'.route('p.pengalaman.detail', $data->id).'" title="Detail Pengalaman Perusahaan">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('p.pengalaman.destroy', $data->id).'" title="Hapus Pengalaman Perusahaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'tanggal_kontrak', 'nomor_kontrak', 'nama_pekerjaan', 'lokasi_pekerjaan', 'status_perubahan', 'aksi_perubahan', 'action' ])
			->make(true);
	}


	public function getDataPerubahan() : array
	{
		try {
			$result = unserialize($this->data_perubahan);
			if($result) {
				return (array) $result;
			} else {
				return [];
			}
		} catch (\Exception $e) {
			return [];
		}
	}


	public function applyUpdate()
	{
		if($this->isAksiCreate() || $this->isAksiUpdate()) {
			$this->update($this->getDataPerubahan());

			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan'	=> 'approved',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);

			return $this;
		} elseif($this->isAksiDelete()) {
			return $this->delete();
		}

	}


	public function reverseUpdate()
	{
		if($this->isAksiUpdate() || $this->isAksiDelete()) {
			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan' => 'rejected',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);
			
			return $this;
		} elseif($this->isAksiCreate()) {
			return $this->delete();
		}

	}


	public function isStatusWait()
	{
		return $this->status_perubahan == 'wait';
	}


	public function isNewData()
	{
		return $this->data_baru == 'wait';
	}

	public function statusPerubahanHtml()
	{
		if($this->status_perubahan == 'approved') {
			return '<span class="text-success"> Terverifikasi </span>';
		} elseif($this->status_perubahan == 'rejected') {
			return '<span class="text-danger"> Ditolak </span>';
		} elseif($this->status_perubahan == 'wait') {
			return '<span class="text-primary"> Menunggu </span>';
		}
	}

	public function isAksiUpdate()
	{
		return $this->aksi_perubahan == 'update';
	}

	public function isAksiCreate()
	{
		return $this->aksi_perubahan == 'create';
	}

	public function isAksiDelete()
	{
		return $this->aksi_perubahan == 'delete';
	}

	public function aksiPerubahanText()
	{
		if($this->isAksiCreate()) return 'Data Baru';
		if($this->isAksiUpdate()) return 'Update Data';
		if($this->isAksiDelete()) return 'Hapus Data';
	}

	public function aksiPerubahanHtml()
	{
		if($this->isAksiCreate()) return '<span class="text-primary"> Data Baru </span>';
		if($this->isAksiUpdate()) return '<span class="text-warning"> Update Data </span>';
		if($this->isAksiDelete()) return '<span class="text-danger"> Hapus Data </span>';
	}

	public function saveFileBuktiFisikPengalaman($request)
	{
		if(!empty($request->file_upload_bukti_fisik_pengalaman)) {
			$file = $request->file('file_upload_bukti_fisik_pengalaman');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/pengalaman_kerja'), $filename);
			$this->update([
				'file_bukti_fisik_pengalaman' => $filename
			]);
		}

		return $this;
	}

	public static function getPengalamanStatusWait()
	{
		return self::where('status_perubahan', 'wait')->get();
	}

	public static function countPengalamanStatusWait()
	{
		return self::where('status_perubahan', 'wait')->count();
	}
}
