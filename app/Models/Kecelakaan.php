<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecelakaan extends Model
{
	protected $table = "kecelakaan";
	protected $fillable = [ 'nama_pekerjaan', 'perusahaan', 'lokasi_kecelakaan', 'waktu_kejadian', 'deskripsi_kecelakaan', 'deskripsi_kerugian', 'sumber_masalah' ];


	public static function createKecelakaan(array $request)
	{
		$kecelakaan = self::create($request);

		return $kecelakaan;
	}


	public function updateKecelakaan(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteKecelakaan()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('kecelakaan.edit', $data->id).'" title="Edit Kecelakaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('kecelakaan.destroy', $data->id).'" title="Hapus Kecelakaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
