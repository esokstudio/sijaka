<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RantaiPasokPeralatanKontruksi extends Model
{
	protected $table = 'rantai_pasok_peralatan_kontruksi';
	protected $fillable = [ 'id_rantai_pasok', 'nama_peralatan', 'nama_sub_peralatan', 'merk_peralatan', 'jumlah_unit', 'file_surat_keterangan_k3', 'file_bukti_kepemilikan', 'file_foto' ];

	/**
	 * 	Relationship methods
	 * */
	public function rantaiPasok()
	{
		return $this->belongsTo('App\Models\RantaiPasok', 'id_rantai_pasok');
	}


	public static function createRantaiPasokPeralatanKontruksi($request)
	{
		$material = self::create($request->all());
		$material->saveFileSuratKeteranganK3($request);
		$material->saveFileBuktiKepemilikan($request);
		$material->saveFileFoto($request);
		return $material;
	}

	public function updateRantaiPasokPeralatanKontruksi($request)
	{
		$this->update($request->all());
		$this->saveFileSuratKeteranganK3($request);
		$this->saveFileBuktiKepemilikan($request);
		$this->saveFileFoto($request);
		return $this;
	}

	public function deleteRantaiPasokPeralatanKontruksi()
	{
		$this->removeFileSuratKeteranganK3();
		$this->removeFileBuktiKepemilikan();
		$this->removeFileFoto();
		return $this->delete();
	}



	/**
	 *  Helper methods
	 * */
	public function namaBadanUsaha()
	{
		return $this->rantaiPasok->nama_badan_usaha ?? '-';
	}

	public function alamat()
	{
		return $this->rantaiPasok->alamat ?? '-';
	}

	
	// Keterangan K3
	public function saveFileSuratKeteranganK3($request)
	{
		if(!empty($request->file_upload_surat_keterangan_k3)) {
			$this->removeFileSuratKeteranganK3();
			$file = $request->file('file_upload_surat_keterangan_k3');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/surat_keterangan_k3'), $filename);
			$this->update([
				'file_surat_keterangan_k3' => $filename
			]);
		}

		return $this;
	}

	public function removeFileSuratKeteranganK3()
	{
		if($this->isHasFileSuratKeteranganK3()) {
			\File::delete($this->fileSuratKeteranganK3Path());
			$this->update([
				'file_surat_keterangan_k3' => null
			]);
		}

		return $this;
	}

	public function fileSuratKeteranganK3Path()
	{
		return storage_path('app/public/surat_keterangan_k3/'.$this->file_surat_keterangan_k3);
	}

	public function fileSuratKeteranganK3Link()
	{
		return url('storage/surat_keterangan_k3/'.$this->file_surat_keterangan_k3);
	}

	public function isHasFileSuratKeteranganK3()
	{
		if(empty($this->file_surat_keterangan_k3)) return false;
		return \File::exists($this->fileSuratKeteranganK3Path());
	}


	// Bukti Kepemilikan
	public function saveFileBuktiKepemilikan($request)
	{
		if(!empty($request->file_upload_bukti_kepemilikan)) {
			$this->removeFileBuktiKepemilikan();
			$file = $request->file('file_upload_bukti_kepemilikan');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/bukti_kepemilikan'), $filename);
			$this->update([
				'file_bukti_kepemilikan' => $filename
			]);
		}

		return $this;
	}

	public function removeFileBuktiKepemilikan()
	{
		if($this->isHasFileBuktiKepemilikan()) {
			\File::delete($this->fileBuktiKepemilikanPath());
			$this->update([
				'file_bukti_kepemilikan' => null
			]);
		}

		return $this;
	}

	public function fileBuktiKepemilikanPath()
	{
		return storage_path('app/public/bukti_kepemilikan/'.$this->file_bukti_kepemilikan);
	}

	public function fileBuktiKepemilikanLink()
	{
		return url('storage/bukti_kepemilikan/'.$this->file_bukti_kepemilikan);
	}

	public function isHasFileBuktiKepemilikan()
	{
		if(empty($this->file_bukti_kepemilikan)) return false;
		return \File::exists($this->fileBuktiKepemilikanPath());
	}


	// File Foto
	public function saveFileFoto($request)
	{
		if(!empty($request->file_upload_foto)) {
			$this->removeFileFoto();
			$file = $request->file('file_upload_foto');
			$filename = date('Ymdhis_').$file->getClientOriginalName();
			$file->move(storage_path('app/public/foto_item_rantai_pasok'), $filename);
			$this->update([
				'file_foto' => $filename
			]);
		}

		return $this;
	}

	public function removeFileFoto()
	{
		if($this->isHasFileFoto()) {
			\File::delete($this->fileFotoPath());
			$this->update([
				'file_foto' => null
			]);
		}

		return $this;
	}

	public function fileFotoPath()
	{
		return storage_path('app/public/foto_item_rantai_pasok/'.$this->file_foto);
	}

	public function fileFotoLink()
	{
		return url('storage/foto_item_rantai_pasok/'.$this->file_foto);
	}

	public function isHasFileFoto()
	{
		if(empty($this->file_foto)) return false;
		return \File::exists($this->fileFotoPath());
	}



	/**
	 *  Static methods
	 * */
	public static function dataTable($request)
	{
		$data = self::select([ 'rantai_pasok_peralatan_kontruksi.*' ]);

		if($request->id_rantai_pasok) {
			$data = $data->where('id_rantai_pasok', $request->id_rantai_pasok);
		}

		return \DataTables::eloquent($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item edit" href="javascript:void(0);" data-update-href="'.route('rantai_pasok_peralatan_kontruksi.update', $data->id).'" data-get-href="'.route('rantai_pasok_peralatan_kontruksi.get', $data->id).'" title="Edit Rantai Pasok">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('rantai_pasok_peralatan_kontruksi.destroy', $data->id).'" title="Hapus Rantai Pasok">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->editColumn('file_surat_keterangan_k3', function($data){
				return '<a href="'.$data->fileSuratKeteranganK3Link().'" target="_blank"> Klik Disini </a>';
			})
			->editColumn('file_bukti_kepemilikan', function($data){
				return '<a href="'.$data->fileBuktiKepemilikanLink().'" target="_blank"> Klik Disini </a>';
			})
			->editColumn('file_foto', function($data){
				return '<img src="'.$data->fileFotoLink().'" style="width: 30px; height: 30px;">';
			})
			->rawColumns([ 'file_surat_keterangan_k3', 'file_bukti_kepemilikan', 'file_foto', 'status_perubahan', 'action' ])
			->make(true);
	}
}
