<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
	protected $table = 'pelatihan';
	protected $fillable = [ 'nama_pelatihan', 'id_sumber_dana', 'tahun_anggaran', 'penanggung_jawab', 'id_jenis_keahlian', 'id_tingkat_keahlian', 'id_klasifikasi', 'id_sub_klasifikasi', 'id_metode_pelatihan', 'waktu_pelaksanaan', 'lokasi_pelaksanaan', 'keterangan', 'jumlah_peserta_pria', 'jumlah_peserta_wanita' ];


	public function sumberDana()
	{
		return $this->belongsTo('App\Models\SumberDana', 'id_sumber_dana');
	}


	public function jenisKeahlian()
	{
		return $this->belongsTo('App\Models\JenisKeahlian', 'id_jenis_keahlian');
	}


	public function tingkatKeahlian()
	{
		return $this->belongsTo('App\Models\TingkatKeahlian', 'id_tingkat_keahlian');
	}


	public function klasifikasi()
	{
		return $this->belongsTo('App\Models\Klasifikasi', 'id_klasifikasi');
	}


	public function subKlasifikasi()
	{
		return $this->belongsTo('App\Models\SubKlasifikasi', 'id_sub_klasifikasi');
	}


	public function metodePelatihan()
	{
		return $this->belongsTo('App\Models\MetodePelatihan', 'id_metode_pelatihan');
	}


	public function namaSumberDana()
	{
		return $this->sumberDana ? $this->sumberDana->sumber_dana : '-';
	}


	public function namaJenisKeahlian()
	{
		return $this->jenisKeahlian ? $this->jenisKeahlian->nama_jenis_keahlian : '-';
	}


	public function namaTingkatKeahlian()
	{
		return $this->tingkatKeahlian ? $this->tingkatKeahlian->nama_tingkat_keahlian : '-';
	}


	public function namaKlasifikasi()
	{
		return $this->klasifikasi ? $this->klasifikasi->nama_klasifikasi : '-';
	}


	public function namaSubKlasifikasi()
	{
		return $this->subKlasifikasi ? $this->subKlasifikasi->nama_sub_klasifikasi : '-';
	}


	public function namaMetodePelatihan()
	{
		return $this->metodePelatihan ? $this->metodePelatihan->nama_metode_pelatihan : '-';
	}


	public static function createPelatihan(array $request)
	{
		$pelatihan = self::create($request);

		return $pelatihan;
	}


	public function updatePelatihan(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deletePelatihan()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::with([ 'sumberDana', 'jenisKeahlian', 'tingkatKeahlian', 'klasifikasi', 'subKlasifikasi', 'metodePelatihan' ]);

		return \DataTables::eloquent($data)
			->editColumn('sumber_dana.sumber_dana', function($data){
				return $data->namaSumberDana();
			})
			->editColumn('jenis_keahlian.nama_jenis_keahlian', function($data){
				return $data->namaJenisKeahlian();
			})
			->editColumn('tingkat_keahlian.nama_tingkat_keahlian', function($data){
				return $data->namaTingkatKeahlian();
			})
			->editColumn('klasifikasi.nama_klasifikasi', function($data){
				return $data->namaKlasifikasi();
			})
			->editColumn('sub_klasifikasi.nama_sub_klasifikasi', function($data){
				return $data->namaSubKlasifikasi();
			})
			->editColumn('metode_pelatihan.nama_metode_pelatihan', function($data){
				return $data->namaMetodePelatihan();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('pelatihan.edit', $data->id).'" title="Edit Pelatihan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('pelatihan.destroy', $data->id).'" title="Hapus Pelatihan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
