<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TingkatKeahlian extends Model
{
	protected $table = "tingkat_keahlian";
	protected $fillable = [ 'id_jenis_keahlian', 'nama_tingkat_keahlian' ];


	public function jenisKeahlian()
	{
		return $this->belongsTo('App\Models\JenisKeahlian', 'id_jenis_keahlian');
	}


	public function namaJenisKeahlian()
	{
		return $this->jenisKeahlian ? $this->jenisKeahlian->nama_jenis_keahlian : '-';
	}


	public static function createTingkatKeahlian(array $request)
	{
		$tingkatKeahlian = self::create($request);

		return $tingkatKeahlian;
	}


	public function updateTingkatKeahlian(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteTingkatKeahlian()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::with([ 'jenisKeahlian' ]);

		return \DataTables::eloquent($data)
			->editColumn('jenis_keahlian.nama_jenis_keahlian', function($data){
				return $data->namaJenisKeahlian();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('tingkat_keahlian.edit', $data->id).'" title="Edit Tingkat Keahlian">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('tingkat_keahlian.destroy', $data->id).'" title="Hapus Tingkat Keahlian">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
