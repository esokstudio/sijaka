<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PaketPekerjaan extends Model
{
	use SoftDeletes;
	
	protected $table = "paket_pekerjaan";
	protected $fillable = [ 'tahun_anggaran', 'id_perusahaan', 'id_instansi', 'nama_pekerjaan', 'lokasi_pekerjaan', 'nomor_kontrak', 'tanggal_kontrak', 'pagu_anggaran', 'nilai_kontrak', 'nilai_target', 'satuan_target', 'awal_pelaksanaan', 'akhir_pelaksanaan', 'progress_fisik', 'progress_keuangan', 'status', 'nama_pelaksana_kegiatan', 'nama_petugas', 'nama_skema_pekerjaan' ];


	/**
	 * 	Relationship
	 * */
	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan')->withTrashed();
	}

	public function instansi()
	{
		return $this->belongsTo('App\Models\Instansi', 'id_instansi');
	}

	public function penilaian()
	{
		return $this->hasOne('App\Models\Penilaian', 'id_paket_pekerjaan');
	}

	public function realisasiPaketPekerjaan()
	{
		return $this->hasMany('App\Models\RealisasiPaketPekerjaan', 'id_paket_pekerjaan');
	}

	public function pengalamanPerusahaan()
	{
		return $this->hasOne('App\Models\Pengalaman', 'id_paket_pekerjaan');
	}


	/**
	 * 	Helper
	 * */
	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '-';
	}

	public function alamatPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->alamat : '-';
	}

	public function direkturPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->direktur : '-';
	}

	public function namaInstansi()
	{
		return $this->instansi ? $this->instansi->nama_instansi : '-';
	}

	public function paguAnggaranText()
	{
		return 'Rp. '.number_format($this->pagu_anggaran);
	}

	public function nilaiKontrakText()
	{
		return 'Rp. '.number_format($this->nilai_kontrak);
	}

	public function jangkaWaktuPelaksanaan()
	{
		$awal = new \Carbon\Carbon($this->awal_pelaksanaan);
		$akhir = new \Carbon\Carbon($this->akhir_pelaksanaan);
		return $awal->diffInDays($akhir) + 1;
	}

	public function realisasiByTahunAndBulan($tahun, $bulan)
	{
		$realisasi = $this->realisasiPaketPekerjaan;
		$result = null;

		foreach($realisasi as $r) {
			if($r->tahun == $tahun && $r->bulan == $bulan) $result = $r;
		}

		return $result;
	}

	public function totalProgressFisik()
	{
		$progressFisik = 0;
		foreach($this->realisasiPaketPekerjaan as $realisasi) {
			$progressFisik += $realisasi->progress_fisik;
		}

		return $progressFisik;
	}

	public function totalProgressKeuangan()
	{
		$progressKeuangan = 0;
		foreach($this->realisasiPaketPekerjaan as $realisasi) {
			$progressKeuangan += $realisasi->progress_keuangan;
		}

		return $progressKeuangan;
	}

	public function createPengalamanPerusahaan()
	{
		if(!$this->pengalamanPerusahaan) {
			Pengalaman::create([
				'id_perusahaan'		=> $this->id_perusahaan,
				'nama_pekerjaan'	=> $this->nama_pekerjaan,
				'lokasi_pekerjaan'	=> $this->lokasi_pekerjaan,
				'nomor_kontrak'		=> $this->nomor_kontrak,
				'tanggal_kontrak'	=> $this->tanggal_kontrak,
				'pagu_anggaran'		=> $this->pagu_anggaran,
				'nilai_kontrak'		=> $this->nilai_kontrak,
				'nilai_target'		=> $this->nilai_target,
				'satuan_target'		=> $this->satuan_target,
				'awal_pelaksanaan'	=> $this->awal_pelaksanaan,
				'akhir_pelaksanaan'	=> $this->akhir_pelaksanaan,
				'file_bukti_fisik_pengalaman' => null,
				'id_paket_pekerjaan'=> $this->id,
				'data_perubahan'	=> serialize([]),
				'status_perubahan'	=> 'approved',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);
			$this->load('pengalamanPerusahaan');
		}

		return $this;
	}



	/**
	 * 	CRUD methods
	 * */
	public static function createPaketPekerjaan(array $request)
	{
		if(auth()->user()->isPengguna()) {
			$request['id_instansi'] = auth()->user()->id_instansi;
		}

		$request['tanggal_kontrak'] = Carbon::createFromFormat('d/m/Y', $request['tanggal_kontrak'])->format('Y-m-d');
		$request['awal_pelaksanaan'] = Carbon::createFromFormat('d/m/Y', $request['awal_pelaksanaan'])->format('Y-m-d');
		$request['akhir_pelaksanaan'] = Carbon::createFromFormat('d/m/Y', $request['akhir_pelaksanaan'])->format('Y-m-d');

		$paketPekerjaan = self::create($request);

		return $paketPekerjaan;
	}


	public function updatePaketPekerjaan(array $request)
	{
		$request['tanggal_kontrak'] = Carbon::createFromFormat('d/m/Y', $request['tanggal_kontrak'])->format('Y-m-d');
		$request['awal_pelaksanaan'] = Carbon::createFromFormat('d/m/Y', $request['awal_pelaksanaan'])->format('Y-m-d');
		$request['akhir_pelaksanaan'] = Carbon::createFromFormat('d/m/Y', $request['akhir_pelaksanaan'])->format('Y-m-d');
		$this->update($request);


		return $this;
	}


	public function deletePaketPekerjaan()
	{
		return $this->delete();
	}


	public static function dataTable($request)
	{
		$data = self::with([ 'perusahaan', 'instansi' ])
					->select([ 'paket_pekerjaan.*' ]);

		if(auth()->user()->isPengguna()) {
			$data = $data->where('id_instansi', auth()->user()->id_instansi);
		}

		if(!empty($request->tahun_anggaran)) {
			if($request->tahun_anggaran != 'all') {
				$data = $data->where('tahun_anggaran', $request->tahun_anggaran);
			}
		}

		if(!empty($request->id_perusahaan)) {
			if($request->id_perusahaan != 'all') {
				$data = $data->where('id_perusahaan', $request->id_perusahaan);
			}
		}

		if(!empty($request->id_instansi)) {
			if($request->id_instansi != 'all') {
				$data = $data->where('id_instansi', $request->id_instansi);
			}
		}

		if(!empty($request->status)) {
			if($request->status != 'all') {
				$data = $data->where('status', $request->status);
			}
		}

		return \DataTables::eloquent($data)
			->editColumn('tahun_anggaran', function($data){
				return $data->tahun_anggaran ?? '-';
			})
			->editColumn('perusahaan.nama_perusahaan', function($data){
				return $data->namaPerusahaan();
			})
			->editColumn('pagu_anggaran', function($data){
				return $data->paguAnggaranText();
			})
			->editColumn('nilai_kontrak', function($data){
				return $data->nilaiKontrakText();
			})
			->editColumn('nilai_target', function($data){
				return $data->nilai_target . ' ' . $data->satuan_target;
			})
			->editColumn('awal_pelaksanaan', function($data){
				return $data->awal_pelaksanaan . ' - ' . $data->akhir_pelaksanaan;
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('paket_pekerjaan.realisasi', $data->id).'" title="Realisasi Paket Keahlian">
							<i class="mdi mdi-tools"></i> Realisasi
						</a>
						<a class="dropdown-item" href="'.route('paket_pekerjaan.detail', $data->id).'" title="Detail Paket Pekerjaan">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item" href="'.route('paket_pekerjaan.edit', $data->id).'" title="Edit Paket Pekerjaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('paket_pekerjaan.destroy', $data->id).'" title="Hapus Paket Pekerjaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public static function syncPaketPekerjaanAndPengalamanPerusahaan()
	{
		$paketPekerjaan = self::has('penilaian')
							  ->doesntHave('pengalamanPerusahaan')
							  ->get();
		foreach($paketPekerjaan as $paket) {
			$paket->createPengalamanPerusahaan();
		}
		
		return count($paketPekerjaan);
	}


	public static function setNamaSkemaPaketPekerjaan()
	{
		$paketPekerjaan = self::has('penilaian')
							  ->with([ 'penilaian' ])
							  ->where('nama_skema_pekerjaan', null)
							  ->get();
		foreach($paketPekerjaan as $paket) {
			$paket->update([
				'nama_skema_pekerjaan' => $paket->penilaian->nama_skema_penilaian,
			]);
		}
		
		return count($paketPekerjaan);
	}


	public static function generateLaporanFisikDanKeuangan($request)
	{
		$tahun = $request->tahun;
		$isWithPhotoLink = $request->is_with_photo_link == 'yes';
		$totalColumn = $isWithPhotoLink? 52 : 38;
		$monthList = [ 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' ];

		$filename = 'Realisasi_Fisik_Dan_Keuangan_' . $tahun . '.xlsx';

		$headerStyle = [ 'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'wrap_text' => true ];
		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			'LAPORAN REALISASI FISIK DAN KEUANGAN'=>'string', '' => 'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string', ''=>'string', ''=>'string', ''=>'string', ''=>'string',
			''=>'string',
		];

		$width = [5,60,40,15,15,15,15,30,30,40,40,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15];

		if($isWithPhotoLink) {
			for ($i = 1; $i <= 12 ; $i++) { 
				$width[] = 15;
			}
		}

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> $width,
			'halign' => 'center', 'font-style' => 'bold', 'wrap_text' => true
		]);

		$writer->markMergedCell('Sheet1', $start_row=0, $start_col=0, $end_row=0, $end_col=$totalColumn);

		$namaInstansi = auth()->user()->isAdmin() ? 'DINAS PUTR KABUPATEN CIREBON' : auth()->user()->namaInstansi();

		$writer->writeSheetRow('Sheet1', [
			$namaInstansi,
		], [ 'halign' => 'center', 'font-style' => 'bold' ]);
		$writer->markMergedCell('Sheet1', $start_row=1, $start_col=0, $end_row=1, $end_col=$totalColumn);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$data = [
			'NO',
			'URAIAN',
			'DINAS/BIDANG',
			'PAGU',
			'KONTRAK',
			'TARGET',
			'',
			'NAMA PERUSAHAAN',
			'NAMA DIREKTUR',
			'ALAMAT',
			'NOMOR KONTRAK',
			'TANGGAL KONTRAK',
			'WAKTU PELAKSANAAN',
			'',
			'REALISASI',
			'','','','','','','','','','','','','','','','','','','','','','','',
		];
		if($isWithPhotoLink) {
			for ($i = 1; $i <= 12 ; $i++) { 
				$data[] = '';
			}
		}
		$data[] = 'REKAPITULASI';
		$data[] = '';
		$writer->writeSheetRow('Sheet1', $data, $headerStyle);


		$data = [
			'',
			'',
			'',
			'',
			'',
			'VOLUME',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
		];
		foreach($monthList as $month) {
			$data[] = $month;
			$data[] = '';
			if($isWithPhotoLink) {
				$data[] = '';
			}
		}
		$data[] = '';
		$data[] = '';
		$writer->writeSheetRow('Sheet1', $data, $headerStyle);


		$data = [
			'',
			'',
			'',
			'',
			'',
			'NILAI',
			'SATUAN',
			'',
			'',
			'',
			'',
			'',
			'AWAL',
			'AKHIR',
		];
		for($i = 1; $i <= 12; $i++) {
			$data[] = 'Keuangan Rp.';
			$data[] = 'Fisik %';
			if($isWithPhotoLink) {
				$data[] = 'Bukti Fisik';
			}
		}
		$data[] = 'Keuangan Rp.';
		$data[] = 'Fisik %';
		$writer->writeSheetRow('Sheet1', $data, $headerStyle);

		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=0, $end_row=5, $end_col=0);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=1, $end_row=5, $end_col=1);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=2, $end_row=5, $end_col=2);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=3, $end_row=5, $end_col=3);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=4, $end_row=5, $end_col=4);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=5, $end_row=3, $end_col=6);
		$writer->markMergedCell('Sheet1', $start_row=4, $start_col=5, $end_row=4, $end_col=6);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=7, $end_row=5, $end_col=7);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=8, $end_row=5, $end_col=8);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=9, $end_row=5, $end_col=9);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=10, $end_row=5, $end_col=10);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=11, $end_row=5, $end_col=11);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=12, $end_row=4, $end_col=13);
		$colEnd = $isWithPhotoLink ? 49 : 37;
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=14, $end_row=3, $end_col=$colEnd);
		$colStart = $colEnd + 1;
		$colEnd = $colStart + 1;
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=$colStart, $end_row=4, $end_col=$colEnd);

		$columnNumber = 14;
		$increaseNumber = $isWithPhotoLink ? 2 : 1;
		for ($i = 1; $i <= 12; $i++) { 
			$colStart = $columnNumber;
			$colEnd = $columnNumber + $increaseNumber;
			$writer->markMergedCell('Sheet1', $start_row=4, $start_col=$colStart, $end_row=4, $end_col=$colEnd);
			$columnNumber = $colEnd + 1;
		}

		$row = 6;

		$paketPekerjaan = self::whereHas('realisasiPaketPekerjaan', function($query) use ($tahun) {
			$query->where('tahun', $tahun);
		})
		->with([ 'realisasiPaketPekerjaan', 'perusahaan', 'instansi' ]);

		if(auth()->user()->isPengguna()) {
			$paketPekerjaan = $paketPekerjaan->where('id_instansi', auth()->user()->id_instansi);
		} else {
			if(!empty($request->id_instansi)) {
				if($request->id_instansi != 'all') {
					$paketPekerjaan = $paketPekerjaan->where('id_instansi', $request->id_instansi);
				}
			}
		}
		
		$paketPekerjaan = $paketPekerjaan->orderBy('id_instansi', 'asc')
										 ->orderBy('tanggal_kontrak', 'asc')
										 ->get();

		$iteration = 1;
		foreach($paketPekerjaan as $p) {
			$data = [
				$iteration++,
				$p->nama_pekerjaan,
				$p->namaInstansi(),
				$p->pagu_anggaran,
				$p->nilai_kontrak,
				$p->nilai_target,
				$p->satuan_target,
				$p->namaPerusahaan(),
				$p->direkturPerusahaan(),
				$p->alamatPerusahaan(),
				$p->nomor_kontrak,
				$p->tanggal_kontrak,
				$p->awal_pelaksanaan,
				$p->akhir_pelaksanaan,
			];			

			$totalKeuangan = 0;
			$totalFisik = 0;
			foreach($monthList as $month) {
				$realisasi = $p->realisasiByTahunAndBulan($tahun, $month);
				$data[] = !empty($realisasi) ? $realisasi->progress_keuangan : '';
				$data[] = !empty($realisasi) ? $realisasi->progress_fisik.'%' : '';
				if($isWithPhotoLink) {
					$data[] = !empty($realisasi) ? '=HYPERLINK("' . $realisasi->fotoLink() .'", "Klik Disini")' : '';
				}
				if($realisasi) {
					$totalKeuangan += $realisasi->progress_keuangan;
					$totalFisik += (int) $realisasi->progress_fisik;
				}
			}

			$data[] = $totalKeuangan;
			$data[] = $totalFisik.'%';
			$writer->writeSheetRow('Sheet1', $data, $bodyStyle);

			$row++;
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}


	public static function updateProgress()
	{
		foreach(self::all() as $paketPekerjaan) {
			$paketPekerjaan->update([
				'progress_fisik'	=> $paketPekerjaan->totalProgressFisik(),
				'progress_keuangan'	=> $paketPekerjaan->totalProgressKeuangan(),
			]);
		}
	}


	public static function exportToExcel($request)
	{
		$paketPekerjaan = self::with([ 'perusahaan', 'instansi' ])
					->select([ 'paket_pekerjaan.*' ]);

		if(auth()->user()->isPengguna()) {
			$paketPekerjaan = $paketPekerjaan->where('id_instansi', auth()->user()->id_instansi);
		}

		if(!empty($request->tahun_anggaran)) {
			if($request->tahun_anggaran != 'all') {
				$paketPekerjaan = $paketPekerjaan->where('tahun_anggaran', $request->tahun_anggaran);
			}
		}

		if(!empty($request->id_perusahaan)) {
			if($request->id_perusahaan != 'all') {
				$paketPekerjaan = $paketPekerjaan->where('id_perusahaan', $request->id_perusahaan);
			}
		}

		if(!empty($request->id_instansi)) {
			if($request->id_instansi != 'all') {
				$paketPekerjaan = $paketPekerjaan->where('id_instansi', $request->id_instansi);
			}
		}

		if(!empty($request->status)) {
			if($request->status != 'all') {
				$paketPekerjaan = $paketPekerjaan->where('status', $request->status);
			}
		}

		$paketPekerjaan = $paketPekerjaan->orderBy('id_instansi', 'asc')
										 ->orderBy('tanggal_kontrak', 'asc')
										 ->get();

		$filename = date('YmdHis_').'Paket_Pekerjaan.xlsx';

		$headerStyle = [ 'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'wrap_text' => true ];
		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin' ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,15,15,30,40,40,50,30,20,20,12,12,15,15,12,20,12,25,25,15]
		]);

		$writer->writeSheetRow('Sheet1', [
			'PAKET PEKERJAAN',
		], [ 'halign' => 'center', 'font-style' => 'bold' ]);
		$writer->markMergedCell('Sheet1', $start_row=1, $start_col=0, $end_row=1, $end_col=16);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$data = [
			'No',
			'Tgl Kontrak',
			'Tahun Anggaran',
			'Nomor Kontrak',
			'Perusahaan',
			'Instansi',
			'Nama Pekerjaan',
			'Lokasi Pekerjaan',
			'Pagu Anggaran',
			'Nilai Kontrak',
			'Nilai Volume',
			'Satuan Volume',
			'Awal Pelaksanaan',
			'Akhir Pelaksanaan',
			'Progress Fisik',
			'Progress Keuangan',
			'Status',
			'Nama Pelaksana Kegiatan',
			'Nama Petugas / Ahli K3',
			'Dibuat Pada',
		];
		$writer->writeSheetRow('Sheet1', $data, $headerStyle);

		$iteration = 1;
		foreach($paketPekerjaan as $p) {
			$data = [
				$iteration++,
				$p->tanggal_kontrak ? \Carbon\Carbon::createFromFormat('Y-m-d', $p->tanggal_kontrak)->format('d/m/Y') : '-',
				$p->tahun_anggaran ?? '-',
				$p->nomor_kontrak,
				$p->namaPerusahaan(),
				$p->namaInstansi(),
				$p->nama_pekerjaan,
				$p->lokasi_pekerjaan,
				$p->pagu_anggaran,
				$p->nilai_kontrak,
				$p->nilai_target,
				$p->satuan_target,
				$p->awal_pelaksanaan ? \Carbon\Carbon::createFromFormat('Y-m-d', $p->awal_pelaksanaan)->format('d/m/Y') : '-',
				$p->akhir_pelaksanaan ? \Carbon\Carbon::createFromFormat('Y-m-d', $p->akhir_pelaksanaan)->format('d/m/Y') : '-',
				$p->progress_fisik.'%',
				$p->progress_keuangan,
				$p->status,
				$p->nama_pelaksana_kegiatan,
				$p->nama_petugas,
				$p->created_at->format('d/m/Y'),
			];
			$writer->writeSheetRow('Sheet1', $data, $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}


	public static function importFromExcel($request)
	{
		if(!empty($request->file))
		{
			$file = $request->file('file');
			$ext = $file->getClientOriginalExtension();

			if(in_array($ext, [ 'xlsx', 'xls' ]))
			{
				$filename = 'paket_pekerjaan_'.date('Ymdhis') . '.'.$ext;
				$filepath = \Helper::temps($filename);
				$path = \Helper::temps('');
				$file->move($path, $filename);

				if($xlsx = \App\MyClass\SimpleXLSX::parse($filepath)) {

					$iter = 1;
					$count = 0;
					foreach($xlsx->rows() as $row) {
						if($iter > 1) {
							$tahunAnggaran = $row[0];
							$namaPerusahaan = $row[1];
							$namaPekerjaan = $row[2];
							$lokasi = $row[3];
							$nomorKontrak = $row[4];
							$tglKontrak = $row[5];
							$paguAnggaran = $row[6];
							$nilaiKontrak = $row[7];
							$nilaiVolume = $row[8];
							$satuanVolume = $row[9];
							$skemaPekerjaan = $row[10];
							$status = $row[11];
							$awalPelaksanaan = $row[12];
							$akhirPelaksanaan = $row[13];
							$namaPelaksana = $row[14];
							$namaPetugas = $row[15];

							if(!empty($tahunAnggaran) && !empty($namaPerusahaan) && !empty($namaPekerjaan) && !empty($lokasi) && !empty($nomorKontrak) && !empty($tglKontrak) && $paguAnggaran != "" && $nilaiKontrak != "" && $nilaiVolume != "" && !empty($satuanVolume) && !empty($skemaPekerjaan) && !empty($status) && !empty($awalPelaksanaan) && !empty($namaPelaksana) && !empty($namaPetugas))
							{

								$perusahaan = \App\Models\Perusahaan::where('nama_perusahaan', $namaPerusahaan)->first();

								if(empty($perusahaan)) {
									$perusahaan = \App\Models\Perusahaan::create([
										'nama_perusahaan'	=> $namaPerusahaan,
										'alamat'			=> '-',
									]);
								}

								$paketPekerjaan = self::where('nomor_kontrak', $nomorKontrak)->first();

								if(!$paketPekerjaan) {
									try {
										self::create([
											'tahun_anggaran'	=> $tahunAnggaran,
											'id_perusahaan'		=> $perusahaan->id,
											'id_instansi'		=> auth()->user()->isPengguna() ? auth()->user()->id_instansi : null,
											'nama_pekerjaan'	=> $namaPekerjaan,
											'lokasi_pekerjaan'	=> $lokasi,
											'nomor_kontrak'		=> $nomorKontrak,
											'tanggal_kontrak'	=> \Carbon\Carbon::createFromFormat('d/m/Y', $tglKontrak)->format('Y-m-d'),
											'pagu_anggaran'		=> $paguAnggaran,
											'nilai_kontrak'		=> $nilaiKontrak,
											'nilai_target'		=> $nilaiVolume,
											'satuan_target'		=> $satuanVolume,
											'awal_pelaksanaan'	=> \Carbon\Carbon::createFromFormat('d/m/Y', $awalPelaksanaan)->format('Y-m-d'),
											'akhir_pelaksanaan' => !empty($akhirPelaksanaan) ? \Carbon\Carbon::createFromFormat('d/m/Y', $akhirPelaksanaan)->format('Y-m-d') : null,
											'status'			=> $status,
											'nama_pelaksana_kegiatan' => $namaPelaksana,
											'nama_petugas'		=> $namaPetugas,
											'nama_skema_pekerjaan' => $skemaPekerjaan,
										]);
										$count++;
									} catch (Exception $e) {}
								} else {
									try {
										$paketPekerjaan->update([
											'tahun_anggaran'	=> $tahunAnggaran,
											'id_perusahaan'		=> $perusahaan->id,
											'nama_pekerjaan'	=> $namaPekerjaan,
											'lokasi_pekerjaan'	=> $lokasi,
											'tanggal_kontrak'	=> \Carbon\Carbon::createFromFormat('d/m/Y', $tglKontrak)->format('Y-m-d'),
											'pagu_anggaran'		=> $paguAnggaran,
											'nilai_kontrak'		=> $nilaiKontrak,
											'nilai_target'		=> $nilaiVolume,
											'satuan_target'		=> $satuanVolume,
											'awal_pelaksanaan'	=> \Carbon\Carbon::createFromFormat('d/m/Y', $awalPelaksanaan)->format('Y-m-d'),
											'akhir_pelaksanaan' => !empty($akhirPelaksanaan) ? \Carbon\Carbon::createFromFormat('d/m/Y', $akhirPelaksanaan)->format('Y-m-d') : null,
											'status'			=> $status,
											'nama_pelaksana_kegiatan' => $namaPelaksana,
											'nama_petugas'		=> $namaPetugas,
											'nama_skema_pekerjaan' => $skemaPekerjaan,
										]);
									} catch (Exception $e) {}
								}
							}

						}

						$iter++;
					}

					return $count;

				} else {
					throw new \Exception("Pembacaan File Gagal");
				}
			}
			else
			{
				throw new \Exception("Ekstensi tidak didukung");
			}
		} 
		else 
		{
			throw new \Exception("File Kosong");
		}
	}


	public static function jumlahPaketPekerjaanDataChart()
	{
		$listTahun = [];

		for ($tahun = date('Y'); $tahun >= (date('Y') - 2); $tahun--) { 
			$jumlahData = PaketPekerjaan::where('tahun_anggaran', $tahun)->count();
			if($jumlahData > 0) {
				$listTahun[] = (int) $tahun;
			}
		}

		$listTahun = array_reverse($listTahun);
		$results = [];
		$dataInstansi = Instansi::where('tampilkan_grafik', 'yes')->get();

		foreach($dataInstansi as $instansi) {
			$results[$instansi->id] = (object) [
				'instansi' => $instansi,
				'data' => [],
				'data_per_kategori' => []
			];
			foreach($listTahun as $tahun) {
				$paketPekerjaan = PaketPekerjaan::where('tahun_anggaran', $tahun)
							->where('id_instansi', $instansi->id)
							->get();
				$results[$instansi->id]->data[$tahun] = count($paketPekerjaan);
				$results[$instansi->id]->data_per_kategori[$tahun] = [];
				foreach($paketPekerjaan as $p) {
					$namaSkema = $p->nama_skema_pekerjaan ?? 'Lainnya';
					if(array_key_exists($namaSkema, $results[$instansi->id]->data_per_kategori[$tahun])) {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema]++;
					} else {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema] = 1;
					}
				}
			}
		}

		return $results;
	}


	public static function paguAnggaranDataChart()
	{
		$listTahun = [];

		for ($tahun = date('Y'); $tahun >= (date('Y') - 2); $tahun--) { 
			$jumlahData = PaketPekerjaan::where('tahun_anggaran', $tahun)->count();
			if($jumlahData > 0) {
				$listTahun[] = (int) $tahun;
			}
		}

		$listTahun = array_reverse($listTahun);
		$results = [];
		$dataInstansi = Instansi::where('tampilkan_grafik', 'yes')->get();

		foreach($dataInstansi as $instansi) {
			$results[$instansi->id] = (object) [
				'instansi' => $instansi,
				'data' => [],
				'data_per_kategori' => []
			];
			foreach($listTahun as $tahun) {
				$jumlah = 0;
				$paketPekerjaan = PaketPekerjaan::where('tahun_anggaran', $tahun)
									->where('id_instansi', $instansi->id)
									->get();
				foreach($paketPekerjaan as $pp) {
					$jumlah = $pp->pagu_anggaran;
				}
				$results[$instansi->id]->data[$tahun] = $jumlah;
				$results[$instansi->id]->data_per_kategori[$tahun] = [];
				foreach($paketPekerjaan as $p) {
					$namaSkema = $p->nama_skema_pekerjaan ?? 'Lainnya';
					if(array_key_exists($namaSkema, $results[$instansi->id]->data_per_kategori[$tahun])) {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema] += $p->pagu_anggaran;
					} else {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema] = $p->pagu_anggaran;
					}
				}
			}
		}

		return $results;
	}

	public static function realisasiAnggaranDataChart()
	{
		$listTahun = [];

		for ($tahun = date('Y'); $tahun >= (date('Y') - 2); $tahun--) { 
			$jumlahData = PaketPekerjaan::where('tahun_anggaran', $tahun)->count();
			if($jumlahData > 0) {
				$listTahun[] = (int) $tahun;
			}
		}

		$listTahun = array_reverse($listTahun);
		$results = [];
		$dataInstansi = Instansi::where('tampilkan_grafik', 'yes')->get();

		foreach($dataInstansi as $instansi) {
			$results[$instansi->id] = (object) [
				'instansi' => $instansi,
				'data' => [],
				'data_per_kategori' => []
			];
			foreach($listTahun as $tahun) {
				$jumlah = 0;
				$paketPekerjaan = PaketPekerjaan::where('tahun_anggaran', $tahun)
									->with([ 'realisasiPaketPekerjaan' ])
									->where('id_instansi', $instansi->id)
									->get();
				foreach($paketPekerjaan as $pp) {
					$jumlah = $pp->totalProgressKeuangan();
				}
				$results[$instansi->id]->data[$tahun] = $jumlah;
				$results[$instansi->id]->data_per_kategori[$tahun] = [];
				foreach($paketPekerjaan as $p) {
					$namaSkema = $p->nama_skema_pekerjaan ?? 'Lainnya';
					if(array_key_exists($namaSkema, $results[$instansi->id]->data_per_kategori[$tahun])) {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema] += $p->totalProgressKeuangan();
					} else {
						$results[$instansi->id]->data_per_kategori[$tahun][$namaSkema] = $p->totalProgressKeuangan();
					}
				}
			}
		}

		return $results;
	}
}
