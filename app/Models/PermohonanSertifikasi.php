<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermohonanSertifikasi extends Model
{
	protected $table = 'permohonan_sertifikasi';
	protected $fillable = [ 'id_user', 'nama_pemohon', 'nama_perusahaan', 'klasifikasi', 'sub_klasifikasi', 'kualifikasi', 'file_ijazah', 'file_surat_permohonan', 'file_ktp', 'file_pernyataan_kebenaran_data', 'file_pas_foto', 'tahun_anggaran', 'nomor_telepon', 'email', 'status', 'tindak_lanjut' ];

	public function user()
	{
		return $this->belongsTo('App\User', 'id_user');
	}

	public function profilPeserta()
	{
		return $this->hasOne('App\Models\ProfilPeserta', 'id_user', 'id_user');
	}

	public function pendidikanPeserta()
	{
		return $this->hasMany('App\Models\PendidikanPeserta', 'id_user', 'id_user');
	}


	public function pengalamanPeserta()
	{
		return $this->hasMany('App\Models\PengalamanPeserta', 'id_user', 'id_user');
	}


	public static function createPermohonanSertifikasi($request)
	{
		$permohonan = self::create([
			'id_user'			=> auth()->user()->id,
			'nama_pemohon'		=> $request->nama_pemohon,
			'nama_perusahaan'	=> $request->nama_perusahaan,
			'klasifikasi'		=> $request->klasifikasi,
			'sub_klasifikasi'	=> $request->sub_klasifikasi,
			'kualifikasi'		=> $request->kualifikasi,
			'tahun_anggaran'	=> $request->tahun_anggaran,
			'nomor_telepon'		=> $request->nomor_telepon,
			'email'				=> $request->email,
		]);

		$permohonan->saveFileIjazah($request);
		$permohonan->saveFileSuratPermohonan($request);
		$permohonan->saveFileKtp($request);
		$permohonan->saveFilePernyataanKebenaranData($request);
		$permohonan->saveFilePasFoto($request);

		return $permohonan;
	}


	public function saveFileIjazah($request)
	{
		if(!empty($request->file_ijazah))
		{
			$file = $request->file('file_ijazah');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/uploads/ijazah/'), $filename);
			$this->update([
				'file_ijazah'	=> $filename
			]);
		}

		return $this;
	}


	public function saveFileSuratPermohonan($request)
	{
		if(!empty($request->file_surat_permohonan))
		{
			$file = $request->file('file_surat_permohonan');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/uploads/permohonan/'), $filename);
			$this->update([
				'file_surat_permohonan'	=> $filename
			]);
		}

		return $this;
	}


	public function saveFileKtp($request)
	{
		if(!empty($request->file_ktp))
		{
			$file = $request->file('file_ktp');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/uploads/ktp/'), $filename);
			$this->update([
				'file_ktp'	=> $filename
			]);
		}

		return $this;
	}


	public function saveFilePernyataanKebenaranData($request)
	{
		if(!empty($request->file_pernyataan_kebenaran_data))
		{
			$file = $request->file('file_pernyataan_kebenaran_data');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/uploads/penyataan_kebenaran_data/'), $filename);
			$this->update([
				'file_pernyataan_kebenaran_data'	=> $filename
			]);
		}

		return $this;
	}


	public function saveFilePasFoto($request)
	{
		if(!empty($request->file_pas_foto))
		{
			$file = $request->file('file_pas_foto');
			$filename = date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/uploads/pas_foto/'), $filename);
			$this->update([
				'file_pas_foto'	=> $filename
			]);
		}

		return $this;
	}


	public function statusHtml()
	{
		if($this->status == 'Menunggu') return '<span class="text-primary"> Menunggu </span>';
		if($this->status == 'Disetujui') return '<span class="text-success"> Disetujui </span>';
		if($this->status == 'Ditolak') return '<span class="text-danger"> Ditolak </span>';
	}


	public static function dt()
	{
		if(auth()->user()->isPeserta()) {
			$data = self::where('id_user', auth()->user()->id)->get();
		} else {
			$data = self::all();
		}

		return \DataTables::of($data)
			->editColumn('created_at', function($data){
				return date('Y-m-d H:i:s', strtotime($data->created_at));
			})
			->editColumn('tahun_anggaran', function($data){
				return $data->tahun_anggaran ?? '-';
			})
			->addColumn('status', function($data){
				return $data->statusHtml();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('permohonan_sertifikasi.detail', $data->id).'" title="Detail Permohonan Sertifikasi">
							<i class="mdi mdi-magnify"></i> Detail
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('permohonan_sertifikasi.destroy', $data->id).'" title="Hapus Permohonan Sertifikasi">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->addColumn('peserta_action', function($data){
				$button = '
				<a> Detail </a>';

				return $button;
			})
			->rawColumns([ 'status', 'peserta_action', 'action' ])
			->make(true);
	}


	public function fileIjazahPath()
	{
		return storage_path('app/public/uploads/ijazah/'.$this->file_ijazah);
	}


	public function fileIjazahLink()
	{
		return url('storage/uploads/ijazah/'.$this->file_ijazah);
	}


	public function fileSuratPermohonanPath()
	{
		return storage_path('app/public/uploads/permohonan/'.$this->file_surat_permohonan);
	}


	public function fileSuratPermohonanLink()
	{
		return url('storage/uploads/permohonan/'.$this->file_surat_permohonan);
	}


	public function fileKtpPath()
	{
		return storage_path('app/public/uploads/ktp/'.$this->file_ktp);
	}


	public function fileKtpLink()
	{
		return url('storage/uploads/ktp/'.$this->file_ktp);
	}


	public function fileSuratPernyataanKebenaranDataPath()
	{
		return storage_path('app/public/uploads/pernyataan_kebenaran_data/'.$this->file_surat_permohonan);
	}


	public function fileSuratPernyataanKebenaranDataLink()
	{
		return url('storage/uploads/pernyataan_kebenaran_data/'.$this->file_surat_permohonan);
	}


	public function filePasFotoPath()
	{
		return storage_path('app/public/uploads/pas_foto/'.$this->file_pas_foto);
	}


	public function filePasFotoLink()
	{
		return url('storage/uploads/pas_foto/'.$this->file_pas_foto);
	}


	/**
	*	Static methods
	*/
	public static function exportToExcel($request)
	{
		$permohonan = self::select([ 'permohonan_sertifikasi.*' ]);

		if(!empty($request->tahun_anggaran)) {
			if($request->tahun_anggaran != 'all') {
				$permohonan = $permohonan->where('tahun_anggaran', $request->tahun_anggaran);
			}
		}

		if(!empty($request->start_date)) {
			if($request->start_date != 'all') {
				$permohonan = $permohonan->where('created_at', '>=', $request->start_date.' 00:00:00');
			}
		}

		if(!empty($request->end_date)) {
			if($request->end_date != 'all') {
				$permohonan = $permohonan->where('created_at', '<=', $request->end_date.' 23:59:59');
			}
		}

		if(!empty($request->status)) {
			if($request->status != 'all') {
				$permohonan = $permohonan->where('status', $request->status);
			}
		}

		$permohonan = $permohonan->orderBy('created_at', 'asc')
								 ->get();

		$filename = date('YmdHis_').'Permohonan_Sertifikasi.xlsx';

		$headerStyle = [ 'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'wrap_text' => true ];
		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin' ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,15,25,25,25,25,25,25,25,25,25,25]
		]);

		$writer->writeSheetRow('Sheet1', [
			'PERMOHONAN SERTIFIKASI',
		], [ 'halign' => 'center', 'font-style' => 'bold' ]);
		$writer->markMergedCell('Sheet1', $start_row=1, $start_col=0, $end_row=1, $end_col=11);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$data = [
			'No',
			'Tgl Pendaftaran',
			'Nama Pemohon',
			'Nama Perusahaan',
			'Klasifikasi',
			'Sub Klasifikasi',
			'Kualifikasi',
			'Tahun Anggaran',
			'Nomor Whatsapp',
			'Email',
			'Status',
			'Catatan',
		];
		$writer->writeSheetRow('Sheet1', $data, $headerStyle);

		$iteration = 1;
		foreach($permohonan as $p) {
			$data = [
				$iteration++,
				$p->created_at->format('Y-m-d'),
				$p->nama_pemohon,
				$p->nama_perusahaan,
				$p->klasifikasi,
				$p->sub_klasifikasi,
				$p->kualifikasi,
				$p->tahun_anggaran,
				$p->nomor_telepon,
				$p->email,
				$p->status,
				$p->tindak_lanjut,
			];
			$writer->writeSheetRow('Sheet1', $data, $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}

}
