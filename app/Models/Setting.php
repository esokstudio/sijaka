<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $fillable = [ 'setting_name', 'setting_value', 'label', 'description', 'type' ];


	public static function get($name, $defaultValue = null)
	{
		$setting = self::where('setting_name', $name)->first();

		if(!$setting) {
			$setting = Setting::create([
				'setting_name'	=> $name,
				'setting_value'	=> $defaultValue,
			]);
		}

		return $setting;
	}


	public static function getValue($name, $defaultValue = null)
	{
		return self::get($name, $defaultValue)->setting_value;
	}


	public static function setValue($name, $value = null)
	{
		$setting = self::get($name, $value);

		$setting->update([
			'setting_value'	=> $value,
		]);

		return $setting;
	}


	public static function setValues($settings)
	{
		if(is_array($settings))
		{
			foreach($settings as $name => $value)
			{
				self::setValue($name, $value);
			}

			return true;
		}

		return false;
	}


	public function increase()
	{
		$value = $this->setting_value;

		if(is_numeric($value)) {
			$this->update([
				'setting_value'	=> (int) $this->setting_value + 1
			]);
		}

		return $this;
	}


	public function set($value = null, $label = null, $description = null, $type = null)
	{
		$data = [];

		if(!empty($value)) $data['setting_value'] = $value;
		if(!empty($label)) $data['label'] = $label;
		if(!empty($description)) $data['description'] = $description;
		if(!empty($type)) $data['type'] = $type;

		$this->update($data);

		return $this;
	}


	public function setSerializeValue($value)
	{
		$this->setValue($this->setting_name, serialize($value));

		return $this;
	}


	public function getSerializeValue()
	{
		return unserialize($this->setting_value);
	}


	public static function initializeWebsiteSettings()
	{
		$configs = config('cms_config');
		return self::initializeSettings($configs);
	}


	public static function initializeThemeSettings()
	{
		self::initializeWebsiteSettings();
		try {
			$configs = config('theme_config')[self::getValue('website_theme')];
			return self::initializeSettings($configs);
		} catch (\Exception $e) {
			return [];
		}
	}


	private static function initializeSettings($configs)
	{
		$dataConfigs = [];

		foreach($configs as $key => $data) {
			$dataConfigs[$key] = array_merge($data, [
				'is_exists'	=> false,
				'value'		=> $data['default'],
				'min'		=> $data['min'] ?? null,
				'max'		=> $data['max'] ?? null,
				'max_length'=> $data['max_length'] ?? null,
			]);
		}

		$settings = self::all();
		foreach($settings as $setting) 
		{
			$name = $setting->setting_name;
			if(array_key_exists($name, $dataConfigs)) 
			{
				$config = $dataConfigs[$name];
				if($setting->label != $config['label']
					|| $setting->description != $config['description']
					|| $setting->type != $config['type'])
				{
					$setting->set(null, $config['description'], $config['type']);
				}
				$dataConfigs[$name]['is_exists'] = true;
				$dataConfigs[$name]['value'] = $setting->setting_value;
			}
		}

		foreach($dataConfigs as $key => $data)
		{
			if(!$data['is_exists']) {
				self::create([
					'setting_name'	=> $key,
					'setting_value'	=> $data['default'],
					'label'			=> $data['label'],
					'description'	=> $data['description'],
					'type'			=> $data['type'],
				]);
			}
		}

		return $dataConfigs;
	}


	/**
	*
	*	Setting Helper
	*
	*/
	public static function temps($path = '')
	{
		return storage_path('app/public/temp_files/'.$path);
	}


	public static function systems($path = '')
	{
		return storage_path('app/public/system/'.$path);
	}

}