<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $fillable = [ 'tag_name', 'slug', 'created_by', 'updated_by' ];


	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}


	public function createdByName()
	{
		return $this->createdBy ? $this->createdBy->name : '-';
	}


	public function updatedBy()
	{
		return $this->belongsTo('App\User', 'updated_by');
	}


	public function updatedByName()
	{
		return $this->updatedBy ? $this->updatedBy->name : '-';
	}


	public function generateSlug()
	{
		$base = \Str::of($this->tag_name)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$postCategory = self::where('slug', $slug)
								->where('id', '!=', $this->id)
								->first();

			if($postCategory) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public static function createTag($request)
	{
		$tag = self::create([
			'tag_name'		=> $request->tag_name,
			'created_by'	=> auth()->user()->id,
		]);

		$tag->generateSlug();

		return $tag;
	}


	public function updateTag($request)
	{
		$this->update([
			'tag_name'		=> $request->tag_name,
			'updated_by'	=> auth()->user()->id,
		]);

		$this->generateSlug();

		return $this;
	}


	public function deleteTag()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('tag.edit', $data->id).'" title="Edit Tag">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('tag.destroy', $data->id).'" title="Hapus Tag">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
