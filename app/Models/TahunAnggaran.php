<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TahunAnggaran extends Model
{
	protected $table = "tahun_anggaran";
	protected $fillable = [ 'tahun' ];

	public static function createTahunAnggaran($request)
	{
		$tahunAnggaran = self::create([
			'tahun'		=> $request->tahun,
		]);

		return $tahunAnggaran;
	}


	public function updateTahunAnggaran($request)
	{
		$this->update([
			'tahun'		=> $request->tahun,
		]);

		return $this;
	}


	public function deleteTahunAnggaran()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('tahun_anggaran.edit', $data->id).'" title="Edit Tahun Anggaran">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('tahun_anggaran.destroy', $data->id).'" title="Hapus Tahun Anggaran">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
