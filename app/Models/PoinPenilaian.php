<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoinPenilaian extends Model
{
	protected $table = 'poin_penilaian';
	protected $fillable = [ 'id_penilaian', 'aspek_kinerja', 'indikator', 'bobot', 'skor', 'nilai_kinerja' ];

	public function skorText()
	{
		if($this->skor == 1) return 'Cukup';
		if($this->skor == 2) return 'Baik';
		if($this->skor == 3) return 'Sangat Baik';
	}


	public function isSkorSangatKurang()
	{
		return $this->skor <= 50;
	}

	public function isSkorKurang()
	{
		return $this->skor >= 51 && $this->skor <= 60;
	}

	public function isSkorCukup()
	{
		return $this->skor >= 61 && $this->skor <= 70;
	}

	public function isSkorBaik()
	{
		return $this->skor >= 71 && $this->skor <= 80;
	}

	public function isSkorSangatBaik()
	{
		return $this->skor >= 81 && $this->skor <= 100;
	}
}
