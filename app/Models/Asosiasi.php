<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asosiasi extends Model
{
	use SoftDeletes;
	
	protected $table = 'asosiasi';
	protected $fillable = [ 'nama_asosiasi', 'slug' ];


	public function perusahaan()
	{
		return $this->hasMany('App\Models\Perusahaan', 'id_asosiasi');
	}


	public static function createAsosiasi(array $request)
	{
		return self::create($request);
	}


	public function updateAsosiasi(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteAsosiasi()
	{
		return $this->delete();
	}

	public function generateSlug()
	{
		$base = \Str::of($this->nama_asosiasi)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$post = self::where('slug', $slug)
						->where('id', '!=', $this->id)
						->first();
			if($post) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('asosiasi.edit', $data->id).'" title="Edit Asosiasi">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('asosiasi.destroy', $data->id).'" title="Hapus Asosiasi">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item" href="'.route('asosiasi.export_perusahaan', $data->id).'" title="Export Perusahaan Asosiasi" target="_blank">
							<i class="mdi mdi-file-excel"></i> Export Perusahaan
						</a>
						<a class="dropdown-item" href="'.route('asosiasi.list_perusahaan', $data->id).'" title="List Perusahaan Asosiasi">
							<i class="mdi mdi-magnify"></i> List Perusahaan
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function exportListPerusahaanToExcel()
	{
		$data = $this->perusahaan;

		$filename = 'Perusahaan.xlsx';

		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 20 ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			'Asosiasi' => 'string',
			'Nama Perusahaan' => 'string',
			'Direktur' => 'string',
			'Alamat' => 'string',
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [25,25,20,20],
			'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 5, 'wrap_text' => true
		]);

		$iter = 1;
		foreach($data as $perusahaan)
		{
			$writer->writeSheetRow('Sheet1', [
				$perusahaan->namaAsosiasi(),
				$perusahaan->nama_perusahaan,
				$perusahaan->direktur,
				$perusahaan->alamat,
			], $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}
}
