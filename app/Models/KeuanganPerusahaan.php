<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeuanganPerusahaan extends Model
{
	protected $table = 'keuangan_perusahaan';
	protected $fillable = [ 'id_perusahaan', 'nama', 'alamat', 'jumlah_saham', 'nilai_satuan_saham', 'modal_dasar', 'modal_disetor', 'data_perubahan', 'status_perubahan', 'aksi_perubahan', 'data_baru' ];


	/**
	 * 	Relationship
	 * */
	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan');
	}

	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '';
	}


	public static function createKeuanganPerusahaan(array $request)
	{
		return self::create([
			'id_perusahaan'		=> auth()->user()->id_perusahaan,
			'data_perubahan'	=> serialize($request),
			'status_perubahan'	=> 'wait',
			'aksi_perubahan'	=> 'create',
			'data_baru'			=> 'yes',
		]);
	}


	public function updateKeuanganPerusahaan(array $request)
	{
		$this->update([
			'data_perubahan'	=> serialize($request),
			'status_perubahan'	=> 'wait',
		]);

		if(empty($this->aksi_perubahan)) {
			$this->update([
				'aksi_perubahan'	=> 'update',
				'data_baru'			=> 'no',
			]);
		}

		return $this;
	}


	public function deleteKeuanganPerusahaan()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::where('created_at', '!=', 'null');

		if(auth()->user()->isPerusahaan()) {
			$data = $data->where('id_perusahaan', perusahaan()->id);
		}

		return \DataTables::eloquent($data)
			->editColumn('nama', function($data){
				return tmpText($data, 'nama');
			})
			->editColumn('alamat', function($data){
				return tmpText($data, 'alamat');
			})
			->editColumn('jumlah_saham', function($data){
				return tmpText($data, 'jumlah_saham');
			})
			->editColumn('nilai_satuan_saham', function($data){
				return tmpText($data, 'nilai_satuan_saham');
			})
			->editColumn('modal_dasar', function($data){
				return tmpText($data, 'modal_dasar');
			})
			->editColumn('modal_disetor', function($data){
				return tmpText($data, 'modal_disetor');
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('p.keuangan.edit', $data->id).'" title="Edit Keuangan Perusahaan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item" href="'.route('p.keuangan.detail', $data->id).'" title="Detail Keuangan Perusahaan">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('p.keuangan.destroy', $data->id).'" title="Hapus Keuangan Perusahaan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'nama', 'alamat', 'jumlah_saham', 'nilai_satuan_saham', 'modal_dasar', 'modal_disetor', 'action' ])
			->make(true);
	}


	public function getDataPerubahan() : array
	{
		try {
			$result = unserialize($this->data_perubahan);
			if($result) {
				return (array) $result;
			} else {
				return [];
			}
		} catch (\Exception $e) {
			return [];
		}
	}


	public function applyUpdate()
	{
		if($this->isAksiCreate() || $this->isAksiUpdate()) {
			$this->update($this->getDataPerubahan());

			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan'	=> 'approved',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);

			return $this;
		} elseif($this->isAksiDelete()) {
			return $this->delete();
		}

	}


	public function reverseUpdate()
	{
		if($this->isAksiUpdate() || $this->isAksiDelete()) {
			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan' => 'rejected',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);
			
			return $this;
		} elseif($this->isAksiCreate()) {
			return $this->delete();
		}

	}


	public function isStatusWait()
	{
		return $this->status_perubahan == 'wait';
	}


	public function isNewData()
	{
		return $this->data_baru == 'wait';
	}

	public function statusPerubahanHtml()
	{
		if($this->status_perubahan == 'approved') {
			return '<span class="text-success"> Terverifikasi </span>';
		} elseif($this->status_perubahan == 'rejected') {
			return '<span class="text-danger"> Ditolak </span>';
		} elseif($this->status_perubahan == 'wait') {
			return '<span class="text-primary"> Menunggu Verifikasi </span>';
		}
	}

	public function isAksiUpdate()
	{
		return $this->aksi_perubahan == 'update';
	}

	public function isAksiCreate()
	{
		return $this->aksi_perubahan == 'create';
	}

	public function isAksiDelete()
	{
		return $this->aksi_perubahan == 'delete';
	}

	public function aksiPerubahanText()
	{
		if($this->isAksiCreate()) return 'Data Baru';
		if($this->isAksiUpdate()) return 'Update Data';
		if($this->isAksiDelete()) return 'Hapus Data';
	}

	public function aksiPerubahanHtml()
	{
		if($this->isAksiCreate()) return '<span class="text-primary"> Data Baru </span>';
		if($this->isAksiUpdate()) return '<span class="text-warning"> Update Data </span>';
		if($this->isAksiDelete()) return '<span class="text-danger"> Hapus Data </span>';
	}

	public static function getKeuanganStatusWait()
	{
		return self::where('status_perubahan', 'wait')->get();
	}

	public static function countKeuanganStatusWait()
	{
		return self::where('status_perubahan', 'wait')->count();
	}
}
