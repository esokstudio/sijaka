<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = [ 'title', 'slug', 'content', 'thumbnail', 'id_category', 'tags', 'is_published', 'created_by', 'updated_by' ];


	public function postCategory()
	{
		return $this->belongsTo('App\Models\PostCategory', 'id_category');
	}


	public function postCategoryName()
	{
		return $this->postCategory ? $this->postCategory->category_name : '- Tanpa Kategori -';
	}


	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}


	public function createdByName()
	{
		return $this->createdBy ? $this->createdBy->name : '-';
	}


	public function updatedBy()
	{
		return $this->belongsTo('App\User', 'updated_by');
	}


	public function updatedByName()
	{
		return $this->updatedBy ? $this->updatedBy->name : '-';
	}


	public function createdAtText($format = 'd M Y')
	{
		return date($format, strtotime($this->created_at));
	}


	public function generateSlug()
	{
		$base = \Str::of($this->title)->slug('-');

		$found = false;
		$iter = 0;
		$slug = '';
		while(!$found) {
			$slug = $iter > 0 ? $base.'-'.$iter : $base;
			$post = self::where('slug', $slug)
						->where('id', '!=', $this->id)
						->first();

			if($post) {
				$iter++;
			} else {
				$found = true;
			}
		}

		$this->update([
			'slug'	=> $slug
		]);

		return $this;
	}


	public function thumbnailPath()
	{
		return storage_path('app/public/cms/posts/'.$this->thumbnail);
	}


	public function thumbnailLink()
	{
		if($this->isHasThumbnail()) {
			return url('storage/cms/posts/'.$this->thumbnail);
		} else {
			return url('images/no-image.jpg');
		}
	}


	public function isHasThumbnail()
	{
		if(empty($this->thumbnail)) return false;

		return \File::exists($this->thumbnailPath());
	}


	public function removeThumbnail()
	{
		if($this->isHasThumbnail())
		{
			\File::delete($this->thumbnailPath());
			$this->update([
				'thumbnail'	=> null,
			]);
		}

		return $this;
	}


	public function setThumbnail($request)
	{
		if(!empty($request->thumbnail))
		{
			$this->removeThumbnail();
			$file = $request->file('thumbnail');
			$filename = $this->slug ?? date('Ymd_His');
			$filename = $filename.'.'.$file->getClientOriginalExtension();

			$file->move(storage_path('app/public/cms/posts/'), $filename);
			$this->update([
				'thumbnail'	=> $filename
			]);
		}

		return $this;
	}


	public static function createPost($request)
	{
		$post = self::create([
			'title'			=> $request->title,
			'content'		=> \Helper::fixContent($request->content),
			'id_category'	=> $request->id_category,
			'is_published'	=> $request->is_published,
			'created_by'		=> auth()->user()->id,
		]);

		$post->generateSlug();
		$post->setThumbnail($request);
		$post->setTags($request);

		return $post;
	}


	public function updatePost($request)
	{
		$this->update([
			'title'			=> $request->title,
			'content'		=> \Helper::fixContent($request->content),
			'id_category'	=> $request->id_category,
			'is_published'	=> $request->is_published,
			'slug'			=> $request->slug,
			'updated_by'	=> auth()->user()->id,
		]);

		$this->setThumbnail($request);
		$this->setTags($request);

		return $this;
	}


	public function deletePost()
	{
		$this->removeThumbnail();
		return $this->delete();
	}


	public function getLink()
	{
		return url($this->fullSlug());
	}


	public function fullSlug()
	{
		return 'post/'.$this->slug;
	}


	public function modifiedAt()
	{
		return $this->updated_at ?? $this->created_at;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->editColumn('post_category.category_name', function($data){
				return $data->postCategoryName();
			})
			->editColumn('tags', function($data){
				return $data->tagsHtml();
			})
			->editColumn('is_published', function($data){
				return $data->publishedHtml();
			})
			->editColumn('thumbnail', function($data){
				$html = '<img src="'.$data->thumbnailLink().'" style="max-width: 30px; max-height: 30px;">';
				return $html;
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('post.edit', $data->id).'" title="Edit Postingan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('post.destroy', $data->id).'" title="Hapus Postingan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item" href="'.$data->getLink().'" title="Kunjungi Postingan" target="_blank">
							<i class="mdi mdi-web"></i> Kunjungi
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'tags', 'is_published', 'thumbnail', 'action' ])
			->make(true);
	}


	public function isPublished()
	{
		return $this->is_published == 'yes';
	}


	public function publishedHtml()
	{
		if($this->isPublished()) {
			return '<span class="text-success"> Ya </span>';
		} else {
			return '<span class="text-danger"> Tidak </span>';
		}
	}


	public function setTags($request)
	{
		if(!empty($request->tags))
		{
			$result = '';
			$iter = 0;
			foreach($request->tags as $tag)
			{
				if($iter > 0) $result .= '-'; 
				$result .= '['.$tag.']';
				$iter++;
			}

			$this->update([
				'tags'	=> $result,
			]);
		}

		return $this;
	}


	public function tags()
	{
		$tags = $this->tagsToArray();
		$results = [];

		foreach($tags as $tag) {
			$tag = Tag::find($tag);
			if($tag) {
				$results[] = $tag;
			}
		}

		return $results;
	}


	public function tagsHtml()
	{
		$bgColors = [ 'bg-danger', 'bg-warning', 'bg-primary', 'bg-success', 'bg-secondary', 'bg-info', 'bg-dark' ];
		$tags = $this->tags();
		$result = '';
		$iter = 0;

		foreach($tags as $tag) {
			if($iter == count($bgColors)) $iter = 0;
			$bgColor = $bgColors[$iter];
			$class = $bgColor.' text-white p-1 mr-1 rounded';
			$result .= '<small class="'.$class.'">'.$tag->tag_name.'</small>';
			$iter++;
		}

		return !empty($result) ? $result : '- Tanpa Tag -';
	}


	public function tagsToArray()
	{
		$array = [];
		$tags = explode('-', $this->tags);
		foreach($tags as $tag) {
			$tag = str_replace('[', '', $tag);
			$tag = str_replace(']', '', $tag);
			$array[] = $tag;
		}

		return $array;
	}


	public function tagsToArrayAsString()
	{
		$result = '[';
		$iter = 0;

		foreach($this->tagsToArray() as $tag)
		{
			if($iter > 0) $result .= ',';
			$result .= $tag;
			$iter++;
		}

		$result .= ']';
		return $result;
	}


	public static function getBySlug($slug)
	{
		return self::where('slug', $slug)->first();
	}


	public static function publishedPosts($limit = 0, $skip = 0)
	{
		$posts = self::where('is_published', 'yes')
					 ->orderBy('created_at', 'desc');

		if($limit > 0) {
			$posts = $posts->take($limit);
		}

		if($skip > 0) {
			$posts = $posts->skip($skip);
		}

		$posts = $posts->get();

		return $posts;
	}


	public static function amountOfPublishedPosts()
	{
		return self::where('is_published', 'yes')->count();
	}


	public function subtitle($limitChar = 50)
	{
		$content = trim(strip_tags($this->content));
		$subtitle = substr($content, 0, $limitChar);
		if(strlen($content) > $limitChar) $subtitle .= "...";
		return $subtitle;
	}


	public static function whereLike($search, $take = 0, $skip = 0)
	{
		$posts = self::where('title', 'like', '%'.$search.'%')
					 ->orWhere('content', 'like', '%'.$search.'%');

		if($take > 0) {
			$posts = $posts->take($take);
		}

		if($skip > 0) {
			$posts = $posts->skip($skip);
		}

		return $posts->get();
	}
}