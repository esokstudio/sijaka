<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NavSubmenu extends Model
{
	protected $fillable = [ 'id_nav_menu', 'title', 'source', 'link', 'position', 'is_open_new_tab' ];


	public function setPosition()
	{
		if(self::count() > 0) {
			$lastPosition = self::orderBy('position', 'desc')
								->where('id_nav_menu', $this->id_nav_menu)
								->first()->position;
			$position = (int) $lastPosition + 1;
		} else {
			$position = 1;
		}

		$this->update([
			'position'	=> $position
		]);
		self::autoResetPosition($this->id_nav_menu);

		return $this;
	}


	public function getLink()
	{
		if ($this->source == 'page') {
			return url($this->link);
		} elseif ($this->source == 'post') {
			return url('post').'/'.$this->link;
		} elseif ($this->source == 'web') {
			return url('').'/'.$this->link;
		} elseif ($this->source == 'url') {
			return $this->link;
		}
	}


	public function sourceText()
	{
		if ($this->source == 'page') {
			return 'Laman';
		} elseif ($this->source == 'post') {
			return 'Postingan';
		} elseif ($this->source == 'web') {
			return 'Website Ini';
		} elseif ($this->source == 'url') {
			return 'URL';
		}
	}


	public static function autoResetPosition($idNavMenu)
	{
		$submenus = self::orderBy('position', 'asc')
						->where('id_nav_menu', $idNavMenu)
						->get();
		$iter = 1;

		foreach($submenus as $submenu)
		{
			if($submenu->position != $iter) {
				$submenu->update([
					'position'	=> $iter
				]);
			}
			$iter++;
		}

		return $submenus;
	}


	public static function createNavSubmenu(array $request)
	{
		$submenu = self::create($request);
		$submenu->setPosition($submenu->id_nav_menu);

		return $submenu;
	}


	public function updateNavSubmenu(array $request)
	{
		$this->update($request);
		self::autoResetPosition($this->id_nav_menu);

		return $this;
	}


	public function deleteNavSubmenu()
	{
		$menuID = $this->id_nav_menu;
		$result = $this->delete();
		self::autoResetPosition($menuID);

		return $result;
	}


	public static function dt($navMenuID)
	{
		$data = self::where('id_nav_menu', $navMenuID)->get();

		return \DataTables::of($data)
			->addColumn('source', function($data){
				return $data->sourceText();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('nav_submenu.edit', [$data->id_nav_menu, $data->id]).'" title="Edit Submenu">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('nav_submenu.destroy', [$data->id_nav_menu, $data->id]).'" title="Hapus Submenu">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('nav_submenu.switch_to_up', [$data->id_nav_menu, $data->id]).'" data-message="Yakin ingin naikan posisi?" title="Naikan Urutan">
							<i class="mdi mdi-arrow-up"></i> Naikan Urutan
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('nav_submenu.switch_to_down', [$data->id_nav_menu, $data->id]).'" data-message="Yakin ingin turunkan posisi?" title="Turunkan Urutan">
							<i class="mdi mdi-arrow-down"></i> Turunkan Urutan
						</a>
						<a class="dropdown-item" href="'.$data->getLink().'" title="Kunjungi Link" target="_blank">
							<i class="mdi mdi-web"></i> Kunjungi
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function switchToUp()
	{
		self::autoResetPosition($this->id_nav_menu);
		if($this->position != 1) {
			$otherSubmenu = self::where('position', ($this->position - 1))
							 ->where('id_nav_menu', $this->id_nav_menu)
							 ->first();

			$otherSubmenu->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position - 1,
			]);
		}

		return $this;
	}


	public function switchToDown()
	{
		self::autoResetPosition($this->id_nav_menu);
		$total = self::count();

		if($this->position != $total ) {
			$otherSubmenu = self::where('position', ($this->position + 1))
							 ->where('id_nav_menu', $this->id_nav_menu)
							 ->first();

			$otherSubmenu->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position + 1,
			]);
		}

		return $this;
	}
}
