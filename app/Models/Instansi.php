<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instansi extends Model
{
	use SoftDeletes;

	protected $table = 'instansi';
	protected $fillable = [ 'nama_instansi', 'tampilkan_grafik' ];


	public static function createInstansi(array $request)
	{
		return self::create($request);
	}

	public function updateInstansi(array $request)
	{
		$this->update($request);
		return $this;
	}

	public function deleteInstansi()
	{
		return $this->delete();
	}

	public function tampilkanGrafikHtml()
	{
		return $this->tampilkan_grafik == 'yes' ? "<span class='text-success'> Ya </span>" : "<span class='text-danger'> Tidak </span>";
	}


	public static function dataTable($request)
	{
		$data = self::select([ 'instansi.*' ]);

		return \DataTables::eloquent($data)
			->addColumn('tampilkan_grafik', function($data){
				return $data->tampilkanGrafikHtml();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('instansi.edit', $data->id).'" title="Edit Instansi">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('instansi.destroy', $data->id).'" title="Hapus Instansi">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action', 'tampilkan_grafik' ])
			->make(true);
	}
}
