<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewStatistic extends Model
{
	protected $fillable = [ 'content_type', 'id_content', 'user_agent', 'browser', 'platform' ];

	const TYPE_POST		= 'post';
	const TYPE_PAGE		= 'page';
	const TYPE_OTHER	= 'other';


	public function content()
	{
		if($this->isTypePost()) {
			return $this->belongsTo('App\Models\Post', 'id_content');
		} else {
			return $this->belongsTo('App\Models\Page', 'id_content');
		}
	}


	public function isTypePost()
	{
		return $this->content_type == self::TYPE_POST;
	}


	public function isTypePage()
	{
		return $this->content_type == self::TYPE_PAGE;
	}


	public static function amountOfViewToday()
	{
		return self::where('created_at', 'like', '%'.date('Y-m-d').'%')->count();
	}


	public static function amountOfViewThisWeek()
	{
		$startDate = today()->addDays(-date('N')+1);
		$endDate = today()->addDays(6-date('N')+1);
		$startDate = date('Y-m-d 00:00:00', strtotime($startDate));
		$endDate = date('Y-m-d 23:59:59', strtotime($endDate));

		return self::where('created_at', '>=', $startDate)
					->where('created_at', '<=', $endDate)
					->count();
	}


	public static function amountOfViewThisMonth()
	{
		return self::where('created_at', '>=', date('Y-m-01 00:00:00'))
					->where('created_at', '<=', date('Y-m-t 23:59:59'))
					->count();
	}


	public static function viewStatisticThisMonth()
	{
		return self::where('created_at', '>=', date('Y-m-01 00:00:00'))
					->where('created_at', '<=', date('Y-m-t 23:59:59'))
					->orderBy('created_at', 'asc')
					->get();
	}


	public static function viewStatisticLastMonth()
	{
		$startDate = today()->addMonths(-1)->setDay(1);
		$endDate = today()->addMonths(-1)->setDay(date('t', strtotime($startDate)));
		$startDate = date('Y-m-d 00:00:00', strtotime($startDate));
		$endDate = date('Y-m-d 23:59:59', strtotime($endDate));
		return self::where('created_at', '>=', $startDate)
					->where('created_at', '<=', $endDate)
					->orderBy('created_at', 'asc')
					->get();
	}


	public static function writeStatistic($contentType, $contentId = 0)
	{
		if(auth()->guest()) {
			$statistic = self::create([
				'content_type'	=> $contentType,
				'id_content'	=> $contentId,
				'user_agent'	=> \App\MyClass\UserAgent::userAgent(),
				'browser'		=> \App\MyClass\UserAgent::userBrowser(),
				'platform'		=> \App\MyClass\UserAgent::userPlatform(),
			]);	

			return true;
		}

		return false;
	}


	public static function resumeOfViewThisMonth()
	{
		$startDate = today()->setDay(1);
		$endDate = today()->setDay(date('t'));
		$date = date('Y-m-d', strtotime($startDate));
		$results = [];

		while($date <= date('Y-m-d', strtotime($endDate))) 
		{
			$results[$date] = 0;
			$date = new \Carbon\Carbon($date);
			$date->addDays(1);
			$date = date('Y-m-d', strtotime($date));
		}

		foreach(self::viewStatisticThisMonth() as $statistic)
		{
			$date = date('Y-m-d', strtotime($statistic->created_at));
			$results[$date]++;
		}

		return $results;
	}


	public static function resumeOfViewLastMonth()
	{
		$startDate = today()->addMonths(-1)->setDay(1);
		$endDate = today()->addMonths(-1)->setDay(date('t', strtotime($startDate)));
		$date = date('Y-m-d', strtotime($startDate));
		$results = [];

		while($date <= date('Y-m-d', strtotime($endDate))) 
		{
			$results[$date] = 0;
			$date = new \Carbon\Carbon($date);
			$date->addDays(1);
			$date = date('Y-m-d', strtotime($date));
		}

		foreach(self::viewStatisticLastMonth() as $statistic)
		{
			$date = date('Y-m-d', strtotime($statistic->created_at));
			$results[$date]++;
		}

		return $results;
	}


	public static function resumeOfViewYearly($year = null)
	{
		if(empty($year)) $year = date('Y');
		$resume = function($month) use($year) {
			return self::whereBetween('created_at', [ date($year.'-'.$month.'-01'), date($year.'-'.$month.'-t') ])
					   ->where('user_agent', '!=', 'Go-http-client/1.1')
					   ->count();
		};

		$results = [
			'Januari' => $resume('01'),
			'Februari' => $resume('02'),
			'Maret' => $resume('03'),
			'April' => $resume('04'),
			'Mei' => $resume('05'),
			'Juni' => $resume('06'),
			'Juli' => $resume('07'),
			'Agustus' => $resume('08'),
			'September' => $resume('09'),
			'Oktober' => $resume('10'),
			'November' => $resume('11'),
			'Desember' => $resume('12'),
		];

		return $results;
	}

}
