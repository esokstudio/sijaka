<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengalamanPeserta extends Model
{
	protected $table = 'pengalaman_peserta';
	protected $fillable = [ 'id_user', 'nama_pekerjaan', 'lokasi_pekerjaan', 'nilai_kontrak', 'mulai', 'selesai', 'jabatan' ];


	public static function createPengalaman(array $request)
	{
		return self::create($request);
	}


	public function updatePengalaman(array $request)
	{
		$this->update($request);
		return $this;
	}


	public function deletePengalaman()
	{
		return $this->delete();
	}


	public static function dt()
	{
		if(auth()->user()->isPeserta()) {
			$data = self::where('id_user', auth()->user()->id)->get();
		} else {
			$data = self::all();
		}

		return \DataTables::of($data)
			->editColumn('created_at', function($data){
				return date('Y-m-d H:i:s', strtotime($data->created_at));
			})
			->addColumn('peserta_action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('website.pengalaman_peserta', $data->id).'" title="Edit Pengalaman Peserta">
							<i class="mdi mdi-pencil"></i> Edit
						</a>
						<a class="dropdown-item" href="'.route('website.pengalaman_peserta', $data->id).'" title="Edit Pengalaman Peserta">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->addColumn('peserta_action', function($data){
				$button = '
				<a href="'.route('website.pengalaman_peserta', $data->id).'" title="Edit Pengalaman Peserta">
					<i class="fa fa-edit"></i> Edit
				</a> |
				<a href="'.route('website.pengalaman_peserta', $data->id).'" title="Edit Pengalaman Peserta">
					<i class="fa fa-trash"></i> Hapus
				</a>';

				return $button;
			})
			->rawColumns([ 'peserta_action', 'action' ])
			->make(true);
	}
}
