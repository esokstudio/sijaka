<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KategoriPeraturan extends Model
{
	use SoftDeletes;

	protected $table = "kategori_peraturan";
	protected $fillable = [ 'nama_kategori' ];

	public static function createKategoriPeraturan($request)
	{
		$kategoriPeraturan = self::create([
			'nama_kategori'		=> $request->nama_kategori,
		]);

		return $kategoriPeraturan;
	}


	public function updateKategoriPeraturan($request)
	{
		$this->update([
			'nama_kategori'		=> $request->nama_kategori,
		]);

		return $this;
	}


	public function deleteKategoriPeraturan()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('kategori_peraturan.edit', $data->id).'" title="Edit Kategori Peraturan">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('kategori_peraturan.destroy', $data->id).'" title="Hapus Kategori Peraturan">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
