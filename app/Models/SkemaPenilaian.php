<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkemaPenilaian extends Model
{
	protected $table = 'skema_penilaian';
	protected $fillable = [ 'nama_skema_penilaian' ];


	/**
	 * Relationship
	 * 
	 * */
	public function aspekKinerja()
	{
		return $this->hasMany('App\Models\AspekKinerja', 'id_skema_penilaian');
	}


	public static function createSkemaPenilaian(array $request)
	{
		return self::create($request);
	}


	public function updateSkemaPenilaian(array $request)
	{
		$this->update($request);
		return $this;
	}


	public function deleteSkemaPenilaian()
	{
		$this->deleteAllSpekKinerja();
		return $this->delete();
	}


	public function deleteAllSpekKinerja()
	{
		foreach($this->aspekKinerja as $aspekKinerja) {
			$aspekKinerja->deleteAspekKinerja();
		}

		return $this;
	}


	public function totalBobot()
	{
		$total = 0;
		foreach($this->aspekKinerja as $aspekKinerja) {
			foreach($aspekKinerja->indikatorPenilaian as $indikatorPenilaian) {
				$total += (int) $indikatorPenilaian->bobot;
			}
		}

		return $total;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('total_bobot', function($data){
				return $data->totalBobot();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('skema_penilaian.edit', $data->id).'" title="Edit Nama Skema">
							<i class="mdi mdi-pencil"></i> Edit Nama Skema
						</a>
						<a class="dropdown-item" href="'.route('skema_penilaian.setting', $data->id).'" title="Atur Aspek & Indikator">
							<i class="mdi mdi-pencil"></i> Atur Aspek & Indikator
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('skema_penilaian.destroy', $data->id).'" title="Hapus Skema Penilaian">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function poinPenilaian()
	{
		$result = [];

		foreach($this->aspekKinerja as $aspekKinerja)
		{
			$indikatorData = [];
			foreach($aspekKinerja->indikatorPenilaian as $indikatorPenilaian)
			{
				$indikatorData[] = [
					'indikator'	=> $indikatorPenilaian->nama_indikator_penilaian,
					'bobot'		=> $indikatorPenilaian->bobot,
				];
			}

			$result[] = [
				'aspek_kinerja'	=> $aspekKinerja->nama_aspek_kinerja,
				'list_indikator'=> $indikatorData,
			];
		}

		return $result;
	}
}
