<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AktePendirianPerusahaan extends Model
{
	protected $table = 'akte_pendirian_perusahaan';
	protected $fillable = [ 'id_perusahaan', 'no_akte', 'nama_notaris', 'alamat', 'kota', 'provinsi', 'tanggal_akte', 'data_perubahan', 'status_perubahan', 'aksi_perubahan', 'data_baru' ];


	/**
	 * 	Relationship
	 * */
	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan');
	}

	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '';
	}


	public function updateAktePendirianPerusahaan(array $request)
	{
		$this->update([
			'data_perubahan'	=> serialize($request),
			'status_perubahan'	=> 'wait',
		]);

		if(empty($this->aksi_perubahan)) {
			$this->update([
				'aksi_perubahan'	=> 'update',
				'data_baru'			=> 'no',
			]);
		}

		return $this;
	}

	
	public function getDataPerubahan() : array
	{
		try {
			$result = unserialize($this->data_perubahan);
			if($result) {
				return (array) $result;
			} else {
				return [];
			}
		} catch (\Exception $e) {
			return [];
		}
	}


	public function applyUpdate()
	{
		if($this->isAksiCreate() || $this->isAksiUpdate()) {
			$this->update($this->getDataPerubahan());

			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan'	=> 'approved',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);

			return $this;
		} elseif($this->isAksiDelete()) {
			return $this->delete();
		}

	}


	public function reverseUpdate()
	{
		if($this->isAksiUpdate() || $this->isAksiDelete()) {
			$this->update([
				'data_perubahan'	=> serialize([]),
				'status_perubahan' => 'rejected',
				'aksi_perubahan'	=> null,
				'data_baru'			=> 'no',
			]);
			
			return $this;
		} elseif($this->isAksiCreate()) {
			return $this->delete();
		}

	}


	public function isStatusWait()
	{
		return $this->status_perubahan == 'wait';
	}


	public function isNewData()
	{
		return $this->data_baru == 'wait';
	}

	public function statusPerubahanHtml()
	{
		if($this->status_perubahan == 'approved') {
			return '<span class="text-success"> Terverifikasi </span>';
		} elseif($this->status_perubahan == 'rejected') {
			return '<span class="text-danger"> Ditolak </span>';
		} elseif($this->status_perubahan == 'wait') {
			return '<span class="text-primary"> Menunggu Verifikasi </span>';
		}
	}

	public function isAksiUpdate()
	{
		return $this->aksi_perubahan == 'update';
	}

	public function isAksiCreate()
	{
		return $this->aksi_perubahan == 'create';
	}

	public function isAksiDelete()
	{
		return $this->aksi_perubahan == 'delete';
	}

	public function aksiPerubahanText()
	{
		if($this->isAksiCreate()) return 'Data Baru';
		if($this->isAksiUpdate()) return 'Update Data';
		if($this->isAksiDelete()) return 'Hapus Data';
	}

	public function aksiPerubahanHtml()
	{
		if($this->isAksiCreate()) return '<span class="text-primary"> Data Baru </span>';
		if($this->isAksiUpdate()) return '<span class="text-warning"> Update Data </span>';
		if($this->isAksiDelete()) return '<span class="text-danger"> Hapus Data </span>';
	}

	public static function getAktePendirianStatusWait()
	{
		return self::where('status_perubahan', 'wait')->get();
	}

	public static function countAktePendirianStatusWait()
	{
		return self::where('status_perubahan', 'wait')->count();
	}
}
