<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkGroup extends Model
{
	protected $fillable = [ 'title', 'position' ];


	public function links()
	{
		return $this->hasMany('App\Models\Link', 'id_link_group')
					->orderBy('position', 'asc');
	}


	public function setPosition()
	{
		if(self::count() > 0) {
			$lastPosition = self::orderBy('position', 'desc')->first()->position;
			$position = (int) $lastPosition + 1;
		} else {
			$position = 1;
		}

		$this->update([
			'position'	=> $position
		]);
		self::autoResetPosition();

		return $this;
	}


	public static function autoResetPosition()
	{
		$groups = self::orderBy('position', 'asc')->get();
		$iter = 1;

		foreach($groups as $group)
		{
			if($group->position != $iter) {
				$group->update([
					'position'	=> $iter
				]);
			}
			$iter++;
		}

		return $groups;
	}


	public static function createLinkGroup(array $request)
	{
		$group = self::create($request);
		$group->setPosition();

		return $group;
	}


	public function updateLinkGroup(array $request)
	{
		$this->update($request);
		self::autoResetPosition();

		return $this;
	}


	public function deleteLinkGroup()
	{
		$result = $this->delete();
		self::autoResetPosition();

		return $result;
	}


	public static function dt()
	{
		$data = self::all();

		return \DataTables::of($data)
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('link_group.edit', $data->id).'" title="Edit Grup Link">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('link_group.destroy', $data->id).'" title="Hapus Grup Link">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('link_group.switch_to_up', $data->id).'" data-message="Yakin ingin naikan posisi?" title="Naikan Urutan">
							<i class="mdi mdi-arrow-up"></i> Naikan Urutan
						</a>
						<a class="dropdown-item switch-position" href="javascript:void(0);" data-href="'.route('link_group.switch_to_down', $data->id).'" data-message="Yakin ingin turunkan posisi?" title="Turunkan Urutan">
							<i class="mdi mdi-arrow-down"></i> Turunkan Urutan
						</a>
						<a class="dropdown-item" href="'.route('link', $data->id).'" title="Atur Link">
							<i class="mdi mdi-menu"></i> Atur Link
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}


	public function switchToUp()
	{
		self::autoResetPosition();
		if($this->position != 1) {
			$linkGroup = self::where('position', ($this->position - 1))->first();

			$linkGroup->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position - 1,
			]);
		}

		return $this;
	}


	public function switchToDown()
	{
		self::autoResetPosition();
		$total = self::count();

		if($this->position != $total ) {
			$linkGroup = self::where('position', ($this->position + 1))->first();

			$linkGroup->update([
				'position'	=> $this->position,
			]);

			$this->update([
				'position'	=> $this->position + 1,
			]);
		}

		return $this;
	}
}
