<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AspekKinerja extends Model
{
	protected $table = 'aspek_kinerja';
	protected $fillable = [ 'id_skema_penilaian', 'nama_aspek_kinerja' ];


	/**
	 * 	Relationship
	 * */
	public function indikatorPenilaian()
	{
		return $this->hasMany('App\Models\IndikatorPenilaian', 'id_aspek_kinerja');
	}


	public static function createAspekKinerja(array $request)
	{
		return self::create($request);
	}


	public function updateAspekKinerja(array $request)
	{
		$this->update($request);
		return $this;
	}


	public function deleteAspekKinerja()
	{
		$this->deleteAllIndikatorPenilaian();
		return $this->delete();
	}


	public function deleteAllIndikatorPenilaian()
	{
		foreach($this->indikatorPenilaian as $indikatorPenilaian) {
			$indikatorPenilaian->deleteIndikatorPenilaian();
		}

		return $this;
	}
}
