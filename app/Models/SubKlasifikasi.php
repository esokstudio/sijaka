<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubKlasifikasi extends Model
{
	protected $table = "sub_klasifikasi";
	protected $fillable = [ 'id_klasifikasi', 'nama_sub_klasifikasi' ];


	public function klasifikasi()
	{
		return $this->belongsTo('App\Models\Klasifikasi', 'id_klasifikasi');
	}


	public function namaKlasifikasi()
	{
		return $this->klasifikasi ? $this->klasifikasi->nama_klasifikasi : '-';
	}


	public static function createSubKlasifikasi(array $request)
	{
		$subKlasifikasi = self::create($request);

		return $subKlasifikasi;
	}


	public function updateSubKlasifikasi(array $request)
	{
		$this->update($request);

		return $this;
	}


	public function deleteSubKlasifikasi()
	{
		return $this->delete();
	}


	public static function dt()
	{
		$data = self::with([ 'klasifikasi' ]);

		return \DataTables::eloquent($data)
			->editColumn('klasifikasi.nama_klasifikasi', function($data){
				return $data->namaKlasifikasi();
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('sub_klasifikasi.edit', $data->id).'" title="Edit Sub Klasifikasi">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('sub_klasifikasi.destroy', $data->id).'" title="Hapus Sub Klasifikasi">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'action' ])
			->make(true);
	}
}
