<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
	protected $table = 'penilaian';
	protected $fillable = [ 'tahun_anggaran', 'nama_skema_penilaian', 'unit_kerja', 'id_perusahaan', 'id_paket_pekerjaan', 'jangka_waktu_pelaksanaan', 'awal_pelaksanaan', 'akhir_pelaksanaan', 'metode_pemilihan_penyedia', 'nilai_kinerja', 'nilai_keterangan', 'tempat_penilaian', 'nama_penilai_1', 'jabatan_penilai_1', 'nip_penilai_1', 'nama_penilai_2', 'jabatan_penilai_2', 'nip_penilai_2', 'nama_mengetahui', 'jabatan_mengetahui', 'nip_mengetahui', 'id_instansi' ];


	/**
	 * 	Realationship
	 * */
	public function poinPenilaian()
	{
		return $this->hasMany('App\Models\PoinPenilaian', 'id_penilaian');
	}

	public function paketPekerjaan()
	{
		return $this->belongsTo('App\Models\PaketPekerjaan', 'id_paket_pekerjaan')->withTrashed();
	}

	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan')->withTrashed();
	}

	public function namaPekerjaan()
	{
		return $this->paketPekerjaan ? $this->paketPekerjaan->nama_pekerjaan : '-';
	}

	public function lokasiPekerjaan()
	{
		return $this->paketPekerjaan ? $this->paketPekerjaan->lokasi_pekerjaan : '-';
	}

	public function nomorKontrak()
	{
		return $this->paketPekerjaan ? $this->paketPekerjaan->nomor_kontrak : '-';
	}

	public function tanggalKontrak()
	{
		return $this->paketPekerjaan ? $this->paketPekerjaan->tanggal_kontrak : '-';
	}

	public function nilaiKontrak()
	{
		return $this->paketPekerjaan ? $this->paketPekerjaan->nilai_kontrak : '-';
	}

	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '-';
	}


	public static function createPenilaian($request)
	{
		$paketPekerjaan = PaketPekerjaan::find($request->id_paket_pekerjaan);

		$penilaian = self::create([
			'tahun_anggaran'		=> $request->tahun_anggaran,
			'nama_skema_penilaian'	=> $request->nama_skema_penilaian,
			'unit_kerja'			=> $request->unit_kerja,
			'id_perusahaan'			=> $paketPekerjaan->id_perusahaan,
			'id_paket_pekerjaan'	=> $request->id_paket_pekerjaan,
			'jangka_waktu_pelaksanaan' => $paketPekerjaan->jangkaWaktuPelaksanaan(),
			'awal_pelaksanaan'		=> $paketPekerjaan->awal_pelaksanaan,
			'akhir_pelaksanaan'		=> $paketPekerjaan->akhir_pelaksanaan,
			'metode_pemilihan_penyedia' => $request->metode_pemilihan_penyedia,
			'tempat_penilaian'		=> $request->tempat_penilaian,
			'nama_penilai_1'		=> $request->nama_penilai_1,
			'jabatan_penilai_1'		=> $request->jabatan_penilai_1,
			'nip_penilai_1'			=> $request->nip_penilai_1,
			'nama_penilai_2'		=> $request->nama_penilai_2,
			'jabatan_penilai_2'		=> $request->jabatan_penilai_2,
			'nip_penilai_2'			=> $request->nip_penilai_2,
			'nama_mengetahui'		=> $request->nama_mengetahui,
			'jabatan_mengetahui'	=> $request->jabatan_mengetahui,
			'nip_mengetahui'		=> $request->nip_mengetahui,
		]);

		$penilaian->createPoinPenilaian($request->poin);
		$penilaian->countNilai();
		$penilaian->load('perusahaan');
		if($penilaian->perusahaan) {
			$penilaian->perusahaan->generatePenilaian();
		}
		if(auth()->user()->isPenilai()) {
			$penilaian->update([
				'id_instansi'	=> auth()->user()->id_instansi
			]);
		}

		$paketPekerjaan->createPengalamanPerusahaan();

		return $penilaian;
	}


	public function updatePenilaian($request)
	{
		$paketPekerjaan = PaketPekerjaan::find($request->id_paket_pekerjaan);
		$oldPerusahaanId = $this->id_perusahaan;

		$this->update([
			'tahun_anggaran'		=> $request->tahun_anggaran,
			'unit_kerja'			=> $request->unit_kerja,
			'id_perusahaan'			=> $paketPekerjaan->id_perusahaan,
			'id_paket_pekerjaan'	=> $request->id_paket_pekerjaan,
			'jangka_waktu_pelaksanaan' => $paketPekerjaan->jangkaWaktuPelaksanaan(),
			'awal_pelaksanaan'		=> $paketPekerjaan->awal_pelaksanaan,
			'akhir_pelaksanaan'		=> $paketPekerjaan->akhir_pelaksanaan,
			'metode_pemilihan_penyedia' => $request->metode_pemilihan_penyedia,
			'tempat_penilaian'		=> $request->tempat_penilaian,
			'nama_penilai_1'		=> $request->nama_penilai_1,
			'jabatan_penilai_1'		=> $request->jabatan_penilai_1,
			'nip_penilai_1'			=> $request->nip_penilai_1,
			'nama_penilai_2'		=> $request->nama_penilai_2,
			'jabatan_penilai_2'		=> $request->jabatan_penilai_2,
			'nip_penilai_2'			=> $request->nip_penilai_2,
			'nama_mengetahui'		=> $request->nama_mengetahui,
			'jabatan_mengetahui'	=> $request->jabatan_mengetahui,
			'nip_mengetahui'		=> $request->nip_mengetahui,
		]);

		foreach($request->poin as $poinId => $skor) {
			$poin = PoinPenilaian::find($poinId);
			$poin->update([
				'skor'			=> $skor,
				'nilai_kinerja'	=> round(($poin->bobot * $skor / 100), 1)
			]);
		}

		$this->countNilai();
		$this->load('perusahaan');
		if($this->perusahaan) {
			$this->perusahaan->generatePenilaian();
		}

		if($this->id_perusahaan != $oldPerusahaanId) {
			Perusahaan::find($oldPerusahaanId)->generatePenilaian();
		}

		return $this;
	}


	public function createPoinPenilaian($dataPoin)
	{
		foreach($dataPoin as $key => $poin)
		{
			PoinPenilaian::create([
				'id_penilaian'	=> $this->id,
				'aspek_kinerja'	=> $poin['aspek_kinerja'],
				'indikator'		=> $poin['indikator'],
				'bobot'			=> $poin['bobot'],
				'skor'			=> $poin['skor'],
				'nilai_kinerja'	=> round(($poin['bobot'] * $poin['skor'] / 100), 1),
			]);
		}

		return $this;
	}


	public function deletePenilaian()
	{
		PoinPenilaian::where('id_penilaian', $this->id)->delete();
		return $this->delete();
	}


	public function countNilai()
	{
		$this->load('poinPenilaian');
		$nilai = 0;
		foreach($this->poinPenilaian as $poin) {
			$nilai += $poin->nilai_kinerja;
		}
		$nilai = round($nilai, 1);

		$nilaiKeterangan = '';
		if($nilai >= 81.0 && $nilai <= 100) $nilaiKeterangan = "Sangat Baik";
		if($nilai >= 71.0 && $nilai <= 80.9) $nilaiKeterangan = "Baik";
		if($nilai >= 61.0 && $nilai <= 70.9) $nilaiKeterangan = "Cukup";
		if($nilai >= 51.0 && $nilai <= 60.9) $nilaiKeterangan = "Kurang";
		if($nilai <= 50.9) $nilaiKeterangan = "Sangat Kurang";

		$this->update([
			'nilai_kinerja'		=> $nilai,
			'nilai_keterangan'	=> $nilaiKeterangan
		]);

		return $this;
	}


	public function awalPelaksanaanText($format = 'd M Y')
	{
		return date($format, strtotime($this->awal_pelaksanaan));
	}

	public function akhirPelaksanaanText($format = 'd M Y')
	{
		return date($format, strtotime($this->akhir_pelaksanaan));
	}


	public static function dataTable($request)
	{
		$data = self::with([ 'perusahaan', 'paketPekerjaan' ])
					->select([ 'penilaian.*' ])
					->join('paket_pekerjaan','penilaian.id_paket_pekerjaan','=', 'paket_pekerjaan.id');

		if(!empty($request->tahun_anggaran)) {
			$data = $data->where('penilaian.tahun_anggaran', $request->tahun_anggaran);
		}

		if(!empty($request->id_perusahaan)) {
			$data = $data->where('penilaian.id_perusahaan', $request->id_perusahaan);
		}

		if(!empty($request->start_date)) {
			$data = $data->where('paket_pekerjaan.tanggal_kontrak', '>=', $request->start_date);
		}

		if(!empty($request->end_date)) {
			$data = $data->where('paket_pekerjaan.tanggal_kontrak', '<=', $request->end_date);
		}

		if(auth()->user()->isPenilai()) {
			$data = $data->where('penilaian.id_instansi', auth()->user()->id_instansi);
		}

		return \DataTables::eloquent($data)
			->editColumn('tahun_anggaran', function($data){
				return $data->tahun_anggaran ?? '-';
			})
			->editColumn('perusahaan.nama_perusahaan', function($data){
				return '<a href="'. route('perusahaan.detail', $data->id_perusahaan) .'">'. $data->namaPerusahaan() .'</a>';
			})
			->addColumn('action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>';

				$button .= '
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('penilaian.detail', $data->id).'" title="Detail Penilaian">
							<i class="mdi mdi-magnify"></i> Detail 
						</a>
						<a class="dropdown-item" href="'.route('penilaian.edit', $data->id).'" title="Edit Penilaian">
							<i class="mdi mdi-pencil"></i> Edit 
						</a>
						<a class="dropdown-item" href="'.route('penilaian.export_to_excel', $data->id).'" title="Export Penilaian Ke Excel" target="_blank">
							<i class="mdi mdi-file-excel"></i> Export Ke Excel
						</a>
						<a class="dropdown-item delete" href="javascript:void(0);" data-href="'.route('penilaian.destroy', $data->id).'" title="Hapus Penilaian">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'perusahaan.nama_perusahaan', 'action' ])
			->make(true);
	}


	public function poinPenilaianWithGrouping()
	{
		$result = [];
		foreach($this->poinPenilaian as $poinPenilaian)
		{
			if(array_key_exists($poinPenilaian->aspek_kinerja, $result)) {
				$result[$poinPenilaian->aspek_kinerja][] = $poinPenilaian;
			} else {
				$result[$poinPenilaian->aspek_kinerja] = [
					$poinPenilaian
				];
			}
		}

		$finalResult = [];
		foreach($result as $res) {
			$finalResult[] = $res;
		}

		return $finalResult;
	}


	public function generateExcel()
	{
		$filename = 'Lembar_Penilaian_Kinerja_Penyedia_Barang_Jasa.xlsx';
		$paket = $this->paketPekerjaan;
		$perusahaan = $this->perusahaan;

		$headerStyle = [ 'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 25, 'wrap_text' => true ];
		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 20 ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
			''=>'string',//text
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,20,3,10,2,30,10,10,10,10,10,10,15]
		]);

		$writer->writeSheetRow('Sheet1', [
			'LEMBAR PENILAIAN KINERJA PENYEDIA BARANG/JASA',
		], [ 'halign' => 'center', 'font-style' => 'bold' ]);
		$writer->markMergedCell('Sheet1', $start_row=1, $start_col=0, $end_row=1, $end_col=12);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$writer->writeSheetRow('Sheet1', [
			strtoupper($this->nama_skema_penilaian),
			'', '', '', '', '', '', '', '', '', '', '',
			'TAHUN ' . date('Y', strtotime($this->created_at))
		], [ 
			'font-style' => 'bold',
			'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin'
		]);
		$writer->markMergedCell('Sheet1', $start_row=3, $start_col=0, $end_row=3, $end_col=11);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$writer->writeSheetRow('Sheet1', [ '1. ', 'Unit Kerja/Perangkat Daerah', '', '', ':', $this->unit_kerja ]);
		$writer->writeSheetRow('Sheet1', [ '2. ', 'Nama Perusahaan', '', '', ':', $perusahaan->nama_perusahaan ]);
		$writer->writeSheetRow('Sheet1', [ '3. ', 'Alamat Perusahaan', '', '', ':', $perusahaan->alamat ]);
		$writer->writeSheetRow('Sheet1', [ '4. ', 'Paket Pekerjaan', '', '', ':', $this->namaPekerjaan() ]);
		$writer->writeSheetRow('Sheet1', [ '5. ', 'Lokasi Pekerjaan', '', '', ':', $this->lokasiPekerjaan() ]);
		$writer->writeSheetRow('Sheet1', [ '6. ', 'Nilai Kontrak', '', '', ':', number_format($this->nilaiKontrak()) ]);
		$writer->writeSheetRow('Sheet1', [ '7. ', 'Nomor Kontrak', '', '', ':', $this->nomorKontrak(), 'Tanggal : ', date('d M Y', strtotime($this->tanggalKontrak())) ]);
		$writer->writeSheetRow('Sheet1', [ '8. ', 'Jangka Waktu Pelaksanaan', '', '', ':', "{$this->jangka_waktu_pelaksanaan} Hari Kalender", 'Tanggal :', $this->awalPelaksanaanText('d-m-Y') .' sd '. $this->akhirPelaksanaanText('d-m-Y') ]);
		$writer->writeSheetRow('Sheet1', [ '9. ', 'Metode Pemilihan Penyedia', '', '', ':', $this->metode_pemilihan_penyedia ]);

		for ($i = 5; $i <= 13; $i++) { 
			$writer->markMergedCell('Sheet1', $start_row=$i, $start_col=1, $end_row=$i, $end_col=3);
		}
		$writer->markMergedCell('Sheet1', $start_row=11, $start_col=7, $end_row=11, $end_col=9);
		$writer->markMergedCell('Sheet1', $start_row=12, $start_col=7, $end_row=12, $end_col=9);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$writer->writeSheetRow('Sheet1', [ 'No.', 'Aspek Kinerja', 'Indikator', '', '', '', 'Bobot (%)', 'Penilaian', '', '', '', '', '(Nilai x Bobot) / 100' ], $headerStyle);
		$writer->writeSheetRow('Sheet1', [ '', '', '', '', '', '', '', 'Sangat Kurang', 'Kurang', 'Cukup', 'Baik', 'Sangat Baik', '' ], $headerStyle);
		$writer->writeSheetRow('Sheet1', [ '', '', '', '', '', '', '', '≤50', '51-60', '61-70', '71-80', '81-100', '' ], $headerStyle);

		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=0, $end_row=17, $end_col=0);
		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=1, $end_row=17, $end_col=1);
		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=2, $end_row=17, $end_col=5);
		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=6, $end_row=17, $end_col=6);
		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=7, $end_row=15, $end_col=11);
		$writer->markMergedCell('Sheet1', $start_row=15, $start_col=12, $end_row=17, $end_col=12);

		$rowIter = 17;

		$iterGroup = 1;
		$totalBobot = 0;
		$nilaiKinerja = 0;

		$addStyle = [ 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'wrap_text' => true, ];
		$style = [
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'left' ], $addStyle),
			array_merge([ 'halign' => 'left' ], $addStyle),
			array_merge([ 'halign' => 'left' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			array_merge([ 'halign' => 'center' ], $addStyle),
			'height' => 30,
		];

		// dd($style);

		foreach($this->poinPenilaianWithGrouping() as $group)
		{
			$namaAspekKinerja = $group[0]->aspek_kinerja;
			$iterPoin = 1;

			
			foreach($group as $poin)
			{
				if($iterPoin == 1) {

					$writer->writeSheetRow('Sheet1', [ 
						$iterGroup++, 
						$namaAspekKinerja, 
						$iterPoin++, 
						$poin->indikator,
						'', '',
						"{$poin->bobot}%",
						$poin->isSkorSangatKurang() ? $poin->skor : '',
						$poin->isSkorKurang() ? $poin->skor : '',
						$poin->isSkorCukup() ? $poin->skor : '',
						$poin->isSkorBaik() ? $poin->skor : '',
						$poin->isSkorSangatBaik() ? $poin->skor : '',
						$poin->nilai_kinerja,
					], $style);
				} else {
					$writer->writeSheetRow('Sheet1', [ 
						'', 
						'', 
						$iterPoin++, 
						$poin->indikator,
						'', '',
						$poin->bobot . "%",
						$poin->isSkorSangatKurang() ? $poin->skor : '',
						$poin->isSkorKurang() ? $poin->skor : '',
						$poin->isSkorCukup() ? $poin->skor : '',
						$poin->isSkorBaik() ? $poin->skor : '',
						$poin->isSkorSangatBaik() ? $poin->skor : '',
						$poin->nilai_kinerja,
					], $style);
				}

				$rowIter++;
				$writer->markMergedCell('Sheet1', $start_row=$rowIter, $start_col=3, $end_row=$rowIter, $end_col=5);

				$totalBobot += $poin->bobot;
				$nilaiKinerja += $poin->nilai_kinerja;
			}

			$writer->markMergedCell('Sheet1', $start_row=($rowIter - (count($group) - 1)), $start_col=0, $end_row=$rowIter, $end_col=0);
			$writer->markMergedCell('Sheet1', $start_row=($rowIter - (count($group) - 1)), $start_col=1, $end_row=$rowIter, $end_col=1);
		}

		$writer->writeSheetRow('Sheet1', [ '', '', '', '', '', '', $totalBobot . '%', '', '', '', '', '', $nilaiKinerja . ' ('. $this->nilai_keterangan . ')' ], $style);
		$writer->markMergedCell('Sheet1', $start_row=$rowIter+1, $start_col=3, $end_row=$rowIter+1, $end_col=5);

		$writer->writeSheetRow('Sheet1', [ 'Keterangan: Cara penilaian setiap indikator adalah dengan memberikan nilai (angka) pada kolom yang sesuai' ]);
		$writer->markMergedCell('Sheet1', $start_row=$rowIter+2, $start_col=0, $end_row=$rowIter+2, $end_col=12);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		$style = [ 'halign' => 'center', 'font-style' => 'bold' ];

		$writer->writeSheetRow('Sheet1', [ '', '', '', '', '', '', '', '', $this->tempat_penilaian .',' ]);
		// TTD 1 & 2
		$writer->writeSheetRow('Sheet1', [ '', '', ',', '', '', '', '', '', '', 'Penilai,' ], $style);
		$writer->writeSheetRow('Sheet1', [ '', '', $this->jabatan_penilai_2, '', '', '', '', '', '', $this->jabatan_penilai_1 ], $style);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);

		// TTD 1 & 2
		$style = [ 'halign' => 'center', 'font-style' => 'bold,underline' ];
		$writer->writeSheetRow('Sheet1', [ '', '', $this->nama_penilai_2, '', '', '', '', '', '',$this->nama_penilai_1 ], $style);
		$style = [ 'halign' => 'center', 'font-style' => 'bold' ];
		$writer->writeSheetRow('Sheet1', [ '', '', 'NIP ' . $this->nip_penilai_2, '', '', '', '', '', '', 'NIP ' . $this->nip_penilai_1 ], $style);

		$writer->writeSheetRow('Sheet1', [ '' ]);

		// TTD 3
		$writer->writeSheetRow('Sheet1', [ '', '', '', 'Mengetahui,', '', '', '', '', '', '' ], $style);
		$writer->writeSheetRow('Sheet1', [ '', '', '', $this->jabatan_mengetahui, '', '', '', '', '', '' ], $style);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);
		$writer->writeSheetRow('Sheet1', [ '' ]);

		$rowIter += 14;
		// TTD 3
		$style = [ 'halign' => 'center', 'font-style' => 'bold,underline' ];
		$writer->writeSheetRow('Sheet1', [ '', '', '', $this->nama_mengetahui, '', '', '', '', '', '' ], $style);
		$style = [ 'halign' => 'center', 'font-style' => 'bold' ];
		$writer->writeSheetRow('Sheet1', [ '', '', '', 'NIP ' . $this->nip_mengetahui, '', '', '', '', '', '' ], $style);

		for ($i = 1; $i <= 8; $i++)
		{ 
			$writer->markMergedCell('Sheet1', $start_row=$rowIter, $start_col=3, $end_row=$rowIter++, $end_col=8);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}


	public function downloadExcel()
	{
		$result = $this->generateExcel();
		return response()->download($result->filepath);
	}


	public static function exportToExcel($request)
	{
		$data = self::with([ 'perusahaan', 'paketPekerjaan' ])
					->select([ 'penilaian.*' ])
					->join('paket_pekerjaan','penilaian.id_paket_pekerjaan','=', 'paket_pekerjaan.id');

		if(!empty($request->tahun_anggaran)) {
			if($request->tahun_anggaran != 'all') {
				$data = $data->where('penilaian.tahun_anggaran', $request->tahun_anggaran);
			}
		}

		if(!empty($request->id_perusahaan)) {
			if($request->id_perusahaan != 'all') {
				$data = $data->where('penilaian.id_perusahaan', $request->id_perusahaan);
			}
		}

		if(!empty($request->start_date)) {
			$data = $data->where('paket_pekerjaan.tanggal_kontrak', '>=', $request->start_date);
		}

		if(!empty($request->end_date)) {
			$data = $data->where('paket_pekerjaan.tanggal_kontrak', '<=', $request->end_date);
		}

		$data = $data->get();

		$filename = 'Penilaian.xlsx';

		$bodyStyle = [ 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 20 ];

		$writer = new \App\MyClass\XLSXWriter();

		$header = [
			'No' => 'string',
			'Tahun Anggaran' => 'string',
			'Tanggal Kontrak' => 'string',
			'Nomor Kontrak' => 'string',
			'Perusahaan' => 'string',
			'Paket Pekerjaan' => 'string',
			'Nilai Kinerja' => 'string',
			'Nilai Keterangan' => 'string',
			'Penilai 1' => 'string',
			'NIP Penilai 1' => 'string',
			'Jabatan Penilai 1' => 'string',
			'Penilai 2' => 'string',
			'NIP Penilai 2' => 'string',
			'Jabatan Penilai 2' => 'string',
			'Yang Mengetahui' => 'string',
			'NIP Yang Mengetahui' => 'string',
			'Jabatan Yang Mengetahui' => 'string',
		];

		$writer->writeSheetHeader('Sheet1', $header, [
			'widths'=> [5,10,10,20,20,25,10,10,25,20,20,25,20,20,25,20,20],
			'font-style'=>'bold', 'halign'=>'center', 'valign' => 'center', 'border'=>'left,right,top,bottom', 'border-color' => '#000', 'border-style' => 'thin', 'height'=> 5, 'wrap_text' => true
		]);

		$iter = 1;
		foreach($data as $penilaian)
		{
			$writer->writeSheetRow('Sheet1', [
				$iter++,
				$penilaian->tahun_anggaran ?? '-',
				$penilaian->paketPekerjaan ? $penilaian->paketPekerjaan->tanggal_kontrak : '-',
				$penilaian->paketPekerjaan ? $penilaian->paketPekerjaan->nomor_kontrak : '-',
				$penilaian->namaPerusahaan(),
				$penilaian->paketPekerjaan ? $penilaian->paketPekerjaan->nama_pekerjaan : '-',
				$penilaian->nilai_kinerja,
				$penilaian->nilai_keterangan,
				$penilaian->nama_penilai_1,
				$penilaian->nip_penilai_1,
				$penilaian->jabatan_penilai_1,
				$penilaian->nama_penilai_2,
				$penilaian->nip_penilai_2,
				$penilaian->jabatan_penilai_2,
				$penilaian->nama_mengetahui,
				$penilaian->nip_mengetahui,
				$penilaian->jabatan_mengetahui,
			], $bodyStyle);
		}

		$path = \Helper::temps($filename);
		$writer->writeToFile($path);

		return (object) [
			'filepath'	=> $path,
			'filename'	=> $filename
		];
	}
}
