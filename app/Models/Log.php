<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $fillable = [ 'id_user', 'slug', 'description' ];


	public function isHasLink()
	{
		return !empty($this->slug);
	}


	public function link()
	{
		return url($this->slug);
	}


	public function user()
	{
		return $this->belongsTo('App\User', 'id_user');
	}


	public function userName()
	{
		return $this->user ? $this->user->name : '-';
	}


	public static function write($description, $link = null)
	{
		$slug = $link;
		if(\Str::startsWith($slug, url('')) && !empty($slug)) {
			$slug = \Str::replaceFirst(url(''), '', $slug);
		}

		$log = self::create([
			'id_user'		=> auth()->user()->id,
			'slug'			=> $slug,
			'description'	=> $description
		]);

		return $log;
	}
}
