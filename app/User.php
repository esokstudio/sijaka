<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'role', 'last_active_at', 'avatar', 'status_verifikasi', 'id_perusahaan', 'id_instansi', 'status', 'verification_code',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	const ROLE_ADMIN		= 'admin';
	const ROLE_PERUSAHAAN	= 'perusahaan';
	const ROLE_PENGGUNA		= 'pengguna';
	const ROLE_PENILAI		= 'penilai';
	const ROLE_PESERTA		= 'peserta';


	public function perusahaan()
	{
		return $this->belongsTo('App\Models\Perusahaan', 'id_perusahaan');
	}


	public function namaPerusahaan()
	{
		return $this->perusahaan ? $this->perusahaan->nama_perusahaan : '-';
	}


	public function instansi()
	{
		return $this->belongsTo('App\Models\Instansi', 'id_instansi');
	}


	public function namaInstansi()
	{
		return $this->instansi ? $this->instansi->nama_instansi : '-';
	}


	public static function createUser(array $request)
	{
		$request['password'] = \Hash::make($request['password']);
		$user = self::create($request);

		return $user;
	}


	public function updateUser(array $request)
	{
		if(array_key_exists('password', $request)) {
			if(empty($request['password'])) {
				unset($request['password']);
			}
		}

		$this->update($request);

		if(!empty($request['password'])) {
			$this->changePassword($request['password']);
		}

		return $this;
	}


	public function deleteUser()
	{
		return $this->delete();
	}


	public function changePassword($newPassword)
	{
		$this->update([
			'password'  => \Hash::make($newPassword)
		]);

		return $this;
	}


	public function setAvatar($request)
	{
		if(!empty($request->avatar))
		{
			$this->removeAvatar();
			$file = $request->file('avatar');
			$filename = date('Ymdhis_').$this->name.'.'.$file->getClientOriginalExtension();
			$path = storage_path('app/public/avatars');

			$file->move($path, $filename);

			$this->update([
				'avatar'	=> $filename
			]);
		}

		return $this;
	}


	public function avatarPath()
	{
		return storage_path('app/public/avatars/'.$this->avatar);
	}


	public function avatarLink()
	{
		if($this->isHasAvatar()) {
			return url('storage/avatars/'.$this->avatar);
		} else {
			return url('images/blank.jpg');
		}
	}


	public function isHasAvatar()
	{
		if(empty($this->avatar)) return false;

		return \File::exists($this->avatarPath());
	}


	public function removeAvatar()
	{
		if($this->isHasAvatar()) {
			\File::delete($this->avatarPath());
			$this->update([
				'avatar'	=> null,
			]);
		}

		return $this;
	}


	public function comparePassword($oldPassword)
	{
		return \Hash::check($oldPassword, $this->password);
	}


	public static function getByEmail($email)
	{
		return self::where('email', $email)->first();
	}


	public function roleText()
	{
		return self::availableRoles()[$this->role];
	}


	public static function availableRoles()
	{
		return [
			self::ROLE_ADMIN		=> 'Admin',
			self::ROLE_PENGGUNA		=> 'Pengguna',
			self::ROLE_PERUSAHAAN	=> 'Penyedia Jasa',
			self::ROLE_PENILAI		=> 'Penilai Paket Pekerjaan',
			self::ROLE_PESERTA		=> 'Peserta',
		];
	}


	public static function availableRolesForInput()
	{
		$roles = self::availableRoles();

		return $roles;
	}


	public function statusVerifikasiHtml()
	{
		if($this->status_verifikasi == 'approved') {
			return '<span class="text-success"> Terverifikasi </span>';
		} elseif($this->status_verifikasi == 'rejected') {
			return '<span class="text-danger"> Ditolak </span>';
		} elseif($this->status_verifikasi == 'wait') {
			return '<span class="text-primary"> Menunggu </span>';
		}
	}


	public function isVerified()
	{
		return $this->status_verifikasi == 'approved' || $this->status_verifikasi == 'yes';
	}


	public static function dt($role)
	{
		$data = self::with([ 'perusahaan', 'instansi' ])
					->select([ 'users.*' ])
					->where('role', $role)
					->get();

		return \DataTables::of($data)
			->addColumn('role', function($data){
				return $data->roleText();
			})
			->editColumn('status_verifikasi', function($data){
				return $data->statusVerifikasiHtml();
			})
			->editColumn('perusahaan.nama_perusahaan', function($data){
				return $data->namaPerusahaan();
			})
			->editColumn('instansi.nama_instansi', function($data){
				return $data->namaInstansi();
			})
			->addColumn('penyedia_jasa_action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">';

					if($data->isStatusWait()) {
						$button .= '
						<a class="dropdown-item approve-btn" href="javascript:void(0);" data-href="'.route('user.penyedia_jasa.approve', $data->id).'" title="Setujui Verifikasi">
							<i class="mdi mdi-check"></i> Setujui Verifikasi 
						</a>
						<a class="dropdown-item reject-btn" href="javascript:void(0);" data-href="'.route('user.penyedia_jasa.reject', $data->id).'" title="Tolak Verifikasi">
							<i class="mdi mdi-close"></i> Tolak Verifikasi 
						</a>';
					}

					$button .= '
						<a class="dropdown-item delete-btn" href="javascript:void(0);" data-href="'.route('user.penyedia_jasa.destroy', $data->id).'" title="Hapus User">
							<i class="mdi mdi-trash-can"></i> Hapus
						</a>
					</div>
				</div>';

				return $button;
			})
			->addColumn('pengguna_action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('user.pengguna.edit', $data->id).'" title="Edit Pengguna">
				 			<i class="mdi mdi-pencil"></i> Edit 
				 		</a>
						<a class="dropdown-item delete-btn" href="javascript:void(0);" data-href="'.route('user.pengguna.destroy', $data->id).'" title="Hapus Pengguna">
				 			<i class="mdi mdi-trash-can"></i> Hapus
				 		</a>
					</div>
				</div>';

				return $button;
			})
			->addColumn('penilai_action', function($data){
				$button = '
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle py-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Aksi
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="'.route('user.penilai.edit', $data->id).'" title="Edit Penilai">
				 			<i class="mdi mdi-pencil"></i> Edit 
				 		</a>
						<a class="dropdown-item delete-btn" href="javascript:void(0);" data-href="'.route('user.penilai.destroy', $data->id).'" title="Hapus Penilai">
				 			<i class="mdi mdi-trash-can"></i> Hapus
				 		</a>
					</div>
				</div>';

				return $button;
			})
			->rawColumns([ 'status_verifikasi', 'pengguna_action', 'penyedia_jasa_action', 'penilai_action' ])
			->make(true);
	}


	public function approveVerification()
	{
		$this->update([
			'status_verifikasi'	=> 'approved'
		]);

		return $this;
	}


	public function rejectVerification()
	{
		$this->update([
			'status_verifikasi'	=> 'rejected'
		]);

		return $this;
	}


	/**
	 * 	Role Check
	 * */
	public function isAdmin()
	{
		return $this->role == self::ROLE_ADMIN;
	}

	public function isPerusahaan()
	{
		return $this->role == self::ROLE_PERUSAHAAN;
	}

	public function isPenyediaJasa()
	{
		return $this->isPerusahaan();
	}

	public function isPengguna()
	{
		return $this->role == self::ROLE_PENGGUNA;
	}

	public function isPenilai()
	{
		return $this->role == self::ROLE_PENILAI;
	}

	public function isPeserta()
	{
		return $this->role == self::ROLE_PESERTA;
	}


	public function isStatusWait()
	{
		return $this->status_verifikasi == 'wait';
	}


	public function login()
	{
		return auth()->loginUsingId($this->id);
	}


	/**
	 * 	PESERTA
	 * */
	public function pengalamanPeserta()
	{
		return $this->hasMany('App\Models\PengalamanPeserta', 'id_user');
	}

	public function countPengalamanPeserta()
	{
		return \App\Models\PengalamanPeserta::where('id_user', $this->id)->count();
	}

	public function pendidikanPeserta()
	{
		return $this->hasMany('App\Models\PendidikanPeserta', 'id_user');
	}

	public function countPendidikanPeserta()
	{
		return \App\Models\PengalamanPeserta::where('id_user', $this->id)->count();
	}

	public function permohonanSertifikasi()
	{
		return $this->hasMany('App\Models\PermohonanSertifikasi', 'id_user');
	}

	public function countPermohonanSertifikasi()
	{
		return \App\Models\PermohonanSertifikasi::where('id_user', $this->id)->count();
	}

	public function profilPeserta()
	{
		return $this->hasOne('App\Models\ProfilPeserta', 'id_user');
	}

	public function createProfilIfNotExists()
	{
		if(empty($this->profilPeserta)) {
			\App\Models\ProfilPeserta::create([
				'id_user'		=> $this->id,
				'nama_peserta'	=> $this->name,
			]);
		}

		return $this;
	}

	public function sendUserVerificationEmail()
	{
		try {
			config([
				'mail.mailers.smtp.host'		=> setting('smtp_host', 'smtp.gmail.com'),
				'mail.mailers.smtp.port'		=> setting('smtp_port', 587),
				'mail.mailers.smtp.username'	=> setting('smtp_username', 'binkonkabcirebon@gmail.com'),
				'mail.mailers.smtp.password'	=> setting('smtp_password', 'usqw irpx ruin ikox'),
				'mail.mailers.smtp.encryption'	=> 'tls',
				'mail.from.address'				=> setting('mail_from_address', 'binkonkabcirebon@gmail.com'),
				'mail.from.name'				=> setting('mail_from_name', 'Binkon PUTR Kabupaten Cirebon'),
			]);
			\Mail::to($this->email)->send(new \App\Mail\UserVerificationMail($this));
		} catch (Exception $e) {
			return $e;
		}
	}
}
