<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class AppInit extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'app:init';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Inisialisasi Aplikasi';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		$this->initializeSettings();
		$this->initializeDirectories();
		$this->createAdminUser();
	}


	private function createAdminUser()
	{
		$this->line('Cek user admin..');

		$user = User::where('role', 'admin')->first();

		if(!$user)
		{
			$this->line('Membuat user admin..');
			User::create([
				'name'		=> 'Admin',
				'email'		=> 'admin@example.com',
				'password'	=> \Hash::make('pass'),
				'role'		=> User::ROLE_ADMIN,
			]);
		}

		$this->info('[v] Berhasil');
	}


	private function initializeSettings()
	{
		$this->line('Inisialisasi pengaturan..');
		\Setting::setValue('website_theme', 'sijakda_v1');
		\Setting::setValue('app_version', '1.0');



		$this->info('[v] Berhasil');
	}

	private function initializeDirectories()
	{
		$this->line('Inisialisasi direktori..');
		\Artisan::call('app:init_directories');



		$this->info('[v] Berhasil');
	}
}
