<?php

namespace App\Console\Commands\AppCommands;

use Illuminate\Console\Command;

class InitDirectories extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'app:init_directories';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Inisialisasi direktori';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		foreach($this->getDirectories() as $directory)
		{
			if(!\File::exists($directory))
			{
				\File::makeDirectory($directory);
			}
		}
	}


	private function getDirectories()
	{
		return [
			storage_path('app/public/temps'),
			storage_path('app/public/temp_files'),
			storage_path('app/public/cms'),
			storage_path('app/public/cms/image_sliders'),
			storage_path('app/public/cms/pages'),
			storage_path('app/public/cms/posts'),
			storage_path('app/public/cms/post_categories'),
			storage_path('app/public/peraturan'),
			storage_path('app/public/uploads'),
			storage_path('app/public/uploads/ijazah'),
			storage_path('app/public/uploads/permohonan'),
			storage_path('app/public/uploads/ktp'),
			storage_path('app/public/uploads/pernyataan_kebenaran_data'),
			storage_path('app/public/uploads/pas_foto'),
			storage_path('app/public/sbu'),
			storage_path('app/public/realisasi'),
			storage_path('app/public/pengalaman_kerja'),
		];
	}
}
