<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SetStatusTenagaAhliTerampil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:set_tenaga_ahli_terampil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set status tenaga ahli terampil';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App\Models\TenagaAhli::refreshStatus();
    }
}
