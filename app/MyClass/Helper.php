<?php 

namespace App\MyClass;

use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class Helper
{

	public static function fixContent($content)
	{
		$result = str_replace('../../storage', url('storage'), $content);
		$result = str_replace('../storage', url('storage'), $result);
		$result = str_replace('../'.url('/'), url('/'), $result);
		return $result;
	}


	public static function generateSiteMap()
	{
		$sitemap = SitemapGenerator::create(url('/'))->getSitemap();

		// Post
		foreach(\App\Models\Post::publishedPosts() as $post)
		{
			$url = Url::create($post->fullSlug())
					  ->setLastModificationDate($post->modifiedAt())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.5);

			$sitemap->add($url);
		}

		// Page
		foreach(\App\Models\Page::publishedPages() as $page)
		{
			$url = Url::create($page->fullSlug())
					  ->setLastModificationDate($page->modifiedAt())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.7);

			$sitemap->add($url);
		}

		// Static Page
		$staticPages = [
			'kecelakaan',
			'peraturan',
			'penyedia-jasa',
			'paket-pekerjaan',
			'posts',
			'pelatihan-tenaga-ahli',
			'pelatihan-tenaga-terampil',
			'rantai-pasok-material',
			'rantai-pasok-peralatan',
			'asosiasi',
			'daftar'
		];
		foreach($staticPages as $slug)
		{
			$url = Url::create($slug)
					  ->setLastModificationDate(now())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.7);

			$sitemap->add($url);
		}

		// Penyedia Jasa
		foreach(\App\Models\Perusahaan::all() as $perusahaan) {
			$url = Url::create('penyedia-jasa/'.$perusahaan->id.'/detail')
					  ->setLastModificationDate(now())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.5);

			$sitemap->add($url);
		}

		// Asosiasi
		foreach(\App\Models\Asosiasi::all() as $asosiasi) {
			$url = Url::create('asosiasi/'.$asosiasi->slug)
					  ->setLastModificationDate(now())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.5);

			$sitemap->add($url);
		}

		// Rantai Pasok
		foreach(\App\Models\RantaiPasok::all() as $rantaiPasok) {
			$url = Url::create('rantai-pasok/'.$rantaiPasok->id)
					  ->setLastModificationDate(now())
					  ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
					  ->setPriority(0.5);

			$sitemap->add($url);
		}

		return $sitemap->writeToFile(public_path('sitemap.xml'));
	}


	public static function getCustomPagesAvailable() : array
	{
		try {
			$path = resource_path('views/themes/' . setting('website_theme') . '/pages');
			$result = [];
			foreach(\File::files($path) as $file)
			{
				if($file->getExtension() == 'php') {
					$filename = $file->getFilename();
					if(\Str::endsWith($filename, '.blade.php')) {
						$aliasname = \Str::replaceLast('.blade.php', '', $filename);
						$aliasname = str_replace('-', ' ', $aliasname);
						$aliasname = str_replace('_', ' ', $aliasname);
						$aliasname = \Str::title($aliasname);
						$result[] = (object) [
							'filename'	=> \Str::replaceLast('.blade.php', '', $filename),
							'filename_with_ext'	=> $filename,
							'aliasname'	=> $aliasname
						];
					}
				}
			}

			return $result;
		} catch (\Exception $e) {
			return [];
		}
	}


	public static function temps($path = '')
	{
		return storage_path('app/public/temp_files/'.$path);
	}
}