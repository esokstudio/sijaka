<?php 

	/**
	 * URL untuk tampilan depan
	 * Link akan menyesuaikan dengan tema website yang aktif
	 * @param string $target
	 * @return string
	 * */
	function themeUrl($target = '')
	{
		$theme = \Setting::getValue('website_theme');
		return url('themes/'.$theme.'/'.$target);
	}


	/**
	 * Ambil data menu
	 * Sudah terurut berdasarkan posisi
	 * @return array of \App\Models\NavMenu
	 * */
	function getMenus()
	{
		$menus = \App\Models\NavMenu::orderBy('position', 'asc')
									->with([ 'submenus' ])
									->get();

		return $menus;
	}


	/**
	 * Ambil data postingan yang di publish
	 * @param int $page nomor halaman
	 * @return array of \App\Models\Post
	 * */
	function getPublishedPosts($page = 0)
	{
		$posts = \App\Models\Post::orderBy('created_at', 'desc')
								 ->where('is_published', 'yes');

		if($page > 0) {
			$limit = \Setting::getValue('posts_per_page', 5);

			if($page > 1) $posts = $posts->skip(($page - 1) * $limit);

			$posts = $posts->take($limit);
		}

		$posts = $posts->get();

		return $posts;
	}


	/**
	 * Sama dengan function getPublishedPosts()
	 * @param int $page nomor halaman
	 * @return array of \App\Models\Post
	 * */
	function getPosts($page = 1)
	{
		return getPublishedPosts($page);
	}



	/**
	 * Ambil data link group
	 * @param int|null $limit jumlah grup link yang diambil terurut dari paling awal
	 * @return array of \App\Models\LinkGroup
	 * */
	function getLinkGroups($limit = null)
	{
		$linkGroups = \App\Models\LinkGroup::orderBy('position', 'asc')
										   ->with([ 'links' ]);
		if(!empty($limit)) {
			$linkGroups = $linkGroups->take($limit);
		}

		$linkGroups = $linkGroups->get();

		return $linkGroups;
	}


	/**
	 * Ambil data slide foto
	 * @param int|null $limit jumlah slide foto yang di ambil
	 * @return array of \App\Models\ImageSlider
	 * */
	function getImageSliders($limit = null)
	{
		$imageSliders = \App\Models\ImageSlider::where('is_published', 'yes');

		if(!empty($limit)) {
			$imageSliders = $imageSliders->take($limit);
		}

		$imageSliders = $imageSliders->get();

		return $imageSliders;
	}


	/**
	 * Ambil endpoint url untuk pesan
	 * */
	function messageEndpointUrl()
	{
		return route('website.message');
	}


	/**
	 * Ambil url developer dari .env
	 * @return string
	 * */
	function developerUrl()
	{
		$url = env('DEVELOPER_URL', 'https://example.com');
		return $url;
	}


	/**
	 * Ambil nama developer dari .env
	 * @return string
	 * */
	function developerName()
	{
		$name = str_replace("_", " ", env("DEVELOPER_NAME", "NAMA DEVELOPER"));
		return $name;
	}


	function pagination($config)
	{
		return \App\MyClass\Pagination::makeAndGenerate($config);
	}


	function setting($name, $defaultValue = null)
	{
		return \App\Models\Setting::getValue($name, $defaultValue);
	}


	function perusahaan()
	{
		return auth()->user()->perusahaan;
	}


	function tmp($instance, $property)
	{
		if($instance->isStatusWait()) {
			return $instance->getDataPerubahan()[$property] ?? null;
		}

		return $instance->$property;
	}


	function tmpText($instance, $property, $oldValueFromRelation = null, $newValueFromRelation = null)
	{
		$result = $instance->$property;
		if(!empty($oldValueFromRelation)) $result = $instance->$oldValueFromRelation();

		if($instance->isStatusWait()) {
			$newValue = tmp($instance, $property);
			if(!empty($newValueFromRelation)) $newValue = $instance->$newValueFromRelation();

			if($newValue != $result) {
				$result .= ' <span class="text-primary">[' . $newValue . ']</span>';
			}
		}

		return $result;
	}