<?php

namespace App\MyClass;

class SijakdaHelper
{

	public static function permintaanPerubahanCount() : int
	{
		$count = 0;
		$count += \App\Models\Perusahaan::countPerusahaanStatusWait();
		$count += \App\Models\KualifikasiPerusahaan::countKualifikasiStatusWait();
		$count += \App\Models\AktePendirianPerusahaan::countAktePendirianStatusWait();
		$count += \App\Models\PengesahanPerusahaan::countPengesahanStatusWait();
		$count += \App\Models\AktePerubahanPerusahaan::countAktePerubahanStatusWait();
		$count += \App\Models\PengurusPerusahaan::countPengurusStatusWait();
		$count += \App\Models\KeuanganPerusahaan::countKeuanganStatusWait();
		$count += \App\Models\TenagaKerjaPerusahaan::countTenagaKerjaStatusWait();
		$count += \App\Models\Pengalaman::countPengalamanStatusWait();

		return $count;
	}


	public static function setTahunAnggaran()
	{
		$paketPekerjaan = \App\Models\PaketPekerjaan::all();
        foreach($paketPekerjaan as $p) {
            $p->update([
                'tahun_anggaran'    => date('Y', strtotime($p->tanggal_kontrak))
            ]);
        }

        $penilaian = \App\Models\Penilaian::with([ 'paketPekerjaan' ])
                                          ->has('paketPekerjaan')
                                          ->get();
        foreach($penilaian as $p) {
            $p->update([
                'tahun_anggaran'    => $p->paketPekerjaan->tahun_anggaran
            ]);
        }

        $permohonan = \App\Models\PermohonanSertifikasi::where('tahun_anggaran', null)->get();
        foreach($permohonan as $p) {
            $p->update([
                'tahun_anggaran'    => $p->created_at->format('Y')
            ]);
        }
	}
}