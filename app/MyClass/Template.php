<?php 

namespace App\MyClass;

class Template
{

	public static function required()
	{
		return '<span class="text-danger"> * </span>';
	}

	public static function requiredBanner()
	{
		$req = self::required();
		return "<div class='alert alert-info border-0'>
					Kolom bertanda {$req} wajib diisi.
				</div>";
	}

	public static function infoBanner($message = '')
	{
		return "<div class='alert alert-info border-0'>
					{$message}
				</div>";
	}


	public static function titleBanner($title = '')
	{
		return '<div class="header-with-border">
					<h4> '.$title.' </h4>
				</div>';
	}
}