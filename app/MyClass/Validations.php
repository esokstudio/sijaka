<?php 

namespace App\MyClass;

class Validations
{

	public static function validateUser($request, $idException = null)
	{
		$validation = [
			'email'		=> 'required|unique:users,email'
		];

		if(!empty($idException)) $validation['email'] .= ','.$idException;

		$request->validate($validation, [
			'email.unique'	=> 'Email tidak tersedia. Harap ganti.'
		]);
	}
}