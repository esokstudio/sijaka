<?php

namespace App\Http\Middleware;

use Closure;

class NotForPengguna
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!auth()->guest()) {
			if(!auth()->user()->isPengguna()) {
				return $next($request);
			}
		}

		abort(403);
	}
}
