<?php

namespace App\Http\Middleware;

use Closure;

class NotForPenilai
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->guest()) {
            if(!auth()->user()->isPenilai()) {
                return $next($request);
            }
        }

        abort(403);
    }
}
