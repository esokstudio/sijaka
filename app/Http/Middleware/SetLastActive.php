<?php

namespace App\Http\Middleware;

use Closure;

class SetLastActive
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!auth()->guest()) {
			auth()->user()->update([
				'last_active_at'	=> now()
			]);
		}

		return $next($request);
	}
}
