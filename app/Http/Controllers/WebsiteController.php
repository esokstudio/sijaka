<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Page;
use App\Models\ViewStatistic;
use App\Models\Asosiasi;
use App\Models\PermohonanSertifikasi;
use App\Models\PengalamanPeserta;
use App\Models\PendidikanPeserta;
use App\Models\Perusahaan;
use App\Models\RantaiPasok;
use App\Models\Peraturan;
use App\User;
use DB;

class WebsiteController extends Controller
{


	public function __construct()
	{
		$theme = \Setting::getValue('website_theme', 'default');
		$this->themePath = 'themes.'.$theme;
	}


	public function index()
	{
		ViewStatistic::writeStatistic('other');
		return view($this->themePath.'.index');
	}


	public function blog()
	{
		ViewStatistic::writeStatistic('other');
		return view($this->themePath.'.blog');
	}


	public function single($slug)
	{
		$post = Post::getBySlug($slug);

		if($post) 
		{
			if($post->isPublished())
			{
				ViewStatistic::writeStatistic('post', $post->id);
				return view($this->themePath.'.single', [
					'post'	=> $post,
				]);
			}
		}
		abort(404);
	}


	public function page($slug)
	{
		$page = Page::getBySlug($slug);

		if($page) 
		{
			if($page->isPublished())
			{
				ViewStatistic::writeStatistic('page', $page->id);
				if($page->isCustomPage()) 
				{
					return view($this->themePath.'.pages.'.$page->custom_page_file, [
						'page'	=> $page
					]);
				}
				else
				{
					return view($this->themePath.'.page', [
						'page'	=> $page
					]);
				}
			}
		}
		abort(404);
	}


	public function message(Request $request)
	{
		$request->validate([
			'full_name'	=> 'required',
			'email'		=> 'required|email',
			'subject'	=> 'required',
			'message'	=> 'required',
		], [
			'full_name.required'	=> 'Nama lengkap wajib diisi',
			'email.required'		=> 'Email wajib diisi',
			'email.email'			=> 'Masukan email yang valid',
			'subject.required'		=> 'Subjek wajib diisi',
			'message.required'		=> 'Isi pesan wajib diisi',
		]);

		DB::beginTransaction();

		try {
			Message::createMessage($request->all());
			DB::commit();

			if($request->ajax()) {
				return \Res::success();
			} else {
				return redirect()->back();
			}
		} catch (\Exception $e) {
			if($request->ajax()) {
				return \Res::error($e);
			} else {
				return redirect()->back();
			}
		}
	}


	// Custom


	public function daftar()
	{
		return view('daftar');
	}


	public function daftarSave(Request $request)
	{
		$request->validate([
			'email'		=> 'required|unique:users,email',
			'password'	=> 'required|min:8',
			'confirm_password'	=> 'required|same:password',
		], [
			'email.unique'	=> 'Email tidak tersedia. Harap ganti.',
			'password.min'	=> 'Minimal 8 karakter',
			'confirm_password.same'	=> 'Konfirmasi password harus sama dengan password',
		]);

		DB::beginTransaction();

		try {
			$user = \App\User::create([
				'name'		=> $request->name,
				'email'		=> $request->email,
				'password'	=> \Hash::make($request->password),
				'role'		=> \App\User::ROLE_PERUSAHAAN,
				'status_verifikasi'	=> 'wait',
				'id_perusahaan'	=> $request->id_perusahaan,
			]);
			DB::commit();

			return \Res::success([
				'message'	=> 'Harap menunggu verifikasi dari admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function penyediaJasa()
	{
		return view($this->themePath.'.penyedia_jasa');
	}


	public function penyediaJasaDetail(Perusahaan $perusahaan)
	{
		return view($this->themePath.'.penyedia_jasa_detail', [
			'perusahaan'	=> $perusahaan,
		]);
	}


	public function paketPekerjaan()
	{
		return view($this->themePath.'.paket_pekerjaan');
	}


	public function asosiasi()
	{
		return view($this->themePath.'.asosiasi');
	}


	public function pelatihanTenagaTerampil()
	{
		return view($this->themePath.'.pelatihan_tenaga_terampil');
	}


	public function pelatihanTenagaAhli()
	{
		return view($this->themePath.'.pelatihan_tenaga_ahli');
	}


	public function peraturan()
	{
		return view($this->themePath.'.peraturan');
	}

	public function peraturanDetail(Peraturan $peraturan)
	{
		return view($this->themePath.'.peraturan_detail', [
			'peraturan' => $peraturan,
		]);
	}


	public function kecelakaan()
	{
		return view($this->themePath.'.kecelakaan');
	}


	public function asosiasiDetail($asosiasi)
	{
		$asosiasi = Asosiasi::where('slug', $asosiasi)->first();
		if(!$asosiasi) abort(404);
		return view($this->themePath.'.asosiasi_detail', [
			'asosiasi'	=> $asosiasi
		]);
	}



	/**
	 * 	PESERTA
	 * 
	 * */
	public function daftarAkunPeserta()
	{
		if(!auth()->guest()) {
			if(auth()->user()->isPeserta()) {
				return redirect()->route('website.permohonan_sertifikasi');
			} else {
				return redirect()->route('dashboard');
			}
		}

		return view($this->themePath.'.peserta.daftar_akun_peserta');
	}

	public function daftarAkunPesertaSave(Request $request)
	{
		$request->validate([
			'email'		=> 'required|unique:users,email',
			'password'	=> 'required|min:8',
			'confirm_password'	=> 'required:same:password'
		], [
			'email.unique'	=> 'Email tidak tersedia. Harap ganti.',
			'password.min'	=> 'Minimal 8 karakter',
			'confirm_password.same'	=> 'Konfirmasi password harus sama dengan password'
		]);

		DB::beginTransaction();

		try {
			$user = User::create([
				'name'		=> $request->name,
				'email'		=> $request->email,
				'password'	=> \Hash::make($request->password),
				'role'		=> User::ROLE_PESERTA,
				'status'	=> 'Tidak Aktif',
				'verification_code' => \Str::random(30),
			]);
			DB::commit();
			$user->sendUserVerificationEmail();

			return \Res::success([
				'message'	=> 'Email verifikasi akun telah dikirim'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function loginAkunPeserta()
	{
		if(!auth()->guest()) {
			if(auth()->user()->isPeserta()) {
				return redirect()->route('website.permohonan_sertifikasi');
			} else {
				return redirect()->route('dashboard');
			}
		}

		return view($this->themePath.'.peserta.login_akun_peserta');
	}

	public function loginAkunPesertaSave(Request $request)
	{
		DB::beginTransaction();

		try {
			$user = User::getByEmail($request->email);
			if(!$user) {
				return \Res::invalid([
					'message'	=> 'Email/password salah'
				]);
			}

			if($user->comparePassword($request->password)) {
				if($user->status == 'Aktif') {
					$user->login();
					DB::commit();

					return \Res::success();
				} else {
					return \Res::invalid([
						'message'	=> 'Status akun belum aktif. Harap cek email.'
					]);
				}
			} else {
				return \Res::invalid([
					'message'	=> 'Email/password salah'
				]);
			}
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function dashboardPeserta()
	{
		if(auth()->guest()) {
			return redirect()->route('website.login_akun_peserta');
		}

		return view($this->themePath.'.peserta.dashboard_peserta');
	}


	# PENGALAMAN PESERTA
	public function pengalamanPeserta(Request $request)
	{
		if(auth()->guest()) {
			return redirect()->route('website.login_akun_peserta');
		}

		if($request->ajax()) {
			return PengalamanPeserta::dt();
		}

		return view($this->themePath.'.peserta.pengalaman_peserta');
	}

	public function pengalamanPesertaCreate()
	{
		return view($this->themePath.'.peserta.pengalaman_peserta_create');
	}

	public function pengalamanPesertaStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_user'] = auth()->user()->id;
			$pengalaman = PengalamanPeserta::createPengalaman($data);
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	# PENDIDIKAN PESERTA
	public function pendidikanPeserta(Request $request)
	{
		if(auth()->guest()) {
			return redirect()->route('website.login_akun_peserta');
		}

		if($request->ajax()) {
			return PendidikanPeserta::dt();
		}

		return view($this->themePath.'.peserta.pendidikan_peserta');
	}

	public function pendidikanPesertaCreate()
	{
		return view($this->themePath.'.peserta.pendidikan_peserta_create');
	}

	public function pendidikanPesertaStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_user'] = auth()->user()->id;
			$pendidikan = PendidikanPeserta::createPendidikan($data);
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	# PROFIL PESERTA
	public function profilPeserta(Request $request)
	{
		if(auth()->guest()) {
			return redirect()->route('website.login_akun_peserta');
		}

		auth()->user()->createProfilIfNotExists();
		auth()->user()->load('profilPeserta');
		return view($this->themePath.'.peserta.profil_peserta', [
			'profil'	=> auth()->user()->profilPeserta,
		]);
	}

	public function profilPesertaSave(Request $request)
	{
		DB::beginTransaction();

		try {
			auth()->user()->createProfilIfNotExists();
			$profil = auth()->user()->profilPeserta;
			$profil->update($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	# PERMOHONAN SERTIFIKASI
	public function permohonanSertifikasi(Request $request)
	{
		if(auth()->guest()) {
			return redirect()->route('website.login_akun_peserta');
		}

		if($request->ajax()) {
			return PermohonanSertifikasi::dt();
		}

		return view($this->themePath.'.peserta.permohonan_sertifikasi');
	}

	public function permohonanSertifikasiCreate()
	{
		return view($this->themePath.'.peserta.permohonan_sertifikasi_create');
	}

	public function permohonanSertifikasiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$permohonan = PermohonanSertifikasi::createPermohonanSertifikasi($request);
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function rantaiPasokMaterial()
	{
		return view($this->themePath.'.rantai_pasok_material');
	}

	public function rantaiPasokPeralatan()
	{
		return view($this->themePath.'.rantai_pasok_peralatan');
	}

	public function rantaiPasokDetail(RantaiPasok $rantaiPasok)
	{
		return view($this->themePath.'.rantai_pasok_detail', [
			'rantaiPasok' => $rantaiPasok,
		]);
	}

	public function userVerification($code)
	{
		$user = User::where('verification_code', $code)->first();
		$message = '';
		$isSuccess = false;

		if(!empty($user)) {
			if($user->status == "Tidak Aktif") {
				$user->update([
					'status' => 'Aktif'
				]);
				$message = 'Akun berhasil di aktivasi. Sudah dapat digunakan.';
			} else {
				$message = 'Akun kamu telah teraktivasi. Sudah dapat digunakan.';
			}
			$isSuccess = true;
		} else {
			$message = "Kode tidak valid";
    		$isSuccess = false;
		}

		return view($this->themePath.'.peserta.verifikasi_akun', [
			'isSuccess'	=> $isSuccess,
			'message'	=> $message,
		]);
	}
}
