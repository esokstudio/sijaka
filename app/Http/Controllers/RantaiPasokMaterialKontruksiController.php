<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RantaiPasokMaterialKontruksi;
use DB;

class RantaiPasokMaterialKontruksiController extends Controller
{
	public function index(Request $request)
	{
		if($request->ajax()) {
			return RantaiPasokMaterialKontruksi::dataTable($request);
		}
	}


	public function store(Request $request)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokMaterialKontruksi = RantaiPasokMaterialKontruksi::createRantaiPasokMaterialKontruksi($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function get(RantaiPasokMaterialKontruksi $rantaiPasokMaterialKontruksi)
	{
		try {
			return \Res::success([
				'rantaiPasokMaterialKontruksi' => $rantaiPasokMaterialKontruksi,
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}


	public function update(Request $request, RantaiPasokMaterialKontruksi $rantaiPasokMaterialKontruksi)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokMaterialKontruksi->updateRantaiPasokMaterialKontruksi($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function destroy(RantaiPasokMaterialKontruksi $rantaiPasokMaterialKontruksi)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokMaterialKontruksi->deleteRantaiPasokMaterialKontruksi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
