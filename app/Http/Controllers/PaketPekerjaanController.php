<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaketPekerjaan;
use App\Models\RealisasiPaketPekerjaan;
use DB;

class PaketPekerjaanController extends Controller
{

	/**
	*	PAKET PEKERJAAN
	*
	*/	
	public function paketPekerjaanIndex(Request $request)
	{
		if($request->ajax()) {
			return PaketPekerjaan::dataTable($request);
		}

		return view('admin.paket_pekerjaan.index', [
			'title'			=> 'Paket Pekerjaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				]
			]
		]);
	}


	public function paketPekerjaanCreate()
	{
		return view('admin.paket_pekerjaan.create', [
			'title'			=> 'Buat Paket Pekerjaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('paket_pekerjaan.create')
				]
			]
		]);
	}


	public function paketPekerjaanStore(Request $request)
	{
		$request->validate([
			'nomor_kontrak'	=> 'required|unique:paket_pekerjaan,nomor_kontrak'
		], [
			'nomor_kontrak.unique'	=> 'Nomor kontrak tersebut sudah pernah diinput. Harap ganti.',
		]);
		DB::beginTransaction();

		try {
			PaketPekerjaan::createPaketPekerjaan($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function paketPekerjaanDetail(PaketPekerjaan $paketPekerjaan)
	{
		return view('admin.paket_pekerjaan.detail', [
			'title'				=> 'Detail Paket Pekerjaan',
			'paketPekerjaan'	=> $paketPekerjaan,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('paket_pekerjaan.detail', $paketPekerjaan->id)
				]
			]
		]);
	}


	public function paketPekerjaanEdit(PaketPekerjaan $paketPekerjaan)
	{
		return view('admin.paket_pekerjaan.edit', [
			'title'				=> 'Edit Paket Pekerjaan',
			'paketPekerjaan'	=> $paketPekerjaan,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('paket_pekerjaan.edit', $paketPekerjaan->id)
				]
			]
		]);
	}


	public function paketPekerjaanUpdate(Request $request, PaketPekerjaan $paketPekerjaan)
	{
		$request->validate([
			'nomor_kontrak'	=> 'required|unique:paket_pekerjaan,nomor_kontrak,'.$paketPekerjaan->id,
		], [
			'nomor_kontrak.unique'	=> 'Nomor kontrak tersebut sudah pernah diinput. Harap ganti.',
		]);
		DB::beginTransaction();

		try {
			$paketPekerjaan->updatePaketPekerjaan($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function paketPekerjaanDestroy(KategoriPeraturan $kategoriPeraturan)
	{
		DB::beginTransaction();

		try {
			$kategoriPeraturan->deleteKategoriPeraturan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function paketPekerjaanExportToExcel(Request $request)
	{
		try {
			$result = PaketPekerjaan::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}

	public function paketPekerjaanImport(Request $request)
	{
		try {
			$count = PaketPekerjaan::importFromExcel($request);

			return \Res::success([
				'message'	=> 'Berhasil import '. $count . ' data',
			]);
		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 		REALISASI
	 * 
	 * */
	public function realisasiIndex(Request $request, PaketPekerjaan $paketPekerjaan)
	{
		if($request->ajax()) {
			return RealisasiPaketPekerjaan::dt($paketPekerjaan->id);
		}

		return view('admin.realisasi_paket_pekerjaan.index', [
			'title'			=> 'Realisasi Paket Pekerjaan',
			'paketPekerjaan'=> $paketPekerjaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Realisasi',
					'link'	=> route('paket_pekerjaan.realisasi', $paketPekerjaan->id)
				]
			]
		]);
	}

	public function realisasiCreate(PaketPekerjaan $paketPekerjaan)
	{
		return view('admin.realisasi_paket_pekerjaan.create', [
			'title'			=> 'Tambah Realisasi Paket Pekerjaan',
			'paketPekerjaan'=> $paketPekerjaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Realisasi',
					'link'	=> route('paket_pekerjaan.realisasi', $paketPekerjaan->id)
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('paket_pekerjaan.realisasi.create', $paketPekerjaan->id)
				]
			]
		]);
	}


	public function realisasiStore(Request $request, PaketPekerjaan $paketPekerjaan)
	{
		DB::beginTransaction();

		try {
			RealisasiPaketPekerjaan::createRealisasi($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function realisasiEdit(PaketPekerjaan $paketPekerjaan, RealisasiPaketPekerjaan $realisasiPaketPekerjaan)
	{
		return view('admin.realisasi_paket_pekerjaan.edit', [
			'title'			=> 'Edit Realisasi Paket Pekerjaan',
			'paketPekerjaan'=> $paketPekerjaan,
			'realisasi'		=> $realisasiPaketPekerjaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Paket Pekerjaan',
					'link'	=> route('paket_pekerjaan')
				],
				[
					'title'	=> 'Realisasi',
					'link'	=> route('paket_pekerjaan.realisasi', $paketPekerjaan->id)
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('paket_pekerjaan.realisasi.edit', [$paketPekerjaan->id, $realisasiPaketPekerjaan->id])
				]
			]
		]);
	}


	public function realisasiUpdate(Request $request, PaketPekerjaan $paketPekerjaan, RealisasiPaketPekerjaan $realisasiPaketPekerjaan)
	{
		DB::beginTransaction();

		try {
			$realisasiPaketPekerjaan->updateRealisasi($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function realisasiDestroy(PaketPekerjaan $paketPekerjaan, RealisasiPaketPekerjaan $realisasiPaketPekerjaan)
	{
		DB::beginTransaction();

		try {
			$realisasiPaketPekerjaan->deleteRealisasi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 		LAPORAN FISIK KEUANGAN
	 * 
	 * */
	public function laporanFisikKeuangan()
	{
		return view('admin.laporan_fisik_keuangan.index', [
			'title'			=> 'Laporan Fisik Keuangan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Laporan Fisik Keuangan',
					'link'	=> route('laporan_fisik_keuangan')
				]
			]
		]);
	}


	public function laporanFisikKeuanganGenerate(Request $request)
	{
		try {
			$result = PaketPekerjaan::generateLaporanFisikDanKeuangan($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}
}
