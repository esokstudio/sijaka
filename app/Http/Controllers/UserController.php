<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

use App\MyClass\Validations;

class UserController extends Controller
{

	/**
	 * 	PENGGUNA
	 * 
	 * */
	public function penggunaIndex(Request $request)
	{
		if($request->ajax()) {
			return User::dt(User::ROLE_PENGGUNA);
		}

		return view('admin.user.pengguna.index', [
			'title'			=> 'Pengguna',
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Pengguna',
					'link'	=> route('user.pengguna')
				]
			]
		]);
	}

	public function penggunaCreate()
	{
		return view('admin.user.pengguna.create', [
			'title'			=> 'Tambah Pengguna',
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Pengguna',
					'link'	=> route('user.pengguna')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('user.pengguna.create')
				]
			]
		]);
	}

	public function penggunaStore(Request $request)
	{
		Validations::validateUser($request);
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['role'] = User::ROLE_PENGGUNA;
			$data['status_verifikasi'] = 'yes';
			$user = User::createUser($data);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penggunaEdit(User $user)
	{
		return view('admin.user.pengguna.edit', [
			'title'			=> 'Edit Pengguna',
			'user'			=> $user,
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Pengguna',
					'link'	=> route('user.pengguna')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('user.pengguna.create', $user->id)
				]
			]
		]);
	}

	public function penggunaUpdate(Request $request, User $user)
	{
		Validations::validateUser($request, $user->id);
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['status_verifikasi'] = 'yes';
			$user->updateUser($data);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penggunaDestroy(User $user)
	{
		DB::beginTransaction();

		try {
			$user->deleteUser();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 	PENYEDIA JASA
	 * 
	 * */
	public function penyediaJasaIndex(Request $request)
	{
		if($request->ajax()) {
			return User::dt(User::ROLE_PERUSAHAAN);
		}

		return view('admin.user.penyedia_jasa.index', [
			'title'			=> 'Penyedia Jasa',
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Penyedia Jasa',
					'link'	=> route('user.penyedia_jasa')
				]
			]
		]);
	}

	public function penyediaJasaApprove(User $user)
	{
		DB::beginTransaction();

		try {
			$user->approveVerification();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penyediaJasaReject(User $user)
	{
		DB::beginTransaction();

		try {
			$user->rejectVerification();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penyediaJasaDestroy(User $user)
	{
		DB::beginTransaction();

		try {
			$user->deleteUser();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 	PENILAI
	 * 
	 * */
	public function penilaiIndex(Request $request)
	{
		if($request->ajax()) {
			return User::dt(User::ROLE_PENILAI);
		}

		return view('admin.user.penilai.index', [
			'title'			=> 'Penilai Paket Pekerjaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Penilai Paket Pekerjaan',
					'link'	=> route('user.penilai')
				]
			]
		]);
	}

	public function penilaiCreate()
	{
		return view('admin.user.penilai.create', [
			'title'			=> 'Tambah Penilai Paket Pekerjaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Penilai Paket Pekerjaan',
					'link'	=> route('user.penilai')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('user.penilai.create')
				]
			]
		]);
	}

	public function penilaiStore(Request $request)
	{
		Validations::validateUser($request);
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['role'] = User::ROLE_PENILAI;
			$data['status_verifikasi'] = 'yes';
			$user = User::createUser($data);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penilaiEdit(User $user)
	{
		return view('admin.user.penilai.edit', [
			'title'			=> 'Edit Penilai Paket Pekerjaan',
			'user'			=> $user,
			'breadcrumbs'	=> [
				[
					'title'	=> 'User',
					'link'	=> 'javascript:void(0);',
				],
				[
					'title'	=> 'Penilai Paket Pekerjaan',
					'link'	=> route('user.penilai')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('user.penilai.create', $user->id)
				]
			]
		]);
	}

	public function penilaiUpdate(Request $request, User $user)
	{
		Validations::validateUser($request, $user->id);
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['status_verifikasi'] = 'yes';
			$user->updateUser($data);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function penilaiDestroy(User $user)
	{
		DB::beginTransaction();

		try {
			$user->deleteUser();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
