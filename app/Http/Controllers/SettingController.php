<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use DB;

class SettingController extends Controller
{
	
	public function apiKey()
	{
		return view('admin.setting.api', [
			'title'			=> 'Pengaturan API Key',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'API Key',
					'link'	=> route('setting.api_key'),
				]
			]
		]);
	}


	public function generateApiKey(Request $request)
	{
		DB::beginTransaction();

		try {
			Setting::setValue('api_key', \Str::random(16));
			DB::commit();

			return redirect()->route('setting.api_key');
		} catch (\Exception $e) {
			DB::rollback();

			abort(500);
		}
	}


	public function email()
	{
		return view('admin.setting.email', [
			'title'			=> 'Pengaturan Email Notifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Email Notifikasi',
					'link'	=> route('setting.email'),
				]
			]
		]);
	}


	public function saveEmail(Request $request)
	{
		DB::beginTransaction();

		try {
			Setting::setValue('smtp_host', $request->smtp_host);
			Setting::setValue('smtp_port', $request->smtp_port);
			Setting::setValue('smtp_username', $request->smtp_username);
			Setting::setValue('smtp_password', $request->smtp_password);
			Setting::setValue('mail_from_address', $request->mail_from_address);
			Setting::setValue('mail_from_name', $request->mail_from_name);
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			abort(500);
		}
	}
}
