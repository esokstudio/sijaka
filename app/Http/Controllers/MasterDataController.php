<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoriPeraturan;
use App\Models\Peraturan;
use App\Models\TahunAnggaran;
use App\Models\SumberDana;
use App\Models\JenisKeahlian;
use App\Models\TingkatKeahlian;
use App\Models\Klasifikasi;
use App\Models\SubKlasifikasi;
use App\Models\MetodePelatihan;
use App\Models\Kecelakaan;
use App\Models\Pelatihan;
use App\Models\Asosiasi;
use App\Models\Instansi;
use DB;

class MasterDataController extends Controller
{
	/**
	*		KATEGORI PERATURAN
	*
	*/	
	public function kategoriPeraturanIndex(Request $request)
	{
		if($request->ajax()) {
			return KategoriPeraturan::dt();
		}

		return view('admin.kategori_peraturan.index', [
			'title'			=> 'Kategori Peraturan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kategori Peraturan',
					'link'	=> route('kategori_peraturan')
				]
			]
		]);
	}


	public function kategoriPeraturanCreate()
	{
		return view('admin.kategori_peraturan.create', [
			'title'			=> 'Buat Kategori Peraturan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kategori Peraturan',
					'link'	=> route('kategori_peraturan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('kategori_peraturan.create')
				]
			]
		]);
	}


	public function kategoriPeraturanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			KategoriPeraturan::createKategoriPeraturan($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kategoriPeraturanEdit(KategoriPeraturan $kategoriPeraturan)
	{
		return view('admin.kategori_peraturan.edit', [
			'title'				=> 'Edit Kategori Peraturan',
			'kategoriPeraturan'	=> $kategoriPeraturan,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Kategori Peraturan',
					'link'	=> route('kategori_peraturan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('kategori_peraturan.edit', $kategoriPeraturan->id)
				]
			]
		]);
	}


	public function kategoriPeraturanUpdate(Request $request, KategoriPeraturan $kategoriPeraturan)
	{
		DB::beginTransaction();

		try {
			$kategoriPeraturan->updateKategoriPeraturan($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kategoriPeraturanDestroy(KategoriPeraturan $kategoriPeraturan)
	{
		DB::beginTransaction();

		try {
			$kategoriPeraturan->deleteKategoriPeraturan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		PERATURAN
	*
	*/	
	public function peraturanIndex(Request $request)
	{
		if($request->ajax()) {
			return Peraturan::dt();
		}

		return view('admin.peraturan.index', [
			'title'			=> 'Peraturan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Peraturan',
					'link'	=> route('peraturan')
				]
			]
		]);
	}


	public function peraturanCreate()
	{
		return view('admin.peraturan.create', [
			'title'			=> 'Buat Peraturan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Peraturan',
					'link'	=> route('peraturan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('peraturan.create')
				]
			]
		]);
	}


	public function peraturanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Peraturan::createPeraturan($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function peraturanEdit(Peraturan $peraturan)
	{
		return view('admin.peraturan.edit', [
			'title'			=> 'Edit Peraturan',
			'peraturan'		=> $peraturan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Peraturan',
					'link'	=> route('peraturan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('peraturan.edit', $peraturan->id)
				]
			]
		]);
	}


	public function peraturanUpdate(Request $request, Peraturan $peraturan)
	{
		DB::beginTransaction();

		try {
			$peraturan->updatePeraturan($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function peraturanDestroy(Peraturan $peraturan)
	{
		DB::beginTransaction();

		try {
			$peraturan->deletePeraturan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		TAHUN ANGGARAN
	*
	*/	
	public function tahunAnggaranIndex(Request $request)
	{
		if($request->ajax()) {
			return TahunAnggaran::dt();
		}

		return view('admin.tahun_anggaran.index', [
			'title'			=> 'Tahun Anggaran',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tahun Anggaran',
					'link'	=> route('tahun_anggaran')
				]
			]
		]);
	}


	public function tahunAnggaranCreate()
	{
		return view('admin.tahun_anggaran.create', [
			'title'			=> 'Buat Tahun Anggaran',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tahun Anggaran',
					'link'	=> route('tahun_anggaran')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('tahun_anggaran.create')
				]
			]
		]);
	}


	public function tahunAnggaranStore(Request $request)
	{
		DB::beginTransaction();

		try {
			TahunAnggaran::createTahunAnggaran($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tahunAnggaranEdit(TahunAnggaran $tahunAnggaran)
	{
		return view('admin.tahun_anggaran.edit', [
			'title'			=> 'Edit Tahun Anggaran',
			'tahunAnggaran'	=> $tahunAnggaran,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tahun Anggaran',
					'link'	=> route('tahun_anggaran')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('tahun_anggaran.edit', $tahunAnggaran->id)
				]
			]
		]);
	}


	public function tahunAnggaranUpdate(Request $request, TahunAnggaran $tahunAnggaran)
	{
		DB::beginTransaction();

		try {
			$tahunAnggaran->updateTahunAnggaran($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tahunAnggaranDestroy(TahunAnggaran $tahunAnggaran)
	{
		DB::beginTransaction();

		try {
			$tahunAnggaran->deleteTahunAnggaran();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		SUMBER DANA
	*
	*/	
	public function sumberDanaIndex(Request $request)
	{
		if($request->ajax()) {
			return SumberDana::dt();
		}

		return view('admin.sumber_dana.index', [
			'title'			=> 'Sumber Dana',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sumber Dana',
					'link'	=> route('sumber_dana')
				]
			]
		]);
	}


	public function sumberDanaCreate()
	{
		return view('admin.sumber_dana.create', [
			'title'			=> 'Buat Sumber Dana',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sumber Dana',
					'link'	=> route('sumber_dana')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('sumber_dana.create')
				]
			]
		]);
	}


	public function sumberDanaStore(Request $request)
	{
		DB::beginTransaction();

		try {
			SumberDana::createSumberDana($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function sumberDanaEdit(SumberDana $sumberDana)
	{
		return view('admin.sumber_dana.edit', [
			'title'			=> 'Edit Sumber Dana',
			'sumberDana'	=> $sumberDana,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sumber Dana',
					'link'	=> route('sumber_dana')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('sumber_dana.edit', $sumberDana->id)
				]
			]
		]);
	}


	public function sumberDanaUpdate(Request $request, SumberDana $sumberDana)
	{
		DB::beginTransaction();

		try {
			$sumberDana->updateSumberDana($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function sumberDanaDestroy(SumberDana $sumberDana)
	{
		DB::beginTransaction();

		try {
			$sumberDana->deleteSumberDana();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		JENIS KEAHLIAN
	*
	*/	
	public function jenisKeahlianIndex(Request $request)
	{
		if($request->ajax()) {
			return JenisKeahlian::dt();
		}

		return view('admin.jenis_keahlian.index', [
			'title'			=> 'Jenis Keahlian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Jenis Keahlian',
					'link'	=> route('jenis_keahlian')
				]
			]
		]);
	}


	public function jenisKeahlianCreate()
	{
		return view('admin.jenis_keahlian.create', [
			'title'			=> 'Buat Jenis Keahlian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Jenis Keahlian',
					'link'	=> route('jenis_keahlian')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('jenis_keahlian.create')
				]
			]
		]);
	}


	public function jenisKeahlianStore(Request $request)
	{
		DB::beginTransaction();

		try {
			JenisKeahlian::createJenisKeahlian($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function jenisKeahlianEdit(JenisKeahlian $jenisKeahlian)
	{
		return view('admin.jenis_keahlian.edit', [
			'title'			=> 'Edit Jenis Keahlian',
			'jenisKeahlian'	=> $jenisKeahlian,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Jenis Keahlian',
					'link'	=> route('jenis_keahlian')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('jenis_keahlian.edit', $jenisKeahlian->id)
				]
			]
		]);
	}


	public function jenisKeahlianUpdate(Request $request, JenisKeahlian $jenisKeahlian)
	{
		DB::beginTransaction();

		try {
			$jenisKeahlian->updateJenisKeahlian($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function jenisKeahlianDestroy(JenisKeahlian $jenisKeahlian)
	{
		DB::beginTransaction();

		try {
			$jenisKeahlian->deleteJenisKeahlian();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function jenisKeahlianGetTingkatKeahlian(JenisKeahlian $jenisKeahlian)
	{
		try {
			$tingkatKeahlian = $jenisKeahlian->tingkatKeahlian;

			return \Res::success([
				'tingkatKeahlian'	=> $tingkatKeahlian
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}



	/**
	*		TINGKAT KEAHLIAN
	*
	*/	
	public function tingkatKeahlianIndex(Request $request)
	{
		if($request->ajax()) {
			return TingkatKeahlian::dt();
		}

		return view('admin.tingkat_keahlian.index', [
			'title'			=> 'Tingkat Keahlian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tingkat Keahlian',
					'link'	=> route('tingkat_keahlian')
				]
			]
		]);
	}


	public function tingkatKeahlianCreate()
	{
		return view('admin.tingkat_keahlian.create', [
			'title'			=> 'Buat Tingkat Keahlian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tingkat Keahlian',
					'link'	=> route('tingkat_keahlian')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('tingkat_keahlian.create')
				]
			]
		]);
	}


	public function tingkatKeahlianStore(Request $request)
	{
		DB::beginTransaction();

		try {
			TingkatKeahlian::createTingkatKeahlian($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tingkatKeahlianEdit(TingkatKeahlian $tingkatKeahlian)
	{
		return view('admin.tingkat_keahlian.edit', [
			'title'				=> 'Edit Tingkat Keahlian',
			'tingkatKeahlian'	=> $tingkatKeahlian,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Tingkat Keahlian',
					'link'	=> route('tingkat_keahlian')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('tingkat_keahlian.edit', $tingkatKeahlian->id)
				]
			]
		]);
	}


	public function tingkatKeahlianUpdate(Request $request, TingkatKeahlian $tingkatKeahlian)
	{
		DB::beginTransaction();

		try {
			$tingkatKeahlian->updateTingkatKeahlian($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tingkatKeahlianDestroy(TingkatKeahlian $tingkatKeahlian)
	{
		DB::beginTransaction();

		try {
			$tingkatKeahlian->deleteTingkatKeahlian();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}




	/**
	*		KLASIFIKASI
	*
	*/	
	public function klasifikasiIndex(Request $request)
	{
		if($request->ajax()) {
			return Klasifikasi::dt();
		}

		return view('admin.klasifikasi.index', [
			'title'			=> 'Klasifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Klasifikasi',
					'link'	=> route('klasifikasi')
				]
			]
		]);
	}


	public function klasifikasiCreate()
	{
		return view('admin.klasifikasi.create', [
			'title'			=> 'Buat Klasifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Klasifikasi',
					'link'	=> route('klasifikasi')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('klasifikasi.create')
				]
			]
		]);
	}


	public function klasifikasiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Klasifikasi::createKlasifikasi($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function klasifikasiEdit(Klasifikasi $klasifikasi)
	{
		return view('admin.klasifikasi.edit', [
			'title'			=> 'Edit Klasifikasi',
			'klasifikasi'	=> $klasifikasi,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Klasifikasi',
					'link'	=> route('klasifikasi')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('klasifikasi.edit', $klasifikasi->id)
				]
			]
		]);
	}


	public function klasifikasiUpdate(Request $request, Klasifikasi $klasifikasi)
	{
		DB::beginTransaction();

		try {
			$klasifikasi->updateKlasifikasi($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function klasifikasiDestroy(Klasifikasi $klasifikasi)
	{
		DB::beginTransaction();

		try {
			$klasifikasi->deleteKlasifikasi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function klasifikasiGetSubKlasifikasi(Klasifikasi $klasifikasi)
	{
		try {
			$subKlasifikasi = $klasifikasi->subKlasifikasi;

			return \Res::success([
				'subKlasifikasi'	=> $subKlasifikasi
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}



	/**
	*		SUB KLASIFIKASI
	*
	*/	
	public function subKlasifikasiIndex(Request $request)
	{
		if($request->ajax()) {
			return SubKlasifikasi::dt();
		}

		return view('admin.sub_klasifikasi.index', [
			'title'			=> 'Sub Klasifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sub Klasifikasi',
					'link'	=> route('sub_klasifikasi')
				]
			]
		]);
	}


	public function subKlasifikasiCreate()
	{
		return view('admin.sub_klasifikasi.create', [
			'title'			=> 'Buat Sub Klasifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sub Klasifikasi',
					'link'	=> route('sub_klasifikasi')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('sub_klasifikasi.create')
				]
			]
		]);
	}


	public function subKlasifikasiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			SubKlasifikasi::createSubKlasifikasi($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function subKlasifikasiEdit(SubKlasifikasi $subKlasifikasi)
	{
		return view('admin.sub_klasifikasi.edit', [
			'title'			=> 'Edit Sub Klasifikasi',
			'subKlasifikasi'=> $subKlasifikasi,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Sub Klasifikasi',
					'link'	=> route('sub_klasifikasi')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('sub_klasifikasi.edit', $subKlasifikasi->id)
				]
			]
		]);
	}


	public function subKlasifikasiUpdate(Request $request, SubKlasifikasi $subKlasifikasi)
	{
		DB::beginTransaction();

		try {
			$subKlasifikasi->updateSubKlasifikasi($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function subKlasifikasiDestroy(SubKlasifikasi $subKlasifikasi)
	{
		DB::beginTransaction();

		try {
			$subKlasifikasi->deleteSubKlasifikasi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		METODE PELATIHAN
	*
	*/	
	public function metodePelatihanIndex(Request $request)
	{
		if($request->ajax()) {
			return MetodePelatihan::dt();
		}

		return view('admin.metode_pelatihan.index', [
			'title'			=> 'Metode Pelatihan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Metode Pelatihan',
					'link'	=> route('metode_pelatihan')
				]
			]
		]);
	}


	public function metodePelatihanCreate()
	{
		return view('admin.metode_pelatihan.create', [
			'title'			=> 'Buat Metode Pelatihan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Metode Pelatihan',
					'link'	=> route('metode_pelatihan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('metode_pelatihan.create')
				]
			]
		]);
	}


	public function metodePelatihanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			MetodePelatihan::createMetodePelatihan($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function metodePelatihanEdit(MetodePelatihan $metodePelatihan)
	{
		return view('admin.metode_pelatihan.edit', [
			'title'			=> 'Edit Metode Pelatihan',
			'metodePelatihan'=> $metodePelatihan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Metode Pelatihan',
					'link'	=> route('metode_pelatihan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('metode_pelatihan.edit', $metodePelatihan->id)
				]
			]
		]);
	}


	public function metodePelatihanUpdate(Request $request, MetodePelatihan $metodePelatihan)
	{
		DB::beginTransaction();

		try {
			$metodePelatihan->updateMetodePelatihan($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function metodePelatihanDestroy(MetodePelatihan $metodePelatihan)
	{
		DB::beginTransaction();

		try {
			$metodePelatihan->deleteMetodePelatihan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		KECELAKAAN
	*
	*/	
	public function kecelakaanIndex(Request $request)
	{
		if($request->ajax()) {
			return Kecelakaan::dt();
		}

		return view('admin.kecelakaan.index', [
			'title'			=> 'Kecelakaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kecelakaan',
					'link'	=> route('kecelakaan')
				]
			]
		]);
	}


	public function kecelakaanCreate()
	{
		return view('admin.kecelakaan.create', [
			'title'			=> 'Buat Kecelakaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kecelakaan',
					'link'	=> route('kecelakaan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('kecelakaan.create')
				]
			]
		]);
	}


	public function kecelakaanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Kecelakaan::createKecelakaan($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kecelakaanEdit(Kecelakaan $kecelakaan)
	{
		return view('admin.kecelakaan.edit', [
			'title'			=> 'Edit Kecelakaan',
			'kecelakaan'	=> $kecelakaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kecelakaan',
					'link'	=> route('kecelakaan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('kecelakaan.edit', $kecelakaan->id)
				]
			]
		]);
	}


	public function kecelakaanUpdate(Request $request, Kecelakaan $kecelakaan)
	{
		DB::beginTransaction();

		try {
			$kecelakaan->updateKecelakaan($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kecelakaanDestroy(Kecelakaan $kecelakaan)
	{
		DB::beginTransaction();

		try {
			$kecelakaan->deleteKecelakaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		PELATIHAN
	*
	*/	
	public function pelatihanIndex(Request $request)
	{
		if($request->ajax()) {
			return Pelatihan::dt();
		}

		return view('admin.pelatihan.index', [
			'title'			=> 'Pelatihan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> route('pelatihan')
				]
			]
		]);
	}


	public function pelatihanCreate()
	{
		return view('admin.pelatihan.create', [
			'title'			=> 'Buat Pelatihan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> route('pelatihan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('pelatihan.create')
				]
			]
		]);
	}


	public function pelatihanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Pelatihan::createPelatihan($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function pelatihanEdit(Pelatihan $pelatihan)
	{
		return view('admin.pelatihan.edit', [
			'title'			=> 'Edit Pelatihan',
			'pelatihan'		=> $pelatihan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> route('pelatihan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('pelatihan.edit', $pelatihan->id)
				]
			]
		]);
	}


	public function pelatihanUpdate(Request $request, Pelatihan $pelatihan)
	{
		DB::beginTransaction();

		try {
			$pelatihan->updatePelatihan($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function pelatihanDestroy(Pelatihan $pelatihan)
	{
		DB::beginTransaction();

		try {
			$pelatihan->deletePelatihan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		ASOSIASI
	*
	*/	
	public function asosiasiIndex(Request $request)
	{
		if($request->ajax()) {
			return Asosiasi::dt();
		}

		return view('admin.asosiasi.index', [
			'title'			=> 'Asosiasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Asosiasi',
					'link'	=> route('asosiasi')
				]
			]
		]);
	}


	public function asosiasiCreate()
	{
		return view('admin.asosiasi.create', [
			'title'			=> 'Buat Asosiasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Asosiasi',
					'link'	=> route('asosiasi')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('asosiasi.create')
				]
			]
		]);
	}


	public function asosiasiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Asosiasi::createAsosiasi($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function asosiasiExportPerusahaan(Asosiasi $asosiasi)
	{
		try {
			$result = $asosiasi->exportListPerusahaanToExcel();
			return response()->download($result->filepath)->deleteFileAfterSend();

		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}


	public function asosiasiListPerusahaan(Asosiasi $asosiasi)
	{
		return view('admin.asosiasi.perusahaan', [
			'title'			=> 'List Perusahaan',
			'asosiasi'		=> $asosiasi,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Asosiasi',
					'link'	=> route('asosiasi')
				],
				[
					'title'	=> 'List Perusahaan',
					'link'	=> route('asosiasi.list_perusahaan', $asosiasi->id)
				]
			]
		]);
	}


	public function asosiasiEdit(Asosiasi $asosiasi)
	{
		return view('admin.asosiasi.edit', [
			'title'			=> 'Edit Asosiasi',
			'asosiasi'		=> $asosiasi,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Asosiasi',
					'link'	=> route('asosiasi')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('asosiasi.edit', $asosiasi->id)
				]
			]
		]);
	}


	public function asosiasiUpdate(Request $request, Asosiasi $asosiasi)
	{
		DB::beginTransaction();

		try {
			$asosiasi->updateAsosiasi($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function asosiasiDestroy(Asosiasi $asosiasi)
	{
		DB::beginTransaction();

		try {
			$asosiasi->deleteAsosiasi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		INSTANSI
	*
	*/	
	public function instansiIndex(Request $request)
	{
		if($request->ajax()) {
			return Instansi::dataTable($request);
		}

		return view('admin.instansi.index', [
			'title'			=> 'Instansi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Instansi',
					'link'	=> route('instansi')
				]
			]
		]);
	}


	public function instansiCreate()
	{
		return view('admin.instansi.create', [
			'title'			=> 'Buat Instansi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Instansi',
					'link'	=> route('instansi')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('instansi.create')
				]
			]
		]);
	}


	public function instansiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Instansi::createInstansi($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function instansiEdit(Instansi $instansi)
	{
		return view('admin.instansi.edit', [
			'title'			=> 'Edit Instansi',
			'instansi'		=> $instansi,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Instansi',
					'link'	=> route('instansi')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('instansi.edit', $instansi->id)
				]
			]
		]);
	}


	public function instansiUpdate(Request $request, Instansi $instansi)
	{
		DB::beginTransaction();

		try {
			$instansi->updateInstansi($request->all());
			DB::commit();

			return \Res::update([
				'req' => $request->all(),
				'instansi' => $instansi,
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function instansiDestroy(Instansi $instansi)
	{
		DB::beginTransaction();

		try {
			$instansi->deleteInstansi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
