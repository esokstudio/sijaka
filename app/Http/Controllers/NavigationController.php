<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NavMenu;
use App\Models\NavSubmenu;
use App\Models\LinkGroup;
use App\Models\Link;
use DB;

class NavigationController extends Controller
{
	/**
	*		NAV MENU
	*
	*/	
	public function navMenuIndex(Request $request)
	{
		if($request->ajax()) {
			return NavMenu::dt();
		}

		return view('admin.nav_menu.index', [
			'title'			=> 'Menu',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				]
			]
		]);
	}


	public function navMenuCreate()
	{
		return view('admin.nav_menu.create', [
			'title'			=> 'Buat Menu',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('nav_menu.create')
				]
			]
		]);
	}


	public function navMenuStore(Request $request)
	{
		DB::beginTransaction();

		try {
			NavMenu::createNavMenu($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navMenuEdit(NavMenu $navMenu)
	{
		return view('admin.nav_menu.edit', [
			'title'			=> 'Edit Menu',
			'navMenu'		=> $navMenu,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('nav_menu.edit', $navMenu->id)
				]
			]
		]);
	}


	public function navMenuUpdate(Request $request, NavMenu $navMenu)
	{
		DB::beginTransaction();

		try {
			$navMenu->updateNavMenu($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navMenuDestroy(NavMenu $navMenu)
	{
		DB::beginTransaction();

		try {
			$navMenu->deleteNavMenu();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navMenuSwitchToUp(NavMenu $navMenu)
	{
		DB::beginTransaction();

		try {
			$navMenu->switchToUp();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navMenuSwitchToDown(NavMenu $navMenu)
	{
		DB::beginTransaction();

		try {
			$navMenu->switchToDown();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		Submenu
	*
	*/


	public function navSubmenuIndex(Request $request, NavMenu $navMenu)
	{
		if($request->ajax()) {
			return NavSubmenu::dt($navMenu->id);
		}

		return view('admin.nav_menu.submenu_index', [
			'title'			=> 'Atur Submenu ('.$navMenu->title.')',
			'navMenu'		=> $navMenu,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				],
				[
					'title'	=> 'Atur Submenu ('.$navMenu->title.')',
					'link'	=> route('nav_submenu', $navMenu->id)
				]
			]
		]);
	}


	public function navSubmenuCreate(NavMenu $navMenu)
	{
		return view('admin.nav_menu.submenu_create', [
			'title'			=> 'Buat Submenu',
			'navMenu'		=> $navMenu,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				],
				[
					'title'	=> 'Atur Submenu ('.$navMenu->title.')',
					'link'	=> route('nav_submenu', $navMenu->id)
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('nav_submenu.create', $navMenu->id)
				]
			]
		]);
	}


	public function navSubmenuStore(Request $request, NavMenu $navMenu)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_nav_menu'] = $navMenu->id;
			NavSubmenu::createNavSubmenu($data);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navSubmenuEdit(NavMenu $navMenu, NavSubmenu $navSubmenu)
	{
		return view('admin.nav_menu.submenu_edit', [
			'title'			=> 'Edit Submenu',
			'navMenu'		=> $navMenu,
			'navSubmenu'	=> $navSubmenu,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Menu',
					'link'	=> route('nav_menu')
				],
				[
					'title'	=> 'Atur Submenu ('.$navMenu->title.')',
					'link'	=> route('nav_submenu', $navMenu->id)
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('nav_submenu.edit', [$navMenu->id, $navSubmenu->id])
				]
			]
		]);
	}


	public function navSubmenuUpdate(Request $request, NavMenu $navMenu, NavSubmenu $navSubmenu)
	{
		DB::beginTransaction();

		try {
			$navSubmenu->updateNavSubmenu($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navSubmenuDestroy(NavMenu $navMenu, NavSubmenu $navSubmenu)
	{
		DB::beginTransaction();

		try {
			$navSubmenu->deleteNavSubmenu();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navSubmenuSwitchToUp(NavMenu $navMenu, NavSubmenu $navSubmenu)
	{
		DB::beginTransaction();

		try {
			$navSubmenu->switchToUp();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function navSubmenuSwitchToDown(NavMenu $navMenu, NavSubmenu $navSubmenu)
	{
		DB::beginTransaction();

		try {
			$navSubmenu->switchToDown();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		LINK GROUP
	*
	*/	
	public function linkGroupIndex(Request $request)
	{
		if($request->ajax()) {
			return LinkGroup::dt();
		}

		return view('admin.link_group.index', [
			'title'			=> 'Grup Link',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				]
			]
		]);
	}


	public function linkGroupCreate()
	{
		return view('admin.link_group.create', [
			'title'			=> 'Buat Grup Link',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('link_group.create')
				]
			]
		]);
	}


	public function linkGroupStore(Request $request)
	{
		DB::beginTransaction();

		try {
			LinkGroup::createLinkGroup($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkGroupEdit(LinkGroup $linkGroup)
	{
		return view('admin.link_group.edit', [
			'title'			=> 'Edit Grup Link',
			'linkGroup'		=> $linkGroup,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('link_group.edit', $linkGroup->id)
				]
			]
		]);
	}


	public function linkGroupUpdate(Request $request, LinkGroup $linkGroup)
	{
		DB::beginTransaction();

		try {
			$linkGroup->updateLinkGroup($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkGroupDestroy(LinkGroup $linkGroup)
	{
		DB::beginTransaction();

		try {
			$linkGroup->deleteLinkGroup();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkGroupSwitchToUp(LinkGroup $linkGroup)
	{
		DB::beginTransaction();

		try {
			$linkGroup->switchToUp();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkGroupSwitchToDown(LinkGroup $linkGroup)
	{
		DB::beginTransaction();

		try {
			$linkGroup->switchToDown();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		LINK
	*
	*/
	public function linkIndex(Request $request, LinkGroup $linkGroup)
	{
		if($request->ajax()) {
			return Link::dt($linkGroup->id);
		}

		return view('admin.link_group.link_index', [
			'title'			=> 'Atur Link ('.$linkGroup->title.')',
			'linkGroup'		=> $linkGroup,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				],
				[
					'title'	=> 'Atur Link ('.$linkGroup->title.')',
					'link'	=> route('link', $linkGroup->id)
				]
			]
		]);
	}


	public function linkCreate(LinkGroup $linkGroup)
	{
		return view('admin.link_group.link_create', [
			'title'			=> 'Buat Link',
			'linkGroup'		=> $linkGroup,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				],
				[
					'title'	=> 'Atur Link ('.$linkGroup->title.')',
					'link'	=> route('link', $linkGroup->id)
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('link.create', $linkGroup->id)
				]
			]
		]);
	}


	public function linkStore(Request $request, LinkGroup $linkGroup)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_link_group'] = $linkGroup->id;
			Link::createLink($data);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkEdit(LinkGroup $linkGroup, Link $link)
	{
		return view('admin.link_group.link_edit', [
			'title'			=> 'Edit Link',
			'linkGroup'		=> $linkGroup,
			'link'			=> $link,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Grup Link',
					'link'	=> route('link_group')
				],
				[
					'title'	=> 'Atur Link ('.$linkGroup->title.')',
					'link'	=> route('link', $linkGroup->id)
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('link.edit', [$linkGroup->id, $link->id])
				]
			]
		]);
	}


	public function linkUpdate(Request $request, LinkGroup $linkGroup, Link $link)
	{
		DB::beginTransaction();

		try {
			$link->updateLink($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkDestroy(LinkGroup $linkGroup, Link $link)
	{
		DB::beginTransaction();

		try {
			$link->deleteLink();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkSwitchToUp(LinkGroup $linkGroup, Link $link)
	{
		DB::beginTransaction();

		try {
			$link->switchToUp();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function linkSwitchToDown(LinkGroup $linkGroup, Link $link)
	{
		DB::beginTransaction();

		try {
			$link->switchToDown();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
