<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Models\Message;
use App\Models\PermohonanSertifikasi;
use App\User;
use DB;

class AdminController extends Controller
{

	/**
	 * 
	 *	DASHBOARD
	 * 
	 * */

	public function dashboard()
	{
		return view('admin.dashboard', [
			'title' => 'Dashboard'
		]);
	}
	


	/**
	 * 
	 * 	MESSAGE
	 * 
	 * */
	public function messageIndex(Request $request)
	{
		if($request->ajax()) {
			return Message::dt();
		}

		return view('admin.message.index', [
			'title'			=> 'Pesan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pesan',
					'link'	=> route('message')
				]
			]
		]);
	}


	public function messageDetail(Message $message)
	{
		$message->setRead();
		
		return view('admin.message.detail', [
			'title'			=> 'Detail Pesan',
			'message'		=> $message,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pesan',
					'link'	=> route('message')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('message.detail', $message->id)
				]
			]
		]);
	}


	public function messageDestroy(Message $message)
	{
		DB::beginTransaction();

		try {
			$message->deleteMessage();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 	SETTING
	 * 
	 * */
	public function websiteSetting(Request $request)
	{
		if($request->ajax()) {
			return $this->saveSetting($request);
		}

		return view('admin.setting.setting_fields', [
			'title'			=> 'Pengaturan Situs',
			'settings'		=> \Setting::initializeWebsiteSettings(),
			'saveUrl'		=> route('setting.website_save'),
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Situs',
					'link'	=> route('setting.website')
				]
			]
		]);
	}


	public function themeSetting(Request $request)
	{
		if($request->ajax()) {
			return $this->saveSetting($request);
		}

		return view('admin.setting.setting_fields', [
			'title'			=> 'Pengaturan Tema',
			'settings'		=> \Setting::initializeThemeSettings(),
			'saveUrl'		=> route('setting.theme_save'),
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tema',
					'link'	=> route('setting.theme')
				]
			]
		]);
	}


	public function profile()
	{
		return view('admin.setting.profile', [
			'title'			=> 'Atur Profil',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Atur Profil',
					'link'	=> route('setting.profile')
				]
			]
		]);
	}


	public function profileSave(Request $request)
	{
		DB::beginTransaction();

		try {
			auth()->user()->update([
				'name'	=> $request->name,
				'email'	=> $request->email,
			]);
			auth()->user()->setAvatar($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function changePassword()
	{
		return view('admin.setting.change_password', [
			'title'			=> 'Ganti Password',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengaturan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Ganti Password',
					'link'	=> route('setting.change_password')
				]
			]
		]);
	}


	public function changePasswordSave(Request $request)
	{
		$request->validate([
			'old_password'	=> 'required',
			'new_password'	=> 'required',
			'confirm_password'	=> 'required|same:new_password',
		], [
			'confirm_password.same'	=> 'Konfirmasi password baru harus sama dengan password baru'
		]);

		DB::beginTransaction();

		try {
			if(auth()->user()->comparePassword($request->old_password)) {
				auth()->user()->changePassword($request->new_password);
				DB::commit();

				return \Res::update();
			} else {
				DB::commit();

				return \Res::invalid([
					'message'	=> 'Password lama salah',
					'errors'	=> [
						'old_password'	=> 'Password lama salah'
					]
				]);
			}
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	private function saveSetting($request)
	{
		DB::beginTransaction();

		try {
			\Setting::setValues($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 	HELPER
	 * 
	 * */
	public function imageUpload(Request $request)
	{
		if(!empty($request->file))
		{
			$file = $request->file('file');
			$filename = date('YmdHis_').$file->getClientOriginalName();
			$path = storage_path('app/public/cms/contents/image');

			$file->move(storage_path('app/public/cms/contents/image'), $filename);

			return \Res::success([
				'location'	=> url('storage/cms/contents/image/'.$filename),
			]);
		}

		return \Res::invalid([
			'message'	=> 'File is empty',
		]);
	}



	/**
	 * 
	 * 	FORGOT PASSWORD
	 * 
	 * */
	public function forgotPassword()
	{
		return view('auth.forgot_password');
	}


	public function forgotPasswordSave(Request $request)
	{
		$request->validate([
			'email'	=> 'required'
		], [
			'email.required'	=> 'Email wajib diisi'
		]);

		DB::beginTransaction();

		try {
			$user = User::getByEmail($request->email);
			if($user) {
				$passwordReset = PasswordReset::createPasswordReset($user->id);
				$passwordReset->sendPasswordResetLinkToUserEmail();
			}
			DB::commit();

			return \Res::success([
				'message'	=> 'Berhasil dikirim'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function permintaanPerubahan()
	{
		$perusahaan = \App\Models\Perusahaan::getPerusahaanStatusWait();
		$kualifikasi = \App\Models\KualifikasiPerusahaan::getKualifikasiStatusWait();
		$aktePendirian = \App\Models\AktePendirianPerusahaan::getAktePendirianStatusWait();
		$pengesahan = \App\Models\PengesahanPerusahaan::getPengesahanStatusWait();
		$aktePerubahan = \App\Models\AktePerubahanPerusahaan::getAktePerubahanStatusWait();
		$pengurus = \App\Models\PengurusPerusahaan::getPengurusStatusWait();
		$keuangan = \App\Models\KeuanganPerusahaan::getKeuanganStatusWait();
		$tenagaKerja = \App\Models\TenagaKerjaPerusahaan::getTenagaKerjaStatusWait();
		$pengalaman = \App\Models\Pengalaman::getPengalamanStatusWait();

		return view('admin.permintaan_perubahan', [
			'title'				=> 'Permintaan Perubahan',
			'perusahaanList'	=> $perusahaan,
			'kualifikasiList'	=> $kualifikasi,
			'aktePendirianList'	=> $aktePendirian,
			'pengesahanList'	=> $pengesahan,
			'aktePerubahanList'	=> $aktePerubahan,
			'pengurusList'		=> $pengurus,
			'keuanganList'		=> $keuangan,
			'tenagaKerjaList'	=> $tenagaKerja,
			'pengalamanList'	=> $pengalaman,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Permintaan Perubahan',
					'link'	=> route('permintaan_perubahan')
				]
			]
		]);
	}


	public function permohonanSertifikasiIndex(Request $request)
	{
		if($request->ajax()) {
			return PermohonanSertifikasi::dt();
		}

		return view('admin.permohonan_sertifikasi.index', [
			'title'				=> 'Permohonan Sertifikasi',
			'breadcrumbs'		=> [
				[
					'title'	=> 'Permohonan Sertifikasi',
					'link'	=> route('permohonan_sertifikasi')
				]
			]
		]);
	}


	public function permohonanSertifikasiDetail(PermohonanSertifikasi $permohonanSertifikasi)
	{
		$permohonanSertifikasi->user->createProfilIfNotExists();
		return view('admin.permohonan_sertifikasi.detail', [
			'title'				=> 'Detail Permohonan Sertifikasi',
			'permohonanSertifikasi'	=> $permohonanSertifikasi,
			'breadcrumbs'		=> [
				[
					'title'	=> 'Permohonan Sertifikasi',
					'link'	=> route('permohonan_sertifikasi')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('permohonan_sertifikasi.detail', $permohonanSertifikasi->id)
				]
			]
		]);
	}

	public function permohonanSertifikasiDestroy(PermohonanSertifikasi $permohonanSertifikasi)
	{
		try {
			\DB::beginTransaction();
			$permohonanSertifikasi->delete();
			\DB::commit();

			return \Res::success([
				'message'	=> 'Berhasil dihapus'
			]);
		} catch (\Exception $e) {
			\DB::rollback();
			return \Res::error($e);
		}
	}

	public function permohonanSertifikasiApprove(Request $request, PermohonanSertifikasi $permohonanSertifikasi)
	{
		try {
			\DB::beginTransaction();
			$permohonanSertifikasi->update([
				'status'		=> 'Disetujui',
				'tindak_lanjut'	=> $request->tindak_lanjut,
			]);
			\DB::commit();

			return \Res::success([
				'message'	=> 'Berhasil disetujui'
			]);
		} catch (\Exception $e) {
			\DB::rollback();
			return \Res::error($e);
		}
	}

	public function permohonanSertifikasiReject(Request $request, PermohonanSertifikasi $permohonanSertifikasi)
	{
		try {
			\DB::beginTransaction();
			$permohonanSertifikasi->update([
				'status'		=> 'Ditolak',
				'tindak_lanjut'	=> $request->tindak_lanjut,
			]);
			\DB::commit();

			return \Res::success([
				'message'	=> 'Berhasil ditolak'
			]);
		} catch (\Exception $e) {
			\DB::rollback();
			return \Res::error($e);
		}
	}

	public function permohonanSertifikasiExport(Request $request)
	{
		try {
			$result = PermohonanSertifikasi::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}

}
