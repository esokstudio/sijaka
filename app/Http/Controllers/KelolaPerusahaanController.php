<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\KualifikasiPerusahaan;
use App\Models\PengurusPerusahaan;
use App\Models\KeuanganPerusahaan;
use App\Models\TenagaKerjaPerusahaan;
use App\Models\AktePerubahanPerusahaan;
use App\Models\Pengalaman;
use DB;

class KelolaPerusahaanController extends Controller
{
	
	public function editPerusahaan()
	{
		return view('penyedia_jasa.edit_perusahaan', [
			'title'			=> 'Edit Perusahaan',
			'perusahaan'	=> perusahaan(),
			'breadcrumbs'	=> [
				[
					'title'	=> 'Edit Perusahaan',
					'link'	=> route('p.edit_perusahaan')
				]
			]
		]);
	}


	public function editPerusahaanSave(Request $request)
	{
		DB::beginTransaction();

		try {
			auth()->user()->perusahaan->updatePerusahaan($request->except('file_sbu'));
			auth()->user()->perusahaan->setFileSbu($request);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 		KUALIFIKASI
	 * 
	 * */
	public function kualifikasiIndex(Request $request)
	{
		if($request->ajax()) {
			return KualifikasiPerusahaan::dt();
		}

		return view('penyedia_jasa.kualifikasi_index', [
			'title'			=> 'Kualifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kualifikasi',
					'link'	=> route('p.kualifikasi')
				]
			]
		]);
	}


	public function kualifikasiCreate()
	{
		return view('penyedia_jasa.kualifikasi_create', [
			'title'			=> 'Tambah Kualifikasi',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kualifikasi',
					'link'	=> route('p.kualifikasi')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.kualifikasi.create')
				]
			]
		]);
	}


	public function kualifikasiStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = auth()->user()->id_perusahaan;
			KualifikasiPerusahaan::createKualifikasiPerusahaan($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kualifikasiDetail(KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		return view('penyedia_jasa.kualifikasi_detail', [
			'title'			=> 'Detail Kualifikasi',
			'kualifikasi'	=> $kualifikasiPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Kualifikasi',
					'link'	=> route('p.kualifikasi')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.kualifikasi.detail', $kualifikasiPerusahaan->id)
				]
			]
		]);
	}


	public function kualifikasiEdit(KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		return view('penyedia_jasa.kualifikasi_edit', [
			'title'			=> 'Edit Kualifikasi',
			'kualifikasi'	=> $kualifikasiPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Kualifikasi',
					'link'	=> route('p.kualifikasi')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.kualifikasi.edit', $kualifikasiPerusahaan->id)
				]
			]
		]);
	}


	public function kualifikasiUpdate(Request $request, KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		DB::beginTransaction();

		try {
			$kualifikasiPerusahaan->updateKualifikasiPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function kualifikasiDestroy(KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		DB::beginTransaction();

		try {
			$kualifikasiPerusahaan->deleteKualifikasiPerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function editAktePendirian()
	{
		$perusahaan = auth()->user()->perusahaan;
		return view('penyedia_jasa.edit_akte_pendirian', [
			'title'			=> 'Edit Akte Pendirian',
			'aktePendirian'	=> $perusahaan->getAktePendirianPerusahaan(),
			'perusahaan'	=> $perusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Edit Akte Pendirian',
					'link'	=> route('p.edit_akte_pendirian', $perusahaan->id)
				]
			]
		]);
	}


	public function editAktePendirianSave(Request $request)
	{
		DB::beginTransaction();

		try {
			$perusahaan = auth()->user()->perusahaan;
			$aktePendirian = $perusahaan->getAktePendirianPerusahaan();
			$aktePendirian->updateAktePendirianPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function editPengesahan()
	{
		return view('penyedia_jasa.edit_pengesahan', [
			'title'			=> 'Edit Pengesahan',
			'pengesahan'	=> auth()->user()->perusahaan->getPengesahanPerusahaan(),
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Edit Pengesahan',
					'link'	=> route('p.edit_pengesahan')
				]
			]
		]);
	}


	public function editPengesahanSave(Request $request)
	{
		DB::beginTransaction();

		try {
			$pengesahan = auth()->user()->perusahaan->getPengesahanPerusahaan();
			$pengesahan->updatePengesahanPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	 * 
	 * 		PENGURUS
	 * 
	 * */
	public function pengurusIndex(Request $request)
	{
		if($request->ajax()) {
			return PengurusPerusahaan::dt();
		}

		return view('penyedia_jasa.pengurus_index', [
			'title'			=> 'Pengurus',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengurus',
					'link'	=> route('p.pengurus')
				]
			]
		]);
	}

	public function pengurusCreate()
	{
		return view('penyedia_jasa.pengurus_create', [
			'title'			=> 'Tambah Pengurus',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengurus',
					'link'	=> route('p.pengurus')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.pengurus.create')
				]
			]
		]);
	}

	public function pengurusStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = auth()->user()->id_perusahaan;
			PengurusPerusahaan::createPengurusPerusahaan($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengurusEdit(PengurusPerusahaan $pengurusPerusahaan)
	{
		return view('penyedia_jasa.pengurus_edit', [
			'title'			=> 'Edit Pengurus',
			'pengurus'		=> $pengurusPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengurus',
					'link'	=> route('p.pengurus')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.pengurus.edit', $pengurusPerusahaan->id)
				]
			]
		]);
	}

	public function pengurusDetail(PengurusPerusahaan $pengurusPerusahaan)
	{
		return view('penyedia_jasa.pengurus_detail', [
			'title'			=> 'Detail Pengurus',
			'pengurus'		=> $pengurusPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengurus',
					'link'	=> route('p.pengurus')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.pengurus.detail', $pengurusPerusahaan->id)
				]
			]
		]);
	}

	public function pengurusUpdate(Request $request, PengurusPerusahaan $pengurusPerusahaan)
	{
		DB::beginTransaction();

		try {
			$pengurusPerusahaan->updatePengurusPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengurusDestroy(PengurusPerusahaan $pengurusPerusahaan)
	{
		DB::beginTransaction();

		try {
			$pengurusPerusahaan->deletePengurusPerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	 * 
	 * 		KEUANGAN
	 * 
	 * */
	public function keuanganIndex(Request $request)
	{
		if($request->ajax()) {
			return KeuanganPerusahaan::dt();
		}

		return view('penyedia_jasa.keuangan_index', [
			'title'			=> 'Keuangan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Keuangan',
					'link'	=> route('p.keuangan')
				]
			]
		]);
	}

	public function keuanganCreate()
	{
		return view('penyedia_jasa.keuangan_create', [
			'title'			=> 'Tambah Keuangan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Keuangan',
					'link'	=> route('p.keuangan')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.keuangan.create')
				]
			]
		]);
	}

	public function keuanganStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = auth()->user()->id_perusahaan;
			KeuanganPerusahaan::createKeuanganPerusahaan($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function keuanganEdit(KeuanganPerusahaan $keuanganPerusahaan)
	{
		return view('penyedia_jasa.keuangan_edit', [
			'title'			=> 'Edit Keuangan',
			'keuangan'		=> $keuanganPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Keuangan',
					'link'	=> route('p.keuangan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.keuangan.edit', $keuanganPerusahaan->id)
				]
			]
		]);
	}

	public function keuanganDetail(KeuanganPerusahaan $keuanganPerusahaan)
	{
		return view('penyedia_jasa.keuangan_detail', [
			'title'			=> 'Detail Keuangan',
			'keuangan'		=> $keuanganPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Keuangan',
					'link'	=> route('p.keuangan')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.keuangan.detail', $keuanganPerusahaan->id)
				]
			]
		]);
	}

	public function keuanganUpdate(Request $request, KeuanganPerusahaan $keuanganPerusahaan)
	{
		DB::beginTransaction();

		try {
			$keuanganPerusahaan->updateKeuanganPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function keuanganDestroy(KeuanganPerusahaan $keuanganPerusahaan)
	{
		DB::beginTransaction();

		try {
			$keuanganPerusahaan->deleteKeuanganPerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 		TENAGA KERJA
	 * 
	 * */
	public function tenagaKerjaIndex(Request $request)
	{
		if($request->ajax()) {
			return TenagaKerjaPerusahaan::dt();
		}

		return view('penyedia_jasa.tenaga_kerja_index', [
			'title'			=> 'Tenaga Kerja',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tenaga Kerja',
					'link'	=> route('p.tenaga_kerja')
				]
			]
		]);
	}

	public function tenagaKerjaCreate()
	{
		return view('penyedia_jasa.tenaga_kerja_create', [
			'title'			=> 'Tambah Tenaga Kerja',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tenaga Kerja',
					'link'	=> route('p.tenaga_kerja')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.tenaga_kerja.create')
				]
			]
		]);
	}

	public function tenagaKerjaStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = auth()->user()->id_perusahaan;
			TenagaKerjaPerusahaan::createTenagaKerjaPerusahaan($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function tenagaKerjaEdit(TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		return view('penyedia_jasa.tenaga_kerja_edit', [
			'title'			=> 'Edit Tenaga Kerja',
			'tenagaKerja'	=> $tenagaKerjaPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tenaga Kerja',
					'link'	=> route('p.tenaga_kerja')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.tenaga_kerja.edit', $tenagaKerjaPerusahaan->id)
				]
			]
		]);
	}

	public function tenagaKerjaDetail(TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		return view('penyedia_jasa.tenaga_kerja_detail', [
			'title'			=> 'Detail Tenaga Kerja',
			'tenagaKerja'	=> $tenagaKerjaPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tenaga Kerja',
					'link'	=> route('p.tenaga_kerja')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.tenaga_kerja.detail', $tenagaKerjaPerusahaan->id)
				]
			]
		]);
	}

	public function tenagaKerjaUpdate(Request $request, TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		DB::beginTransaction();

		try {
			$tenagaKerjaPerusahaan->updateTenagaKerjaPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function tenagaKerjaDestroy(TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		DB::beginTransaction();

		try {
			$tenagaKerjaPerusahaan->deleteTenagaKerjaPerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	 * 
	 * 		AKTE PERUBAHAN
	 * 
	 * */
	public function aktePerubahanIndex(Request $request)
	{
		if($request->ajax()) {
			return AktePerubahanPerusahaan::dt();
		}

		return view('penyedia_jasa.akte_perubahan_index', [
			'title'			=> 'Akte Perubahan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Akte Perubahan',
					'link'	=> route('p.akte_perubahan')
				]
			]
		]);
	}

	public function aktePerubahanCreate()
	{
		return view('penyedia_jasa.akte_perubahan_create', [
			'title'			=> 'Tambah Akte Perubahan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Akte Perubahan',
					'link'	=> route('p.akte_perubahan')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.akte_perubahan.create')
				]
			]
		]);
	}

	public function aktePerubahanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = auth()->user()->id_perusahaan;
			AktePerubahanPerusahaan::createAktePerubahanPerusahaan($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function aktePerubahanDetail(AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		return view('penyedia_jasa.akte_perubahan_detail', [
			'title'			=> 'Detail Akte Perubahan',
			'aktePerubahan'	=> $aktePerubahanPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Akte Perubahan',
					'link'	=> route('p.akte_perubahan')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.akte_perubahan.detail', $aktePerubahanPerusahaan->id)
				]
			]
		]);
	}

	public function aktePerubahanEdit(AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		return view('penyedia_jasa.akte_perubahan_edit', [
			'title'			=> 'Edit Akte Perubahan',
			'aktePerubahan'	=> $aktePerubahanPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Akte Perubahan',
					'link'	=> route('p.akte_perubahan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.akte_perubahan.edit', $aktePerubahanPerusahaan->id)
				]
			]
		]);
	}

	public function aktePerubahanUpdate(Request $request, AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePerubahanPerusahaan->updateAktePerubahanPerusahaan($request->all());
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function aktePerubahanDestroy(AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePerubahanPerusahaan->deleteAktePerubahanPerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 		PENGALAMAN
	 * 
	 * */
	public function pengalamanIndex(Request $request)
	{
		if($request->ajax()) {
			return Pengalaman::dt();
		}

		return view('penyedia_jasa.pengalaman_index', [
			'title'			=> 'Pengalaman',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengalaman',
					'link'	=> route('p.pengalaman')
				]
			]
		]);
	}

	public function pengalamanCreate()
	{
		return view('penyedia_jasa.pengalaman_create', [
			'title'			=> 'Tambah Pengalaman',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengalaman',
					'link'	=> route('p.pengalaman')
				],
				[
					'title'	=> 'Tambah',
					'link'	=> route('p.pengalaman.create')
				]
			]
		]);
	}

	public function pengalamanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$data = $request->all();
			$data['id_perusahaan'] = perusahaan()->id;
			Pengalaman::createPengalaman($data);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengalamanDetail(Pengalaman $pengalaman)
	{
		return view('penyedia_jasa.pengalaman_detail', [
			'title'			=> 'Detail Pengalaman',
			'pengalaman'	=> $pengalaman,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengalaman',
					'link'	=> route('p.pengalaman')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('p.pengalaman.detail', $pengalaman->id)
				]
			]
		]);
	}

	public function pengalamanEdit(Pengalaman $pengalaman)
	{
		return view('penyedia_jasa.pengalaman_edit', [
			'title'			=> 'Edit Pengalaman',
			'pengalaman'	=> $pengalaman,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pengalaman',
					'link'	=> route('p.pengalaman')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('p.pengalaman.edit', $pengalaman->id)
				]
			]
		]);
	}

	public function pengalamanUpdate(Request $request, Pengalaman $pengalaman)
	{
		DB::beginTransaction();

		try {
			$pengalaman->updatePengalaman($request);
			DB::commit();

			return \Res::success([
				'message'	=> 'Menunggu verifikasi admin'
			]);
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengalamanDestroy(Pengalaman $pengalaman)
	{
		DB::beginTransaction();

		try {
			$pengalaman->deletePengalaman();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
