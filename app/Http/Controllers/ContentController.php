<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PostCategory;
use App\Models\ImageSlider;
use App\Models\Tag;
use App\Models\Post;
use App\Models\Page;
use DB;

class ContentController extends Controller
{

	/**
	*		TAG
	*
	*/	
	public function tagIndex(Request $request)
	{
		if($request->ajax()) {
			return Tag::dt();
		}

		return view('admin.tag.index', [
			'title'			=> 'Tag',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tag',
					'link'	=> route('tag')
				]
			]
		]);
	}


	public function tagCreate()
	{
		return view('admin.tag.create', [
			'title'			=> 'Buat Tag',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tag',
					'link'	=> route('tag')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('tag.create')
				]
			]
		]);
	}


	public function tagStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Tag::createTag($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tagEdit(Tag $tag)
	{
		return view('admin.tag.edit', [
			'title'			=> 'Edit Tag',
			'tag'			=> $tag,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Tag',
					'link'	=> route('tag')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('tag.edit', $tag->id)
				]
			]
		]);
	}


	public function tagUpdate(Request $request, Tag $tag)
	{
		DB::beginTransaction();

		try {
			$tag->updateTag($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tagDestroy(Tag $tag)
	{
		DB::beginTransaction();

		try {
			$tag->deleteTag();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		POST CATEGORY
	*
	*/	
	public function postCategoryIndex(Request $request)
	{
		if($request->ajax()) {
			return PostCategory::dt();
		}

		return view('admin.post_category.index', [
			'title'			=> 'Kategori Postingan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kategori Postingan',
					'link'	=> route('post_category')
				]
			]
		]);
	}


	public function postCategoryCreate()
	{
		return view('admin.post_category.create', [
			'title'			=> 'Buat Kategori Postingan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kategori Postingan',
					'link'	=> route('post_category')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('post_category.create')
				]
			]
		]);
	}


	public function postCategoryStore(Request $request)
	{
		DB::beginTransaction();

		try {
			PostCategory::createPostCategory($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function postCategoryEdit(PostCategory $postCategory)
	{
		return view('admin.post_category.edit', [
			'title'			=> 'Edit Kategori Postingan',
			'postCategory'	=> $postCategory,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Kategori Postingan',
					'link'	=> route('post_category')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('post_category.edit', $postCategory->id)
				]
			]
		]);
	}


	public function postCategoryUpdate(Request $request, PostCategory $postCategory)
	{
		DB::beginTransaction();

		try {
			$postCategory->updatePostCategory($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function postCategoryDestroy(PostCategory $postCategory)
	{
		DB::beginTransaction();

		try {
			$postCategory->deletePostCategory();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
	

	/**
	*/

	public function postIndex(Request $request)
	{
		if($request->ajax()) {
			return Post::dt();
		}

		return view('admin.post.index', [
			'title' 		=> 'Postingan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Postingan',
					'link'	=> route('post')
				]
			]
		]);
	}


	public function postCreate()
	{
		return view('admin.post.create', [
			'title' 		=> 'Buat Postingan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Postingan',
					'link'	=> route('post')
				],
				[
					'title'	=> 'Buat Postingan',
					'link'	=> route('post.create')
				]
			]
		]);
	}


	public function postStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Post::createPost($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function postEdit(Post $post)
	{
		return view('admin.post.edit', [
			'title' 		=> 'Edit Postingan',
			'post'			=> $post,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Postingan',
					'link'	=> route('post')
				],
				[
					'title'	=> 'Edit Postingan',
					'link'	=> route('post.edit', $post->id)
				]
			]
		]);
	}


	public function postUpdate(Request $request, Post $post)
	{
		DB::beginTransaction();

		try {
			$post->updatePost($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function postDestroy(Post $post)
	{
		DB::beginTransaction();

		try {
			$post->deletePost();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		PAGE
	*		
	*/

	public function pageIndex(Request $request)
	{
		if($request->ajax()) {
			return Page::dt();
		}

		return view('admin.page.index', [
			'title' 		=> 'Laman',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Laman',
					'link'	=> route('page')
				]
			]
		]);
	}


	public function pageCreate()
	{
		return view('admin.page.create', [
			'title' 		=> 'Buat Laman',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Laman',
					'link'	=> route('page')
				],
				[
					'title'	=> 'Buat Laman',
					'link'	=> route('page.create')
				]
			]
		]);
	}


	public function pageStore(Request $request)
	{
		DB::beginTransaction();

		try {
			Page::createPage($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function pageEdit(Page $page)
	{
		return view('admin.page.edit', [
			'title' 		=> 'Edit Laman',
			'page'			=> $page,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Laman',
					'link'	=> route('page')
				],
				[
					'title'	=> 'Edit Laman',
					'link'	=> route('page.edit', $page->id)
				]
			]
		]);
	}


	public function pageUpdate(Request $request, Page $page)
	{
		DB::beginTransaction();

		try {
			$page->updatePage($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function pageDestroy(Page $page)
	{
		DB::beginTransaction();

		try {
			$page->deletePage();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		IMAGE SLIDER
	*
	*/	
	public function imageSliderIndex(Request $request)
	{
		if($request->ajax()) {
			return ImageSlider::dt();
		}

		return view('admin.image_slider.index', [
			'title'			=> 'Slide Foto',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Slide Foto',
					'link'	=> route('image_slider')
				]
			]
		]);
	}


	public function imageSliderCreate()
	{
		return view('admin.image_slider.create', [
			'title'			=> 'Buat Slide Foto',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Slide Foto',
					'link'	=> route('image_slider')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('image_slider.create')
				]
			]
		]);
	}


	public function imageSliderStore(Request $request)
	{
		DB::beginTransaction();

		try {
			ImageSlider::createImageSlider($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function imageSliderEdit(ImageSlider $imageSlider)
	{
		return view('admin.image_slider.edit', [
			'title'			=> 'Edit Slide Foto',
			'imageSlider'	=> $imageSlider,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Slide Foto',
					'link'	=> route('image_slider')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('image_slider.edit', $imageSlider->id)
				]
			]
		]);
	}


	public function imageSliderUpdate(Request $request, ImageSlider $imageSlider)
	{
		DB::beginTransaction();

		try {
			$imageSlider->updateImageSlider($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function imageSliderDestroy(ImageSlider $imageSlider)
	{
		DB::beginTransaction();

		try {
			$imageSlider->deleteImageSlider();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
