<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penilaian;
use App\Models\SkemaPenilaian;
use App\Models\AspekKinerja;
use App\Models\IndikatorPenilaian;
use DB;

class PenilaianController extends Controller
{
	
	/**
	 * 
	 * 	PENILAIAN
	 * 
	 * */
	public function index(Request $request)
	{
		if($request->ajax()) {
			return Penilaian::dataTable($request);
		}

		return view('admin.penilaian.index', [
			'title'			=> 'Penilaian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Penilaian',
					'link'	=> route('penilaian')
				]
			]
		]);
	}

	public function create(Request $request)
	{
		if(empty($request->id_skema)) return redirect()->route('penilaian');

		$listPoin = [];
		$skema = SkemaPenilaian::findOrFail($request->id_skema);
		$listPoin = $skema->poinPenilaian();

		return view('admin.penilaian.create', [
			'title'			=> 'Penilaian',
			'skema'			=> $skema,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Penilaian',
					'link'	=> route('penilaian')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('penilaian.create')
				]
			]
		]);
	}

	public function store(Request $request)
	{
		DB::beginTransaction();

		try {
			Penilaian::createPenilaian($request);
			DB::commit();

			return \Res::success();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}


	public function edit(Penilaian $penilaian)
	{
		if(auth()->user()->isPenilai()) {
			$dataPaket = \App\Models\PaketPekerjaan::doesntHave('penilaian')->where('id_instansi', auth()->user()->id_instansi)->get();
		} else {
			$dataPaket = \App\Models\PaketPekerjaan::doesntHave('penilaian')->get();
		}

		$dataPaket[] = $penilaian->paketPekerjaan;
		$penilaian->load('poinPenilaian');

		return view('admin.penilaian.edit', [
			'title'			=> 'Penilaian',
			'penilaian'		=> $penilaian,
			'dataPaket'		=> $dataPaket,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Penilaian',
					'link'	=> route('penilaian')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('penilaian.edit', $penilaian->id)
				]
			]
		]);
	}


	public function update(Request $request, Penilaian $penilaian)
	{
		DB::beginTransaction();

		try {
			$penilaian->updatePenilaian($request);
			DB::commit();

			return \Res::update();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}


	public function destroy(Penilaian $penilaian)
	{
		DB::beginTransaction();

		try {
			$penilaian->deletePenilaian();
			DB::commit();

			return \Res::success();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}


	public function detail(Penilaian $penilaian)
	{
		return view('admin.penilaian.detail', [
			'title'			=> 'Penilaian',
			'penilaian'		=> $penilaian,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Penilaian',
					'link'	=> route('penilaian')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('penilaian.detail', $penilaian->id)
				]
			]
		]);
	}

	public function exportToExcel(Penilaian $penilaian)
	{
		try {
			return $penilaian->downloadExcel();
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}


	public function export(Request $request)
	{
		try {
			$result = Penilaian::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);

		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 	SKEMA PENILAIAN
	 * 
	 * */
	public function skemaPenilaianIndex(Request $request)
	{
		if($request->ajax()) {
			return SkemaPenilaian::dt();
		}

		return view('admin.skema_penilaian.index', [
			'title'			=> 'Skema Penilaian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Skema Penilaian',
					'link'	=> route('skema_penilaian')
				]
			]
		]);
	}

	public function skemaPenilaianCreate()
	{
		return view('admin.skema_penilaian.create', [
			'title'			=> 'Buat Skema Penilaian',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Skema Penilaian',
					'link'	=> route('skema_penilaian')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('skema_penilaian.create')
				]
			]
		]);
	}

	public function skemaPenilaianStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$skema = SkemaPenilaian::createSkemaPenilaian($request->all());
			DB::commit();

			return \Res::success([
				'setting_route'	=> route('skema_penilaian.setting', $skema->id),
			]);	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function skemaPenilaianEdit(SkemaPenilaian $skemaPenilaian)
	{
		return view('admin.skema_penilaian.edit', [
			'title'			=> 'Edit Skema Penilaian',
			'skemaPenilaian'=> $skemaPenilaian,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Skema Penilaian',
					'link'	=> route('skema_penilaian')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('skema_penilaian.edit', $skemaPenilaian->id)
				]
			]
		]);
	}

	public function skemaPenilaianUpdate(Request $request, SkemaPenilaian $skemaPenilaian)
	{
		DB::beginTransaction();

		try {
			$skemaPenilaian->updateSkemaPenilaian($request->all());
			DB::commit();

			return \Res::update();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}


	public function skemaPenilaianDestroy(SkemaPenilaian $skemaPenilaian)
	{
		DB::beginTransaction();

		try {
			$skemaPenilaian->deleteSkemaPenilaian();
			DB::commit();

			return \Res::delete();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function skemaPenilaianSetting(SkemaPenilaian $skemaPenilaian)
	{
		return view('admin.skema_penilaian.setting', [
			'title'			=> 'Atur Aspek & Indikator',
			'skemaPenilaian'=> $skemaPenilaian,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Skema Penilaian',
					'link'	=> route('skema_penilaian')
				],
				[
					'title'	=> 'Atur Aspek & Indikator',
					'link'	=> route('skema_penilaian.setting', $skemaPenilaian->id)
				]
			]
		]);
	}

	public function skemaPenilaianGet(SkemaPenilaian $skemaPenilaian)
	{
		try {
			return \Res::success([
				'skema_penilaian'	=> $skemaPenilaian->load([ 'aspekKinerja.indikatorPenilaian' ]),
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}



	/**
	 * 
	 * 	ASPEK KINERJA
	 * 
	 * */
	public function aspekKinerjaStore(Request $request)
	{
		DB::beginTransaction();

		try {
			AspekKinerja::createAspekKinerja($request->all());
			DB::commit();

			return \Res::save();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function aspekKinerjaUpdate(Request $request, AspekKinerja $aspekKinerja)
	{
		DB::beginTransaction();

		try {
			$aspekKinerja->updateAspekKinerja($request->all());
			DB::commit();

			return \Res::update();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function aspekKinerjaDestroy(AspekKinerja $aspekKinerja)
	{
		DB::beginTransaction();

		try {
			$aspekKinerja->deleteAspekKinerja();
			DB::commit();

			return \Res::delete();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}


	/**
	 * 
	 * 	INDIKATOR PENILAIAN
	 * 
	 * */
	public function indikatorPenilaianStore(Request $request)
	{
		DB::beginTransaction();

		try {
			IndikatorPenilaian::createIndikatorPenilaian($request->all());
			DB::commit();

			return \Res::save();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function indikatorPenilaianUpdate(Request $request, IndikatorPenilaian $indikatorPenilaian)
	{
		DB::beginTransaction();

		try {
			$indikatorPenilaian->updateIndikatorPenilaian($request->all());
			DB::commit();

			return \Res::update();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}

	public function indikatorPenilaianDestroy(IndikatorPenilaian $indikatorPenilaian)
	{
		DB::beginTransaction();

		try {
			$indikatorPenilaian->deleteIndikatorPenilaian();
			DB::commit();

			return \Res::delete();	
		} catch (\Exception $e) {
			DB::rollback();
			
			return \Res::error($e);	
		}
	}
}
