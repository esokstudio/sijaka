<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaketPekerjaan;
use App\Models\TenagaTerampil;
use App\Models\TenagaAhli;

class ApiController extends Controller
{
	
	public function getPaketPekerjaan(Request $request)
	{
		if(empty($request->api_key)) {
			return \Res::invalid([ 'message' => 'Wajib menggunakan api_key' ]);
		}
		if($request->api_key != \Setting::getValue('api_key', 'API_KEY')) {
			return \Res::invalid([ 'message' => 'api_key tidak valid' ]);
		}

		try {
			$columns = [ 'tanggal_kontrak', 'nomor_kontrak', 'nama_pekerjaan', 'lokasi_pekerjaan', 'awal_pelaksanaan', 'akhir_pelaksanaan', 'progress_fisik', 'nama_pelaksana_kegiatan', 'nama_petugas', 'nama_skema_pekerjaan' ];

			$paketPekerjaan = PaketPekerjaan::select($columns);

			if(!empty($request->search)) {
				$search = $request->search;
				$paketPekerjaan = $paketPekerjaan->where('nama_pekerjaan', 'like', '%'. $search .'%')
									->orWhere('nomor_kontrak', 'like', '%'. $search .'%')
									->orWhere('lokasi_pekerjaan', 'like', '%'. $search .'%')
									->orWhere('awal_pelaksanaan', 'like', '%'. $search .'%')
									->orWhere('akhir_pelaksanaan', 'like', '%'. $search .'%')
									->orWhere('progress_fisik', 'like', '%'. $search .'%')
									->orWhere('nama_pelaksana_kegiatan', 'like', '%'. $search .'%')
									->orWhere('nama_petugas', 'like', '%'. $search .'%')
									->orWhere('nama_skema_pekerjaan', 'like', '%'. $search .'%');
			}

			$isSorted = false;
			$sortOrder = in_array($request->sort_order, [ 'asc', 'desc' ]) ? $request->sort_order : 'asc';

			if(!empty($request->sort_by)) {
				if(in_array($request->sort_by, $columns)) {
					$paketPekerjaan = $paketPekerjaan->orderBy($request->sort_by, $sortOrder);
					$isSorted = true;
				}
			}

			if(!$isSorted) {
				$paketPekerjaan = $paketPekerjaan->orderBy('tanggal_kontrak', 'desc');
			}

			$totalData = $paketPekerjaan->count();

			$page = $request->page ?? 1;
			$limit = $request->limit ?? 2;
			$paketPekerjaan = $paketPekerjaan->take($limit)->skip(($page - 1) * $limit);

			$paketPekerjaan = $paketPekerjaan->get();
			$totalResultData = count($paketPekerjaan);

			return \Res::success([
				'data'	=> [
					'page' => $page,
					'total_page' => ceil($totalData / $limit),
					'total_data' => $totalData,
					'total_result_data'	=> $totalResultData,
					'results' => $paketPekerjaan,
				],
			]);
			
		} catch (\Exception $e) {
			return response()->json([
				'code'		=> 422,
				'message'	=> 'Tidak valid. Cek kembali parameter',
				'alt'		=> $e->getMessage().':'.$e->getLine().' '.$e->getCode(),
			]);
		}
	}


	public function getTenagaTerampil(Request $request)
	{
		if(empty($request->api_key)) {
			return \Res::invalid([ 'message' => 'Wajib menggunakan api_key' ]);
		}
		if($request->api_key != \Setting::getValue('api_key', 'API_KEY')) {
			return \Res::invalid([ 'message' => 'api_key tidak valid' ]);
		}

		try {
			$columns = [ 'nama', 'tgl_lahir', 'tempat_lahir', 'jenis_kelamin', 'alamat', 'kota', 'provinsi', 'email', 'no_hp', 'pendidikan', 'klasifikasi', 'instansi', 'no_sertifikat', 'tgl_awal_berlaku_sertifikat', 'tgl_akhir_berlaku_sertifikat', 'status_sertifikat' ];

			$tenagaTerampil = TenagaTerampil::select($columns);

			if(!empty($request->search)) {
				$search = $request->search;
				$tenagaTerampil = $tenagaTerampil->where('nama', 'like', '%'. $search .'%')
									->orWhere('tgl_lahir', 'like', '%'. $search .'%')
									->orWhere('tempat_lahir', 'like', '%'. $search .'%')
									->orWhere('jenis_kelamin', 'like', '%'. $search .'%')
									->orWhere('alamat', 'like', '%'. $search .'%')
									->orWhere('kota', 'like', '%'. $search .'%')
									->orWhere('provinsi', 'like', '%'. $search .'%')
									->orWhere('email', 'like', '%'. $search .'%')
									->orWhere('no_hp', 'like', '%'. $search .'%')
									->orWhere('pendidikan', 'like', '%'. $search .'%')
									->orWhere('klasifikasi', 'like', '%'. $search .'%')
									->orWhere('instansi', 'like', '%'. $search .'%')
									->orWhere('no_sertifikat', 'like', '%'. $search .'%')
									->orWhere('tgl_awal_berlaku_sertifikat', 'like', '%'. $search .'%')
									->orWhere('tgl_akhir_berlaku_sertifikat', 'like', '%'. $search .'%')
									->orWhere('status_sertifikat', 'like', '%'. $search .'%');
			}

			$isSorted = false;
			$sortOrder = in_array($request->sort_order, [ 'asc', 'desc' ]) ? $request->sort_order : 'asc';

			if(!empty($request->sort_by)) {
				if(in_array($request->sort_by, $columns)) {
					$tenagaTerampil = $tenagaTerampil->orderBy($request->sort_by, $sortOrder);
					$isSorted = true;
				}
			}

			if(!$isSorted) {
				$tenagaTerampil = $tenagaTerampil->orderBy('nama', 'desc');
			}

			$totalData = $tenagaTerampil->count();

			$page = $request->page ?? 1;
			$limit = $request->limit ?? 2;
			$tenagaTerampil = $tenagaTerampil->take($limit)->skip(($page - 1) * $limit);

			$tenagaTerampil = $tenagaTerampil->get();
			$totalResultData = count($tenagaTerampil);

			return \Res::success([
				'data'	=> [
					'page' => $page,
					'total_page' => ceil($totalData / $limit),
					'total_data' => $totalData,
					'total_result_data'	=> $totalResultData,
					'results' => $tenagaTerampil,
				],
			]);
			
		} catch (\Exception $e) {
			return response()->json([
				'code'		=> 422,
				'message'	=> 'Tidak valid. Cek kembali parameter',
				'alt'		=> $e->getMessage().':'.$e->getLine().' '.$e->getCode(),
			]);
		}
	}



	public function getTenagaAhli(Request $request)
	{
		if(empty($request->api_key)) {
			return \Res::invalid([ 'message' => 'Wajib menggunakan api_key' ]);
		}
		if($request->api_key != \Setting::getValue('api_key', 'API_KEY')) {
			return \Res::invalid([ 'message' => 'api_key tidak valid' ]);
		}

		try {
			$columns = [ 'nama', 'tgl_lahir', 'tempat_lahir', 'jenis_kelamin', 'alamat', 'kota', 'provinsi', 'email', 'no_hp', 'pendidikan', 'klasifikasi', 'instansi', 'no_sertifikat', 'tgl_awal_berlaku_sertifikat', 'tgl_akhir_berlaku_sertifikat', 'status_sertifikat' ];

			$tenagaAhli = TenagaAhli::select($columns);

			if(!empty($request->search)) {
				$search = $request->search;
				$tenagaAhli = $tenagaAhli->where('nama', 'like', '%'. $search .'%')
									->orWhere('tgl_lahir', 'like', '%'. $search .'%')
									->orWhere('tempat_lahir', 'like', '%'. $search .'%')
									->orWhere('jenis_kelamin', 'like', '%'. $search .'%')
									->orWhere('alamat', 'like', '%'. $search .'%')
									->orWhere('kota', 'like', '%'. $search .'%')
									->orWhere('provinsi', 'like', '%'. $search .'%')
									->orWhere('email', 'like', '%'. $search .'%')
									->orWhere('no_hp', 'like', '%'. $search .'%')
									->orWhere('pendidikan', 'like', '%'. $search .'%')
									->orWhere('klasifikasi', 'like', '%'. $search .'%')
									->orWhere('instansi', 'like', '%'. $search .'%')
									->orWhere('no_sertifikat', 'like', '%'. $search .'%')
									->orWhere('tgl_awal_berlaku_sertifikat', 'like', '%'. $search .'%')
									->orWhere('tgl_akhir_berlaku_sertifikat', 'like', '%'. $search .'%')
									->orWhere('status_sertifikat', 'like', '%'. $search .'%');
			}

			$isSorted = false;
			$sortOrder = in_array($request->sort_order, [ 'asc', 'desc' ]) ? $request->sort_order : 'asc';

			if(!empty($request->sort_by)) {
				if(in_array($request->sort_by, $columns)) {
					$tenagaAhli = $tenagaAhli->orderBy($request->sort_by, $sortOrder);
					$isSorted = true;
				}
			}

			if(!$isSorted) {
				$tenagaAhli = $tenagaAhli->orderBy('nama', 'desc');
			}

			$totalData = $tenagaAhli->count();

			$page = $request->page ?? 1;
			$limit = $request->limit ?? 2;
			$tenagaAhli = $tenagaAhli->take($limit)->skip(($page - 1) * $limit);

			$tenagaAhli = $tenagaAhli->get();
			$totalResultData = count($tenagaAhli);

			return \Res::success([
				'data'	=> [
					'page' => $page,
					'total_page' => ceil($totalData / $limit),
					'total_data' => $totalData,
					'total_result_data'	=> $totalResultData,
					'results' => $tenagaAhli,
				],
			]);
			
		} catch (\Exception $e) {
			return response()->json([
				'code'		=> 422,
				'message'	=> 'Tidak valid. Cek kembali parameter',
				'alt'		=> $e->getMessage().':'.$e->getLine().' '.$e->getCode(),
			]);
		}
	}
}
