<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Perusahaan;
use App\Models\KualifikasiPerusahaan;
use App\Models\AktePendirianPerusahaan;
use App\Models\PengesahanPerusahaan;
use App\Models\AktePerubahanPerusahaan;
use App\Models\PengurusPerusahaan;
use App\Models\KeuanganPerusahaan;
use App\Models\TenagaKerjaPerusahaan;
use App\Models\Pengalaman;
use DB;

class PerusahaanController extends Controller
{

	/**
	*		PERUSAHAAN
	*
	*/	
	public function perusahaanIndex(Request $request)
	{
		if($request->ajax()) {
			return Perusahaan::dt($request);
		}

		return view('admin.perusahaan.index', [
			'title'			=> 'Perusahaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				]
			]
		]);
	}


	public function perusahaanCreate()
	{
		return view('admin.perusahaan.create', [
			'title'			=> 'Buat Perusahaan',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('perusahaan.create')
				]
			]
		]);
	}


	public function perusahaanStore(Request $request)
	{
		DB::beginTransaction();

		try {
			$perusahaan = Perusahaan::createPerusahaan($request->except('file_sbu'));
			$perusahaan->setFileSbu($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function perusahaanDetail(Perusahaan $perusahaan)
	{
		$perusahaan->generatePenilaian();
		return view('admin.perusahaan.detail', [
			'title'			=> 'Detail Perusahaan',
			'perusahaan'	=> $perusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('perusahaan.detail', $perusahaan->id)
				]
			]
		]);
	}


	public function perusahaanEdit(Perusahaan $perusahaan)
	{
		return view('admin.perusahaan.edit', [
			'title'			=> 'Edit Perusahaan',
			'perusahaan'		=> $perusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('perusahaan.edit', $perusahaan->id)
				]
			]
		]);
	}


	public function perusahaanUpdate(Request $request, Perusahaan $perusahaan)
	{
		DB::beginTransaction();

		try {
			$perusahaan->updatePerusahaan($request->except('file_sbu'));
			$perusahaan->setFileSbu($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function perusahaanDestroy(Perusahaan $perusahaan)
	{
		DB::beginTransaction();

		try {
			$perusahaan->deletePerusahaan();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function perusahaanApproveChange(Perusahaan $perusahaan)
	{
		DB::beginTransaction();

		try {
			$perusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function perusahaanRejectChange(Perusahaan $perusahaan)
	{
		DB::beginTransaction();

		try {
			$perusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function perusahaanImport(Request $request)
	{
		try {
			$count = Perusahaan::importFromExcel($request);

			return \Res::success([
				'message'	=> 'Berhasil import '. $count . ' data',
			]);
		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}


	public function perusahaanExport(Request $request)
	{
		try {
			$result = Perusahaan::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);

		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}



	/**
	*		KUALIFIKASI
	*
	*/
	public function kualifikasiDetail($kualifikasiPerusahaan)
	{
		$kualifikasiPerusahaan = KualifikasiPerusahaan::find($kualifikasiPerusahaan);
		if(empty($kualifikasiPerusahaan)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.kualifikasi_detail', [
			'title'			=> 'Detail Kualifikasi',
			'kualifikasi'	=> $kualifikasiPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $kualifikasiPerusahaan->id_perusahaan)
				],
				[
					'title'	=> 'Detail Kualifikasi',
					'link'	=> route('kualifikasi_perusahaan.detail', $kualifikasiPerusahaan->id)
				],
			]
		]);
	}

	public function kualifikasiApproveChange(KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		DB::beginTransaction();

		try {
			$kualifikasiPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function kualifikasiRejectChange(KualifikasiPerusahaan $kualifikasiPerusahaan)
	{
		DB::beginTransaction();

		try {
			$kualifikasiPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		AKTE PENDIRIAN
	*
	*/
	public function aktePendirianApproveChange(AktePendirianPerusahaan $aktePendirianPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePendirianPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function aktePendirianRejectChange(AktePendirianPerusahaan $aktePendirianPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePendirianPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		PENGESAHAN
	*
	*/
	public function pengesahanApproveChange(PengesahanPerusahaan $pengesahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$pengesahanPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengesahanRejectChange(PengesahanPerusahaan $pengesahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePendirianPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		AKTE PERUBAHAN
	*
	*/
	public function aktePerubahanDetail($aktePerubahanPerusahaan)
	{
		$aktePerubahanPerusahaan = AktePerubahanPerusahaan::find($aktePerubahanPerusahaan);
		if(empty($aktePerubahanPerusahaan)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.akte_perubahan_detail', [
			'title'			=> 'Detail Akte Perubahan',
			'aktePerubahan'	=> $aktePerubahanPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $aktePerubahanPerusahaan->id_perusahaan)
				],
				[
					'title'	=> 'Detail Akte Perubahan',
					'link'	=> route('akte_perubahan_perusahaan.detail', $aktePerubahanPerusahaan->id)
				],
			]
		]);
	}

	public function aktePerubahanApproveChange(AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePerubahanPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function aktePerubahanRejectChange(AktePerubahanPerusahaan $aktePerubahanPerusahaan)
	{
		DB::beginTransaction();

		try {
			$aktePerubahanPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		PENGURUS
	*
	*/
	public function pengurusDetail($pengurusPerusahaan)
	{
		$pengurusPerusahaan = PengurusPerusahaan::find($pengurusPerusahaan);
		if(empty($pengurusPerusahaan)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.pengurus_detail', [
			'title'			=> 'Detail Pengurus',
			'pengurus'		=> $pengurusPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $pengurusPerusahaan->id_perusahaan)
				],
				[
					'title'	=> 'Detail Pengurus',
					'link'	=> route('pengurus_perusahaan.detail', $pengurusPerusahaan->id)
				],
			]
		]);
	}

	public function pengurusApproveChange(PengurusPerusahaan $pengurusPerusahaan)
	{
		DB::beginTransaction();

		try {
			$pengurusPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengurusRejectChange(PengurusPerusahaan $pengurusPerusahaan)
	{
		DB::beginTransaction();

		try {
			$pengurusPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}



	/**
	*		KEUANGAN
	*
	*/
	public function keuanganDetail($keuanganPerusahaan)
	{
		$keuanganPerusahaan = KeuanganPerusahaan::find($keuanganPerusahaan);
		if(empty($keuanganPerusahaan)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.keuangan_detail', [
			'title'			=> 'Detail Keuangan',
			'keuangan'		=> $keuanganPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $keuanganPerusahaan->id_perusahaan)
				],
				[
					'title'	=> 'Detail Keuangan',
					'link'	=> route('keuangan_perusahaan.detail', $keuanganPerusahaan->id)
				],
			]
		]);
	}

	public function keuanganApproveChange(KeuanganPerusahaan $keuanganPerusahaan)
	{
		DB::beginTransaction();

		try {
			$keuanganPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function keuanganRejectChange(KeuanganPerusahaan $keuanganPerusahaan)
	{
		DB::beginTransaction();

		try {
			$keuanganPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		TENAGA KERJA
	*
	*/
	public function tenagaKerjaDetail($tenagaKerjaPerusahaan)
	{
		$tenagaKerjaPerusahaan = TenagaKerjaPerusahaan::find($tenagaKerjaPerusahaan);
		if(empty($tenagaKerjaPerusahaan)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.tenaga_kerja_detail', [
			'title'			=> 'Detail Tenaga Kerja',
			'tenagaKerja'	=> $tenagaKerjaPerusahaan,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $tenagaKerjaPerusahaan->id_perusahaan)
				],
				[
					'title'	=> 'Detail Tenaga Kerja',
					'link'	=> route('tenaga_kerja_perusahaan.detail', $tenagaKerjaPerusahaan->id)
				],
			]
		]);
	}

	public function tenagaKerjaApproveChange(TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		DB::beginTransaction();

		try {
			$tenagaKerjaPerusahaan->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function tenagaKerjaRejectChange(TenagaKerjaPerusahaan $tenagaKerjaPerusahaan)
	{
		DB::beginTransaction();

		try {
			$tenagaKerjaPerusahaan->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	/**
	*		PENGALAMAN
	*
	*/
	public function pengalamanDetail($pengalaman)
	{
		$pengalaman = Pengalaman::find($pengalaman);
		if(empty($pengalaman)) return redirect()->route('permintaan_perubahan');

		return view('admin.perusahaan.pengalaman_detail', [
			'title'			=> 'Detail Pengalaman',
			'pengalaman'	=> $pengalaman,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Perusahaan',
					'link'	=> route('perusahaan')
				],
				[
					'title'	=> 'Detail Perusahaan',
					'link'	=> route('perusahaan.detail', $pengalaman->id_perusahaan)
				],
				[
					'title'	=> 'Detail Pengalaman',
					'link'	=> route('pengalaman_perusahaan.detail', $pengalaman->id)
				],
			]
		]);
	}

	public function pengalamanApproveChange(Pengalaman $pengalaman)
	{
		DB::beginTransaction();

		try {
			$pengalaman->applyUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}

	public function pengalamanRejectChange(Pengalaman $pengalaman)
	{
		DB::beginTransaction();

		try {
			$pengalaman->reverseUpdate();
			DB::commit();

			return \Res::success();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
