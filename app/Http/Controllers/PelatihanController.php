<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TenagaTerampil;
use App\Models\TenagaAhli;
use DB;

class PelatihanController extends Controller
{
	/**
	 * 
	 * 	TENAGA TERAMPIL
	 * 
	 * */
	public function tenagaTerampilIndex(Request $request)
	{
		if($request->ajax()) {
			return TenagaTerampil::dt($request);
		}

		return view('admin.tenaga_terampil.index', [
			'title'			=> 'Tenaga Terampil',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Terampil',
					'link'	=> route('tenaga_terampil'),
				],
			]
		]);
	}


	public function tenagaTerampilCreate()
	{
		return view('admin.tenaga_terampil.create', [
			'title'			=> 'Buat Tenaga Terampil',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Terampil',
					'link'	=> route('tenaga_terampil'),
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('tenaga_terampil.create'),
				],
			]
		]);
	}


	public function tenagaTerampilStore(Request $request)
	{
		DB::beginTransaction();

		try {
			TenagaTerampil::createTenagaTerampil($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaTerampilDetail(TenagaTerampil $tenagaTerampil)
	{
		return view('admin.tenaga_terampil.detail', [
			'title'			=> 'Detail Tenaga Terampil',
			'tenagaTerampil'=> $tenagaTerampil,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Terampil',
					'link'	=> route('tenaga_terampil'),
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('tenaga_terampil.detail', $tenagaTerampil->id)
				]
			]
		]);
	}


	public function tenagaTerampilEdit(TenagaTerampil $tenagaTerampil)
	{
		return view('admin.tenaga_terampil.edit', [
			'title'			=> 'Edit Tenaga Terampil',
			'tenagaTerampil'=> $tenagaTerampil,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Terampil',
					'link'	=> route('tenaga_terampil'),
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('tenaga_terampil.edit', $tenagaTerampil->id)
				]
			]
		]);
	}


	public function tenagaTerampilUpdate(Request $request, TenagaTerampil $tenagaTerampil)
	{
		DB::beginTransaction();

		try {
			$tenagaTerampil->updateTenagaTerampil($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaTerampilDestroy(TenagaTerampil $tenagaTerampil)
	{
		DB::beginTransaction();

		try {
			$tenagaTerampil->deleteTenagaTerampil();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaTerampilImport(Request $request)
	{
		try {
			$count = TenagaTerampil::importFromExcel($request);

			return \Res::success([
				'message'	=> 'Berhasil import '. $count . ' data',
			]);
		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}


	public function tenagaTerampilExport(Request $request)
	{
		try {
			$result = TenagaTerampil::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);

		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}




	/**
	 * 
	 * 	TENAGA AHLI
	 * 
	 * */
	public function tenagaAhliIndex(Request $request)
	{
		if($request->ajax()) {
			return TenagaAhli::dt($request);
		}

		return view('admin.tenaga_ahli.index', [
			'title'			=> 'Tenaga Ahli',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Ahli',
					'link'	=> route('tenaga_ahli'),
				],
			]
		]);
	}


	public function tenagaAhliCreate()
	{
		return view('admin.tenaga_ahli.create', [
			'title'			=> 'Buat Tenaga Ahli',
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Ahli',
					'link'	=> route('tenaga_ahli'),
				],
				[
					'title'	=> 'Buat',
					'link'	=> route('tenaga_ahli.create'),
				],
			]
		]);
	}


	public function tenagaAhliStore(Request $request)
	{
		DB::beginTransaction();

		try {
			TenagaAhli::createTenagaAhli($request->all());
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaAhliDetail(TenagaAhli $tenagaAhli)
	{
		return view('admin.tenaga_ahli.detail', [
			'title'			=> 'Detail Tenaga Ahli',
			'tenagaAhli'	=> $tenagaAhli,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Ahli',
					'link'	=> route('tenaga_ahli'),
				],
				[
					'title'	=> 'Detail',
					'link'	=> route('tenaga_ahli.detail', $tenagaAhli->id)
				]
			]
		]);
	}


	public function tenagaAhliEdit(TenagaAhli $tenagaAhli)
	{
		return view('admin.tenaga_ahli.edit', [
			'title'			=> 'Edit Tenaga Ahli',
			'tenagaAhli'	=> $tenagaAhli,
			'breadcrumbs'	=> [
				[
					'title'	=> 'Pelatihan',
					'link'	=> 'javascript:void(0);'
				],
				[
					'title'	=> 'Tenaga Ahli',
					'link'	=> route('tenaga_ahli'),
				],
				[
					'title'	=> 'Edit',
					'link'	=> route('tenaga_ahli.edit', $tenagaAhli->id)
				]
			]
		]);
	}


	public function tenagaAhliUpdate(Request $request, TenagaAhli $tenagaAhli)
	{
		DB::beginTransaction();

		try {
			$tenagaAhli->updateTenagaAhli($request->all());
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaAhliDestroy(TenagaAhli $tenagaAhli)
	{
		DB::beginTransaction();

		try {
			$tenagaAhli->deleteTenagaAhli();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function tenagaAhliImport(Request $request)
	{
		try {
			$count = TenagaAhli::importFromExcel($request);

			return \Res::success([
				'message'	=> 'Berhasil import '. $count . ' data',
			]);
		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}


	public function tenagaAhliExport(Request $request)
	{
		try {
			$result = TenagaAhli::exportToExcel($request);
			$fileData = base64_encode(\File::get($result->filepath));
			\File::delete($result->filepath);

			return \Res::success([
				'filedata'	=> $fileData,
				'filename'	=> $result->filename,
				'filemime'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			]);

		} catch (\Exception $e) {

			return \Res::error($e);
		}
	}
}
