<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RantaiPasok;
use DB;

class RantaiPasokController extends Controller
{
  public function index(Request $request)
  {
    if($request->ajax()) {
      return RantaiPasok::dataTable($request);
    }

    return view('admin.rantai_pasok.index', [
      'title'         => 'Rantai Pasok',
      'breadcrumbs'   => [
        [
          'title' => 'Rantai Pasok',
          'link'  => route('rantai_pasok')
        ]
      ]
    ]);
  }


  public function create()
  {
    return view('admin.rantai_pasok.create', [
      'title'         => 'Buat Rantai Pasok',
      'breadcrumbs'   => [
        [
          'title' => 'Rantai Pasok',
          'link'  => route('rantai_pasok')
        ],
        [
          'title' => 'Buat',
          'link'  => route('rantai_pasok.create')
        ]
      ]
    ]);
  }


  public function store(Request $request)
  {
    DB::beginTransaction();

    try {
      $rantaiPasok = RantaiPasok::createRantaiPasok($request);
      DB::commit();

      return \Res::save([
        'redirectUrl' => route('rantai_pasok.detail', $rantaiPasok->id)
      ]);
    } catch (\Exception $e) {
      DB::rollback();

      return \Res::error($e);
    }
  }


  public function detail(RantaiPasok $rantaiPasok)
  {
    return view('admin.rantai_pasok.detail', [
      'title'     => 'Detail Rantai Pasok',
      'rantaiPasok' => $rantaiPasok,
      'breadcrumbs' => [
        [
          'title' => 'Rantai Pasok',
          'link'  => route('rantai_pasok')
        ],
        [
          'title' => 'Detail',
          'link'  => route('rantai_pasok.detail', $rantaiPasok->id)
        ]
      ]
    ]);
  }


  public function edit(RantaiPasok $rantaiPasok)
  {
    return view('admin.rantai_pasok.edit', [
      'title'     => 'Edit Rantai Pasok',
      'rantaiPasok' => $rantaiPasok,
      'breadcrumbs' => [
        [
          'title' => 'Rantai Pasok',
          'link'  => route('rantai_pasok')
        ],
        [
          'title' => 'Edit',
          'link'  => route('rantai_pasok.edit', $rantaiPasok->id)
        ]
      ]
    ]);
  }


  public function update(Request $request, RantaiPasok $rantaiPasok)
  {
    DB::beginTransaction();

    try {
      $rantaiPasok->updateRantaiPasok($request);
      DB::commit();

      return \Res::update();
    } catch (\Exception $e) {
      DB::rollback();

      return \Res::error($e);
    }
  }


  public function destroy(RantaiPasok $rantaiPasok)
  {
    DB::beginTransaction();

    try {
      $rantaiPasok->deleteRantaiPasok();
      DB::commit();

      return \Res::delete();
    } catch (\Exception $e) {
      DB::rollback();

      return \Res::error($e);
    }
  }
}
