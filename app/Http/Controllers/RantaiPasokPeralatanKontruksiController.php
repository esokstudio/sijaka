<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RantaiPasokPeralatanKontruksi;
use DB;

class RantaiPasokPeralatanKontruksiController extends Controller
{
	public function index(Request $request)
	{
		if($request->ajax()) {
			return RantaiPasokPeralatanKontruksi::dataTable($request);
		}
	}


	public function store(Request $request)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokPeralatanKontruksi = RantaiPasokPeralatanKontruksi::createRantaiPasokPeralatanKontruksi($request);
			DB::commit();

			return \Res::save();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function get(RantaiPasokPeralatanKontruksi $rantaiPasokPeralatanKontruksi)
	{
		try {
			return \Res::success([
				'rantaiPasokPeralatanKontruksi' => $rantaiPasokPeralatanKontruksi,
			]);
		} catch (\Exception $e) {
			return \Res::error($e);
		}
	}


	public function update(Request $request, RantaiPasokPeralatanKontruksi $rantaiPasokPeralatanKontruksi)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokPeralatanKontruksi->updateRantaiPasokPeralatanKontruksi($request);
			DB::commit();

			return \Res::update();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}


	public function destroy(RantaiPasokPeralatanKontruksi $rantaiPasokPeralatanKontruksi)
	{
		DB::beginTransaction();

		try {
			$rantaiPasokPeralatanKontruksi->deleteRantaiPasokPeralatanKontruksi();
			DB::commit();

			return \Res::delete();
		} catch (\Exception $e) {
			DB::rollback();

			return \Res::error($e);
		}
	}
}
