<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor2ndRantaiPasokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rantai_pasok', function (Blueprint $table) {
            $table->text('embedded_maps')->nullable()->after('koordinat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rantai_pasok', function (Blueprint $table) {
            $table->dropColumn('embedded_maps');
        });
    }
}
