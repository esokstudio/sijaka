<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stPermohonanSertifikasiTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permohonan_sertifikasi', function (Blueprint $table) {
			$table->integer('tahun_anggaran')->after('file_pas_foto')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permohonan_sertifikasi', function (Blueprint $table) {
			$table->dropColumn('tahun_anggaran');
		});
	}
}
