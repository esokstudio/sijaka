<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor2ndPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->string('id_asosiasi')->after('jenis_perusahaan')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->dropColumn('id_asosiasi');
		});
	}
}
