<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRantaiPasokMaterialKontruksiTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rantai_pasok_material_kontruksi', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_rantai_pasok')->nullable();
			$table->string('nama_produk')->nullable();
			$table->string('nama_sub_produk')->nullable();
			$table->string('merk_produk')->nullable();
			$table->string('file_sertifikat_tkdn')->nullable();
			$table->string('file_sertifikat_sni')->nullable();
			$table->string('file_foto')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rantai_pasok_material_kontruksi');
	}
}
