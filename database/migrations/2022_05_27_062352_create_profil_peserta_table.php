<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilPesertaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profil_peserta', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_user');
			$table->string('nama_peserta');
			$table->string('tempat_lahir')->nullable();
			$table->date('tgl_lahir')->nullable();
			$table->string('kelurahan')->nullable();
			$table->string('kecamatan')->nullable();
			$table->string('kota')->nullable();
			$table->string('no_ktp')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('profil_peserta');
	}
}
