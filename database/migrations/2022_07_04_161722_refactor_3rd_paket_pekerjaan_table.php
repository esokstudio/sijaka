<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor3rdPaketPekerjaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->bigInteger('progress_fisik')->default(0)->after('akhir_pelaksanaan');
			$table->bigInteger('progress_keuangan')->default(0)->after('progress_fisik');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->dropColumn('progress_fisik');
			$table->dropColumn('progress_keuangan');
		});
	}
}
