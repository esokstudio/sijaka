<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeuanganPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('keuangan_perusahaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan');
			$table->string('nama')->nullable();
			$table->string('alamat')->nullable();
			$table->bigInteger('jumlah_saham')->nullable();
			$table->bigInteger('nilai_satuan_saham')->nullable();
			$table->bigInteger('modal_dasar')->nullable();
			$table->bigInteger('modal_disetor')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('keuangan_perusahaan');
	}
}
