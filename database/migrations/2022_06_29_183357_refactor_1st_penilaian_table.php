<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stPenilaianTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('penilaian', function (Blueprint $table) {
			$table->string('nama_skema_penilaian')->after('id')->nullable();
			$table->date('awal_pelaksanaan')->after('jangka_waktu_pelaksanaan')->nullable();
			$table->date('akhir_pelaksanaan')->after('awal_pelaksanaan')->nullable();
			$table->string('tempat_penilaian')->after('nilai_keterangan')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('penilaian', function (Blueprint $table) {
			$table->dropColumn('nama_skema_penilaian');
			$table->dropColumn('awal_pelaksanaan');
			$table->dropColumn('akhir_pelaksanaan');
			$table->dropColumn('tempat_penilaian');
		});
	}
}
