<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKualifikasiPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kualifikasi_perusahaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan');
			$table->string('sub_bidang_klasifikasi')->nullable();
			$table->string('nomor_kode')->nullable();
			$table->string('kualifikasi')->nullable();
			$table->string('tahun')->nullable();
			$table->bigInteger('nilai')->nullable();
			$table->string('asosiasi')->nullable();
			$table->date('tgl_permohonan')->nullable();
			$table->date('tgl_cetak_pertama')->nullable();
			$table->date('tgl_cetak_perubahan_terakhir')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('kualifikasi_perusahaan');
	}
}
