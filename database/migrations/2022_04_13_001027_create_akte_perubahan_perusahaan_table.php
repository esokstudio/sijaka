<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAktePerubahanPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('akte_perubahan_perusahaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan');
			$table->string('no_akte')->nullable();
			$table->string('nama_notaris')->nullable();
			$table->string('alamat')->nullable();
			$table->string('kota')->nullable();
			$table->string('provinsi')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('akte_perubahan_perusahaan');
	}
}
