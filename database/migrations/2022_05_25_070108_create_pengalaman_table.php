<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengalamanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengalaman', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan')->nullable();
			$table->string('nama_pekerjaan')->nullable();
			$table->string('lokasi_pekerjaan')->nullable();
			$table->string('nomor_kontrak')->nullable();
			$table->date('tanggal_kontrak')->nullable();
			$table->bigInteger('pagu_anggaran')->default(0);
			$table->bigInteger('nilai_kontrak')->default(0);
			$table->bigInteger('nilai_target')->default(0);
			$table->string('satuan_target')->nullable();
			$table->date('awal_pelaksanaan')->nullable();
			$table->date('akhir_pelaksanaan')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pengalaman');
	}
}
