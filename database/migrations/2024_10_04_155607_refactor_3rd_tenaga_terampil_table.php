<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor3rdTenagaTerampilTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tenaga_terampil', function (Blueprint $table) {
			$table->dropColumn('tgl_berlaku_sertifikat');
			$table->date('tgl_awal_berlaku_sertifikat')->after('no_sertifikat')->nullable();
			$table->date('tgl_akhir_berlaku_sertifikat')->after('tgl_awal_berlaku_sertifikat')->nullable();
			$table->string('status_sertifikat')->after('tgl_akhir_berlaku_sertifikat')->default('Tidak Aktif')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tenaga_terampil', function (Blueprint $table) {
			$table->date('tgl_berlaku_sertifikat')->after('no_sertifikat')->nullable();
			$table->dropColumn('tgl_awal_berlaku_sertifikat');
			$table->dropColumn('tgl_akhir_berlaku_sertifikat');
			$table->dropColumn('status_sertifikat');
		});
	}
}
