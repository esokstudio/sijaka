<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add1stRefactorPenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penilaian', function (Blueprint $table) {
            $table->string('nama_mengetahui')->nullable()->after('nip_penilai_2');
            $table->string('jabatan_mengetahui')->nullable()->after('nama_mengetahui');
            $table->string('nip_mengetahui')->nullable()->after('jabatan_mengetahui');
            $table->bigInteger('id_instansi')->after('nip_mengetahui')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penilaian', function (Blueprint $table) {
            $table->dropColumn('nama_mengetahui');
            $table->dropColumn('jabatan_mengetahui');
            $table->dropColumn('nip_mengetahui');
            $table->dropColumn('id_instansi');
        });
    }
}
