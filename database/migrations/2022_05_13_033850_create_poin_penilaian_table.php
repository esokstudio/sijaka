<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoinPenilaianTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('poin_penilaian', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_penilaian');
			$table->string('aspek_kinerja');
			$table->string('indikator');
			$table->integer('bobot');
			$table->integer('skor');
			$table->integer('nilai_kinerja')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('poin_penilaian');
	}
}
