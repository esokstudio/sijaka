<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelatihanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pelatihan', function (Blueprint $table) {
			$table->id();
			$table->string('nama_pelatihan');
			$table->bigInteger('id_sumber_dana');
			$table->integer('tahun_anggaran');
			$table->string('penanggung_jawab');
			$table->bigInteger('id_jenis_keahlian');
			$table->bigInteger('id_tingkat_keahlian');
			$table->bigInteger('id_klasifikasi');
			$table->bigInteger('id_sub_klasifikasi');
			$table->bigInteger('id_metode_pelatihan');
			$table->date('waktu_pelaksanaan');
			$table->string('lokasi_pelaksanaan');
			$table->string('keterangan')->nullable();
			$table->integer('jumlah_peserta_pria')->default(0);
			$table->integer('jumlah_peserta_wanita')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pelatihan');
	}
}
