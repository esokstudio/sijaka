<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendidikanPesertaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pendidikan_peserta', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_user');
			$table->string('jenjang_pendidikan');
			$table->string('nama_sekolah');
			$table->string('jurusan');
			$table->string('kota');
			$table->string('tahun_lulus');
			$table->string('no_ijazah');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pendidikan_peserta');
	}
}
