<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRantaiPasokPeralatanKontruksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rantai_pasok_peralatan_kontruksi', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_rantai_pasok')->nullable();
            $table->string('nama_peralatan')->nullable();
            $table->string('nama_sub_peralatan')->nullable();
            $table->string('merk_peralatan')->nullable();
            $table->integer('jumlah_unit')->nullable();
            $table->string('file_surat_keterangan_k3')->nullable();
            $table->string('file_bukti_kepemilikan')->nullable();
            $table->string('file_foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rantai_pasok_peralatan_kontruksi');
    }
}
