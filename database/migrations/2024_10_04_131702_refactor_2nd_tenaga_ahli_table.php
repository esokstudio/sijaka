<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor2ndTenagaAhliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenaga_ahli', function (Blueprint $table) {
            $table->string('no_sertifikat')->after('id_tenaga_kerja_perusahaan')->nullable();
            $table->date('tgl_berlaku_sertifikat')->after('no_sertifikat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenaga_ahli', function (Blueprint $table) {
            $table->dropColumn('no_sertifikat');
            $table->dropColumn('tgl_berlaku_sertifikat');
        });
    }
}
