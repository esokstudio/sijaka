<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKecelakaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kecelakaan', function (Blueprint $table) {
			$table->id();
			$table->string('nama_pekerjaan');
			$table->string('perusahaan');
			$table->string('lokasi_kecelakaan');
			$table->date('waktu_kejadian');
			$table->text('deskripsi_kecelakaan');
			$table->text('deskripsi_kerugian');
			$table->text('sumber_masalah');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('kecelakaan');
	}
}
