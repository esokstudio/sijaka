<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor2ndPermohonanSertifikasiTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permohonan_sertifikasi', function (Blueprint $table) {
			$table->string('nomor_telepon')->after('tahun_anggaran')->nullable();
			$table->string('email')->after('nomor_telepon')->nullable();
			$table->string('status')->default('Menunggu')->after('email')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permohonan_sertifikasi', function (Blueprint $table) {
			$table->dropColumn('nomor_telepon');
			$table->dropColumn('email');
			$table->dropColumn('status');
		});
	}
}
