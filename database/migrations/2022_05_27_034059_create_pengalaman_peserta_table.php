<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengalamanPesertaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengalaman_peserta', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_user');
			$table->string('nama_pekerjaan');
			$table->string('lokasi_pekerjaan');
			$table->bigInteger('nilai_kontrak')->default(0);
			$table->date('mulai');
			$table->date('selesai');
			$table->string('jabatan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pengalaman_peserta');
	}
}
