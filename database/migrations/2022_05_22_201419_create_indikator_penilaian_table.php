<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndikatorPenilaianTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('indikator_penilaian', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_aspek_kinerja');
			$table->string('nama_indikator_penilaian');
			$table->integer('bobot');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('indikator_penilaian');
	}
}
