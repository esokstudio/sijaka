<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->string('kota')->nullable()->after('alamat');
			$table->string('kodepos')->nullable()->after('kota');
			$table->string('nomor_telepon')->nullable()->after('kodepos');
			$table->string('fax')->nullable()->after('nomor_telepon');
			$table->string('email')->nullable()->after('fax');
			$table->string('website')->nullable()->after('email');
			$table->string('bentuk_perusahaan')->nullable()->after('website');
			$table->string('jenis_perusahaan')->nullable()->after('bentuk_perusahaan');
			$table->longText('data_perubahan')->nullable()->after('jenis_perusahaan');
			$table->string('status_perubahan')->default('yes')->after('data_perubahan');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->dropColumn('kota');
			$table->dropColumn('kodepos');
			$table->dropColumn('nomor_telepon');
			$table->dropColumn('fax');
			$table->dropColumn('email');
			$table->dropColumn('website');
			$table->dropColumn('bentuk_perusahaan');
			$table->dropColumn('jenis_perusahaan');
			$table->dropColumn('data_perubahan');
			$table->dropColumn('status_perubahan');
		});
	}
}
