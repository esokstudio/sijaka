<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function (Blueprint $table) {
			$table->id();
			$table->string('title');
			$table->string('slug')->nullable();
			$table->longText('content')->nullable();
			$table->string('thumbnail')->nullable();
			$table->string('custom_page_file')->nullable();
			$table->string('is_published')->default('yes');
			$table->string('is_custom_page')->default('no');
			$table->bigInteger('created_by')->nullable();
			$table->bigInteger('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pages');
	}
}
