<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stRantaiPasokTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rantai_pasok', function (Blueprint $table) {
			$table->string('koordinat')->nullable()->after('jenis_rantai_pasok');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rantai_pasok', function (Blueprint $table) {
			$table->dropColumn('koordinat');
		});
	}
}
