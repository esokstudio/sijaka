<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewStatisticsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('view_statistics', function (Blueprint $table) {
			$table->id();
			$table->string('content_type');
			$table->bigInteger('id_content');
			$table->text('user_agent')->nullable();
			$table->string('browser')->nullable();
			$table->string('platform')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('view_statistics');
	}
}
