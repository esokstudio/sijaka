<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealisasiPaketPekerjaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('realisasi_paket_pekerjaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_paket_pekerjaan');
			$table->string('tahun')->nullable();
			$table->string('bulan')->nullable();;
			$table->bigInteger('progress_keuangan')->nullable();
			$table->bigInteger('progress_fisik')->nullable();
			$table->string('foto_progress_fisik')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('realisasi_paket_pekerjaan');
	}
}
