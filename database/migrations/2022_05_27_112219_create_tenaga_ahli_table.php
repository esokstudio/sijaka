<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenagaAhliTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tenaga_ahli', function (Blueprint $table) {
			$table->id();
			$table->string('no_ktp')->nullable();
			$table->string('nama');
			$table->date('tgl_lahir')->nullable();
			$table->string('tempat_lahir')->nullable();
			$table->string('jenis_kelamin')->nullable();
			$table->string('alamat')->nullable();
			$table->string('kota')->nullable();
			$table->string('provinsi')->nullable();
			$table->string('email')->nullable();
			$table->string('no_hp')->nullable();
			$table->string('pendidikan')->nullable();
			$table->string('klasifikasi')->nullable();
			$table->string('instansi')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tenaga_ahli');
	}
}
