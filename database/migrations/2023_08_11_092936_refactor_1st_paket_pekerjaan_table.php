<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stPaketPekerjaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->string('nama_skema_pekerjaan')->nullable()->after('nama_petugas');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->dropColumn('nama_skema_pekerjaan');
		});
	}
}
