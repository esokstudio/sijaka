<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor4thPaketPekerjaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->integer('tahun_anggaran')->after('id')->nullable();
			$table->string('nama_pelaksana_kegiatan')->after('status')->nullable();
			$table->string('nama_petugas')->after('nama_pelaksana_kegiatan')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			$table->dropColumn('tahun_anggaran');
			$table->dropColumn('nama_pelaksana_kegiatan');
			$table->dropColumn('nama_petugas');
		});
	}
}
