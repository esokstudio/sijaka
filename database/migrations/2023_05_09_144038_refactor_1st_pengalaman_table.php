<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stPengalamanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pengalaman', function (Blueprint $table) {
			$table->string('file_bukti_fisik_pengalaman')->nullable()->after('akhir_pelaksanaan');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pengalaman', function (Blueprint $table) {
			$table->dropColumn('file_bukti_fisik_pengalaman');
		});
	}
}
