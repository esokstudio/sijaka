<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor5thPaketPekerjaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			// $table->integer('tahun_anggaran')->after('id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('paket_pekerjaan', function (Blueprint $table) {
			// $table->dropColumn('tahun_anggaran');
		});
	}
}
