<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTingkatKeahlianTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tingkat_keahlian', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_jenis_keahlian');
			$table->string('nama_tingkat_keahlian');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tingkat_keahlian');
	}
}
