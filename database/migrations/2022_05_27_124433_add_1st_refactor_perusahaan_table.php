<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add1stRefactorPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->string('file_sbu')->nullable()->after('id_asosiasi');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->dropColumn('file_sbu');
		});
	}
}
