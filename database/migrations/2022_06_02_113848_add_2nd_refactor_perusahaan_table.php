<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add2ndRefactorPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->integer('nilai_kinerja')->nullable()->after('file_sbu');
			$table->string('nilai_keterangan')->nullable()->after('nilai_kinerja');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('perusahaan', function (Blueprint $table) {
			$table->dropColumn('nilai_kinerja');
			$table->dropColumn('nilai_keterangan');
		});
	}
}
