<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengurusPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengurus_perusahaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan');
			$table->string('nama')->nullable();
			$table->date('tgl_lahir')->nullable();
			$table->string('alamat')->nullable();
			$table->string('jabatan')->nullable();
			$table->string('pendidikan')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pengurus_perusahaan');
	}
}
