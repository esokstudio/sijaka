<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeraturanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peraturan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_kategori_peraturan');
			$table->string('nomor');
			$table->string('tahun');
			$table->string('judul');
			$table->string('filename')->nullable();
			$table->string('is_published')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('peraturan');
	}
}
