<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor2ndPengalamanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pengalaman', function (Blueprint $table) {
			$table->bigInteger('id_paket_pekerjaan')->after('file_bukti_fisik_pengalaman')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pengalaman', function (Blueprint $table) {
			$table->dropColumn('id_paket_pekerjaan');
		});
	}
}
