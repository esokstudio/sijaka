<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengesahanPerusahaanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengesahan_perusahaan', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_perusahaan');
			$table->string('menkumham_no')->nullable();
			$table->date('menkumham_tgl')->nullable();
			$table->string('pengadilan_negeri_no')->nullable();
			$table->date('pengadilan_negeri_tgl')->nullable();
			$table->string('lembar_negara_no')->nullable();
			$table->date('lembar_negara_tgl')->nullable();
			$table->longText('data_perubahan')->nullable();
			$table->string('status_perubahan')->default('wait');
			$table->string('aksi_perubahan')->default('create')->nullable();
			$table->string('data_baru')->default('yes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pengesahan_perusahaan');
	}
}
