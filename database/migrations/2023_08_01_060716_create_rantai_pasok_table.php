<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRantaiPasokTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rantai_pasok', function (Blueprint $table) {
			$table->id();
			$table->string('nama_badan_usaha')->nullable();
			$table->string('alamat')->nullable();
			$table->string('jenis_usaha')->nullable();
			$table->string('no_sbu')->nullable();
			$table->date('tgl_sbu')->nullable();
			$table->date('berlaku_sbu')->nullable();
			$table->string('no_nib')->nullable();
			$table->date('tgl_nib')->nullable();
			$table->date('berlaku_nib')->nullable();
			$table->string('nomor_telepon')->nullable();
			$table->string('email')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('jenis_rantai_pasok')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rantai_pasok');
	}
}
