<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stInstansiTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('instansi', function (Blueprint $table) {
			$table->string('tampilkan_grafik')->default('yes')->nullable()->after('nama_instansi');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('instansi', function (Blueprint $table) {
			$table->dropColumn('tampilkan_grafik');
		});
	}
}
