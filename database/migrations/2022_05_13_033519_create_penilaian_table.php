<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('penilaian', function (Blueprint $table) {
			$table->id();
			$table->string('unit_kerja');
			$table->bigInteger('id_perusahaan');
			$table->bigInteger('id_paket_pekerjaan');
			$table->integer('jangka_waktu_pelaksanaan');
			$table->string('metode_pemilihan_penyedia');
			$table->integer('nilai_kinerja')->default(0);
			$table->string('nilai_keterangan')->nullable();
			$table->string('nama_penilai_1');
			$table->string('jabatan_penilai_1');
			$table->string('nip_penilai_1')->nullable();
			$table->string('nama_penilai_2');
			$table->string('jabatan_penilai_2');
			$table->string('nip_penilai_2')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('penilaian');
	}
}
