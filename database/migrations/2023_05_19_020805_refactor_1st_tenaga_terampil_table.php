<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Refactor1stTenagaTerampilTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tenaga_terampil', function (Blueprint $table) {
			$table->bigInteger('id_tenaga_kerja_perusahaan')->after('instansi')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tenaga_terampil', function (Blueprint $table) {
			$table->dropColumn('id_tenaga_kerja_perusahaan');
		});
	}
}
