<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavMenusTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nav_menus', function (Blueprint $table) {
			$table->id();
			$table->string('title');
			$table->string('source');
			$table->text('link')->nullable();
			$table->integer('position')->nullable();
			$table->string('is_open_new_tab')->default('no');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('nav_menus');
	}
}
