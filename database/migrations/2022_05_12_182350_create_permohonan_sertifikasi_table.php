<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonanSertifikasiTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permohonan_sertifikasi', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('id_user');
			$table->string('nama_pemohon');
			$table->string('nama_perusahaan')->nullable();
			$table->string('klasifikasi');
			$table->string('sub_klasifikasi');
			$table->string('kualifikasi');
			$table->string('file_ijazah')->nullable();
			$table->string('file_surat_permohonan')->nullable();
			$table->string('file_ktp')->nullable();
			$table->string('file_pernyataan_kebenaran_data')->nullable();
			$table->string('file_pas_foto')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('permohonan_sertifikasi');
	}
}
