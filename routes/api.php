<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('uploads')->group(function(){
	Route::post('image', 'AdminController@imageUpload');
});

Route::prefix('paket-pekerjaan')->group(function(){
	Route::get('/', 'ApiController@getPaketPekerjaan');
	Route::post('/', 'ApiController@getPaketPekerjaan');
});

Route::prefix('tenaga-terampil')->group(function(){
	Route::get('/', 'ApiController@getTenagaTerampil');
	Route::post('/', 'ApiController@getTenagaTerampil');
});

Route::prefix('tenaga-ahli')->group(function(){
	Route::get('/', 'ApiController@getTenagaAhli');
	Route::post('/', 'ApiController@getTenagaAhli');
});