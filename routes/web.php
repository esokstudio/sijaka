<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('admin')->middleware('auth')->group(function(){

	Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');

	Route::group([ 'middleware' => [] ], function(){

		Route::prefix('post')->group(function(){
			Route::get('/', 'ContentController@postIndex')->name('post');
			Route::get('create', 'ContentController@postCreate')->name('post.create');
			Route::get('{post}/edit', 'ContentController@postEdit')->name('post.edit');

			Route::post('store', 'ContentController@postStore')->name('post.store');
			Route::put('{post}/update', 'ContentController@postUpdate')->name('post.update');
			Route::delete('{post}/destroy', 'ContentController@postDestroy')->name('post.destroy');
		});


		Route::prefix('post-category')->group(function(){
			Route::get('/', 'ContentController@postCategoryIndex')->name('post_category');
			Route::get('create', 'ContentController@postCategoryCreate')->name('post_category.create');
			Route::get('{postCategory}/edit', 'ContentController@postCategoryEdit')->name('post_category.edit');

			Route::post('store', 'ContentController@postCategoryStore')->name('post_category.store');
			Route::put('{postCategory}/update', 'ContentController@postCategoryUpdate')->name('post_category.update');
			Route::delete('{postCategory}/destroy', 'ContentController@postCategoryDestroy')->name('post_category.destroy');
		});


		Route::prefix('tag')->group(function(){
			Route::get('/', 'ContentController@tagIndex')->name('tag');
			Route::get('create', 'ContentController@tagCreate')->name('tag.create');
			Route::get('{tag}/edit', 'ContentController@tagEdit')->name('tag.edit');

			Route::post('store', 'ContentController@tagStore')->name('tag.store');
			Route::put('{tag}/update', 'ContentController@tagUpdate')->name('tag.update');
			Route::delete('{tag}/destroy', 'ContentController@tagDestroy')->name('tag.destroy');
		});


		Route::prefix('page')->group(function(){
			Route::get('/', 'ContentController@pageIndex')->name('page');
			Route::get('create', 'ContentController@pageCreate')->name('page.create');
			Route::get('{page}/edit', 'ContentController@pageEdit')->name('page.edit');

			Route::post('store', 'ContentController@pageStore')->name('page.store');
			Route::put('{page}/update', 'ContentController@pageUpdate')->name('page.update');
			Route::delete('{page}/destroy', 'ContentController@pageDestroy')->name('page.destroy');
		});


		Route::prefix('nav-menu')->group(function(){
			Route::get('/', 'NavigationController@navMenuIndex')->name('nav_menu');
			Route::get('create', 'NavigationController@navMenuCreate')->name('nav_menu.create');
			Route::get('{navMenu}/edit', 'NavigationController@navMenuEdit')->name('nav_menu.edit');
			Route::get('{navMenu}/switch-to-up', 'NavigationController@navMenuSwitchToUp')->name('nav_menu.switch_to_up');
			Route::get('{navMenu}/switch-to-down', 'NavigationController@navMenuSwitchToDown')->name('nav_menu.switch_to_down');
			Route::post('store', 'NavigationController@navMenuStore')->name('nav_menu.store');
			Route::put('{navMenu}/update', 'NavigationController@navMenuUpdate')->name('nav_menu.update');
			Route::delete('{navMenu}/destroy', 'NavigationController@navMenuDestroy')->name('nav_menu.destroy');

			Route::get('{navMenu}/submenu', 'NavigationController@navSubmenuIndex')->name('nav_submenu');
			Route::get('{navMenu}/submenu/create', 'NavigationController@navSubmenuCreate')->name('nav_submenu.create');
			Route::get('{navMenu}/submenu/{navSubmenu}/edit', 'NavigationController@navSubmenuEdit')->name('nav_submenu.edit');
			Route::get('{navMenu}/submenu/{navSubmenu}/switch-to-up', 'NavigationController@navSubmenuSwitchToUp')->name('nav_submenu.switch_to_up');
			Route::get('{navMenu}/submenu/{navSubmenu}/switch-to-down', 'NavigationController@navSubmenuSwitchToDown')->name('nav_submenu.switch_to_down');
			Route::post('{navMenu}/submenu/store', 'NavigationController@navSubmenuStore')->name('nav_submenu.store');
			Route::put('{navMenu}/submenu/{navSubmenu}/update', 'NavigationController@navSubmenuUpdate')->name('nav_submenu.update');
			Route::delete('{navMenu}/submenu/{navSubmenu}/destroy', 'NavigationController@navSubmenuDestroy')->name('nav_submenu.destroy');
		});


		Route::prefix('link-group')->group(function(){
			Route::get('/', 'NavigationController@linkGroupIndex')->name('link_group');
			Route::get('create', 'NavigationController@linkGroupCreate')->name('link_group.create');
			Route::get('{linkGroup}/edit', 'NavigationController@linkGroupEdit')->name('link_group.edit');
			Route::get('{linkGroup}/switch-to-up', 'NavigationController@linkGroupSwitchToUp')->name('link_group.switch_to_up');
			Route::get('{linkGroup}/switch-to-down', 'NavigationController@linkGroupSwitchToDown')->name('link_group.switch_to_down');
			Route::post('store', 'NavigationController@linkGroupStore')->name('link_group.store');
			Route::put('{linkGroup}/update', 'NavigationController@linkGroupUpdate')->name('link_group.update');
			Route::delete('{linkGroup}/destroy', 'NavigationController@linkGroupDestroy')->name('link_group.destroy');

			Route::get('{linkGroup}/link', 'NavigationController@linkIndex')->name('link');
			Route::get('{linkGroup}/link/create', 'NavigationController@linkCreate')->name('link.create');
			Route::get('{linkGroup}/link/{link}/edit', 'NavigationController@linkEdit')->name('link.edit');
			Route::get('{linkGroup}/link/{link}/switch-to-up', 'NavigationController@linkSwitchToUp')->name('link.switch_to_up');
			Route::get('{linkGroup}/link/{link}/switch-to-down', 'NavigationController@linkSwitchToDown')->name('link.switch_to_down');
			Route::post('{linkGroup}/link/store', 'NavigationController@linkStore')->name('link.store');
			Route::put('{linkGroup}/link/{link}/update', 'NavigationController@linkUpdate')->name('link.update');
			Route::delete('{linkGroup}/link/{link}/destroy', 'NavigationController@linkDestroy')->name('link.destroy');
		});


		Route::prefix('image-slider')->group(function(){
			Route::get('/', 'ContentController@imageSliderIndex')->name('image_slider');
			Route::get('create', 'ContentController@imageSliderCreate')->name('image_slider.create');
			Route::get('{imageSlider}/edit', 'ContentController@imageSliderEdit')->name('image_slider.edit');

			Route::post('store', 'ContentController@imageSliderStore')->name('image_slider.store');
			Route::put('{imageSlider}/update', 'ContentController@imageSliderUpdate')->name('image_slider.update');
			Route::delete('{imageSlider}/destroy', 'ContentController@imageSliderDestroy')->name('image_slider.destroy');
		});


		Route::prefix('user')->group(function(){
			Route::get('/', 'AdminController@userIndex')->name('user');
			Route::get('create', 'AdminController@userCreate')->name('user.create');
			Route::get('{user}/edit', 'AdminController@userEdit')->name('user.edit');

			Route::post('store', 'AdminController@userStore')->name('user.store');
			Route::put('{user}/update', 'AdminController@userUpdate')->name('user.update');
			Route::delete('{user}/destroy', 'AdminController@userDestroy')->name('user.destroy');
		});


		Route::prefix('message')->group(function(){
			Route::get('/', 'AdminController@messageIndex')->name('message');
			Route::get('{message}/detail', 'AdminController@messageDetail')->name('message.detail');
			Route::delete('{message}/destroy', 'AdminController@messageDestroy')->name('message.destroy');
		});

	});



	Route::prefix('setting')->group(function(){

		Route::group([ 'middleware' => [ ] ], function(){
			Route::prefix('website')->group(function(){
				Route::get('/', 'AdminController@websiteSetting')->name('setting.website');
				Route::post('/', 'AdminController@websiteSetting')->name('setting.website_save');
			});

			Route::prefix('theme')->group(function(){
				Route::get('/', 'AdminController@themeSetting')->name('setting.theme');
				Route::post('/', 'AdminController@themeSetting')->name('setting.theme_save');
			});
		});

		Route::prefix('change-password')->group(function(){
			Route::get('/', 'AdminController@changePassword')->name('setting.change_password');
			Route::post('/', 'AdminController@changePasswordSave')->name('setting.change_password_save');
		});

		Route::prefix('profile')->group(function(){
			Route::get('/', 'AdminController@profile')->name('setting.profile');
			Route::post('/', 'AdminController@profileSave')->name('setting.profile_save');
		});
	});
});


Route::prefix('forgot-password')->middleware('guest')->group(function(){
	Route::get('/', 'AdminController@forgotPassword')->name('forgot_password');
	Route::post('/', 'AdminController@forgotPasswordSave')->name('forgot_password_save');
});

Route::prefix('reset-password')->middleware('guest')->group(function(){
	Route::get('/', 'AdminController@resetPassword')->name('reset_password');
	Route::post('/', 'AdminController@resetPasswordSave')->name('reset_password_save');
});

Route::get('/', 'WebsiteController@index')->name('website');
Route::get('blog/', 'WebsiteController@blog')->name('website.blog');
Route::post('message', 'WebsiteController@message')->name('website.message');
Route::get('posts/', 'WebsiteController@blog')->name('website.posts');
Route::get('post/{slug}', 'WebsiteController@single')->name('website.post');
Route::get('user-verification/{code}', 'WebsiteController@userVerification')->name('website.user_verification');
Route::get('/{slug}', 'WebsiteController@page');