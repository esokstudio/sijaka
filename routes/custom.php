<?php

/**
 * Digunakan untuk menampung routing ke menu selain cms
 * Untuk kustomisasi
 * */

use Illuminate\Support\Facades\Route;


Route::prefix('admin')->middleware([ 'auth', 'setLastActive' ])->group(function(){


	Route::prefix('setting')->group(function(){
		Route::prefix('api-key')->group(function(){
			Route::get('/', 'SettingController@apiKey')->name('setting.api_key');
			Route::get('generate', 'SettingController@generateApiKey')->name('setting.generate_api_key');
		});

		Route::prefix('email')->group(function(){
			Route::get('/', 'SettingController@email')->name('setting.email');
			Route::post('save', 'SettingController@saveEmail')->name('setting.save_email');
		});
	});


	
	/**
	 * 	MASTER DATA
	 * 	@see Akses hanya untuk admin
	 * */
	Route::group([ 'middleware' => [ 'notPenilai', 'notPengguna', 'notPeserta' ] ],function(){

		Route::prefix('kategori-peraturan')->group(function(){
			Route::get('/', 'MasterDataController@kategoriPeraturanIndex')->name('kategori_peraturan');
			Route::get('create', 'MasterDataController@kategoriPeraturanCreate')->name('kategori_peraturan.create');
			Route::get('{kategoriPeraturan}/edit', 'MasterDataController@kategoriPeraturanEdit')->name('kategori_peraturan.edit');

			Route::post('store', 'MasterDataController@kategoriPeraturanStore')->name('kategori_peraturan.store');
			Route::put('{kategoriPeraturan}/update', 'MasterDataController@kategoriPeraturanUpdate')->name('kategori_peraturan.update');
			Route::delete('{kategoriPeraturan}/destroy', 'MasterDataController@kategoriPeraturanDestroy')->name('kategori_peraturan.destroy');
		});

		Route::prefix('tahun-anggaran')->group(function(){
			Route::get('/', 'MasterDataController@tahunAnggaranIndex')->name('tahun_anggaran');
			Route::get('create', 'MasterDataController@tahunAnggaranCreate')->name('tahun_anggaran.create');
			Route::get('{tahunAnggaran}/edit', 'MasterDataController@tahunAnggaranEdit')->name('tahun_anggaran.edit');

			Route::post('store', 'MasterDataController@tahunAnggaranStore')->name('tahun_anggaran.store');
			Route::put('{tahunAnggaran}/update', 'MasterDataController@tahunAnggaranUpdate')->name('tahun_anggaran.update');
			Route::delete('{tahunAnggaran}/destroy', 'MasterDataController@tahunAnggaranDestroy')->name('tahun_anggaran.destroy');
		});

		Route::prefix('sumber-dana')->group(function(){
			Route::get('/', 'MasterDataController@sumberDanaIndex')->name('sumber_dana');
			Route::get('create', 'MasterDataController@sumberDanaCreate')->name('sumber_dana.create');
			Route::get('{sumberDana}/edit', 'MasterDataController@sumberDanaEdit')->name('sumber_dana.edit');

			Route::post('store', 'MasterDataController@sumberDanaStore')->name('sumber_dana.store');
			Route::put('{sumberDana}/update', 'MasterDataController@sumberDanaUpdate')->name('sumber_dana.update');
			Route::delete('{sumberDana}/destroy', 'MasterDataController@sumberDanaDestroy')->name('sumber_dana.destroy');
		});

		Route::prefix('jenis-keahlian')->group(function(){
			Route::get('/', 'MasterDataController@jenisKeahlianIndex')->name('jenis_keahlian');
			Route::get('create', 'MasterDataController@jenisKeahlianCreate')->name('jenis_keahlian.create');
			Route::get('{jenisKeahlian}/edit', 'MasterDataController@jenisKeahlianEdit')->name('jenis_keahlian.edit');
			Route::get('{jenisKeahlian}/get-tingkat-keahlian', 'MasterDataController@jenisKeahlianGetTingkatKeahlian')->name('jenis_keahlian.get_tingkat_keahlian');

			Route::post('store', 'MasterDataController@jenisKeahlianStore')->name('jenis_keahlian.store');
			Route::put('{jenisKeahlian}/update', 'MasterDataController@jenisKeahlianUpdate')->name('jenis_keahlian.update');
			Route::delete('{jenisKeahlian}/destroy', 'MasterDataController@jenisKeahlianDestroy')->name('jenis_keahlian.destroy');
		});

		Route::prefix('tingkat-keahlian')->group(function(){
			Route::get('/', 'MasterDataController@tingkatKeahlianIndex')->name('tingkat_keahlian');
			Route::get('create', 'MasterDataController@tingkatKeahlianCreate')->name('tingkat_keahlian.create');
			Route::get('{tingkatKeahlian}/edit', 'MasterDataController@tingkatKeahlianEdit')->name('tingkat_keahlian.edit');

			Route::post('store', 'MasterDataController@tingkatKeahlianStore')->name('tingkat_keahlian.store');
			Route::put('{tingkatKeahlian}/update', 'MasterDataController@tingkatKeahlianUpdate')->name('tingkat_keahlian.update');
			Route::delete('{tingkatKeahlian}/destroy', 'MasterDataController@tingkatKeahlianDestroy')->name('tingkat_keahlian.destroy');
		});

		Route::prefix('klasifikasi')->group(function(){
			Route::get('/', 'MasterDataController@klasifikasiIndex')->name('klasifikasi');
			Route::get('create', 'MasterDataController@klasifikasiCreate')->name('klasifikasi.create');
			Route::get('{klasifikasi}/edit', 'MasterDataController@klasifikasiEdit')->name('klasifikasi.edit');
			Route::get('{klasifikasi}/get-sub-klasifikasi', 'MasterDataController@klasifikasiGetSubKlasifikasi')->name('klasifikasi.get_sub_klasifikasi');

			Route::post('store', 'MasterDataController@klasifikasiStore')->name('klasifikasi.store');
			Route::put('{klasifikasi}/update', 'MasterDataController@klasifikasiUpdate')->name('klasifikasi.update');
			Route::delete('{klasifikasi}/destroy', 'MasterDataController@klasifikasiDestroy')->name('klasifikasi.destroy');
		});

		Route::prefix('sub-klasifikasi')->group(function(){
			Route::get('/', 'MasterDataController@subKlasifikasiIndex')->name('sub_klasifikasi');
			Route::get('create', 'MasterDataController@subKlasifikasiCreate')->name('sub_klasifikasi.create');
			Route::get('{subKlasifikasi}/edit', 'MasterDataController@subKlasifikasiEdit')->name('sub_klasifikasi.edit');

			Route::post('store', 'MasterDataController@subKlasifikasiStore')->name('sub_klasifikasi.store');
			Route::put('{subKlasifikasi}/update', 'MasterDataController@subKlasifikasiUpdate')->name('sub_klasifikasi.update');
			Route::delete('{subKlasifikasi}/destroy', 'MasterDataController@subKlasifikasiDestroy')->name('sub_klasifikasi.destroy');
		});

		Route::prefix('metode-pelatihan')->group(function(){
			Route::get('/', 'MasterDataController@metodePelatihanIndex')->name('metode_pelatihan');
			Route::get('create', 'MasterDataController@metodePelatihanCreate')->name('metode_pelatihan.create');
			Route::get('{metodePelatihan}/edit', 'MasterDataController@metodePelatihanEdit')->name('metode_pelatihan.edit');

			Route::post('store', 'MasterDataController@metodePelatihanStore')->name('metode_pelatihan.store');
			Route::put('{metodePelatihan}/update', 'MasterDataController@metodePelatihanUpdate')->name('metode_pelatihan.update');
			Route::delete('{metodePelatihan}/destroy', 'MasterDataController@metodePelatihanDestroy')->name('metode_pelatihan.destroy');
		});

		Route::prefix('asosiasi')->group(function(){
			Route::get('/', 'MasterDataController@asosiasiIndex')->name('asosiasi');
			Route::get('create', 'MasterDataController@asosiasiCreate')->name('asosiasi.create');
			Route::get('{asosiasi}/edit', 'MasterDataController@asosiasiEdit')->name('asosiasi.edit');
			Route::get('{asosiasi}/list-perusahaan', 'MasterDataController@asosiasiListPerusahaan')->name('asosiasi.list_perusahaan');
			Route::get('{asosiasi}/export-perusahaan', 'MasterDataController@asosiasiExportPerusahaan')->name('asosiasi.export_perusahaan');

			Route::post('store', 'MasterDataController@asosiasiStore')->name('asosiasi.store');
			Route::put('{asosiasi}/update', 'MasterDataController@asosiasiUpdate')->name('asosiasi.update');
			Route::delete('{asosiasi}/destroy', 'MasterDataController@asosiasiDestroy')->name('asosiasi.destroy');
		});


		Route::prefix('instansi')->group(function(){
			Route::get('/', 'MasterDataController@instansiIndex')->name('instansi');
			Route::get('create', 'MasterDataController@instansiCreate')->name('instansi.create');
			Route::get('{instansi}/edit', 'MasterDataController@instansiEdit')->name('instansi.edit');

			Route::post('store', 'MasterDataController@instansiStore')->name('instansi.store');
			Route::put('{instansi}/update', 'MasterDataController@instansiUpdate')->name('instansi.update');
			Route::delete('{instansi}/destroy', 'MasterDataController@instansiDestroy')->name('instansi.destroy');
		});

	});
	/**
	 * 	END OF MASTER DATA
	 * */



	/**
	 * 	PERATURAN
	 * 	@see Akses hanya untuk admin
	 * */
	Route::prefix('peraturan')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'MasterDataController@peraturanIndex')->name('peraturan');
		Route::get('create', 'MasterDataController@peraturanCreate')->name('peraturan.create');
		Route::get('{peraturan}/edit', 'MasterDataController@peraturanEdit')->name('peraturan.edit');

		Route::post('store', 'MasterDataController@peraturanStore')->name('peraturan.store');
		Route::put('{peraturan}/update', 'MasterDataController@peraturanUpdate')->name('peraturan.update');
		Route::delete('{peraturan}/destroy', 'MasterDataController@peraturanDestroy')->name('peraturan.destroy');
	});
	/**
	 * 	END OF PERATURAN
	 * */

	
	
	/**
	 * 	KECELAKAAN
	 * 	@see Akses hanya untuk admin
	 * */
	Route::prefix('kecelakaan')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'MasterDataController@kecelakaanIndex')->name('kecelakaan');
		Route::get('create', 'MasterDataController@kecelakaanCreate')->name('kecelakaan.create');
		Route::get('{kecelakaan}/edit', 'MasterDataController@kecelakaanEdit')->name('kecelakaan.edit');

		Route::post('store', 'MasterDataController@kecelakaanStore')->name('kecelakaan.store');
		Route::put('{kecelakaan}/update', 'MasterDataController@kecelakaanUpdate')->name('kecelakaan.update');
		Route::delete('{kecelakaan}/destroy', 'MasterDataController@kecelakaanDestroy')->name('kecelakaan.destroy');
	});
	/**
	 * 	END OF KECELAKAAN
	 * */

	// Route::prefix('pelatihan')->group(function(){
	// 	Route::get('/', 'MasterDataController@pelatihanIndex')->name('pelatihan');
	// 	Route::get('create', 'MasterDataController@pelatihanCreate')->name('pelatihan.create');
	// 	Route::get('{pelatihan}/edit', 'MasterDataController@pelatihanEdit')->name('pelatihan.edit');

	// 	Route::post('store', 'MasterDataController@pelatihanStore')->name('pelatihan.store');
	// 	Route::put('{pelatihan}/update', 'MasterDataController@pelatihanUpdate')->name('pelatihan.update');
	// 	Route::delete('{pelatihan}/destroy', 'MasterDataController@pelatihanDestroy')->name('pelatihan.destroy');
	// });


	/**
	 * 	PELATIHAN (TENAGA TERAMPIL & TENAGA AHLI)
	 * 	@see Akses hanya untuk admin
	 * */
	Route::prefix('pelatihan')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::prefix('tenaga-terampil')->group(function(){
			Route::get('/', 'PelatihanController@tenagaTerampilIndex')->name('tenaga_terampil');
			Route::get('create', 'PelatihanController@tenagaTerampilCreate')->name('tenaga_terampil.create');
			Route::get('{tenagaTerampil}/detail', 'PelatihanController@tenagaTerampilDetail')->name('tenaga_terampil.detail');
			Route::get('{tenagaTerampil}/edit', 'PelatihanController@tenagaTerampilEdit')->name('tenaga_terampil.edit');
			Route::post('export', 'PelatihanController@tenagaTerampilExport')->name('tenaga_terampil.export');

			Route::post('store', 'PelatihanController@tenagaTerampilStore')->name('tenaga_terampil.store');
			Route::post('import', 'PelatihanController@tenagaTerampilImport')->name('tenaga_terampil.import');
			Route::put('{tenagaTerampil}/update', 'PelatihanController@tenagaTerampilUpdate')->name('tenaga_terampil.update');
			Route::delete('{tenagaTerampil}/destroy', 'PelatihanController@tenagaTerampilDestroy')->name('tenaga_terampil.destroy');
		});

		Route::prefix('tenaga-ahli')->group(function(){
			Route::get('/', 'PelatihanController@tenagaAhliIndex')->name('tenaga_ahli');
			Route::get('create', 'PelatihanController@tenagaAhliCreate')->name('tenaga_ahli.create');
			Route::get('{tenagaAhli}/detail', 'PelatihanController@tenagaAhliDetail')->name('tenaga_ahli.detail');
			Route::get('{tenagaAhli}/edit', 'PelatihanController@tenagaAhliEdit')->name('tenaga_ahli.edit');
			Route::post('export', 'PelatihanController@tenagaAhliExport')->name('tenaga_ahli.export');

			Route::post('store', 'PelatihanController@tenagaAhliStore')->name('tenaga_ahli.store');
			Route::post('import', 'PelatihanController@tenagaAhliImport')->name('tenaga_ahli.import');
			Route::put('{tenagaAhli}/update', 'PelatihanController@tenagaAhliUpdate')->name('tenaga_ahli.update');
			Route::delete('{tenagaAhli}/destroy', 'PelatihanController@tenagaAhliDestroy')->name('tenaga_ahli.destroy');
		});
	});
	/**
	 * 	END OF PELATIHAN (TENAGA TERAMPIL & TENAGA AHLI)
	 * */


	/**
	 * 	PERMOHONAN SERTIFIKASI
	 * 	@see Akses hanya untuk admin
	 * */
	Route::prefix('permohonan-sertifikasi')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'AdminController@permohonanSertifikasiIndex')->name('permohonan_sertifikasi');
		Route::get('{permohonanSertifikasi}/detail', 'AdminController@permohonanSertifikasiDetail')->name('permohonan_sertifikasi.detail');
		Route::delete('{permohonanSertifikasi}/destroy', 'AdminController@permohonanSertifikasiDestroy')->name('permohonan_sertifikasi.destroy');
		Route::post('{permohonanSertifikasi}/approve', 'AdminController@permohonanSertifikasiApprove')->name('permohonan_sertifikasi.approve');
		Route::post('{permohonanSertifikasi}/reject', 'AdminController@permohonanSertifikasiReject')->name('permohonan_sertifikasi.reject');
		Route::post('export', 'AdminController@permohonanSertifikasiExport')->name('permohonan_sertifikasi.export_to_excel');
	});
	/**
	 * 	END OF PERMOHONAN SERTIFIKASI
	 * */



	/**
	 * 	PERUSAHAAN
	 * 	@see Akses hanya untuk admin
	 * */
	Route::prefix('perusahaan')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'PerusahaanController@perusahaanIndex')->name('perusahaan');
		Route::get('create', 'PerusahaanController@perusahaanCreate')->name('perusahaan.create');
		Route::get('{perusahaan}/edit', 'PerusahaanController@perusahaanEdit')->name('perusahaan.edit');
		Route::get('{perusahaan}/detail', 'PerusahaanController@perusahaanDetail')->name('perusahaan.detail');

		Route::post('store', 'PerusahaanController@perusahaanStore')->name('perusahaan.store');
		Route::post('import', 'PerusahaanController@perusahaanImport')->name('perusahaan.import');
		Route::post('export', 'PerusahaanController@perusahaanExport')->name('perusahaan.export');
		Route::put('{perusahaan}/update', 'PerusahaanController@perusahaanUpdate')->name('perusahaan.update');
		Route::delete('{perusahaan}/destroy', 'PerusahaanController@perusahaanDestroy')->name('perusahaan.destroy');
		Route::post('{perusahaan}/approve-change', 'PerusahaanController@perusahaanApproveChange')->name('perusahaan.approve_change');
		Route::post('{perusahaan}/reject-change', 'PerusahaanController@perusahaanRejectChange')->name('perusahaan.reject_change');
	});
	/**
	 * 	END OF PERUSAHAAN
	 * */



	/**
	 * 	DETAIL PERUSAHAAN DAN APPROVAL PERUBAHAN
	 * 	@see Akses hanya untuk admin
	 * */
	Route::group([ 'middleware' => [ 'notPenilai', 'notPengguna', 'notPeserta' ] ], function(){

		Route::prefix('kualifikasi-perusahaan')->group(function(){
			Route::get('{kualifikasiPerusahaan}/detail', 'PerusahaanController@kualifikasiDetail')->name('kualifikasi_perusahaan.detail');

			Route::post('{kualifikasiPerusahaan}/approveChange', 'PerusahaanController@kualifikasiApproveChange')->name('kualifikasi_perusahaan.approve_change');
			Route::post('{kualifikasiPerusahaan}/rejectChange', 'PerusahaanController@kualifikasiRejectChange')->name('kualifikasi_perusahaan.reject_change');
		});

		Route::prefix('akte-pendirian-perusahaan')->group(function(){
			Route::post('{aktePendirianPerusahaan}/approveChange', 'PerusahaanController@aktePendirianApproveChange')->name('akte_pendirian_perusahaan.approve_change');
			Route::post('{aktePendirianPerusahaan}/rejectChange', 'PerusahaanController@aktePendirianRejectChange')->name('akte_pendirian_perusahaan.reject_change');
		});

		Route::prefix('pengesahan-perusahaan')->group(function(){
			Route::post('{pengesahanPerusahaan}/approveChange', 'PerusahaanController@pengesahanApproveChange')->name('pengesahan_perusahaan.approve_change');
			Route::post('{pengesahanPerusahaan}/rejectChange', 'PerusahaanController@pengesahanRejectChange')->name('pengesahan_perusahaan.reject_change');
		});

		Route::prefix('akte-perubahan-perusahaan')->group(function(){
			Route::get('{aktePerubahanPerusahaan}/detail', 'PerusahaanController@aktePerubahanDetail')->name('akte_perubahan_perusahaan.detail');

			Route::post('{aktePerubahanPerusahaan}/approveChange', 'PerusahaanController@aktePerubahanApproveChange')->name('akte_perubahan_perusahaan.approve_change');
			Route::post('{aktePerubahanPerusahaan}/rejectChange', 'PerusahaanController@aktePerubahanRejectChange')->name('akte_perubahan_perusahaan.reject_change');
		});

		Route::prefix('pengurus-perusahaan')->group(function(){
			Route::get('{pengurusPerusahaan}/detail', 'PerusahaanController@pengurusDetail')->name('pengurus_perusahaan.detail');

			Route::post('{pengurusPerusahaan}/approveChange', 'PerusahaanController@pengurusApproveChange')->name('pengurus_perusahaan.approve_change');
			Route::post('{pengurusPerusahaan}/rejectChange', 'PerusahaanController@pengurusRejectChange')->name('pengurus_perusahaan.reject_change');
		});

		Route::prefix('keuangan-perusahaan')->group(function(){
			Route::get('{keuanganPerusahaan}/detail', 'PerusahaanController@keuanganDetail')->name('keuangan_perusahaan.detail');

			Route::post('{keuanganPerusahaan}/approveChange', 'PerusahaanController@keuanganApproveChange')->name('keuangan_perusahaan.approve_change');
			Route::post('{keuanganPerusahaan}/rejectChange', 'PerusahaanController@keuanganRejectChange')->name('keuangan_perusahaan.reject_change');
		});

		Route::prefix('tenaga-kerja-perusahaan')->group(function(){
			Route::get('{tenagaKerjaPerusahaan}/detail', 'PerusahaanController@tenagaKerjaDetail')->name('tenaga_kerja_perusahaan.detail');

			Route::post('{tenagaKerjaPerusahaan}/approveChange', 'PerusahaanController@tenagaKerjaApproveChange')->name('tenaga_kerja_perusahaan.approve_change');
			Route::post('{tenagaKerjaPerusahaan}/rejectChange', 'PerusahaanController@tenagaKerjaRejectChange')->name('tenaga_kerja_perusahaan.reject_change');
		});

		Route::prefix('pengalaman-perusahaan')->group(function(){
			Route::get('{pengalaman}/detail', 'PerusahaanController@pengalamanDetail')->name('pengalaman_perusahaan.detail');

			Route::post('{pengalaman}/approveChange', 'PerusahaanController@pengalamanApproveChange')->name('pengalaman_perusahaan.approve_change');
			Route::post('{pengalaman}/rejectChange', 'PerusahaanController@pengalamanRejectChange')->name('pengalaman_perusahaan.reject_change');
		});
	});
	/**
	 * 	END OF DETAIL PERUSAHAAN DAN APPROVAL PERUBAHAN
	 * */



	/**
	 * 	PERMINTAAN PERUBAHAN
	 * 	@see Akses hanya untuk admin
	 * */
	Route::get('permintaan-perubahan', 'AdminController@permintaanPerubahan')->name('permintaan_perubahan')->middleware([ 'notPengguna', 'notPenilai', 'notPeserta' ]);
	/**
	 * 	END OF PERMINTAAN PERUBAHAN
	 * */



	/**
	 * 	PAKET PEKERJAAN DAN LAPORAN FISIK KEUANGAN
	 * 	@see Akses hanya untuk admin dan pengguna
	 * */
	Route::group([ 'middleware' => [ 'notPenilai', 'notPeserta' ] ], function(){
		Route::prefix('paket-pekerjaan')->group(function(){
			Route::get('/', 'PaketPekerjaanController@paketPekerjaanIndex')->name('paket_pekerjaan');
			Route::get('create', 'PaketPekerjaanController@paketPekerjaanCreate')->name('paket_pekerjaan.create');
			Route::get('{paketPekerjaan}/detail', 'PaketPekerjaanController@paketPekerjaanDetail')->name('paket_pekerjaan.detail');
			Route::get('{paketPekerjaan}/edit', 'PaketPekerjaanController@paketPekerjaanEdit')->name('paket_pekerjaan.edit');

			Route::post('store', 'PaketPekerjaanController@paketPekerjaanStore')->name('paket_pekerjaan.store');
			Route::post('import', 'PaketPekerjaanController@paketPekerjaanImport')->name('paket_pekerjaan.import');
			Route::post('export-to-excel', 'PaketPekerjaanController@paketPekerjaanExportToExcel')->name('paket_pekerjaan.export_to_excel');
			Route::put('{paketPekerjaan}/update', 'PaketPekerjaanController@paketPekerjaanUpdate')->name('paket_pekerjaan.update');
			Route::delete('{paketPekerjaan}/destroy', 'PaketPekerjaanController@paketPekerjaanDestroy')->name('paket_pekerjaan.destroy');

			Route::prefix('{paketPekerjaan}/realisasi')->group(function(){
				Route::get('/', 'PaketPekerjaanController@realisasiIndex')->name('paket_pekerjaan.realisasi');
				Route::get('create', 'PaketPekerjaanController@realisasiCreate')->name('paket_pekerjaan.realisasi.create');
				Route::post('store', 'PaketPekerjaanController@realisasiStore')->name('paket_pekerjaan.realisasi.store');
				Route::get('{realisasiPaketPekerjaan}/edit', 'PaketPekerjaanController@realisasiEdit')->name('paket_pekerjaan.realisasi.edit');
				Route::put('{realisasiPaketPekerjaan}/update', 'PaketPekerjaanController@realisasiUpdate')->name('paket_pekerjaan.realisasi.update');
			});
		});

		Route::prefix('laporan-fisik-keuangan')->group(function(){
			Route::get('/', 'PaketPekerjaanController@laporanFisikKeuangan')->name('laporan_fisik_keuangan');
			Route::post('generate', 'PaketPekerjaanController@laporanFisikKeuanganGenerate')->name('laporan_fisik_keuangan.generate');
		});
	});
	/**
	 * 	END OF PAKET PEKERJAAN DAN LAPORAN FISIK KEUANGAN
	 * */




	/**
	 * 	MENU PENILAIAN
	 * 	@see Akses hanya untuk admin dan penilai
	 * */
	Route::group([ 'middleware' => [ 'notPengguna', 'notPeserta' ] ], function(){

		Route::prefix('penilaian')->group(function(){
			Route::get('/', 'PenilaianController@index')->name('penilaian');
			Route::get('create', 'PenilaianController@create')->name('penilaian.create');
			Route::get('{penilaian}/detail', 'PenilaianController@detail')->name('penilaian.detail');
			Route::get('{penilaian}/edit', 'PenilaianController@edit')->name('penilaian.edit');
			Route::get('{penilaian}/export-to-excel', 'PenilaianController@exportToExcel')->name('penilaian.export_to_excel');

			Route::post('store', 'PenilaianController@store')->name('penilaian.store');
			Route::post('export', 'PenilaianController@export')->name('penilaian.export');
			Route::put('{penilaian}/update', 'PenilaianController@update')->name('penilaian.update');
			Route::delete('{penilaian}/destroy', 'PenilaianController@destroy')->name('penilaian.destroy');
		});

		Route::prefix('skema-penilaian')->group(function(){
			Route::get('/', 'PenilaianController@skemaPenilaianIndex')->name('skema_penilaian');
			Route::get('create', 'PenilaianController@skemaPenilaianCreate')->name('skema_penilaian.create');
			Route::get('{skemaPenilaian}/edit', 'PenilaianController@skemaPenilaianEdit')->name('skema_penilaian.edit');
			Route::get('{skemaPenilaian}/setting', 'PenilaianController@skemaPenilaianSetting')->name('skema_penilaian.setting');
			Route::get('{skemaPenilaian}/get', 'PenilaianController@skemaPenilaianGet')->name('skema_penilaian.get');

			Route::post('store', 'PenilaianController@skemaPenilaianStore')->name('skema_penilaian.store');
			Route::put('{skemaPenilaian}/update', 'PenilaianController@skemaPenilaianUpdate')->name('skema_penilaian.update');
			Route::delete('{skemaPenilaian}/destroy', 'PenilaianController@skemaPenilaianDestroy')->name('skema_penilaian.destroy');
		});

		Route::prefix('aspek-kinerja')->group(function(){
			Route::post('store', 'PenilaianController@aspekKinerjaStore')->name('aspek_kinerja.store');
			Route::put('{aspekKinerja}/update', 'PenilaianController@aspekKinerjaUpdate')->name('aspek_kinerja.update');
			Route::delete('{aspekKinerja}/destroy', 'PenilaianController@aspekKinerjaDestroy')->name('aspek_kinerja.destroy');
		});

		Route::prefix('indikator-penilaian')->group(function(){
			Route::post('store', 'PenilaianController@indikatorPenilaianStore')->name('indikator_penilaian.store');
			Route::put('{indikatorPenilaian}/update', 'PenilaianController@indikatorPenilaianUpdate')->name('indikator_penilaian.update');
			Route::delete('{indikatorPenilaian}/destroy', 'PenilaianController@indikatorPenilaianDestroy')->name('indikator_penilaian.destroy');
		});

	});
	/**
	 * 	END OF PENILAIAN
	 * */



	/**
	 * 	USER
	 * 	@see Akses hanya admin
	 * */
	Route::prefix('user')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::prefix('pengguna')->group(function(){
			Route::get('/', 'UserController@penggunaIndex')->name('user.pengguna');
			Route::get('create', 'UserController@penggunaCreate')->name('user.pengguna.create');
			Route::get('{user}/edit', 'UserController@penggunaEdit')->name('user.pengguna.edit');
			Route::post('store', 'UserController@penggunaStore')->name('user.pengguna.store');
			Route::put('{user}/edit', 'UserController@penggunaUpdate')->name('user.pengguna.update');
			Route::delete('{user}/destroy', 'UserController@penggunaDestroy')->name('user.pengguna.destroy');
		});

		Route::prefix('penyedia-jasa')->group(function(){
			Route::get('/', 'UserController@penyediaJasaIndex')->name('user.penyedia_jasa');
			Route::post('{user}/approve', 'UserController@penyediaJasaApprove')->name('user.penyedia_jasa.approve');
			Route::post('{user}/reject', 'UserController@penyediaJasaReject')->name('user.penyedia_jasa.reject');
			Route::delete('{user}/destroy', 'UserController@penyediaJasaDestroy')->name('user.penyedia_jasa.destroy');
		});

		Route::prefix('penilai')->group(function(){
			Route::get('/', 'UserController@penilaiIndex')->name('user.penilai');
			Route::get('create', 'UserController@penilaiCreate')->name('user.penilai.create');
			Route::get('{user}/edit', 'UserController@penilaiEdit')->name('user.penilai.edit');
			Route::post('store', 'UserController@penilaiStore')->name('user.penilai.store');
			Route::put('{user}/edit', 'UserController@penilaiUpdate')->name('user.penilai.update');
			Route::delete('{user}/destroy', 'UserController@penilaiDestroy')->name('user.penilai.destroy');
		});
	});
	/**
	 * 	END OF USER
	 * */


	/**
	 * 	RANTAI PASOK
	 * 	@see Akses hanya admin
	 * */
	Route::prefix('rantai-pasok')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'RantaiPasokController@index')->name('rantai_pasok');
		Route::get('create', 'RantaiPasokController@create')->name('rantai_pasok.create');
		Route::get('{rantaiPasok}/detail', 'RantaiPasokController@detail')->name('rantai_pasok.detail');
		Route::get('{rantaiPasok}/edit', 'RantaiPasokController@edit')->name('rantai_pasok.edit');

		Route::post('store', 'RantaiPasokController@store')->name('rantai_pasok.store');
		Route::put('{rantaiPasok}/update', 'RantaiPasokController@update')->name('rantai_pasok.update');
		Route::delete('{rantaiPasok}/destroy', 'RantaiPasokController@destroy')->name('rantai_pasok.destroy');
	});

	Route::prefix('rantai-pasok-material-kontruksi')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'RantaiPasokMaterialKontruksiController@index')->name('rantai_pasok_material_kontruksi');
		Route::post('store', 'RantaiPasokMaterialKontruksiController@store')->name('rantai_pasok_material_kontruksi.store');
		Route::get('{rantaiPasokMaterialKontruksi}/get', 'RantaiPasokMaterialKontruksiController@get')->name('rantai_pasok_material_kontruksi.get');
		Route::put('{rantaiPasokMaterialKontruksi}/update', 'RantaiPasokMaterialKontruksiController@update')->name('rantai_pasok_material_kontruksi.update');
		Route::delete('{rantaiPasokMaterialKontruksi}/destroy', 'RantaiPasokMaterialKontruksiController@destroy')->name('rantai_pasok_material_kontruksi.destroy');
	});	

	Route::prefix('rantai-pasok-peralatan-kontruksi')->middleware([ 'notPenilai', 'notPengguna', 'notPeserta' ])->group(function(){
		Route::get('/', 'RantaiPasokPeralatanKontruksiController@index')->name('rantai_pasok_peralatan_kontruksi');
		Route::post('store', 'RantaiPasokPeralatanKontruksiController@store')->name('rantai_pasok_peralatan_kontruksi.store');
		Route::get('{rantaiPasokPeralatanKontruksi}/get', 'RantaiPasokPeralatanKontruksiController@get')->name('rantai_pasok_peralatan_kontruksi.get');
		Route::put('{rantaiPasokPeralatanKontruksi}/update', 'RantaiPasokPeralatanKontruksiController@update')->name('rantai_pasok_peralatan_kontruksi.update');
		Route::delete('{rantaiPasokPeralatanKontruksi}/destroy', 'RantaiPasokPeralatanKontruksiController@destroy')->name('rantai_pasok_peralatan_kontruksi.destroy');
	});	
});



/**
*	MENU PENYEDIA JASA
* 	@see Akses hanya penyedia jasa
* */
Route::prefix('penyedia-jasa')->middleware([ 'auth', 'notPenilai', 'notAdmin', 'notPengguna', 'notPeserta' ])->group(function(){

	Route::prefix('edit-perusahaan')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@editPerusahaan')->name('p.edit_perusahaan');
		Route::post('/', 'KelolaPerusahaanController@editPerusahaanSave')->name('p.edit_perusahaan_save');
	});

	Route::prefix('kualifikasi')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@kualifikasiIndex')->name('p.kualifikasi');
		Route::get('create', 'KelolaPerusahaanController@kualifikasiCreate')->name('p.kualifikasi.create');
		Route::get('{kualifikasiPerusahaan}/edit', 'KelolaPerusahaanController@kualifikasiEdit')->name('p.kualifikasi.edit');
		Route::get('{kualifikasiPerusahaan}/detail', 'KelolaPerusahaanController@kualifikasiDetail')->name('p.kualifikasi.detail');

		Route::post('store', 'KelolaPerusahaanController@kualifikasiStore')->name('p.kualifikasi.store');
		Route::put('{kualifikasiPerusahaan}/update', 'KelolaPerusahaanController@kualifikasiUpdate')->name('p.kualifikasi.update');
		Route::delete('{kualifikasiPerusahaan}/destroy', 'KelolaPerusahaanController@kualifikasiDestroy')->name('p.kualifikasi.destroy');
	});

	Route::prefix('akte-pendirian')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@editAktePendirian')->name('p.edit_akte_pendirian');
		Route::post('/', 'KelolaPerusahaanController@editAktePendirianSave')->name('p.edit_akte_pendirian_save');
	});

	Route::prefix('pengesahan')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@editPengesahan')->name('p.edit_pengesahan');
		Route::post('/', 'KelolaPerusahaanController@editPengesahanSave')->name('p.edit_pengesahan_save');
	});

	Route::prefix('akte-perubahan')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@aktePerubahanIndex')->name('p.akte_perubahan');
		Route::get('create', 'KelolaPerusahaanController@aktePerubahanCreate')->name('p.akte_perubahan.create');
		Route::get('{aktePerubahanPerusahaan}/detail', 'KelolaPerusahaanController@aktePerubahanDetail')->name('p.akte_perubahan.detail');
		Route::get('{aktePerubahanPerusahaan}/edit', 'KelolaPerusahaanController@aktePerubahanEdit')->name('p.akte_perubahan.edit');

		Route::post('store', 'KelolaPerusahaanController@aktePerubahanStore')->name('p.akte_perubahan.store');
		Route::put('{aktePerubahanPerusahaan}/update', 'KelolaPerusahaanController@aktePerubahanUpdate')->name('p.akte_perubahan.update');
		Route::delete('{aktePerubahanPerusahaan}/destroy', 'KelolaPerusahaanController@aktePerubahanDestroy')->name('p.akte_perubahan.destroy');
	});

	Route::prefix('pengurus')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@pengurusIndex')->name('p.pengurus');
		Route::get('create', 'KelolaPerusahaanController@pengurusCreate')->name('p.pengurus.create');
		Route::get('{pengurusPerusahaan}/edit', 'KelolaPerusahaanController@pengurusEdit')->name('p.pengurus.edit');
		Route::get('{pengurusPerusahaan}/detail', 'KelolaPerusahaanController@pengurusDetail')->name('p.pengurus.detail');

		Route::post('store', 'KelolaPerusahaanController@pengurusStore')->name('p.pengurus.store');
		Route::put('{pengurusPerusahaan}/update', 'KelolaPerusahaanController@pengurusUpdate')->name('p.pengurus.update');
		Route::delete('{pengurusPerusahaan}/destroy', 'KelolaPerusahaanController@pengurusDestroy')->name('p.pengurus.destroy');
	});

	Route::prefix('keuangan')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@keuanganIndex')->name('p.keuangan');
		Route::get('create', 'KelolaPerusahaanController@keuanganCreate')->name('p.keuangan.create');
		Route::get('{keuanganPerusahaan}/edit', 'KelolaPerusahaanController@keuanganEdit')->name('p.keuangan.edit');
		Route::get('{keuanganPerusahaan}/detail', 'KelolaPerusahaanController@keuanganDetail')->name('p.keuangan.detail');

		Route::post('store', 'KelolaPerusahaanController@keuanganStore')->name('p.keuangan.store');
		Route::put('{keuanganPerusahaan}/update', 'KelolaPerusahaanController@keuanganUpdate')->name('p.keuangan.update');
		Route::delete('{keuanganPerusahaan}/destroy', 'KelolaPerusahaanController@keuanganDestroy')->name('p.keuangan.destroy');
	});

	Route::prefix('tenaga-kerja')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@tenagaKerjaIndex')->name('p.tenaga_kerja');
		Route::get('create', 'KelolaPerusahaanController@tenagaKerjaCreate')->name('p.tenaga_kerja.create');
		Route::get('{tenagaKerjaPerusahaan}/edit', 'KelolaPerusahaanController@tenagaKerjaEdit')->name('p.tenaga_kerja.edit');
		Route::get('{tenagaKerjaPerusahaan}/detail', 'KelolaPerusahaanController@tenagaKerjaDetail')->name('p.tenaga_kerja.detail');

		Route::post('store', 'KelolaPerusahaanController@tenagaKerjaStore')->name('p.tenaga_kerja.store');
		Route::put('{tenagaKerjaPerusahaan}/update', 'KelolaPerusahaanController@tenagaKerjaUpdate')->name('p.tenaga_kerja.update');
		Route::delete('{tenagaKerjaPerusahaan}/destroy', 'KelolaPerusahaanController@tenagaKerjaDestroy')->name('p.tenaga_kerja.destroy');
		});

	Route::prefix('pengalaman')->group(function(){
		Route::get('/', 'KelolaPerusahaanController@pengalamanIndex')->name('p.pengalaman');
		Route::get('create', 'KelolaPerusahaanController@pengalamanCreate')->name('p.pengalaman.create');
		Route::get('{pengalaman}/edit', 'KelolaPerusahaanController@pengalamanEdit')->name('p.pengalaman.edit');
		Route::get('{pengalaman}/detail', 'KelolaPerusahaanController@pengalamanDetail')->name('p.pengalaman.detail');

		Route::post('store', 'KelolaPerusahaanController@pengalamanStore')->name('p.pengalaman.store');
		Route::put('{pengalaman}/update', 'KelolaPerusahaanController@pengalamanUpdate')->name('p.pengalaman.update');
		Route::delete('{pengalaman}/destroy', 'KelolaPerusahaanController@pengalamanDestroy')->name('p.pengalaman.destroy');
	});
});
/**
 * 	END OF PENYEDIA JASA
 * */



/**
 *	MENU WEBSITE
 * 	@see Akses untuk umum
 * */
Route::get('/', 'WebsiteController@index')->name('/');
Route::get('kecelakaan', 'WebsiteController@kecelakaan')->name('website.kecelakaan');
Route::get('peraturan', 'WebsiteController@peraturan')->name('website.peraturan');
Route::get('peraturan-detail/{peraturan}', 'WebsiteController@peraturanDetail')->name('website.peraturan_detail');
Route::get('penyedia-jasa', 'WebsiteController@penyediaJasa')->name('website.penyedia_jasa');
Route::get('penyedia-jasa/{perusahaan}/detail', 'WebsiteController@penyediaJasaDetail')->name('website.penyedia_jasa.detail');
Route::get('paket-pekerjaan', 'WebsiteController@paketPekerjaan')->name('website.paket_pekerjaan');
Route::get('posts', 'WebsiteController@posts')->name('website.posts');
Route::get('post/{slug}', 'WebsiteController@post')->name('website.post');
Route::get('page/{slug}', 'WebsiteController@page')->name('website.page');
Route::get('pelatihan-tenaga-ahli', 'WebsiteController@pelatihanTenagaAhli')->name('website.pelatihan_tenaga_ahli');
Route::get('pelatihan-tenaga-terampil', 'WebsiteController@pelatihanTenagaTerampil')->name('website.pelatihan_tenaga_terampil');
Route::prefix('daftar')->middleware('guest')->group(function(){
	Route::get('/', 'WebsiteController@daftar')->name('website.daftar');
	Route::post('/', 'WebsiteController@daftarSave')->name('website.daftar_save');
});
Route::get('asosiasi', 'WebsiteController@asosiasi')->name('website.asosiasi');
Route::get('asosiasi/{asosiasi}', 'WebsiteController@asosiasiDetail')->name('website.asosiasi.detail');
Route::get('rantai-pasok-material', 'WebsiteController@rantaiPasokMaterial')->name('website.rantai_pasok_material');
Route::get('rantai-pasok-peralatan', 'WebsiteController@rantaiPasokPeralatan')->name('website.rantai_pasok_peralatan');
Route::get('rantai-pasok/{rantaiPasok}', 'WebsiteController@rantaiPasokDetail')->name('website.rantai_pasok.detail');
/**
 * 	END OF WEBSITE
 * */



/**
 * 	DAFTAR & LOGIN PESERTA
 * */
Route::prefix('daftar-akun-peserta')->group(function(){
	Route::get('/', 'WebsiteController@daftarAkunPeserta')->name('website.daftar_akun_peserta');
	Route::post('/', 'WebsiteController@daftarAkunPesertaSave')->name('website.daftar_akun_peserta_save');
});

Route::prefix('login-akun-peserta')->group(function(){
	Route::get('/', 'WebsiteController@loginAkunPeserta')->name('website.login_akun_peserta');
	Route::post('/', 'WebsiteController@loginAkunPesertaSave')->name('website.login_akun_peserta_save');
});
/**
 * 	END OF WEBSITE
 * */



/**
 * 	PESERTA
 * 	@see Akses hanya untuk peserta
 * */
Route::group([ 'middleware' => [ 'notAdmin', 'notPengguna', 'notPenilai' ] ], function(){
	Route::prefix('dashboard-peserta')->group(function(){
		Route::get('/', 'WebsiteController@dashboardPeserta')->name('website.dashboard_peserta');
	});

	Route::prefix('pengalaman-peserta')->group(function(){
		Route::get('/', 'WebsiteController@pengalamanPeserta')->name('website.pengalaman_peserta');
		Route::get('create', 'WebsiteController@pengalamanPesertaCreate')->name('website.pengalaman_peserta.create');
		Route::post('store', 'WebsiteController@pengalamanPesertaStore')->name('website.pengalaman_peserta.store');
	});

	Route::prefix('profil-peserta')->group(function(){
		Route::get('/', 'WebsiteController@profilPeserta')->name('website.profil_peserta');
		Route::post('save', 'WebsiteController@profilPesertaSave')->name('website.profil_peserta.save');;
	});

	Route::prefix('pendidikan-peserta')->group(function(){
		Route::get('/', 'WebsiteController@pendidikanPeserta')->name('website.pendidikan_peserta');
		Route::get('create', 'WebsiteController@pendidikanPesertaCreate')->name('website.pendidikan_peserta.create');
		Route::post('store', 'WebsiteController@pendidikanPesertaStore')->name('website.pendidikan_peserta.store');
	});

	Route::prefix('permohonan-sertifikasi')->group(function(){
		Route::get('/', 'WebsiteController@permohonanSertifikasi')->name('website.permohonan_sertifikasi');

		Route::get('create', 'WebsiteController@permohonanSertifikasiCreate')->name('website.permohonan_sertifikasi.create');
		Route::post('store', 'WebsiteController@permohonanSertifikasiStore')->name('website.permohonan_sertifikasi.store');
	});
});
/**
 * 	END OF PESERTA
 * */